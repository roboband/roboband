<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Гарантия и сервис");
?><br>
<h3>Гарантийные обязательства</h3>
 <br>
 <br>
<p>
	 Все товары (электронные устройства) включают в себя гарантийные обязательства и комплектуются соответствующими гарантийными талонами производителей (в случае, если производитель комплектует свою продукцию такими талонами).
</p>
<blockquote>
	 Вся продукция &nbsp;<b>Apple</b>&nbsp; представленная в нашем магазине сертифицирована для продажи на территории Российской Федерации, официально ввезена на территорию страны и имеет гарантию производителя сроком 1 год.
</blockquote>
 По вопросам гарантийного обслуживания купленного оборудования Вы можете обратиться в соответствующие сервисные центры:<br>
 <br>
 <b>Apple</b>&nbsp;<span style="background-color: #ffffff;">в&nbsp;любом из&nbsp;</span><a href="http://www.apple.com/ru/buy/locator/map.html?tySearch=1&viaProduct=4&viaSpecial=-1&strCountry=RUS&lat=55.755786&lng=37.617633&gCountry=RU" target="_blank" style="border: 0px;">авторизованных сервисных центров</a><span style="background-color: #ffffff;">;<br>
 </span><b>JBL</b> в любом из&nbsp;<a href="http://ru.jbl.com/service-centers-ru.html">авторизованных сервисных центров</a>;<br>
 <br>
<p>
	 Корпорация <b>Apple</b> не использует гарантийные талоны, узнать о статусе гарантии на любое устройство можно на&nbsp;<span style="color: windowtext;"><a href="https://selfsolve.apple.com/agreementWarrantyDynamic.do?tySearch=1&viaProduct=4&viaSpecial=-1&strCountry=RUS&lat=55.755786&lng=37.617633&gCountry=RU">специальной странице</a>.</span>
</p>
<blockquote>
	 В ситуации, когда производитель товара не имеет лицензированного сервисного центра в России, наш магазин осуществляет гарантийное обслуживание силами собственного сервисного центра.
</blockquote>
<p>
	 Подробнее о деталях и условиях действия гарантийных обязательств и гарантийного ремонта вы можете узнать на сайтах производителей.
</p>
<blockquote>
	 На аксессуары и другие товары не содержащие электронных элементов, устанавливается гарантия 2 недели со дня продажи.
</blockquote>
 <br>
 <br>
<h3>Обмен и возврат товара</h3>
 <br>
 <br>
<p>
	 Обмен и возврат товара осуществляется в соответствии с Законом Российской Федерации о защите прав потребителей.<br>
	 Просто позвоните по телефону интернет-магазина +7 (499) 322-05-48, и менеджер займётся вашим вопросом.
</p>
<p>
</p>
<h4>Для аксессуаров не содержащих электронных элементов</h4>
<p>
</p>
<p>
	 Если вы еще не получили заказанный/оплаченный товар, то можете отказаться от него в любой момент. В этом случае вы не оплачиваете доставку и получаете все деньги обратно. Деньги возвращаются тем способом, которым были внесены: если вы оплачивали по кредитной карте, сумма вернется на кредитную карту, если делали банковский перевод — деньги вернутся на ваш банковский счет, и т.п. В зависимости от способа оплаты, возврат денежных средств занимает до 10 дней.
</p>
<p>
	 Если товар был вам доставлен менее 7 дней назад и вы его не вскрывали, мы также вернем вам все деньги за товар, но оплата за доставку не возвращается. Возврат товара надлежащего качества возможен только в случае, если сохранены его товарный вид, потребительские свойства, а также документ, подтверждающий факт и условия покупки указанного товара. Пожалуйста, сохраняйте чеки!
</p>
<p>
	 Если так случилось, что вы вскрыли товар, а он оказался ненадлежащего качества, вы можете вернуть или обменять этот товар в течение 14 дней. Во&nbsp;всех случаях мы&nbsp;настоятельно рекомендуем проверять товар сразу: в&nbsp;магазине&nbsp;— на&nbsp;кассе, при&nbsp;доставке курьером&nbsp;— в&nbsp;присутствии курьера.
</p>
<p>
</p>
<h4>Для электронных устройств (технически сложных товаров)</h4>
<p>
</p>
<p>
	 Если вы еще не получили заказанный/оплаченный товар, то можете отказаться от него в любой момент. В этом случае вы не оплачиваете доставку и получаете все деньги обратно. Деньги возвращаются тем способом, которым были внесены: если вы оплачивали по кредитной карте, сумма вернется на кредитную карту, если делали банковский перевод — вернутся на ваш банковский счет и т.п. В зависимости от способа оплаты, возврат денежных средств занимает до 10 дней.
</p>
<p>
	 Если товар был вам доставлен менее 7 дней назад, и вы его не вскрывали, мы также в течение 10 дней вернем вам все деньги за товар, но оплата доставки не возвращается.
</p>
<p>
	 Если вы вскрыли упаковку и в течение 14 дней с момента приобретения обнаружили в товаре недостатки, то можете потребовать возврата уплаченной суммы или обмена товара на аналогичный с перерасчетом цены. В этом случае в обязательном порядке будет проведена экспертиза в сервисном центре.
</p>
<p>
	 Если экспертиза выявит заводской брак, вы вправе потребовать возврата уплаченных денег либо замены товара на аналогичный. Возврат денег обычно занимает до 10 дней.
</p>
<p>
	 Если экспертиза установит, что недостатки товара возникли вследствие обстоятельств, за которые не отвечает продавец/изготовитель (например, экран разбит в результате падения, устройство не включается, т.к. на него попала вода и т.п.), вы будете обязаны возместить расходы на проведение экспертизы, хранение и транспортировку товара.
</p>
<p>
</p>
<h4>Адрес пункта приема товара</h4>
<p>
</p>
<p>
	 Возврат товара, купленного в интернет-магазине, осуществляется Покупателем по адресу: г. Москва, ул. Доброслободская, д. 6 стр. 1. Обмен и возврат товара проводится с пн.-пт., с 10:00 до 18:00. Просим вас предупредить о вашем визите по телефону менеджера +7 (499) 322-05-48.
</p>
<p>
	 Если вы живёте в Москве, то мы можем выслать курьера за товаром легче 5 кг, но в этом случае оплата расходов на доставку товара от вас до магазина/сервиса будет за ваш счет.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>