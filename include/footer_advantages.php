<div class="wrapper_inner footer_bottom">

	<div class="usp-item">
		<div class="usp-item__image"><img hidth="60" height="60" src="/images/footer_advantages/check_size.png"></div>
		<div class="usp-item__title">Качество</div>
		<div class="usp-item__subtitle">Работаем только с официальными поставщиками, поэтому гарантируем вам качество товара.</div>
	</div>

	<div class="usp-item">
		<div class="usp-item__image"><img hidth="60" height="60" src="/images/footer_advantages/sequrity.png"></div>
		<div class="usp-item__title">Выгода</div>
		<div class="usp-item__subtitle">Мы предлагаем умеренные цены на все товары, без ущерба качеству продукции и сервиса.</div>
	</div>

	<div class="usp-item">
		<div class="usp-item__image"><img hidth="60" height="60" src="/images/footer_advantages/time.png"></div>
		<div class="usp-item__title">Вся Россия</div>
		<div class="usp-item__subtitle">Оперативно доставляем заказы по всей России, предалагая лучшее сочетание сроков и стоимости доставки.</div>
	</div>

	<div class="usp-item">
		<div class="usp-item__image"><img hidth="60" height="60" src="/images/footer_advantages/delivery.png"></div>
		<div class="usp-item__title">Ассортимент</div>
		<div class="usp-item__subtitle">Мы рады предложить вам действительно интересные товары, многие из которых вы сможете приобрести только у нас.</div>
	</div>

</div>