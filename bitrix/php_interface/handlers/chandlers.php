<?
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

class CHandlers
{
	// Обработчик вызывается после оформления заказа компонентом sale.order.ajax
	// Добавляет код примененного купона в комментарий к заказу
	function OnSaleComponentOrderOneStepCompleteHandler($ID, $arOrder, $arParams)
	{
		if(!empty($_SESSION["COUPONS"]))
		{
			$comment = $arOrder["USER_DESCRIPTION"].GetMessage("HANDLER_ORDER_COUPON").implode(", ", $_SESSION["COUPONS"]).".";
			CSaleOrder::Update($ID, array("USER_DESCRIPTION" => $comment));
			unset($_SESSION["COUPONS"]);
		}
	}
}
?>