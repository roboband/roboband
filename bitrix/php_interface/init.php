<?
CModule::AddAutoloadClasses('', array(
		'CHandlers' =>	'/bitrix/php_interface/handlers/chandlers.php',
	));

AddEventHandler("sale", "OnSaleComponentOrderOneStepComplete", array("CHandlers", "OnSaleComponentOrderOneStepCompleteHandler"));


AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "DropPreviewText");
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "DropPreviewText");

function DropPreviewText($arFields)
{
if (@$_REQUEST['mode']=='import')
{
unset($arFields['PREVIEW_TEXT']);
unset($arFields['PREVIEW_TEXT_TYPE']);
}
}

// developer tools
function pre($arr)
{
	$backtrace = debug_backtrace();
	$cp = $backtrace[0]["file"] . ", " . $backtrace[0]["line"];
	echo $cp;
	echo "<pre>";
	print_r($arr);
	echo "</pre>";
}

function logToFile($path, $var, $trace = false)
{
	$backtrace = debug_backtrace();
	$cp = ConvertTimeStamp(time(), 'FULL') . ' — '  . $backtrace[0]['file'] . ', ' . $backtrace[0]['line'].PHP_EOL;

	$puttedData = PHP_EOL.PHP_EOL.$cp.print_r($var, true);
	if ($trace)
	{
		$puttedData .= print_r($backtrace, true);
	}

	$io = CBXVirtualIo::GetInstance();
	$pathDir = substr($path, 0, strrpos($path, '/') + 1);
	if (!$io->DirectoryExists($io->RelativeToAbsolutePath($pathDir)))
	{
		$io->CreateDirectory($io->RelativeToAbsolutePath($pathDir));
	}

	file_put_contents($io->RelativeToAbsolutePath($path), $puttedData, FILE_APPEND);
}


?>
