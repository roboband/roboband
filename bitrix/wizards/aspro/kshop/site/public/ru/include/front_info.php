<a href="<?=SITE_DIR?>company/" > 
  <h3>О магазине</h3>
 </a> 
<p> Сайт, торгующий товарами в Интернете. Позволяет пользователям сформировать заказ на покупку, выбрать способ оплаты и доставки заказа в сети Интернет. Выбрав необходимые товары или услуги, пользователь обычно имеет возможность тут же на сайте выбрать метод оплаты и доставки. </p>
 
<p name="about_show_more_content" style="display: none;"> 	Основное отличие Интернет-магазина от традиционного &mdash; в типе торговой площадки. 	Обычному магазину нужен торговый зал, витрины, ценники, а также продавцы, кассиры и опытные консультанты, у онлайн-магазина же вся инфраструктура реализована программно. 	Другими словами, Интернет-магазин — это совокупность программ, работающих на Web-сайте, которые позволяют покупателю дистанционно выбрать товар из каталога и оформить его заказ. 	Функции витрины и торгового зала выполняют &laquo;страницы&raquo; с иллюстрированным каталогом товаров, а консультантов — подсказки, инструкции и описания. 	Все остальное — как в обычном магазине. Даже в интерфейсе Интернет-магазина сохраняются привычные элементы, например виртуальная «тележка» («корзина»), куда мы по пути к кассе складываем выбранные товары. </p>
 
 <a name="about_show_more" ><i class="arrow"><b></b></i><span class="pseudo">Подробнее</span></a> 
<script>$("a[name=about_show_more]").click(function(){ $(this).blur().find(".arrow").toggleClass("down"); $("p[name=about_show_more_content]").slideToggle(200);});</script>
