<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!defined("WIZARD_SITE_ID"))
	return;

if (!defined("WIZARD_SITE_DIR"))
	return;

if (COption::GetOptionString("main", "upload_dir") == "")
	COption::SetOptionString("main", "upload_dir", "upload");

/* �� ��������� ������ � ���� init.php */
if(COption::GetOptionString("aspro.kshop", "wizard_installed", "N") == 'N'){
	//$file = fopen(WIZARD_SITE_ROOT_PATH."/bitrix/php_interface/init.php", "ab");
	//fwrite($file, file_get_contents(WIZARD_ABSOLUTE_PATH."/site/services/main/bitrix/init.php"));
	//fclose($file);
	COption::SetOptionString("aspro.kshop", "wizard_installed", "Y");
}

if(COption::GetOptionString("aspro.kshop", "wizard_installed", "N", WIZARD_SITE_ID) == "Y" && !WIZARD_INSTALL_DEMO_DATA)
{
	$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID;//."_".WIZARD_THEME_ID;
	$wizard =& $this->GetWizard();
	
	/* ��������� �������� */
	$themeID = $wizard->GetVar("themeID");
	if($wizard->GetVar('siteLogoSet', true)){
		$ff = CFile::GetByID($wizard->GetVar("siteLogo"));
		if($zr = $ff->Fetch())
		{
			$strOldFile = str_replace("//", "/", WIZARD_SITE_ROOT_PATH."/".(COption::GetOptionString("main", "upload_dir", "upload"))."/".$zr["SUBDIR"]."/".$zr["FILE_NAME"]);
			@copy($strOldFile, WIZARD_SITE_PATH."include/logo.jpg");
			___writeToAreasFile(WIZARD_SITE_PATH."include/logo.php", '<a href="'.WIZARD_SITE_DIR.'"><img src="'.WIZARD_SITE_DIR.'include/logo.jpg"  /></a>');
			CFile::Delete($siteLogo);
		}/*else if (!file_exists(WIZARD_SITE_PATH."include/logo.jpg")){
			//copy(WIZARD_THEME_ABSOLUTE_PATH."/images/logo.png", WIZARD_SITE_PATH."include/bx_default_logo.jpg");
			//copy(WIZARD_THEME_ABSOLUTE_PATH."/images/logo.png", .$themeID."/lang/".LANGUAGE_ID."logo.png"))
			___writeToAreasFile(WIZARD_SITE_PATH."include/logo.php", '<a href="'.WIZARD_SITE_DIR.'"><img src="'.$bitrixTemplateDir.'/themes/'.$themeID.'images/logo.png"  /></a>');		
		}*/
	}
	
	/* ��������� �������� ����� */
	
	if($wizard->GetVar('siteNameSet', true)){	___writeToAreasFile(WIZARD_SITE_PATH."include/description.php", $wizard->GetVar("siteName"));}
	
	___writeToAreasFile(WIZARD_SITE_PATH."include/work_time.php", $wizard->GetVar("siteWorkTime"));

	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."include/phone.php", array("PHONE" => $wizard->GetVar("siteTelephone")));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."include/copyright.php", array("COPYRIGHT" => $wizard->GetVar("siteCopy")));
	
	
	if ($wizard->GetVar("shopVk"))
	{ WizardServices::ReplaceMacrosRecursive($bitrixTemplateDir, array("SITE_VK" => $wizard->GetVar("shopVk"))); }
	if ($wizard->GetVar("shopTwitter"))
	{ WizardServices::ReplaceMacrosRecursive($bitrixTemplateDir, array("SITE_TWITTER" => $wizard->GetVar("shopTwitter"))); }
	if ($wizard->GetVar("shopFacebook"))
	{ WizardServices::ReplaceMacrosRecursive($bitrixTemplateDir, array("SITE_FACEBOOK" => $wizard->GetVar("shopFacebook"))); }

	
	/* �������������� ������ ���� */
	
	if($wizard->GetVar('rewriteIndex', true)){
		if($wizard->GetVar('siteLogoSet', true)){
			CopyDirFiles(
				WIZARD_ABSOLUTE_PATH."/site/public/".LANGUAGE_ID."/_index_.php",
				WIZARD_SITE_PATH."/_index.php",
				$rewrite = true,
				$recursive = true,
				$delete_after_copy = false
			);
		} else {
			CopyDirFiles(
				WIZARD_ABSOLUTE_PATH."/site/public/".LANGUAGE_ID."/_index.php",
				WIZARD_SITE_PATH."/_index.php",
				$rewrite = true,
				$recursive = true,
				$delete_after_copy = false
			);
		}
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", Array("SITE_DIR" => WIZARD_SITE_DIR));
	}
	//die;
	return;
}

// �������� �����

$path = str_replace("//", "/", WIZARD_ABSOLUTE_PATH."/site/public/".LANGUAGE_ID."/"); 

$handle = @opendir($path);
if ($handle)
{
	while ($file = readdir($handle))
	{
		if (in_array($file, array(".", "..")))
			continue;
			
		CopyDirFiles(
			$path.$file,
			WIZARD_SITE_PATH."/".$file,
			$rewrite = true, 
			$recursive = true,
			$delete_after_copy = false,
			$exclude = "bitrix"
		);
		if($wizard->GetVar('siteLogoSet', true)){
			CopyDirFiles(
				WIZARD_SITE_PATH."/_index_.php",
				WIZARD_SITE_PATH."/_index.php",
				$rewrite = true,
				$recursive = true,
				$delete_after_copy = true
			);
		}
		else
		{
			DeleteDirFilesEx(WIZARD_SITE_DIR."/_index_.php");
		}
	}
}

WizardServices::PatchHtaccess(WIZARD_SITE_PATH);

WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, Array("SITE_DIR" => WIZARD_SITE_DIR));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."include/email.php", Array("SITE_EMAIL" => $wizard->GetVar("shopEmail")));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."include/address.php", Array("SITE_ADDRESS" => trim($wizard->GetVar("shopLocation")).', '.trim($wizard->GetVar("shopAdr"))));

copy(WIZARD_THEME_ABSOLUTE_PATH."/images/favicon.ico", WIZARD_SITE_PATH."favicon.ico");
copy(WIZARD_THEME_ABSOLUTE_PATH."/images/favicon.ico", $_SERVER["DOCUMENT_ROOT"]."/bitrix/favicon.ico");

$arUrlRewrite = array(); 
$arNewUrlRewrite = array();
if (file_exists(WIZARD_SITE_ROOT_PATH."/urlrewrite.php"))
{
	include(WIZARD_SITE_ROOT_PATH."/urlrewrite.php");
}



$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."bitrix/services/ymarket/([\\w\\d\\-]+)?(/)?(([\\w\\d\\-]+)(/)?)?#",
		"RULE" => "REQUEST_OBJECT=\$1&METHOD=\$4",
		"ID" => "",
		"PATH" => WIZARD_SITE_DIR."bitrix/services/ymarket/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."personal/history-of-orders/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => WIZARD_SITE_DIR."personal/history-of-orders/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."contacts/stores/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => WIZARD_SITE_DIR."contacts/stores/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => WIZARD_SITE_DIR."personal/order/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."info/articles/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."info/articles/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."company/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."company/news/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."info/article/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."info/article/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."info/brands/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."info/brands/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."info/brand/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."info/brand/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."services/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."services/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."catalog/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => WIZARD_SITE_DIR."catalog/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."stores/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."stores/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."news/index.php",
	);
$arNewUrlRewrite[] = array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."sale/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."sale/index.php",
	);
	
	
	
foreach ($arNewUrlRewrite as $arUrl)
{
	if (!in_array($arUrl, $arUrlRewrite))
	{
		CUrlRewriter::Add($arUrl);
	}
}


function ___writeToAreasFile($fn, $text)
{
	if(file_exists($fn) && !is_writable($abs_path) && defined("BX_FILE_PERMISSIONS"))
		@chmod($abs_path, BX_FILE_PERMISSIONS);

	$fd = @fopen($fn, "wb");
	if(!$fd)
		return false;

	if(false === fwrite($fd, $text))
	{
		fclose($fd);
		return false;
	}

	fclose($fd);

	if(defined("BX_FILE_PERMISSIONS"))
		@chmod($fn, BX_FILE_PERMISSIONS);
}

CheckDirPath(WIZARD_SITE_PATH."include/");


	$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID;//."_".WIZARD_THEME_ID;
	$wizard =& $this->GetWizard();
	
	/* ��������� �������� */
	$templateID = $wizard->GetVar("templateID");
	$themeVarName = $templateID."_themeID";
	$themeID = $wizard->GetVar($themeVarName);
	//$themeID = $wizard->GetVar("themeID");
	
	
	if($wizard->GetVar('siteLogoSet', true)){
		$ff = CFile::GetByID($wizard->GetVar("siteLogo"));
		if($zr = $ff->Fetch())
		{
			$strOldFile = str_replace("//", "/", WIZARD_SITE_ROOT_PATH."/".(COption::GetOptionString("main", "upload_dir", "upload"))."/".$zr["SUBDIR"]."/".$zr["FILE_NAME"]);
			@copy($strOldFile, WIZARD_SITE_PATH."include/logo.jpg");
			___writeToAreasFile(WIZARD_SITE_PATH."include/logo.php", '<a href="'.WIZARD_SITE_DIR.'"><img src="'.WIZARD_SITE_DIR.'include/logo.jpg"  /></a>');
			CFile::Delete($siteLogo);
		}/*else if (!file_exists(WIZARD_SITE_PATH."include/logo.jpg")){
			//copy(WIZARD_THEME_ABSOLUTE_PATH."/images/logo.png", WIZARD_SITE_PATH."include/bx_default_logo.jpg");
			//copy(WIZARD_THEME_ABSOLUTE_PATH."/images/logo.png", .$themeID."/lang/".LANGUAGE_ID."logo.png"))
			
			$bxTemplateDir = BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID;//."_".WIZARD_THEME_ID;
			
			___writeToAreasFile(WIZARD_SITE_PATH."include/logo.php", '<a href="'.WIZARD_SITE_DIR.'"><img src="'.$bxTemplateDir.'/themes/'.$themeID.'/images/logo.png" ggg  /></a>');		
		}*/
	}
	
	/* ��������� �������� ����� */
	
	if($wizard->GetVar('siteNameSet', true)){	___writeToAreasFile(WIZARD_SITE_PATH."include/description.php", $wizard->GetVar("siteName"));}
	
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."include/phone.php", array("PHONE" => $wizard->GetVar("siteTelephone")));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."include/copyright.php", array("COPYRIGHT" => $wizard->GetVar("siteCopy")));
	___writeToAreasFile(WIZARD_SITE_PATH."include/work_time.php", $wizard->GetVar("siteWorkTime"));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."include/email.php", Array("SITE_EMAIL" => $wizard->GetVar("siteEmail")));



$wizard =& $this->GetWizard();

/*if($wizard->GetVar('siteNameSet', true)){
	___writeToAreasFile(WIZARD_SITE_PATH."include/description.php", $wizard->GetVar("siteName"));
	WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH."include/", Array("SITE_NAME" => $wizard->GetVar("siteName")));
	WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH."company/", Array("SITE_NAME" => $wizard->GetVar("siteName")));
}*/

___writeToAreasFile(WIZARD_SITE_PATH."include/copy.php", $wizard->GetVar("siteCopy"));
___writeToAreasFile(WIZARD_SITE_PATH."include/phone_feedback.php", $wizard->GetVar("siteTelephone"));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."contacts/index.php", Array("COMPANY_PHONE" => $wizard->GetVar("siteTelephone")));

/*if($wizard->GetVar('siteLogoSet', true)){
	$siteLogo = $wizard->GetVar("siteLogo");
	if($siteLogo>0)
	{
		$ff = CFile::GetByID($siteLogo);
		if($zr = $ff->Fetch())
		{
			$strOldFile = str_replace("//", "/", WIZARD_SITE_ROOT_PATH."/".(COption::GetOptionString("main", "upload_dir", "upload"))."/".$zr["SUBDIR"]."/".$zr["FILE_NAME"]);
			@copy($strOldFile, WIZARD_SITE_PATH."include/logo.jpg");
			___writeToAreasFile(WIZARD_SITE_PATH."include/logo.php", '<a href="'.WIZARD_SITE_DIR.'"><img src="'.WIZARD_SITE_DIR.'include/logo.jpg"  /></a>');
			CFile::Delete($siteLogo);
		}
	}
	else if(!file_exists(WIZARD_SITE_PATH."include/bx_default_logo.jpg") || WIZARD_INSTALL_DEMO_DATA)
	{
		copy(WIZARD_THEME_ABSOLUTE_PATH."/lang/".LANGUAGE_ID."/logo.jpg", WIZARD_SITE_PATH."include/bx_default_logo.jpg");
		___writeToAreasFile(WIZARD_SITE_PATH."include/logo.php",  '<a href="'.WIZARD_SITE_DIR.'"><img src="'.WIZARD_SITE_DIR.'include/bx_default_logo.jpg"  /></a>');
	}
}*/


$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID;//."_".WIZARD_THEME_ID;

CWizardUtil::ReplaceMacrosRecursive(WIZARD_SITE_PATH, Array("SITE_DIR" => WIZARD_SITE_DIR));
CWizardUtil::ReplaceMacrosRecursive($bitrixTemplateDir, Array("SITE_DIR" => WIZARD_SITE_DIR));



CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/.section.php", array("SITE_DESCRIPTION" => htmlspecialcharsbx($wizard->GetVar("siteMetaDescription"))));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/.section.php", array("SITE_KEYWORDS" => htmlspecialcharsbx($wizard->GetVar("siteMetaKeywords"))));

?>