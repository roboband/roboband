<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?
$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID;
if(!CModule::IncludeModule("form")) return;
if(!CModule::IncludeModule("main")) return;

$FORM_SID = "FEEDBACK_".WIZARD_SITE_ID;

$dbSite = CSite::GetByID(WIZARD_SITE_ID);
if($arSite = $dbSite -> Fetch()) $lang = $arSite["LANGUAGE_ID"];
if(strlen($lang) <= 0) $lang = "ru";
	
WizardServices::IncludeServiceLang("forms.php", $lang);

if( COption::GetOptionString("aspro.kshop", "wizard_installed", "N", WIZARD_SITE_ID) == "Y" ){
	if( ($arForm = CForm::GetBySID( $FORM_SID )->Fetch()) && ($arForm["ID"]>0) )
	{
		/*��������� �������� �������*/
		$eventTypeExists = false;
		$db_res = CEventType::GetList( array ("TYPE_ID" => "FORM_FILLING_FEEDBACK_".WIZARD_SITE_ID));
		if ($db_res) 
		{ 
			$count = $db_res->SelectedRowsCount(); 
			if ($count>0) { $eventTypeExists = true; } 
		}
		if (!$eventTypeExists)
		{
			$oEventType = new CEventType();
			$arFields = array(	"LID" => $lang,
								"EVENT_NAME" => "FORM_FILLING_FEEDBACK_".WIZARD_SITE_ID,
								"NAME" => GetMessage("EVENT_NEW_FEEDBACK_NAME"),
								"DESCRIPTION" => GetMessage("EVENT_NEW_FEEDBACK_DESCRIPTION"));		
			$oEventTypeSrcID = $oEventType->Add($arFields);
		}

		/*��������� �������� ������*/
		$eventMessageExists = false;
		$eventMessageID = 0;
		$by = "id";
		$order = "asc";
		$db_res = CEventMessage::GetList( $by, $order, array ("TYPE_ID"=>"FORM_FILLING_FEEDBACK_".WIZARD_SITE_ID));
		if ($db_res) 
		{ 
			$count = $db_res->SelectedRowsCount(); 
			if ($count>0) 
			{
				$eventMessageExists = true; 
				if ($count==1)
				{
					while ($res = $db_res->GetNext()) { $eventMessageID = $res["ID"]; }
				}
			} 
		}


		$arFields = array(  "ACTIVE" => "Y",
							"EVENT_NAME" => "FORM_FILLING_FEEDBACK_".WIZARD_SITE_ID,
							"LID" => WIZARD_SITE_ID,
							"EMAIL_FROM" => $wizard->GetVar("shopEmail"),
							"EMAIL_TO" => $wizard->GetVar("shopEmail"),
							"SUBJECT" => GetMessage("NEW_QUESTION_EMAIL_SUBJECT"),
							"MESSAGE" => GetMessage("NEW_QUESTION_EMAIL_TEXT"),
							"BODY_TYPE" => "html");

		$oEventMessage = new CEventMessage();
		if (!$eventMessageExists){ $oEventMessage->Add($arFields);}
			elseif (intVal($eventMessageID)>0){ $oEventMessage->Update($eventMessageID, $arFields); }
	
		CForm::Set(array("NAME" => GetMessage("FEEDBACK_FORM_NAME"), "SID" => $FORM_SID, "arMAIL_TEMPLATE"=>"FORM_FILLING_FEEDBACK_".WIZARD_SITE_ID), $arForm["ID"], "N");
	
		/*�������� �������*/
		CWizardUtil::ReplaceMacros($bitrixTemplateDir."/header.php", array("FORM_FEEDBACK_ID" => $arForm["ID"]));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."contacts/index.php", array("FORM_FEEDBACK_ID" => $arForm["ID"]));;
		return;
	}

}

$arFields = array(
	"NAME"				=> GetMessage("FEEDBACK_FORM_NAME"),
	"SID"				=> $FORM_SID,
	"C_SORT"			=> 300,
	"BUTTON"			=> GetMessage("FEEDBACK_BUTTON_NAME"),
	"DESCRIPTION"		=> GetMessage("FEEDBACK_FORM_DESCRIPTION"),
	"DESCRIPTION_TYPE"	=> "text",
	"STAT_EVENT1"		=> "form",
	"STAT_EVENT2"		=> "",
	"arSITE"			=> array( WIZARD_SITE_ID ),
	"arMENU"			=> array( "ru" => GetMessage("FEEDBACK_FORM_NAME") ),
	"arGROUP"			=> array( "2" => "10" ),
	"MAIL_EVENT_TYPE"	=> "FORM_FILLING_FEEDBACK_".WIZARD_SITE_ID
);

$form_id = CForm::Set($arFields);

if( $form_id < 0 )
	return;


	
	
/* ��������� ������� */

$arANSWER = array();
$arANSWER[] = array( "MESSAGE" => " ", "C_SORT" => 100, "ACTIVE" => "Y", "FIELD_TYPE" => "text", "FIELD_PARAM" => "" );
$arFields = array( "FORM_ID" => $form_id, "ACTIVE" => "Y", "TITLE" => GetMessage("FEEDBACK_FORM_QUESTION_1"), "TITLE_TYPE" => "text", "SID" => "CLIENT_NAME", "C_SORT" => 100, "ADDITIONAL" => "N", "REQUIRED" => "Y", "arANSWER" => $arANSWER );
CFormField::Set($arFields);

$arANSWER = array();
$arANSWER[] = array( "MESSAGE" => " ", "C_SORT" => 100, "ACTIVE" => "Y", "FIELD_TYPE" => "text", "FIELD_PARAM" => "class=\"phone\"" );
$arFields = array( "FORM_ID" => $form_id, "ACTIVE" => "Y", "TITLE" => GetMessage("FEEDBACK_FORM_QUESTION_2"), "TITLE_TYPE" => "text", "SID" => "PHONE", "C_SORT" => 200, "ADDITIONAL" => "N", "REQUIRED" => "Y", "arANSWER" => $arANSWER );
CFormField::Set($arFields);

$arANSWER = array();
$arANSWER[] = array( "MESSAGE" => " ", "C_SORT" => 100, "ACTIVE" => "Y", "FIELD_TYPE" => "email", "FIELD_PARAM" => "" );
$arFields = array( "FORM_ID" => $form_id, "ACTIVE" => "Y", "TITLE" => GetMessage("FEEDBACK_FORM_QUESTION_3"), "TITLE_TYPE" => "text", "SID" => "EMAIL", "C_SORT" => 300, "ADDITIONAL" => "N", "REQUIRED" => "N", "arANSWER" => $arANSWER );
CFormField::Set($arFields);

$arANSWER = array();
$arANSWER[] = array( "MESSAGE" => " ", "C_SORT" => 100, "ACTIVE" => "Y", "FIELD_TYPE" => "textarea", "FIELD_PARAM" => "left" );
$arFields = array( "FORM_ID" => $form_id, "ACTIVE" => "Y", "TITLE" => GetMessage("FEEDBACK_FORM_QUESTION_4"), "TITLE_TYPE" => "text", "SID" => "POST", "C_SORT" => 400, "ADDITIONAL" => "N", "REQUIRED" => "Y", "arANSWER" => $arANSWER );
CFormField::Set($arFields);


/* ��������� ������ */
$arFields = array( "FORM_ID" => $form_id, "C_SORT" => 100, "ACTIVE" => "Y", "TITLE" => "DEFAULT", "DEFAULT_VALUE" => "Y", "arPERMISSION_VIEW" => array(2), "arPERMISSION_MOVE" => array(2), "arPERMISSION_EDIT" => array(2), "arPERMISSION_DELETE" => array(2) );
CFormStatus::Set($arFields);


/*��������� �������� �������*/
$eventTypeExists = false;
$db_res = CEventType::GetList( array ("TYPE_ID" => "FORM_FILLING_FEEDBACK_".WIZARD_SITE_ID));
if ($db_res) 
{ 
	$count = $db_res->SelectedRowsCount(); 
	if ($count>0) { $eventTypeExists = true; } 
}
if (!$eventTypeExists)
{
	$oEventType = new CEventType();
	$arFields = array(	"LID" => $lang,
						"EVENT_NAME" => "FORM_FILLING_FEEDBACK_".WIZARD_SITE_ID,
						"NAME" => GetMessage("EVENT_NEW_FEEDBACK_NAME"),
						"DESCRIPTION" => GetMessage("EVENT_NEW_FEEDBACK_DESCRIPTION"));		
	$oEventTypeSrcID = $oEventType->Add($arFields);
}

/*��������� �������� ������*/
$eventMessageExists = false;
$eventMessageID = 0;
$by = "id";
$order = "asc";
$db_res = CEventMessage::GetList( $by, $order, array ("TYPE_ID"=>"FORM_FILLING_FEEDBACK_".WIZARD_SITE_ID));
if ($db_res) 
{ 
	$count = $db_res->SelectedRowsCount(); 
	if ($count>0) 
	{
		$eventMessageExists = true; 
		if ($count==1)
		{
			while ($res = $db_res->GetNext()) { $eventMessageID = $res["ID"]; }
		}
	} 
}


$arFields = array(  "ACTIVE" => "Y",
					"EVENT_NAME" => "FORM_FILLING_FEEDBACK_".WIZARD_SITE_ID,
					"LID" => WIZARD_SITE_ID,
					"EMAIL_FROM" => $wizard->GetVar("shopEmail"),
					"EMAIL_TO" => $wizard->GetVar("shopEmail"),
					"SUBJECT" => GetMessage("NEW_FEEDBACK_EMAIL_SUBJECT"),
					"MESSAGE" => GetMessage("NEW_FEEDBACK_EMAIL_TEXT"),
					"BODY_TYPE" => "html");

$oEventMessage = new CEventMessage();
if (!$eventMessageExists){ $oEventMessage->Add($arFields);}
	elseif (intVal($eventMessageID)>0){ $oEventMessage->Update($eventMessageID, $arFields); }

CForm::Set(array("NAME" => GetMessage("FEEDBACK_FORM_NAME"), "SID" => $FORM_SID, "arMAIL_TEMPLATE"=>"FORM_FILLING_FEEDBACK_".WIZARD_SITE_ID), $form_id, "N");	 

CWizardUtil::ReplaceMacros($bitrixTemplateDir."/header.php", array("FEEDBACK_FORM_ID" => $form_id));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."contacts/index.php", array("FORM_FEEDBACK_ID" => $form_id));
?>