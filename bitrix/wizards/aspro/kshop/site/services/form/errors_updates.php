<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
// set active mail events for forms
CModule::IncludeModule("form");
$rsForms = CForm::GetList($by="s_id", $order="desc", array());
while($arForm = $rsForms->Fetch()){
	$arTemplatesIDs = array();
	if($arForm["MAIL_EVENT_TYPE"]){
		$rsTemplate = CEventMessage::GetList($by="site_id", $order="desc", array("TYPE_ID" => $arForm["MAIL_EVENT_TYPE"], "ACTIVE" => "Y"));
		while($arTemplate = $rsTemplate->Fetch()){
			if($arTemplate["ID"]){
				$arTemplatesIDs[] = $arTemplate["ID"];
			}
		}
	}
	if($arTemplatesIDs){
		CForm::Set(array("arMAIL_TEMPLATE" => $arTemplatesIDs), $arForm["ID"]);
	}
}
?>