<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("iblock"))
	return;
//COption::SetOptionString("aspro.kshop", "wizard_installed", "Y", false, $siteID); 


if( COption::GetOptionString("aspro.kshop", "wizard_installed", "N", WIZARD_SITE_ID) == "Y" && !WIZARD_INSTALL_DEMO_DATA )
	return;

	
$arTypes = Array(
	Array(
		"ID" => "aspro_kshop_catalog",
		"SECTIONS" => "Y",
		"IN_RSS" => "N",
		"SORT" => 100,
		"LANG" => Array(0 => "ru"),
	),
	Array(
		"ID" => "aspro_kshop_content",
		"SECTIONS" => "Y",
		"IN_RSS" => "N",
		"SORT" => 200,
		"LANG" => Array(0 => "ru"),
	),
	Array(
		"ID" => "aspro_kshop_adv",
		"SECTIONS" => "Y",
		"IN_RSS" => "N",
		"SORT" => 300,
		"LANG" => Array(0 => "ru"),
	)
);

$arLanguages = Array();
$rsLanguage = CLanguage::GetList($by, $order, array());
while($arLanguage = $rsLanguage->Fetch())
	$arLanguages[] = $arLanguage["LID"];
	
$iblockType = new CIBlockType;
foreach($arTypes as $arType)
{
	$dbType = CIBlockType::GetList(Array(),Array("=ID" => $arType["ID"]));
	if($dbType->Fetch())
		continue;

	foreach($arLanguages as $languageID)
	{
		WizardServices::IncludeServiceLang("type.php", $languageID);
		$code = strtoupper($arType["ID"]);
		$arType["LANG"][$languageID]["NAME"] = GetMessage($code."_TYPE_NAME");
		$arType["LANG"][$languageID]["ELEMENT_NAME"] = GetMessage($code."_ELEMENT_NAME");

		if ($arType["SECTIONS"] == "Y")
			$arType["LANG"][$languageID]["SECTION_NAME"] = GetMessage($code."_SECTION_NAME");
	}
	$newIblockType = $iblockType->Add($arType);
	
    if(IntVal($newIblockType)<=0)
    { $GLOBALS["ASPRO_KSHOP_WIZARD_LAST_ERROR"]["UNABLE_TO_CREATE_IBLOCK_TYPE_".strtoupper($arType["ID"])] = $iblockType->LAST_ERROR;}
}

COption::SetOptionString('iblock','combined_list_mode','Y');
?>