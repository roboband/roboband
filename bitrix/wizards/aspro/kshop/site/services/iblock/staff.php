<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("iblock"))
	return;
$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID;

if( COption::GetOptionString("aspro.kshop", "wizard_installed", "N", WIZARD_SITE_ID) == "Y" && !WIZARD_INSTALL_DEMO_DATA ){
	$iblockCode = "aspro_kshop_staff_".WIZARD_SITE_ID; 
	$iblockType = "aspro_kshop_content";
	
	$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
	$iblockID = false; 
	if ($arIBlock = $rsIBlock->Fetch()){
		$iblockID = $arIBlock["ID"];
		WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_CONTENT_TYPE" => $iblockType));
		WizardServices::ReplaceMacrosRecursive($bitrixTemplateDir, array("IBLOCK_CONTENT_TYPE" => $iblockType));
		WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_STAFF_ID" => $iblockID));
		WizardServices::ReplaceMacrosRecursive($bitrixTemplateDir, array("IBLOCK_STAFF_ID" => $iblockID));
	}
	return;
}

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/staff.xml"; 
$iblockCode = "aspro_kshop_staff_".WIZARD_SITE_ID; 
$iblockCheckInstallCode = "aspro_kshop_staff"; 
$iblockType = "aspro_kshop_content"; 

$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
$iblockID = false; 
if ($arIBlock = $rsIBlock->Fetch())
{
	$iblockID = $arIBlock["ID"]; 
	if (WIZARD_INSTALL_DEMO_DATA)
	{
		CIBlock::Delete($arIBlock["ID"]); 
		$iblockID = false; 
	}
}



if($iblockID == false)
{
	$permissions = Array(
			"1" => "X",
			"2" => "R"
		);
	$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "content_editor"));
	if($arGroup = $dbGroup -> Fetch())
	{
		$permissions[$arGroup["ID"]] = 'W';
	};
	$iblockID = WizardServices::ImportIBlockFromXML(
		$iblockXMLFile,
		$iblockCode,
		$iblockType,
		WIZARD_SITE_ID,
		$permissions
	);

	if ($iblockID < 1)
		return;
	
	//IBlock fields
	$iblock = new CIBlock;
	$arFields = array('ACTIVE' => 'Y', 
	'FIELDS' => array (
	  'IBLOCK_SECTION' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'ACTIVE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'Y',
	  ),
	  'ACTIVE_FROM' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '=today',
	  ),
	  'ACTIVE_TO' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SORT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '0',
	  ),
	  'PREVIEW_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'FROM_DETAIL' => 'Y',
		  'SCALE' => 'Y',
		  'WIDTH' => 155,
		  'HEIGHT' => 165,
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'DELETE_WITH_DETAIL' => 'Y',
		  'UPDATE_WITH_DETAIL' => 'Y',
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'PREVIEW_TEXT_TYPE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'text',
	  ),
	  'PREVIEW_TEXT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'DETAIL_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'SCALE' => 'Y',
		  'WIDTH' => 155,
		  'HEIGHT' => 165,
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'DETAIL_TEXT_TYPE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'text',
	  ),
	  'DETAIL_TEXT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'XML_ID' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'CODE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'UNIQUE' => 'Y',
		  'TRANSLITERATION' => 'Y',
		  'TRANS_LEN' => 100,
		  'TRANS_CASE' => 'L',
		  'TRANS_SPACE' => '_',
		  'TRANS_OTHER' => '_',
		  'TRANS_EAT' => 'Y',
		  'USE_GOOGLE' => 'N',
		),
	  ),
	  'TAGS' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_NAME' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'FROM_DETAIL' => 'N',
		  'SCALE' => 'N',
		  'WIDTH' => '',
		  'HEIGHT' => '',
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'DELETE_WITH_DETAIL' => 'N',
		  'UPDATE_WITH_DETAIL' => 'N',
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'SECTION_DESCRIPTION_TYPE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'text',
	  ),
	  'SECTION_DESCRIPTION' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_DETAIL_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'SCALE' => 'N',
		  'WIDTH' => '',
		  'HEIGHT' => '',
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'SECTION_XML_ID' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_CODE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'UNIQUE' => 'N',
		  'TRANSLITERATION' => 'Y',
		  'TRANS_LEN' => 100,
		  'TRANS_CASE' => 'L',
		  'TRANS_SPACE' => '_',
		  'TRANS_OTHER' => '_',
		  'TRANS_EAT' => 'Y',
		  'USE_GOOGLE' => 'N',
		),
	  ),
	  'LOG_SECTION_ADD' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_SECTION_EDIT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_SECTION_DELETE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_ELEMENT_ADD' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_ELEMENT_EDIT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_ELEMENT_DELETE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	),
	'CODE' => $iblockCode, 
	'XML_ID' => $iblockCode);
	$iblock->Update($iblockID, $arFields);
}
else
{
	$arSites = array(); 
	$db_res = CIBlock::GetSite($iblockID);
	while ($res = $db_res->Fetch())
		$arSites[] = $res["LID"]; 
	if (!in_array(WIZARD_SITE_ID, $arSites))
	{
		$arSites[] = WIZARD_SITE_ID;
		$iblock = new CIBlock;
		$iblock->Update($iblockID, array("LID" => $arSites));
	}
}

//iblock user fields begin
	$dbSite = CSite::GetByID(WIZARD_SITE_ID);
	if($arSite = $dbSite -> Fetch()) $lang = $arSite["LANGUAGE_ID"];
	if(strlen($lang) <= 0) $lang = "ru";
	WizardServices::IncludeServiceLang("user_options.php", $lang);
	
	$db_prop = CIBlockProperty::GetList(array(), array("IBLOCK_ID"=>$iblockID));
	$arIblockProps = array();
	while ($prop = $db_prop->GetNext())
	{
		$arIblockProps[$prop["CODE"]] = $prop["ID"];
	}
		
	CUserOptions::SetOption( "form", "form_element_".$iblockID, array(
	"tabs" => "edit1--#--".GetMessage("FORM_FORM_ELEMENT_544_SOTRUDNIK")."--,--ACTIVE--#--".GetMessage("FORM_FORM_ELEMENT_544_AKTIVNOST")."--,--NAME--#--*���--,--DETAIL_PICTURE--#--"
	.GetMessage("FORM_FORM_ELEMENT_544_FOTOGRAFIYA")."--,--PROPERTY_".$arIblockProps["EMAIL"]."--#--E-mail--,--PROPERTY_".$arIblockProps["POST"]."--#--".GetMessage("FORM_FORM_ELEMENT_544_DOLZHNOST")."--,--PROPERTY_".$arIblockProps["PHONE"]."--#--"
	.GetMessage("FORM_FORM_ELEMENT_544_TELEFON")."--;--edit2--#--".GetMessage("FORM_FORM_ELEMENT_544_OTDELY")."--,--SECTIONS--#--".GetMessage("FORM_FORM_ELEMENT_544_OTDELY")."--;--" ));	
	
//iblock user fields end

WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_CONTENT_TYPE" => $iblockType));
WizardServices::ReplaceMacrosRecursive($bitrixTemplateDir, array("IBLOCK_CONTENT_TYPE" => $iblockType));
WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_STAFF_ID" => $iblockID));
WizardServices::ReplaceMacrosRecursive($bitrixTemplateDir, array("IBLOCK_STAFF_ID" => $iblockID));
?>