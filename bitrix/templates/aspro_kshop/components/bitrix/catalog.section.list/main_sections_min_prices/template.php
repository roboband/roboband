<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if( count( $arResult["SECTIONS"] ) >= 1 ):?>

<?if (strlen($arParams["SECTION_LIST_TITLE"]) > 0):?>
	<ul class="tabs main_section_list">
		<li data-code="HIT" class="cur"><span><?=$arParams["SECTION_LIST_TITLE"]?></span><i class="triangle"></i></li>
		<li class="stretch"></li>
	</ul>
<?endif;?>

<div class="catalog_block main_section_list">
	<?foreach($arResult["SECTIONS"] as $arItem){
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT"));
	?>

		<div class="catalog_item_wrapp">
			<div class="catalog_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="image">
					<a href="<?=$arItem["SECTION_PAGE_URL"]?>" class="thumb">
						<?if( !empty($arItem["UF_MAIN_IMAGE"]) ):?>
							<?$img = CFile::ResizeImageGet($arItem["UF_MAIN_IMAGE"], array( "width" => 165, "height" => 165 ), BX_RESIZE_IMAGE_PROPORTIONAL,true );?>
							<img border="0" src="<?=$img["src"]?>" alt="<?=$arItem["NAME"];?>" title="<?=$arItem["NAME"];?>" />
						<?/*elseif( !empty($arItem["PICTURE"])):?>
							<?$img = CFile::ResizeImageGet($arItem["PICTURE"], array( "width" => 165, "height" => 165 ), BX_RESIZE_IMAGE_PROPORTIONAL,true );?>
							<img border="0" src="<?=$img["src"]?>" alt="<?=$arItem["NAME"];?>" title="<?=$arItem["NAME"];?>" />
						<?*/else:?>
							<img border="0" src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" />
						<?endif;?>
					</a>
				</div>

				<div class="section-title">
					<a href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
				</div>

				<?if ($arItem["MIN_PRICE"]["CATALOG_PRICE_".$arParams["CATALOG_PRICE_ID"]] > 0):?>
					<div class="section-price">
						<a href="<?=$arItem["SECTION_PAGE_URL"]?>">
							от <?=round($arItem["MIN_PRICE"]["CATALOG_PRICE_".$arParams["CATALOG_PRICE_ID"]])?>
							<?=($arItem["MIN_PRICE"]["CATALOG_CURRENCY_".$arParams["CATALOG_PRICE_ID"]] == "RUB")?'<span class="price-rouble"></span>':$arItem["MIN_PRICE"]["CATALOG_CURRENCY_".$arParams["CATALOG_PRICE_ID"]]?>
						</a>
					</div>
				<?endif;?>
			</div>
		</div>
	<?}?>
</div>

<div class="clear"></div>

<?endif;?>
