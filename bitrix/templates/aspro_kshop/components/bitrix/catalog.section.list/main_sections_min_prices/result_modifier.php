<?
/*
	$arSections = array();
	$arSectionsDepth3 = array();
	foreach( $arResult["SECTIONS"] as $arItem ) {
		if( $arItem["DEPTH_LEVEL"] == 1 ) { $arSections[$arItem["ID"]] = $arItem;}
		elseif( $arItem["DEPTH_LEVEL"] == 2 ) {$arSections[$arItem["IBLOCK_SECTION_ID"]]["SECTIONS"][$arItem["ID"]] = $arItem;}
		elseif( $arItem["DEPTH_LEVEL"] == 3 ) {$arSectionsDepth3[] = $arItem;}
	}
	foreach( $arSectionsDepth3 as $arItem) {
		foreach( $arSections as $key => $arSection) {
			if (is_array($arSection["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]) && !empty($arSection["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]])) {
				$arSections[$key]["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["SECTIONS"][$arItem["ID"]] = $arItem;
			}
		}
	}
	$arResult["SECTIONS"] = $arSections;
*/

if (!intval($arParams["CATALOG_PRICE_ID"]))
{
	$arParams["CATALOG_PRICE_ID"] = 1;
}

$arSectionsId = array();
foreach ($arResult["SECTIONS"] as $key => $arSection)
{
	if ($arSection["UF_SHOW_ON_MAIN"] == true)
	{
		//$arSectionsId[] = $arSection["ID"];

		$arOrder = array("CATALOG_PRICE_".$arParams["CATALOG_PRICE_ID"] => "ASC");
		$arFilter = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "CATALOG_AVAILABLE" => "Y", "ACTIVE" => "Y", "SECTION_ID" => $arSection["ID"], "INCLUDE_SUBSECTIONS" => "Y", ">CATALOG_PRICE_".$arParams["CATALOG_PRICE_ID"] => 0);
		$arSelect = array("ID", "NAME", "IBLOCK_SECTION_ID", "CATALOG_GROUP_".$arParams["CATALOG_PRICE_ID"]);
		$dbRes = CIBlockElement::GetList($arOrder, $arFilter, false, array("nTopCount" => 1), $arSelect);
		$arItem = $dbRes->fetch();

		$arResult["SECTIONS"][$key]["MIN_PRICE"] = $arItem;

		/*
		// Удалим разделы, для которых не нашли ни одной цены
		if (empty($arResult["SECTIONS"][$key]["MIN_PRICE"]["CATALOG_PRICE_".$arParams["CATALOG_PRICE_ID"]]))
		{
			unset($arResult["SECTIONS"][$key]);
		}
		*/
	}
	else
	{
		unset($arResult["SECTIONS"][$key]); // Удалим разделы, которые не нужено отображать
	}
}

// Отсортируем разделы по значению сортировки SORT
$arSort = array();
foreach ($arResult["SECTIONS"] as $key => $arSection)
{
	$arSort[$key] = $arSection["SORT"];
}
array_multisort($arSort, SORT_NUMERIC, $arResult["SECTIONS"]);
?>