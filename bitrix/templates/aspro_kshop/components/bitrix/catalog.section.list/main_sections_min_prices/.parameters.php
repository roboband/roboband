<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$arTemplateParameters['CATALOG_PRICE_ID'] = array(
	'PARENT' => '',
	'NAME' => GetMessage("SL_CAT_SECT_LIST_PRICE_TYPE_ID"),
	'TYPE' => 'STRING',
	'SORT' => 800,
	'DEFAULT' => 1
);

$arTemplateParameters['SECTION_LIST_TITLE'] = array(
		'PARENT' => '',
		'NAME' => GetMessage("SL_CAT_SECT_LIST_TITLE"),
		'TYPE' => 'STRING',
		'SORT' => 800,
		'DEFAULT' => GetMessage("SL_CAT_SECT_LIST_TITLE_DEFAULT"),
);

?>