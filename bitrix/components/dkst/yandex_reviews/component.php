<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!is_numeric($arParams['COUNT']))
	$arParams['COUNT'] = 10;
if($arParams['INCLUDE_JQUERY'] == 'Y')
	CJSCore::Init(array("jquery"));
if($arParams["YANDEX_ID"]) {
	session_start();
	$_SESSION['YA_REVIEWS'] = $arParams;
	$this->IncludeComponentTemplate();
}
?>