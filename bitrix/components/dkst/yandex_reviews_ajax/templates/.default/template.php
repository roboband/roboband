<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin(GetMessage('COMPOSITE_LOADING'));?>
<?
if(is_array($arResult["REVIEWS"])):

if(!isset($arResult["REVIEWS"]["errors"])):
?>
	<div class="hiddenblock page"><?=$arResult["REVIEWS"]["modelOpinions"]["page"];?></div>
	<div class="hiddenblock count"><?=$arResult["REVIEWS"]["modelOpinions"]["count"];?></div>
	<div class="hiddenblock total"><?=$arResult["REVIEWS"]["modelOpinions"]["total"];?></div>
	<? //functions for help
		function rdate($param, $time=0) {
			if(intval($time)==0)$time=time();
			if(strpos($param,'F')===false) return date($param, $time);
				else return date(str_replace('F',GetMessage('MONTH_'.date('n',$time)),$param), $time);
		}
		function date_text($date) {
			$date = substr($date, 0, -3);
			$day = 24*60*60;
			if (date("d m Y", mktime(0, 0, 0)) == date("d m Y", $date)){
				return GetMessage('TODAY');
			}
			elseif (date("d m Y", mktime(0, 0, 0) - $day) == date("d m Y", $date)){
				return GetMessage('YESTERDAY');
			}
			elseif(date("Y") == date("Y", $date)){
				return rdate("j F Y", $date);
			}
			else{
				return rdate("j F Y", $date);
			}
		}

		
	?>
	<div id="catalog-item-opinions">
	<ul class="data">
	<?//reviews...
	foreach ($arResult["REVIEWS"]["modelOpinions"]["opinion"] as $review) {
	$grade = $review["grade"] + 3;
	?>
	<li class="row">
	<div class="block-first">
		<div class="grade ok" title="<?=GetMessage('RATE_'.$grade);?>"></div>
		<div class="grade <? echo $grade >= 2 ? "ok" : "no"?>" title="<?=GetMessage('RATE_'.$grade);?>"></div>
		<div class="grade <? echo $grade >= 3 ? "ok" : "no"?>" title="<?=GetMessage('RATE_'.$grade);?>"></div>
		<div class="grade <? echo $grade >= 4 ? "ok" : "no"?>" title="<?=GetMessage('RATE_'.$grade);?>"></div>
		<div class="grade <? echo $grade == 5 ? "ok" : "no"?>" title="<?=GetMessage('RATE_'.$grade);?>"></div>
		<div class="grade-title">
			<?=GetMessage('RATE_'.$grade);?>
		</div>
		<div class="period"><?=GetMessage($review["usageTime"]);?></div>
		<div class="period-label"><?=GetMessage("USAGE_TIME");?></div>
	</div>
	<?if(isset($review["pro"]) && $review["pro"] !== ""):?>
	<div class="block-text">
		<div class="label"><?=GetMessage('PRO');?></div>
		<div class="text"><?=$review["pro"];?></div>
	</div>
	<?endif;?>
	<?if(isset($review["contra"]) && $review["contra"] !== ""):?>
	<div class="block-text">
		<div class="label"><?=GetMessage('CONTRA');?></div>
		<div class="text"><?=$review["contra"]?></div>
	</div>
	<?endif;?>
	<?if(isset($review["text"]) && $review["text"] !== ""):?>
	<div class="block-text">
		<?if((isset($review["pro"]) && $review["pro"] !== "")||(isset($review["contra"]) && $review["contra"] !== "")):?>
			<div class="label"><?=GetMessage('TEXT');?></div>
		<?endif;?>
		<div class="text">
		<?=$review["text"];?>
		</div>
	</div>
	<?endif;?>
	<div class="block-last">
		<?=GetMessage('AUTHOR');?>
		<?if($review["anonymous"]):?>
		<span title="<?=GetMessage('ANONYMOUS');?>" class="tp_user"><?=GetMessage('ANONYMOUS');?></span>
		<?else:?>
		<span title="	<?=$review["author"];?>" class="tp_user"><?=$review["author"];?></span>
		<?endif;?>
		&nbsp;&nbsp;&nbsp;
		<span title="<?=date_text($review["date"]);?>" class="date"><?=date_text($review["date"]);?></span>
	</div>
	</li>
	<?}?>
	</ul>
	</div>
<?else://print errors?>
	<?=GetMessage('ERROR');?>
	<?
	global $USER;
	if($USER->IsAdmin())
	{?>
		<div class="debug">
			<p><?=GetMessage('ADMIN_INFO');?></p>
			<?=GetMessage('ERROR_LIST');?>
				<ul>
					<?foreach ($arResult["REVIEWS"]["errors"] as $err) {?>
						<li><?=$err;?></li>
					<?}?>
				</ul>
			<a href="http://api.yandex.ru/market/content/doc/dg/concepts/error-codes.xml" target="_blank"><?=GetMessage('ERROR_INFO');?></a>
		</div>
	<?
	}
	?>
<?
endif;
else:
	echo GetMessage('ERROR');
	global $USER;
	if($USER->IsAdmin())
	{
		?><div class="debug"><?
			?><p><?echo GetMessage('ADMIN_INFO');?></p><?
			echo 'query error '.$json['errno'];
		?></div><?
	}
endif;
?>

<?if(method_exists($this, 'createFrame')) $frame->end();?>
