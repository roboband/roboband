<?
$MESS ['MONTH_1'] = "Января";
$MESS ['MONTH_2'] = "Февраля";
$MESS ['MONTH_3'] = "Марта";
$MESS ['MONTH_4'] = "Апреля";
$MESS ['MONTH_5'] = "Мая";
$MESS ['MONTH_6'] = "Июня";
$MESS ['MONTH_7'] = "Июля";
$MESS ['MONTH_8'] = "Августа";
$MESS ['MONTH_9'] = "Сентября";
$MESS ['MONTH_10'] = "Октября";
$MESS ['MONTH_11'] = "Ноября";
$MESS ['MONTH_12'] = "Декабря";

$MESS ['TODAY'] = "Cегодня";
$MESS ['YESTERDAY'] = "Вчера";

$MESS ['RATE_1'] = "ужасная модель";
$MESS ['RATE_2'] = "плохая модель";
$MESS ['RATE_3'] = "обычная модель";
$MESS ['RATE_4'] = "хорошая модель";
$MESS ['RATE_5'] = "отличная модель";

$MESS ['USAGE_TIME'] = "Опыт использования:";
$MESS ['FEW_DAYS'] = "несколько дней";
$MESS ['FEW_WEEKS'] = "несколько недель";
$MESS ['FEW_MONTHS'] = "несколько месяцев";
$MESS ['FEW_YEARS'] = "более года";

$MESS ['HELPFULNESS'] = "Отзыв полезен: ";
$MESS ['HELPFULNESS_YES'] = "Да";
$MESS ['HELPFULNESS_NO'] = "Нет";

$MESS ['PRO'] = "Достоинства:";
$MESS ['CONTRA'] = "Недостатки:";
$MESS ['TEXT'] = "Комментарий:";

$MESS ['AUTHOR'] = "Автор:";
$MESS ['ANONYMOUS'] = "пользователь скрыл свои данные";

$MESS ['REVIEWS_0'] = "отзыв";
$MESS ['REVIEWS_1'] = "отзыва";
$MESS ['REVIEWS_2'] = "отзывов";

$MESS ['ERROR'] = "Не удалось загрузить отзывы...";
$MESS ['ADMIN_INFO'] = "Информация только для администратора:";
$MESS ['ERROR_INFO'] = "Сообщения об ошибках";
$MESS ['ERROR_LIST'] = "Список ошибок:";

$MESS ['COMPOSITE_LOADING'] = "Загрузка...";
?>