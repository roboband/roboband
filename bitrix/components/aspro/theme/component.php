<?
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	$arParams["MODULE_ID"] = trim($arParams["MODULE_ID"]);	
	if (!$arParams["MODULE_ID"]) die;	
	if (!CModule::IncludeModule($arParams["MODULE_ID"])) die;
	global $TEMPLATE_OPTIONS, $SITE_THEME, $USER;	
	$bDemoMode = ($arParams["DEMO"]=="Y") ? true : false; 
	$bIsAdmin = $USER->IsAdmin();
	
	$arResult = array();
	$arResult["DEMO_MODE"] = $bDemoMode;
	$arResult["IS_ADMIN"] = $bIsAdmin;
	
	foreach ($TEMPLATE_OPTIONS as $templateOptionKey=>$templateOptionValue)
	{
		$arOptionValues = array();
		foreach ($templateOptionValue["VALUES"] as  $i=>$j) { $arOptionValues[] = $j["VALUE"]; }
	
		if ($bDemoMode) 
		{
			foreach ($_SESSION as $sessionKey=>$sessionValue)
			{
				if ($sessionKey == $templateOptionValue["ID"] && in_array($sessionValue, $arOptionValues)) 
				{ 
					$TEMPLATE_OPTIONS[$templateOptionKey]["CURRENT_VALUE"] = $sessionValue; 
				}
			}
		}		
		foreach($_REQUEST as $requestKey=>$requestValue)
		{
			if (strToUpper($requestKey) == $templateOptionValue["ID"] && in_array(strToUpper($requestValue), $arOptionValues))
			{
				if ($bDemoMode) 
				{
					$_SESSION[strToUpper($requestKey)] = strToUpper($requestValue);
					$TEMPLATE_OPTIONS[$templateOptionKey]["CURRENT_VALUE"] = strToUpper($requestValue);
				}
				elseif ($bIsAdmin)
				{
					COption::SetOptionString($arParams["MODULE_ID"], strToUpper($requestKey), strToUpper($requestValue), false, SITE_ID);		
					$TEMPLATE_OPTIONS[$templateOptionKey]["CURRENT_VALUE"] = strToUpper($requestValue);
				}
			}
		}
	}
	
	$arResult["ITEMS"] = $TEMPLATE_OPTIONS;
	if ($bDemoMode || $bIsAdmin) {$this->IncludeComponentTemplate(); }
	$SITE_THEME = strToLower($TEMPLATE_OPTIONS["COLOR_THEME"]["CURRENT_VALUE"]);
	$APPLICATION->AddHeadString('<link rel="shortcut icon" href="'.SITE_TEMPLATE_PATH.'/themes/'.$SITE_THEME.'/images/favicon.ico" type="image/x-icon" />',true);	
	$APPLICATION->AddHeadString('<link rel="apple-touch-icon" sizes="57x57" href="'.SITE_TEMPLATE_PATH.'/themes/'.$SITE_THEME.'/images/favicon_57.png" />',true);
	$APPLICATION->AddHeadString('<link rel="apple-touch-icon" sizes="72x72" href="'.SITE_TEMPLATE_PATH.'/themes/'.$SITE_THEME.'/images/favicon_72.png" />',true);
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/themes/'.$SITE_THEME.'/style.css');
	return $TEMPLATE_OPTIONS;
?>
