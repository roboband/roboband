<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// ������� ������ ����������
if(!CModule::IncludeModule("iblock")) return;


// ������ ������ 
// START
$arrSiteId = array(''=>GetMessage('SITE_ID_ALL'));
$rsSites = CSite::GetList($by="sort", $order="desc", Array());
while ($arS = $rsSites->Fetch())
{
    $arrSiteId[$arS['LID']] = '['.$arS['LID'].'] '.$arS['NAME'];  
}  
// END

// ��������������� ������
// START

    $arIBlockType = CIBlockParameters::GetIBlockTypes();
    if(empty($arCurrentValues["IBLOCK_TYPE_RECOMMEND"])){
        foreach($arIBlockType as $key => $value) {
           $arCurrentValues["IBLOCK_TYPE_RECOMMEND"] = $key;
           break;
        } 
    }    
    // ������� ��������
    $arIBlockRecommend = array('' => GetMessage('SELECT_CHANGE'));
    if($arCurrentValues["IBLOCK_TYPE_RECOMMEND"]) {
        

        $rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE_RECOMMEND"], "ACTIVE"=>"Y"));
        while($arr=$rsIBlock->Fetch())
        {
            $arIBlockRecommend[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
        }    
        
    }
    //�������� ���������� 
    $arPropertyListRecommend = array('' => GetMessage('SELECT_CHANGE'));
    $textPeremenRecommend = '';  
    if($arCurrentValues["IBLOCK_ID_RECOMMEND"]) { 

        $rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID_RECOMMEND"]));
        while($arr = $rsProp->Fetch())
        {
            $textPeremenRecommend .=  '#PROP_'.$arr["CODE"].'# - '.$arr["NAME"].'<br />'; 
            if($arr["PROPERTY_TYPE"] == "L") {   
                $arPropertyListRecommend[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
            }
        }
        
    } 
    //�������� �������� ����������
    $arRecommend_fillter_1_list = array('' => GetMessage('SELECT_CHANGE'));

    if($arCurrentValues["IBLOCK_ID_RECOMMEND"] && $arCurrentValues["PROPERTY_FILLTER_1_RECOMMEND"]) { 

        
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID_RECOMMEND"], "CODE"=>$arCurrentValues["PROPERTY_FILLTER_1_RECOMMEND"]));
        while($enum_fields = $property_enums->GetNext())
        {      
            $arRecommend_fillter_1_list[$enum_fields["ID"]] = "[".$enum_fields["XML_ID"]."] ".$enum_fields["VALUE"];   
        }    
            
    }    

    //������� ���� ���
    $arPriceRecommend = array();
    if(CModule::IncludeModule("catalog")) {
        $dbPrice = CCatalogGroup::GetList();
        while($arPrice = $dbPrice->Fetch()) {
            $arPriceRecommend[$arPrice["NAME"]] = "[".$arPrice["ID"]."] ".$arPrice["NAME"];
        } 
    }
 
    $sort_by_iblock_list = array(
        'SORT' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_SORT'),
        'ID' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_ID'),   
        'NAME' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_NAME'),
        'CREATED' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_CREATED'),    
        'SHOWS' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_SHOWS'),  
        'RAND' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_RAND'),                                    
    );  
    $sort_order_iblock_list = array(
        'ASC' => GetMessage('SORT_ORDER_IBLOCK_LIST_VALUE_ASC'),
        'DESC' => GetMessage('SORT_ORDER_IBLOCK_LIST_VALUE_DESC'),        
    );         
        
// END


// ������������� ������
// START

    $arIBlockType = CIBlockParameters::GetIBlockTypes();
    if(empty($arCurrentValues["IBLOCK_TYPE_VIEWED"])){
        foreach($arIBlockType as $key => $value) {
           $arCurrentValues["IBLOCK_TYPE_VIEWED"] = $key;
           break;
        } 
    }    
    
    // ������� ��������
    $arIBlockViewed = array('' => GetMessage('SELECT_CHANGE'));
    if($arCurrentValues["IBLOCK_TYPE_VIEWED"]) {
        

        $rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE_VIEWED"], "ACTIVE"=>"Y"));
        while($arr=$rsIBlock->Fetch())
        {
            $arIBlockViewed[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
        }    
        
    }
    //�������� ���������� 
    $arPropertyListViewed = array('' => GetMessage('SELECT_CHANGE'));
    $textPeremenViewed = '';  
    if($arCurrentValues["IBLOCK_ID_VIEWED"]) { 

        $rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID_VIEWED"]));
        while($arr = $rsProp->Fetch())
        {
            $textPeremenViewed .=  '#PROP_'.$arr["CODE"].'# - '.$arr["NAME"].'<br />'; 
            if($arr["PROPERTY_TYPE"] == "L") {   
                $arPropertyListViewed[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
            }
        }
        
    } 
    //�������� �������� ����������
    $arViewed_fillter_1_list = array('' => GetMessage('SELECT_CHANGE'));

    if($arCurrentValues["IBLOCK_ID_VIEWED"] && $arCurrentValues["PROPERTY_FILLTER_1_VIEWED"]) { 

        
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID_VIEWED"], "CODE"=>$arCurrentValues["PROPERTY_FILLTER_1_VIEWED"]));
        while($enum_fields = $property_enums->GetNext())
        {      
            $arViewed_fillter_1_list[$enum_fields["ID"]] = "[".$enum_fields["XML_ID"]."] ".$enum_fields["VALUE"];   
        }    
            
    }    

    //������� ���� ���
    $arPriceViewed = array();
    if(CModule::IncludeModule("catalog")) {
        $dbPrice = CCatalogGroup::GetList();
        while($arPrice = $dbPrice->Fetch()) {
            $arPriceViewed[$arPrice["NAME"]] = "[".$arPrice["ID"]."] ".$arPrice["NAME"];
        } 
    }  
 
    $sort_by_viewed_list = array(
        'DATE_VISIT' => GetMessage('SORT_BY_VIEWED_VALUE_DATE_VISIT'),
        'VIEW_COUNT' => GetMessage('SORT_BY_VIEWED_VALUE_VIEW_COUNT'),                                    
    );  
    $sort_order_viewed_list = array(
        'DESC' => GetMessage('SORT_ORDER_IBLOCK_LIST_VALUE_DESC'),        
        'ASC' => GetMessage('SORT_ORDER_IBLOCK_LIST_VALUE_ASC'),    
    );         
        
// END


// ������� ������
// START
if(CModule::IncludeModule("catalog")) {
    $arDiscount_list = array('' => GetMessage('SELECT_CHANGE'));
    $rsDiscount = CCatalogDiscount::GetList(
        array(), 
        Array(),
        false,
        false,
        array('ID','NAME','SITE_ID')
    );
    while($arrDiscount = $rsDiscount->Fetch())
    {    
        $arDiscount_list[$arrDiscount["ID"]] = "[".$arrDiscount["ID"]."] ".$arrDiscount["NAME"];
    }
}
// END

$arDiscount_Type = array(
    'Y' => GetMessage('COUPON_ONE_TIME_VALUE_Y'),
    'O' => GetMessage('COUPON_ONE_TIME_VALUE_O'),
    'N' => GetMessage('COUPON_ONE_TIME_VALUE_N')
);

$arDiscount_life_time_action = array(
    'DELETE' => GetMessage('COUPON_TIME_LIFE_ACTION_VALUE_DELETE'),    
    'DEACTION' => GetMessage('COUPON_TIME_LIFE_ACTION_VALUE_DEACTION')    
);

//���������� ����� �� ���������
//START  
$tpl_mailing = QueryGetData(
    $_SERVER['HTTP_HOST'],
    80,
    COption::GetOptionString('sotbit.mailing','TEMPLATE_MAILING_DEF'),
    "", 
    $error_number, 
    $error_text
);
if(empty($tpl_mailing)){
    $tpl_mailing = QueryGetData(
        $_SERVER['HTTP_HOST'],
        80,
        '/bitrix/components/sotbit/sotbit.mailing.logic/lang/ru/tpl_mailing/default.php',
        "", 
        $error_number, 
        $error_text
    );          
}           
$MESSAGE_DEFAULT = CSotbitMailingHelp::ReplaceVariables($tpl_mailing, array('SITE_URL' => $_SERVER['HTTP_HOST'], 'MESSAGE_TEXT'=> GetMessage("MESSAGE_DEFAULT")));
//END
  
//��������  ���������� �������
// TABS - ��� ���� � ����
// TABS_NAME - ��� ����
// PARENT - ��� ������ � ���� 
// PARENT_NAME - ��� ������ � ����
// NAME - ��� ��������
// TYPE -  ���  ��������   
// ------STRING - ������
// ------INT - �������� ���� 
// ------CHECKBOX - ������������� ��/���
// ------TEXT - ����� html ���������
// ------TEXTAREA - ����� ���������� ����
// ------TABS_INFO - ��� � ������, ��� ���������� ��������
// ------PHP - ������� php ����  � ������ ��������
// ------DATE_PERIOD - ��������� � �����
// ------USER_ID - ����� id ������������

  
$arTemplateParameters = array();
	
  // ���������� �������������  
$arTemplateParameters["SITE_ID"] = array(    
    "PARENT" => "PARAM_USER_FILLTER",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_USER_FILLTER"),
    "NAME" => GetMessage('SITE_ID_TITLE'),
    "TYPE" => "LIST",
    "VALUES" => $arrSiteId,
    "SORT" => "2",
);     
    
   //��������� ������ ������� ������
   
$arTemplateParameters["LAST_LOGIN"] = array(    
    "PARENT" => "PARAM_USER_FILLTER",                                        
    "PARENT_NAME" => GetMessage("GROUP_PARAM_USER_FILLTER"),
    "NAME" => GetMessage("LAST_LOGIN_TITLE"),
    "TYPE" => "DATE_PERIOD_AGO",
    "SORT" => "22",
); 
            
// �������������� ������
// START
$arTemplateParameters["RECOMMEND_SHOW"] =  array(
    "TABS" => 'TABS_RECOMMEND',
    "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
    "PARENT" => "PARAM_RECOMMEND_SETTING",
    "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_SETTING_NAME"),
    "NAME" => GetMessage("RECOMMEND_SHOW_TITLE"),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "4",  
    "SORT" => "7",
    "REFRESH" => "Y",
);  

if($arCurrentValues["RECOMMEND_SHOW"]=='Y'){

 
    $arTemplateParameters["IBLOCK_TYPE_RECOMMEND"] =  array(   
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),    
        "PARENT" => "PARAM_IBLOCK_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_FILLTER_NAME"),
        "NAME" => GetMessage('IBLOCK_TYPE_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arIBlockType,
        "REFRESH" => "Y",
        "SORT" => "10",
    );
    $arTemplateParameters["IBLOCK_ID_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_IBLOCK_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_FILLTER_NAME"),
        "NAME" => GetMessage('IBLOCK_ID_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arIBlockRecommend,
        "REFRESH" => "Y",
        "SORT" => "20",
    ); 
    $arTemplateParameters["PROPERTY_FILLTER_1_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_IBLOCK_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_FILLTER_NAME"),
        "NAME" => GetMessage('PROPERTY_FILLTER_1_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arPropertyListRecommend,
        "REFRESH" => "Y",
        "SORT" => "40",
    ); 
    $arTemplateParameters["PROPERTY_FILLTER_1_VALUE_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_IBLOCK_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_RECOMMEND_NAME"),
        "NAME" => GetMessage('PROPERTY_FILLTER_1_VALUE_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arRecommend_fillter_1_list,
        "REFRESH" => "N",
        "SORT" => "50",
        "NOTES" => GetMessage('PROPERTY_FILLTER_1_VALUE_RECOMMEND_NOTES'),
    );   
      
    $arTemplateParameters["TOP_COUNT_FILLTER_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_IBLOCK_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_RECOMMEND_NAME"),
        "NAME" => GetMessage('TOP_COUNT_FILLTER_RECOMMEND_TITLE'),
        "TYPE" => "INT",
        "SORT" => "50",
        "DEFAULT" => "4"
    );     
         
           
    $arTemplateParameters["SORT_BY_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_SORT_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_SORT_NAME"),
        "NAME" => GetMessage('SORT_BY_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $sort_by_iblock_list,
        "REFRESH" => "N",
        "SORT" => "20",
    );      
    $arTemplateParameters["SORT_ORDER_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_SORT_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_SORT_NAME"),
        "NAME" => GetMessage('SORT_ORDER_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $sort_order_iblock_list,
        "REFRESH" => "N",
        "SORT" => "40",
    );  
      
      //��� ���� 
    $arTemplateParameters["PRICE_TYPE_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_PRICE_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_PRICE_NAME"),
        "NAME" => GetMessage('PRICE_TYPE_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arPriceRecommend,
        "REFRESH" => "N",
        "SORT" => "40",
    );    
      
      //������� ��� ������ �������
    $arTemplateParameters["IMG_WIDTH_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_TEMP_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_RECOMMEND_NAME"),
        "NAME" => GetMessage("IMG_WIDTH_RECOMMEND_TITLE"),
        "TYPE" => "INT",
        "DEFAULT" => "100" ,
        "SORT" => "10",
    );    

    $arTemplateParameters["IMG_HEIGHT_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),    
        "PARENT" => "PARAM_TEMP_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_RECOMMEND_NAME"),
        "NAME" => GetMessage("IMG_HEIGHT_RECOMMEND_TITLE"),
        "TYPE" => "INT",
        "DEFAULT" => "200" ,
        "SORT" => "20",
    );        
      
      
    $arTemplateParameters["TEMP_TOP_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),  
        "PARENT" => "PARAM_TEMP_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_RECOMMEND_NAME"),
        "NAME" => GetMessage("TEMP_TOP_RECOMMEND_TITLE"),
        "COLS" => 40,
        "HEIGHT" => 160,
        "TYPE" => "TEXT",
        "DEFAULT" => GetMessage("TEMP_TOP_RECOMMEND_DEFAULT"), 
        "NOTES"  =>  GetMessage("TEMP_TOP_RECOMMEND_NOTES"),
        "SORT" => "40",
    );  
    $arTemplateParameters["TEMP_LIST_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"), 
        "PARENT" => "PARAM_TEMP_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_RECOMMEND_NAME"),
        "NAME" => GetMessage("TEMP_LIST_RECOMMEND_TITLE"),
        "COLS" => 40,
        "HEIGHT" => 240,
        "TYPE" => "TEXT",
        "DEFAULT" => GetMessage("TEMP_LIST_RECOMMEND_DEFAULT"), 
        "NOTES"  =>  GetMessage("TEMP_LIST_RECOMMEND_NOTES", array('#IBLOCK_PROP#'=>$textPeremenRecommend)),
        "SORT" => "50",
    );    
    $arTemplateParameters["TEMP_BOTTOM_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_TEMP_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_RECOMMEND_NAME"),
        "NAME" => GetMessage("TEMP_BOTTOM_RECOMMEND_TITLE"),
        "COLS" => 40,
        "HEIGHT" => 160,
        "TYPE" => "TEXT",
        "DEFAULT" => GetMessage("TEMP_BOTTOM_RECOMMEND_DEFAULT"), 
        "NOTES"  =>  GetMessage("TEMP_BOTTOM_RECOMMEND_NOTES"),
        "SORT" => "60",
    );    
      
      // PHP �����������
    $arTemplateParameters["PHP_RECOMMEND_FILLTER_BEFORE"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),     
        "PARENT" => "PHP_MODIF_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_RECOMMEND_NAME"),
        "NAME" => GetMessage("PHP_RECOMMEND_FILLTER_BEFORE_TITLE"),
        "HEIGHT" => 250,
        "WIDTH" => 1000,
        "TYPE" => "PHP",
        "SORT" => "70",
        "NOTES" => GetMessage("PHP_RECOMMEND_FILLTER_BEFORE_NOTES"),
    );      
        
    $arTemplateParameters["PHP_RECOMMEND_FILLTER_WHILE_AFTER"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),     
        "PARENT" => "PHP_MODIF_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_RECOMMEND_NAME"),
        "NAME" => GetMessage("PHP_RECOMMEND_FILLTER_WHILE_AFTER_TITLE"),
        "HEIGHT" => 250,
        "WIDTH" => 1000,
        "TYPE" => "PHP",
        "SORT" => "90",
        "NOTES" => GetMessage("PHP_RECOMMEND_FILLTER_WHILE_AFTER_NOTES"),
    );     
    
}
//END
     
     
  //������������� ������
  //START
$arTemplateParameters["VIEWED_SHOW"] =  array(
    "TABS" => 'TABS_VIEWED',
    "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),   
    "PARENT" => "PARAM_VIEWED_SETTING",
    "PARENT_NAME" => GetMessage("GROUP_VIEWED_SETTING_NAME"),
    "NAME" => GetMessage("VIEWED_SHOW_TITLE"),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "4" ,
    "SORT" => "7",
    "REFRESH" => "Y",
);  

if($arCurrentValues["VIEWED_SHOW"]=='Y'){ 
    
    $arTemplateParameters["IBLOCK_TYPE_VIEWED"] =  array(   
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),    
        "PARENT" => "PARAM_IBLOCK_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_VIEWED_FILLTER_NAME"),
        "NAME" => GetMessage('IBLOCK_TYPE_VIEWED_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arIBlockType,
        "REFRESH" => "Y",
        "SORT" => "10",
    );
    $arTemplateParameters["IBLOCK_ID_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),     
        "PARENT" => "PARAM_IBLOCK_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_VIEWED_FILLTER_NAME"),
        "NAME" => GetMessage('IBLOCK_ID_VIEWED_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arIBlockViewed,
        "REFRESH" => "Y",
        "SORT" => "20",
    ); 
    $arTemplateParameters["PROPERTY_FILLTER_1_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),       
        "PARENT" => "PARAM_IBLOCK_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_VIEWED_FILLTER_NAME"),
        "NAME" => GetMessage('PROPERTY_FILLTER_1_VIEWED_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arPropertyListViewed,
        "REFRESH" => "Y",
        "SORT" => "40",
    ); 
    $arTemplateParameters["PROPERTY_FILLTER_1_VALUE_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),     
        "PARENT" => "PARAM_IBLOCK_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_VIEWED_NAME"),
        "NAME" => GetMessage('PROPERTY_FILLTER_1_VALUE_VIEWED_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arViewed_fillter_1_list,
        "REFRESH" => "N",
        "SORT" => "50",
        "NOTES" => GetMessage('PROPERTY_FILLTER_1_VALUE_VIEWED_NOTES'),
    );    
    $arTemplateParameters["TOP_COUNT_FILLTER_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),     
        "PARENT" => "PARAM_IBLOCK_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_VIEWED_NAME"),
        "NAME" => GetMessage('TOP_COUNT_FILLTER_VIEWED_TITLE'),
        "TYPE" => "INT",
        "SORT" => "50",
        "DEFAULT" => "4"
    );            
    $arTemplateParameters["SORT_BY_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),    
        "PARENT" => "PARAM_SORT_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_VIEWED_SORT_NAME"),
        "NAME" => GetMessage('SORT_BY_VIEWED_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $sort_by_viewed_list,
        "REFRESH" => "N",
        "SORT" => "20",
    );      
    $arTemplateParameters["SORT_ORDER_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),     
        "PARENT" => "PARAM_SORT_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_VIEWED_SORT_NAME"),
        "NAME" => GetMessage('SORT_ORDER_VIEWED_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $sort_order_viewed_list,
        "REFRESH" => "N",
        "SORT" => "40",
    );  
      
      //��� ���� 
    $arTemplateParameters["PRICE_TYPE_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),       
        "PARENT" => "PARAM_PRICE_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_VIEWED_PRICE_NAME"),
        "NAME" => GetMessage('PRICE_TYPE_VIEWED_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arPriceViewed,
        "REFRESH" => "N",
        "SORT" => "40",
    );   
      
      
      
      //������� ��� ������ �������
    $arTemplateParameters["IMG_WIDTH_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),    
        "PARENT" => "PARAM_TEMP_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_VIEWED_NAME"),
        "NAME" => GetMessage("IMG_WIDTH_VIEWED_TITLE"),
        "TYPE" => "INT",
        "DEFAULT" => "100" ,
        "SORT" => "10",
    );    

    $arTemplateParameters["IMG_HEIGHT_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),       
        "PARENT" => "PARAM_TEMP_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_VIEWED_NAME"),
        "NAME" => GetMessage("IMG_HEIGHT_VIEWED_TITLE"),
        "TYPE" => "INT",
        "DEFAULT" => "200" ,
        "SORT" => "20",
    );        
    $arTemplateParameters["TEMP_TOP_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),     
        "PARENT" => "PARAM_TEMP_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_VIEWED_NAME"),
        "NAME" => GetMessage("TEMP_TOP_VIEWED_TITLE"),
        "COLS" => 40,
        "HEIGHT" => 160,
        "TYPE" => "TEXT",
        "DEFAULT" => GetMessage("TEMP_TOP_VIEWED_DEFAULT"), 
        "NOTES"  =>  GetMessage("TEMP_TOP_VIEWED_NOTES"),
        "SORT" => "40",
    );  
    $arTemplateParameters["TEMP_LIST_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),    
        "PARENT" => "PARAM_TEMP_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_VIEWED_NAME"),
        "NAME" => GetMessage("TEMP_LIST_VIEWED_TITLE"),
        "COLS" => 40,
        "HEIGHT" => 240,
        "TYPE" => "TEXT",
        "DEFAULT" => GetMessage("TEMP_LIST_VIEWED_DEFAULT"), 
        "NOTES"  =>  GetMessage("TEMP_LIST_VIEWED_NOTES", array('#IBLOCK_PROP#'=>$textPeremenViewed)),
        "SORT" => "50",
    );    
    $arTemplateParameters["TEMP_BOTTOM_VIEWED"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),     
        "PARENT" => "PARAM_TEMP_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_VIEWED_NAME"),
        "NAME" => GetMessage("TEMP_BOTTOM_VIEWED_TITLE"),
        "COLS" => 40,
        "HEIGHT" => 160,
        "TYPE" => "TEXT",
        "DEFAULT" => GetMessage("TEMP_BOTTOM_VIEWED_DEFAULT"), 
        "NOTES"  =>  GetMessage("TEMP_BOTTOM_VIEWED_NOTES"),
        "SORT" => "60",
    );    
      
      // PHP �����������  
    $arTemplateParameters["PHP_VIEWED_FILLTER_BEFORE"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),        
        "PARENT" => "PHP_MODIF_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_VIEWED_NAME"),
        "NAME" => GetMessage("PHP_VIEWED_FILLTER_BEFORE_TITLE"),
        "HEIGHT" => 250,
        "WIDTH" => 1000,
        "TYPE" => "PHP",
        "SORT" => "70",
        "NOTES" => GetMessage("PHP_VIEWED_FILLTER_BEFORE_NOTES"),
    );        
    $arTemplateParameters["PHP_VIEWED_FILLTER_WHILE_AFTER"] =  array(
        "TABS" => 'TABS_VIEWED',
        "TABS_NAME" => GetMessage("TABS_VIEWED_NAME"),        
        "PARENT" => "PHP_MODIF_VIEWED",
        "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_VIEWED_NAME"),
        "NAME" => GetMessage("PHP_VIEWED_FILLTER_WHILE_AFTER_TITLE"),
        "HEIGHT" => 250,
        "WIDTH" => 1000,
        "TYPE" => "PHP",
        "SORT" => "90",
        "NOTES" => GetMessage("PHP_VIEWED_FILLTER_WHILE_AFTER_NOTES"),
    );  
       
}
  //END
     

  //������ �� ������
$arTemplateParameters["COUPON_ADD"] =  array(
    "TABS" => 'TABS_DISCOUNT',
    "TABS_NAME" => GetMessage("TABS_DISCOUNT_NAME"), 
    "PARENT" => "PARAM_COUPON_ADD",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_COUPON_ADD"),
    "NAME" => GetMessage("COUPON_ADD_TITLE"),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "4" ,
    "SORT" => "10",
    "REFRESH" => "Y",
); 


if($arCurrentValues["COUPON_ADD"]=='Y'){

    $arTemplateParameters["COUPON_DISCOUNT_ID"] =  array(
        "TABS" => 'TABS_DISCOUNT',
        "TABS_NAME" => GetMessage("TABS_DISCOUNT_NAME"), 
        "PARENT" => "PARAM_COUPON_ADD",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_COUPON_ADD"),
        "NAME" => GetMessage("COUPON_DISCOUNT_ID_TITLE"),
        "TYPE" => "LIST",
        "VALUES" => $arDiscount_list,
        "SORT" => "20",
    );     
              
    $arTemplateParameters["COUPON_ONE_TIME"] =  array(
        "TABS" => 'TABS_DISCOUNT',
        "TABS_NAME" => GetMessage("TABS_DISCOUNT_NAME"),   
        "PARENT" => "PARAM_COUPON_ADD",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_COUPON_ADD"),
        "NAME" => GetMessage("COUPON_ONE_TIME_TITLE"),
        "TYPE" => "LIST",
        "VALUES" => $arDiscount_Type,
        "SORT" => "30",
    ); 
        
    $arTemplateParameters["COUPON_TIME_LIFE"] =  array(
        "TABS" => 'TABS_DISCOUNT',
        "TABS_NAME" => GetMessage("TABS_DISCOUNT_NAME"),   
        "PARENT" => "PARAM_COUPON_ADD",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_COUPON_ADD"),
        "NAME" => GetMessage("COUPON_TIME_LIFE_TITLE"),
        "TYPE" => "INT",
        "DEFAULT" => "48" ,
        "SORT" => "40",
    );     
         
    $arTemplateParameters["COUPON_TIME_LIFE_ACTION"] =  array(
        "TABS" => 'TABS_DISCOUNT',
        "TABS_NAME" => GetMessage("TABS_DISCOUNT_NAME"),   
        "PARENT" => "PARAM_COUPON_ADD",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_COUPON_ADD"),
        "NAME" => GetMessage("COUPON_TIME_LIFE_ACTION_TITLE"),
        "TYPE" => "LIST",
        "VALUES" => $arDiscount_life_time_action,
        "SORT" => "50",
    );  
  
}       
 
        
 
  //������ ������
$arTemplateParameters["SUBJECT"] = array(
    "PARENT" => "MESSAGE_INFO",
    "PARENT_NAME" => GetMessage("GROUP_MESSAGE_INFO_NAME"),
    "NAME" => GetMessage("SUBJECT_TITLE"),
    "TYPE" => "STRING",
    "DEFAULT" => GetMessage("SUBJECT_DEFAULT"),
    "SORT" => "10",
    "SIZE" => '50'
); 
$arTemplateParameters["EMAIL_FROM"] = array(
    "PARENT" => "MESSAGE_INFO",
    "PARENT_NAME" => GetMessage("GROUP_MESSAGE_INFO_NAME"),
    "NAME" => GetMessage("EMAIL_FROM_TITLE"),
    "TYPE" => "STRING",
    "DEFAULT" => COption::GetOptionString('main', 'email_from'),
    "SORT" => "20",
    "SIZE" => '50'
);    
$arTemplateParameters["EMAIL_TO"] = array(
    "PARENT" => "MESSAGE_INFO",
    "PARENT_NAME" => GetMessage("GROUP_MESSAGE_INFO_NAME"),
    "NAME" => GetMessage("EMAIL_TO_TITLE"),
    "TYPE" => "STRING",
    "DEFAULT" => "#USER_EMAIL#" ,
    "SORT" => "30",
    "SIZE" => '50'
); 

$arTemplateParameters["BCC"] = array(
    "PARENT" => "MESSAGE_INFO",
    "PARENT_NAME" => GetMessage("GROUP_MESSAGE_INFO_NAME"),
    "NAME" => GetMessage("BCC_TITLE"),
    "TYPE" => "STRING",
    "DEFAULT" => "" ,
    "SORT" => "40",
    "SIZE" => '50'
); 
      
$arTemplateParameters["MESSAGE"] = array(
    "PARENT" => "MESSAGE_INFO",
    "PARENT_NAME" => GetMessage("GROUP_MESSAGE_INFO_NAME"),
    "TYPE" => "TEXT",
    "DEFAULT" => $MESSAGE_DEFAULT, 
    "NOTES"  =>  GetMessage("GROUP_MESSAGE_NOTES", array("#PROP#"=>$textPeremen)),
    "SORT" => "60",
);


  
  // PHP ����������� 
  // START 
$arTemplateParameters["PHP_FILLTER_USER_PARAM_BEFORE"] = array(
    "PARENT" => "PHP_MODIF_MAILING",                                      
    "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_MAILING_NAME"),
    "NAME" => GetMessage("PHP_FILLTER_USER_PARAM_BEFORE_TITLE"),
    "HEIGHT" => 200,
    "WIDTH" => 1000,
    "TYPE" => "PHP",
    "SORT" => "40",
    "NOTES" => GetMessage("PHP_FILLTER_USER_PARAM_BEFORE_NOTES"),
);
$arTemplateParameters["PHP_FILLTER_USER_PARAM_AFTER"] = array(
    "PARENT" => "PHP_MODIF_MAILING",                                      
    "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_MAILING_NAME"),
    "NAME" => GetMessage("PHP_FILLTER_USER_PARAM_AFTER_TITLE"),
    "HEIGHT" => 200,
    "WIDTH" => 1000,
    "TYPE" => "PHP",
    "SORT" => "50",
    "NOTES" => GetMessage("PHP_FILLTER_USER_PARAM_AFTER_NOTES"),
);  
      
$arTemplateParameters["PHP_MESSAGE_FOREACH_BEFORE"] = array(
    "PARENT" => "PHP_MODIF_MAILING",
    "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_MAILING_NAME"),
    "NAME" => GetMessage("PHP_MESSAGE_FOREACH_BEFORE_TITLE"),
    "HEIGHT" => 200,
    "WIDTH" => 1000,
    "TYPE" => "PHP",
    "SORT" => "70",
    "NOTES" => GetMessage("PHP_MESSAGE_FOREACH_BEFORE_NOTES"),
);    
$arTemplateParameters["PHP_MESSAGE_FOREACH_ITEM_BEFORE"] = array(
    "PARENT" => "PHP_MODIF_MAILING",                                      
    "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_MAILING_NAME"),
    "NAME" => GetMessage("PHP_MESSAGE_FOREACH_ITEM_BEFORE_TITLE"),
    "HEIGHT" => 200,
    "WIDTH" => 1000,
    "TYPE" => "PHP",
    "SORT" => "80",
    "NOTES" => GetMessage("PHP_MESSAGE_FOREACH_ITEM_BEFORE_NOTES"),
);    
  // END    
  
  
$arTemplateParameters["TABS_SOTBIT_MAILING_INSTRUCTION"] = array(
    "TABS" => 'TABS_SOTBIT_MAILING_INSTRUCTION',
    "TABS_NAME" => GetMessage("TABS_SOTBIT_MAILING_INSTRUCTION_TITLE"),   
    "PARENT" => "PARAM_TABS_SOTBIT_MAILING_INSTRUCTION",
    "PARENT_NAME" => GetMessage("GROUP_TABS_SOTBIT_MAILING_INSTRUCTION_NAME"),    
    "TYPE" => "TABS_INFO",
    "DEFAULT" => GetMessage("TABS_SOTBIT_MAILING_INSTRUCTION_DEFAULT"),    
);     
  
  
 


?>
