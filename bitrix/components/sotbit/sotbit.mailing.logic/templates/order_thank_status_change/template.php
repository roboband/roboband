<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $DB;  


// ������������ ������ ��������� 
if(!CModule::IncludeModule("iblock"))
{
    return;
}

if(!CModule::IncludeModule("sale"))
{
    return;
}





if(empty($arParams['MORE_OPTIONS_TEMPLATE']["ORDER_FILLTER_ID"])){
    return;    
}

//������� ������
//START

    // ������� �������������� ������
    // START
        $ARR_USER_ID_SEND = ''; 

        // ������� �������
        $arOrderStatus = array();
        $dbStatusList = CSaleStatus::GetList(array("SORT" => "ASC"),array(),false,false,array("ID", "NAME"));
        while($arStatusList = $dbStatusList->Fetch())
        {  
            $arOrderStatus[$arStatusList['ID']] = $arStatusList['NAME'];    
        }
        //������� ������� ������  
        $arOrderPaySystemId = array();        
        $dbpaySystem = CSalePaySystem::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), Array());
        while($paySystem = $dbpaySystem->Fetch()){
            $arOrderPaySystemId[$paySystem['ID']] = $paySystem['NAME'];    
        } 
        
        //������� ������������������ ������ ��������  
        $arOrderDeliveryId = array();     
        $rsDeliveryServicesList = CSaleDeliveryHandler::GetList(array(), array()); 
        while ($arDeliveryService = $rsDeliveryServicesList->GetNext())
        {    
            
            
            if (!is_array($arDeliveryService) || !is_array($arDeliveryService["PROFILES"])) continue;
            foreach ($arDeliveryService["PROFILES"] as $profile_id => $arDeliveryProfile)
            {
                $delivery_id = $arDeliveryService["SID"].":".$profile_id;
                $arOrderDeliveryId[$delivery_id] = $arDeliveryService["NAME"]." - ".$arDeliveryProfile["TITLE"];     
            }  
        } 
        //������� ������� ������ ��������   
        $dbDelivery = CSaleDelivery::GetList(array("SORT"=>"ASC", "NAME"=>"ASC"),array());
        while ($arDelivery = $dbDelivery->GetNext())
        {
            $arOrderDeliveryId[$arDelivery["ID"]] = $arDelivery["NAME"];
        }        
           
           
        // �������� ��������������
        $arLocationValue = array();
        $db_location = CSaleLocation::GetList();
        while($arr_location = $db_location->Fetch()){
            $arLocationValue[$arr_location['ID']] = $arr_location;
        }
      
        //������� ���� �������
        $propertyOrderInfo = array();
        $db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"),array(),false,false,array());
        while($arr_props = $db_props->Fetch())
        {  
            $propertyOrderInfo[$arr_props["CODE"]] = $arr_props;    
        }         
        
               
            
        
    // END
    
    
    //������� ������ ������������
    $arrEmailSend = array();
     
    //�������� �������
    //START
        $arFilterOrder = array();
        
    
        if(!empty($arParams["SITE_ID"])){
            $arFilterOrder['LID'] = $arParams["SITE_ID"];    
        }  
            
        //id ������
        if($arParams['MORE_OPTIONS_TEMPLATE']["ORDER_FILLTER_ID"]){
            $arFilterOrder['ID'] = $arParams['MORE_OPTIONS_TEMPLATE']["ORDER_FILLTER_ID"];    
        }
        //�������
        if($arParams['MORE_OPTIONS_TEMPLATE']["ORDER_FILLTER_STATUS"]){
            $arFilterOrder['STATUS_ID'] = $arParams['MORE_OPTIONS_TEMPLATE']["ORDER_FILLTER_STATUS"];    
        }
        
     

    //END      
    $orderIdMailing = array();
     

     
     
    include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_FILLTER_ORDER_PARAM_BEFORE.php");
    $dbResOrder = CSaleOrder::GetList($arSortOrder,  $arFilterOrder, false, false, $arSelectOrder);
    while($arItemsOrder = $dbResOrder->Fetch())
    {   

        $EmailSend = array();

        
        //��������� ��������
        $arItemsOrder['PRICE_PRINT'] = SaleFormatCurrency($arItemsOrder['PRICE'], $arItemsOrder['CURRENCY']); 
        $arItemsOrder['PRICE_DELIVERY_PRINT'] = SaleFormatCurrency($arItemsOrder['PRICE_DELIVERY'], $arItemsOrder['CURRENCY']); 
        
        
        //��������� ������ ��� ��������  
        foreach($arItemsOrder as $ko=>$vo) { 
            $arItemsOrder['ORDER_'.$ko] = $vo;
            //������,��������,������ 
            if(in_array($ko,array('PAYED','CANCELED','ALLOW_DELIVERY'))) {
                $arItemsOrder['ORDER_'.$ko.'_PRINT'] = GetMessage('SELECT_PARAM_'.$vo);        
            }
            //������ ������ 
            elseif($ko=='STATUS_ID'){
                $arItemsOrder['ORDER_'.$ko.'_PRINT'] = $arOrderStatus[$vo] ; 
            }
            //������� ������
            elseif($ko=='PAY_SYSTEM_ID'){
                $arItemsOrder['ORDER_'.$ko.'_PRINT'] = $arOrderPaySystemId[$vo] ; 
            }            
            //������ ��������
            elseif($ko=='DELIVERY_ID'){
                $arItemsOrder['ORDER_'.$ko.'_PRINT'] = $arOrderDeliveryId[$vo] ; 
            }  
                                
            unset($arItemsOrder[$ko]);                
        }
        //������� ������ �� ������
        //START  
        foreach($propertyOrderInfo as $k=>$v){
            $arItemsOrder['ORDER_PROP_'.$k] = '';        
        } 
        $arItemsOrder['ORDER_EMAIL'] = '';  
        $order_props = CSaleOrderPropsValue::GetOrderProps($arItemsOrder['ORDER_ID']);
        while($arProps = $order_props->Fetch())
        {    
            
            if($arProps['IS_EMAIL'] == 'Y'){
                $arItemsOrder['ORDER_EMAIL'] = $arProps["VALUE"];
                $arItemsOrder['ORDER_PROP_'.$arProps["CODE"]] = $arProps["VALUE"];                
            } 
            elseif($arProps['IS_LOCATION'] == 'Y') {
                $arItemsOrder['ORDER_PROP_'.$arProps["CODE"]] = $arProps["VALUE"];
                 
                $arItemsOrder['ORDER_PROP_'.$arProps["CODE"].'_COUNTRY'] = $arLocationValue[$arProps["VALUE"]]['COUNTRY_NAME']; 
                $arItemsOrder['ORDER_PROP_'.$arProps["CODE"].'_CITY'] = $arLocationValue[$arProps["VALUE"]]['CITY_NAME'] ;                       
            }
            else{
                $arItemsOrder['ORDER_PROP_'.$arProps["CODE"]] = $arProps["VALUE"];                   
            }      
             
        }    
        //END  
        

        
        // ������� ������������
        //START
        $rsUser = CUser::GetByID($arItemsOrder['ORDER_USER_ID']);
        $arUser = $rsUser->Fetch();
        foreach($arUser as $ku => $vu){
            $arItemsOrder['USER_'.$ku]  = $vu;       
        }
        //END        
                    
        
        
        $EmailSend = $arItemsOrder;      
        $EmailSend['PARAM_1'] = $arItemsOrder['ORDER_ID'];  
        $EmailSend['PARAM_3'] = $arItemsOrder['ORDER_USER_ID'];
        $EmailSend['PARAM_MESSEGE']['ORDER_ID'] = $arItemsOrder['ORDER_ID'];
            
        
        $phpIncludeFunction = array();
        include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_FILLTER_ORDER_PARAM_AFTER.php");

        if($phpIncludeFunction['isContinue']){
            continue;    
        }  
        if($phpIncludeFunction['isBreak']){
            break;    
        }         
        

        $orderIdMailing[] = $arItemsOrder['ORDER_ID'];  
        
        if($EmailSend['USER_ID']){
            $ARR_USER_ID_SEND[] = $EmailSend['USER_ID'];               
        }         
        
        if($EmailSend) {
            $arrEmailSend[$arItemsOrder['ORDER_ID']] = $EmailSend;               
        }              
  
        
        
    }
       
 
                                  
//END



//������� ������ ������
//START
    //������� ������ ������
if($orderIdMailing){
    
    
    $arrProductId = array();
    $arrUserId = array();
    $basketFillter = array(
        "ORDER_ID" => $orderIdMailing, 
    ); 
    $basketSelect = array(
        'ID',
        'NAME',
        'DETAIL_PAGE_URL',
        'PRICE',
        'CURRENCY',
        'PRODUCT_ID',
        'PRODUCT_PRICE_ID',
        'USER_ID',
        'QUANTITY',
        'ORDER_ID'    
    );
    include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_FILLTER_BASKET_PARAM_AFTER.php"); 
        
    $resBasketItems = CSaleBasket::GetList(
       $basketSort,
       $basketFillter, 
       false,
       false,
       $basketSelect
    );
    while($arItems = $resBasketItems->Fetch())
    {
        //������� ID ������ ��� �������� �������� 
        //START 
        $mxResult = CCatalogSku::GetProductInfo($arItems['PRODUCT_ID']);
        if(is_array($mxResult)) {
            $arrProductId[$mxResult['ID']] = $mxResult['ID']; 
            $arItems['ITEM_PRODUCT_ID'] = $mxResult['ID'];       
            
            //��� �������
            $arrProductIdOffer[$arItems['PRODUCT_ID']] = $mxResult['ID']; 
            $arrProductId[$arItems['PRODUCT_ID']] = $arItems['PRODUCT_ID'];                    
        } else {
            $arrProductId[$arItems['PRODUCT_ID']] = $arItems['PRODUCT_ID'];     
            $arItems['ITEM_PRODUCT_ID'] = $arItems['PRODUCT_ID'];
        }
        //END
        
        $arBasketItemsItem[$arItems['ORDER_ID']][] = $arItems;
    }
    

    
    //������� ���������� �� �������
    //START
    
    $arrProductIdInfo = array();
    $arSelectProduct = Array(
        "ID", 
        "NAME",
        "IBLOCK_ID", 
        "PREVIEW_TEXT",
        'DETAIL_TEXT',
        'PREVIEW_PICTURE',
        'DETAIL_PICTURE',
        'LIST_PAGE_URL',
        'DETAIL_PAGE_URL'
    );
    $arFilterProduct = Array(
        "ID"=> $arrProductId
    );
    
    $arSortProduct = array();
    
    include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_BASKET_PRODUCT_FILLTER_BEFORE.php");
    $res = CIBlockElement::GetList($arSortProduct, $arFilterProduct, false, false, $arSelectProduct);
    while($ob = $res->GetNextElement())
    {      
        $arFields = $ob->GetFields();

        //������ �������
        if(strpos($arFields['LIST_PAGE_URL'],'?')) {
            $arFields['LIST_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['LIST_PAGE_URL'].'&MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];        
        } else {
            $arFields['LIST_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['LIST_PAGE_URL'].'?MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];          
        }  
                
        // ��������� ������
        if(strpos($arFields['DETAIL_PAGE_URL'],'?')) {
            $arFields['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['DETAIL_PAGE_URL'].'&MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];        
        } else {
            $arFields['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['DETAIL_PAGE_URL'].'?MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];          
        }         
        //������� ��������
        //START
        if($arFields['PREVIEW_PICTURE']){
            $arFields['PICTURE'] = $arFields['PREVIEW_PICTURE'];    
        } 
        elseif($arFields['DETAIL_PICTURE']) {
            $arFields['PICTURE'] = $arFields['DETAIL_PICTURE'];              
        }      
        //END
        $arFields['PICTURE_SRC'] = '';
        $arFields['PICTURE_WIDTH'] = '';   
        $arFields['PICTURE_HEIGHT'] = '';         
        if($arFields['PICTURE']) {
            $fileImg = CFile::ResizeImageGet($arFields['PICTURE'], array('width'=> $arParams['FORGET_BASKET_IMG_WIDTH'], 'height'=> $arParams['FORGET_BASKET_IMG_HEIGHT'] ), BX_RESIZE_IMAGE_PROPORTIONAL, true);  
            $arFields['PICTURE_SRC'] = $arParams['MAILING_SITE_URL'].$fileImg["src"];   
            $arFields['PICTURE_WIDTH'] = $fileImg["width"];  
            $arFields['PICTURE_HEIGHT'] = $fileImg["height"];                              
        } 
        
        //������� ��������
        $arFields['PROPERTIES'] = $ob->GetProperties();
        foreach($arFields['PROPERTIES'] as $kprop => $vprop) {
            $display_value = CIBlockFormatProperties::GetDisplayValue($arFields, $vprop, "news_out");   
            
            if(is_array($display_value['DISPLAY_VALUE'])){
                $display = '';
                foreach($display_value['DISPLAY_VALUE'] as $v) {
                    $display .= $v.' ';    
                }    
                $display_value['DISPLAY_VALUE'] = $display;    
            }  
            $arFields["PROP_".$kprop] = $display_value['DISPLAY_VALUE'];      
        }        
        unset($arFields['PROPERTIES']);
        
            //��� �������
            //START 
            if($arrProductIdOffer[$arFields['ID']]){
                foreach($arFields as $k => $v){
                    $arFields['OFFER_'.$k] = $v; 
                    unset($arFields[$k]);   
                }   
                $arFields['ID'] = $arrProductIdOffer[$arFields['OFFER_ID']]; 

            }             
            if($arrProductIdInfo[$arFields['ID']]){
                $arFields = array_merge($arrProductIdInfo[$arFields['ID']], $arFields);        
            }
            //END        
        
        
        $phpIncludeFunction = array();
        include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_BASKET_PRODUCT_FILLTER_WHILE_AFTER.php");

        if($phpIncludeFunction['isContinue']){
            continue;    
        }  
        if($phpIncludeFunction['isBreak']){
            break;    
        }          
        
        
        if($arFields) {
            $arrProductIdInfo[$arFields['ID']] = $arFields;             
        }
          


    }    
    //END


    foreach($arBasketItemsItem as $korder=>$vbasitems){
        $EmailSend = array();
        
        //�������� ������� ��� �������
        //START
        $FORGET_BASKET = $arParams['TEMP_FORGET_BASKET_TOP'];
        $i = 0;     
        
        $arrEmailSend[$korder]['BASKET_PRICE_ALL'] = 0;
        $arrEmailSend[$korder]['BASKET_COUNT'] = count($vbasitems);
        foreach($vbasitems as $item) {
            
            $arrEmailSend[$korder]['BASKET_PRICE_ALL'] = $arrEmailSend[$korder]['BASKET_PRICE_ALL'] + $item['PRICE']*$item['QUANTITY'];
            $CURRENCY = $item['CURRENCY'];
            $BasketItem = array();
            //���������� ������ � �������
            if(strpos($item["DETAIL_PAGE_URL"],'?')) {
                $item['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$item["DETAIL_PAGE_URL"].'&MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];        
            } else {
                $item['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$item["DETAIL_PAGE_URL"].'?MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];          
            }             
            $item['PRICE_FORMAT'] = SaleFormatCurrency($item['PRICE'], $item['CURRENCY']);
            //���������� ������
            $item['BORDER_TABLE_STYLE'] = '';
            if($i > 0){
               $item['BORDER_TABLE_STYLE'] = ' border-top: 1px solid #E6EAEC; ';  
            }   
            $item['QUANTITY'] = floor($item['QUANTITY']); 
            
                                    
            foreach($item as $k=>$v) {
                $BasketItem['BASKET_'.$k] = $v;      
            }

            foreach($arrProductIdInfo[$item['ITEM_PRODUCT_ID']] as $k=>$v) {
                $BasketItem['PRODUCT_'.$k] = $v;                     
            }
            // ������� ���������� � �������
            $FORGET_BASKET .= CSotbitMailingHelp::ReplaceVariables($arParams["TEMP_FORGET_BASKET_LIST"] , $BasketItem);                 
            $i++;         
        }
        $arrEmailSend[$korder]['BASKET_PRICE_ALL_FORMAT'] = SaleFormatCurrency($arrEmailSend[$korder]['BASKET_PRICE_ALL'], $CURRENCY);
        
        $FORGET_BASKET .= $arParams['TEMP_FORGET_BASKET_BOTTOM'];
        $arrEmailSend[$korder]['FORGET_BASKET'] = $FORGET_BASKET;
        //END            
    }
    

}                          
//END


//������������� ������
// START
if($arParams["RECOMMEND_SHOW"] == 'Y' && CModule::IncludeModule("iblock")) {
    
    
    //������� ���������� �� �������
    //START
    
    $arrRecommendProduct = array();
    $arSelectRecommend = Array(
        "ID", 
        "NAME",
        "IBLOCK_ID", 
        "PREVIEW_TEXT",
        'DETAIL_TEXT',
        'PREVIEW_PICTURE',
        'DETAIL_PICTURE',
        'LIST_PAGE_URL',
        'DETAIL_PAGE_URL',
    );
    $arFilterRecommend = Array(
        "ACTIVE" => 'Y' 
    );
    if($arParams["IBLOCK_ID_RECOMMEND"]) {
        $arFilterRecommend['IBLOCK_ID'] = $arParams["IBLOCK_ID_RECOMMEND"];    
    }
    if($arParams['PROPERTY_FILLTER_1_RECOMMEND'] &&  $arParams['PROPERTY_FILLTER_1_VALUE_RECOMMEND']) {
         $arFilterRecommend["PROPERTY_".$arParams["PROPERTY_FILLTER_1_RECOMMEND"]]  = $arParams["PROPERTY_FILLTER_1_VALUE_RECOMMEND"];   
    }
    
    if($arParams['TOP_COUNT_FILLTER_RECOMMEND']){
        $arNavStartParams = array('nTopCount'=>$arParams['TOP_COUNT_FILLTER_RECOMMEND']);    
    }  else {
        $arNavStartParams = false;              
    }
    
    $arSortRecpmmend = Array(
        $arParams["SORT_BY_RECOMMEND"] => $arParams["SORT_ORDER_RECOMMEND"]
    );   
    
    
    
    //������� ����
    //START
    if(empty($arParams['PRICE_TYPE_RECOMMEND']) && CModule::IncludeModule("catalog")){
        $dbPrice = CCatalogGroup::GetList(array(),array('BASE'=>'Y'));
        while($arPrice = $dbPrice->Fetch()) {
            $arParams['PRICE_TYPE_RECOMMEND'] = array($arPrice['NAME']);        
        }      
    }
    elseif(!is_array($arParams['PRICE_TYPE_RECOMMEND'])){
        $arParams['PRICE_TYPE_RECOMMEND'] = array($arParams['PRICE_TYPE_RECOMMEND']);        
    }   
    $arParams['RECOMMEND_PRICES'] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID_RECOMMEND"], $arParams['PRICE_TYPE_RECOMMEND']);
    
    foreach($arParams['RECOMMEND_PRICES'] as $value)
    {
        $arSelectRecommend[] = $value["SELECT"];
    }    
    
    //END
        
    include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_RECOMMEND_FILLTER_BEFORE.php");        
                 
    $res = CIBlockElement::GetList($arSortRecpmmend, $arFilterRecommend, false, $arNavStartParams, $arSelectRecommend);
    while($ob = $res->GetNextElement())
    {      
        $arFields = $ob->GetFields();

 
        //������ �������
        if(strpos($arFields['LIST_PAGE_URL'],'?')) {
            $arFields['LIST_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['LIST_PAGE_URL'].'&MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];        
        } else {
            $arFields['LIST_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['LIST_PAGE_URL'].'?MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];          
        }  
                
        // ��������� ������
        if(strpos($arFields['DETAIL_PAGE_URL'],'?')) {
            $arFields['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['DETAIL_PAGE_URL'].'&MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];        
        } else {
            $arFields['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['DETAIL_PAGE_URL'].'?MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];          
        }       
        //������� ��������
        //START
        if($arFields['PREVIEW_PICTURE']){
            $arFields['PICTURE'] = $arFields['PREVIEW_PICTURE'];    
        } 
        elseif($arFields['DETAIL_PICTURE']) {
            $arFields['PICTURE'] = $arFields['DETAIL_PICTURE'];              
        }      
        //END
        $arFields['PICTURE_SRC'] = '';
        $arFields['PICTURE_WIDTH'] = '';   
        $arFields['PICTURE_HEIGHT'] = '';         
        if($arFields['PICTURE']) {
            $fileImg = CFile::ResizeImageGet($arFields['PICTURE'], array('width'=> $arParams['IMG_WIDTH_RECOMMEND'], 'height'=> $arParams['IMG_HEIGHT_RECOMMEND'] ), BX_RESIZE_IMAGE_PROPORTIONAL, true);  
            $arFields['PICTURE_SRC'] = $arParams['MAILING_SITE_URL'].$fileImg["src"];   
            $arFields['PICTURE_WIDTH'] = $fileImg["width"];  
            $arFields['PICTURE_HEIGHT'] = $fileImg["height"];                              
        } 
    
        //������� ��������
        $arFields['PROPERTIES'] = $ob->GetProperties();
        foreach($arFields['PROPERTIES'] as $kprop => $vprop) {
            $display_value = CIBlockFormatProperties::GetDisplayValue($arFields, $vprop, "news_out");   
            
            if(is_array($display_value['DISPLAY_VALUE'])){
                $display = '';
                foreach($display_value['DISPLAY_VALUE'] as $v) {
                    $display .= $v.' ';    
                }    
                $display_value['DISPLAY_VALUE'] = $display;    
            }  
            $arFields["PROP_".$kprop] = $display_value['DISPLAY_VALUE'];      
        }        
        unset($arFields['PROPERTIES']);  
        
        
        
        //������� ���� ������
        //START
        if(CModule::IncludeModule("catalog") && CModule::IncludeModule("sale")) {
            $priceProduct = array();
            $ID_IBLOCK_PRODUCT = $arFields['IBLOCK_ID'];
            $ID_PRODUCT = $arFields['ID'];

            //���� ���� ������ ������� ����
            if(CCatalogSKU::IsExistOffers($ID_PRODUCT)){ 
                if(empty($arInfo)) {
                    $arInfo = CCatalogSKU::GetInfoByProductIBlock($ID_IBLOCK_PRODUCT);
                }
                //FILLTER
                $arOffersRecommendFillter = array(
                    'IBLOCK_ID' => $arInfo['IBLOCK_ID'], 
                    'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $ID_PRODUCT                
                );  
                //SELECT              
                $arOffersRecommendSelect = array(
                    'ID',
                    'IBLOCK_ID'  
                );
                foreach($arParams['RECOMMEND_PRICES'] as $value)
                {
                    $arOffersRecommendSelect[] = $value["SELECT"];
                } 
                              
                $rsOffers = CIBlockElement::GetList(array('SORT'=>'ASC'), $arOffersRecommendFillter,false,false,$arOffersRecommendSelect); 
                while ($arOffer = $rsOffers->GetNext()) 
                { 
                    $arrSiteId = CSotbitMailingHelp::GetSiteId();
                    $priceProduct = CIBlockPriceTools::GetItemPrices($ID_IBLOCK_PRODUCT, $arParams['RECOMMEND_PRICES'], $arOffer, true, array(), 0, $arrSiteId);
                    if($priceProduct) { 
                        break;
                    }
                } 
                       
            } else {
                $arrSiteId = CSotbitMailingHelp::GetSiteId();
                $priceProduct = CIBlockPriceTools::GetItemPrices($ID_IBLOCK_PRODUCT, $arParams['RECOMMEND_PRICES'], $arFields, true, array(), 0, $arrSiteId); 
            }
            
             
            //
            foreach($arParams['RECOMMEND_PRICES'] as $k=>$v) {
                //������������ ����
                $arFields['PRICE_OPTIMAL'] = floor($priceProduct[$k]['DISCOUNT_VALUE']);
                $arFields['PRICE_OPTIMAL_CURRENCY'] = $priceProduct[$k]['PRINT_DISCOUNT_VALUE'];  
                //���� �� �������
                $arFields['PRINT_NO_DISCOUNT_PRICE'] = ''; 
                $arFields['NO_DISCOUNT_PRICE'] = '';  
                $arFields['PRINT_DISCOUNT_DIFF'] = ''; 
                $arFields['DISCOUNT_DIFF'] = ''; 
                                   
                if($priceProduct[$k]['VALUE'] > $priceProduct[$k]['DISCOUNT_VALUE']) {
                    $arFields['PRINT_NO_DISCOUNT_PRICE'] = $priceProduct[$k]['PRINT_VALUE']; 
                    $arFields['NO_DISCOUNT_PRICE'] = $priceProduct[$k]['VALUE'];  
                  
                    $arFields['PRINT_DISCOUNT_DIFF'] = $priceProduct[$k]['PRINT_VALUE']; 
                    $arFields['DISCOUNT_DIFF'] = $priceProduct[$k]['VALUE'];                                            
                }
                // ���� ��� ������
                $arFields['PRINT_PRICE'] = $priceProduct[$k]['PRINT_DISCOUNT_VALUE'];  
                $arFields['PRICE'] = $priceProduct[$k]['DISCOUNT_VALUE'];           
            } 
    
        }  
        //END          
        
        $phpIncludeFunction = array();
        include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_RECOMMEND_FILLTER_WHILE_AFTER.php");  
        
        if($phpIncludeFunction['isContinue']){
            continue;    
        }  
        if($phpIncludeFunction['isBreak']){
            break;    
        }          
                    
        if($arFields){
            $arrRecommendProduct[] = $arFields;             
        }           
        
        
        
    }  
    
    //END   
    // �������� ��������������� ������ 
    if(count($arrRecommendProduct) > 0) {
        $i = 0;  
        $RECOMMEND_PRODUCT_ID_ARRAY = array();
        $RECOMMEND_PRODUCT = $arParams['TEMP_TOP_RECOMMEND'];         
        foreach($arrRecommendProduct as $ItemRecommendProduct) {
            $ItemRecommendProduct['BORDER_TABLE_STYLE'] = '';
            $RECOMMEND_PRODUCT_ID_ARRAY[] = $ItemRecommendProduct['ID'];
            if($i > 0){
                $ItemRecommendProduct['BORDER_TABLE_STYLE'] = ' border-top: 1px solid #E6EAEC; ';  
            }             
            $RECOMMEND_PRODUCT .= CSotbitMailingHelp::ReplaceVariables($arParams["TEMP_LIST_RECOMMEND"] , $ItemRecommendProduct);   
            $i++;           
        }
        $RECOMMEND_PRODUCT .= $arParams['TEMP_BOTTOM_RECOMMEND'];        
    }  
    

    
    
}     
// END


// ������������� ������ ��� �������������
// START
use \Bitrix\Catalog\CatalogViewedProductTable as CatalogViewedProductTable;
if($arParams["VIEWED_SHOW"] == 'Y' && CModule::IncludeModule("iblock") && CModule::IncludeModule("catalog")) {  
    //������� FUSER_ID ������������� ��������
    //START 
    $arr_user_id_views = $ARR_USER_ID_SEND;
    
    $arr_result_fuser_id = array();
    $arr_result_user_id = array();
    if(CModule::IncludeModule('sale')) {
        foreach($arr_user_id_views as $item_user_id_views) {
            $res_FUSER_ID = CSaleUser::GetList(array('USER_ID' => $item_user_id_views)); 
            if($res_FUSER_ID['USER_ID'] == $item_user_id_views){
                $arr_result_fuser_id[$item_user_id_views] = $res_FUSER_ID['ID']; 
                $arr_result_user_id[$res_FUSER_ID['ID']] = $item_user_id_views;           
            }       
        }        
    }

    //END
    
    //������� id ������������� �������
    //START
    if($arr_result_fuser_id) {
        $arr_user_product_id = array();
        $arr_product_id = array();
        
        //fillter
        $viewedFillter = array(
            "FUSER_ID" => $arr_result_fuser_id    
        );
        //sort
        $viewedOrder = array(
            $arParams['SORT_BY_VIEWED'] => $arParams['SORT_ORDER_VIEWED']    
        );
        
        $viewedIterator = CatalogViewedProductTable::GetList(array(
            "filter" => $viewedFillter,
            'order' => $viewedOrder,
            'limit' => 1000000000
        ));
        while($viewedProduct = $viewedIterator->fetch())
        {
            if(count($arr_user_product_id[$arr_result_user_id[$viewedProduct['FUSER_ID']]]) < $arParams['TOP_COUNT_FILLTER_VIEWED']){
                $arr_user_product_id[$arr_result_user_id[$viewedProduct['FUSER_ID']]][] = $viewedProduct['PRODUCT_ID'];
                $arr_product_id[] = $viewedProduct['PRODUCT_ID'];            
            }
        }        
    }
    //END
    
    // ������� ������ 
    // START
    if($arr_product_id) {

        $arrViewedProduct = array();
        $arSelectViewed = Array(
            "ID", 
            "NAME",
            "IBLOCK_ID", 
            "PREVIEW_TEXT",
            'DETAIL_TEXT',
            'PREVIEW_PICTURE',
            'DETAIL_PICTURE',
            'LIST_PAGE_URL',
            'DETAIL_PAGE_URL',
        );
        $arFilterViewed = Array(
            "ACTIVE" => 'Y' 
        );
        if($arParams["IBLOCK_ID_VIEWED"]) {
            $arFilterViewed['IBLOCK_ID'] = $arParams["IBLOCK_ID_VIEWED"];    
        }
        if($arr_product_id) {
            $arFilterViewed['ID'] = $arr_product_id;    
        }        
        if($arParams['PROPERTY_FILLTER_1_VIEWED'] &&  $arParams['PROPERTY_FILLTER_1_VALUE_VIEWED']) {
             $arFilterViewed["PROPERTY_".$arParams["PROPERTY_FILLTER_1_VIEWED"]]  = $arParams["PROPERTY_FILLTER_1_VALUE_VIEWED"];   
        }
        


        $arNavStartParamsViewed = false;              
        $arSortViewed = Array();   
        
        
        
        //������� ����
        //START
        if(empty($arParams['PRICE_TYPE_VIEWED']) && CModule::IncludeModule("catalog")){
            $dbPrice = CCatalogGroup::GetList(array(),array('BASE'=>'Y'));
            while($arPrice = $dbPrice->Fetch()) {
                $arParams['PRICE_TYPE_VIEWED'] = array($arPrice['NAME']);        
            }      
        }
        elseif(!is_array($arParams['PRICE_TYPE_VIEWED'])){
            $arParams['PRICE_TYPE_VIEWED'] = array($arParams['PRICE_TYPE_VIEWED']);        
        }   
        $arParams['VIEWED_PRICES'] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID_VIEWED"], $arParams['PRICE_TYPE_VIEWED']);
        
        foreach($arParams['VIEWED_PRICES'] as $value)
        {
            $arSelectViewed[] = $value["SELECT"];
        }    
        
        //END
          
        include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_VIEWED_FILLTER_BEFORE.php");        
                     
        $res = CIBlockElement::GetList($arSortViewed, $arFilterViewed, false, $arNavStartParamsViewed, $arSelectViewed);
        while($ob = $res->GetNextElement())
        {      
            $arFields = $ob->GetFields();

     
            //������ �������
            if(strpos($arFields['LIST_PAGE_URL'],'?')) {
                $arFields['LIST_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['LIST_PAGE_URL'].'&MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];        
            } else {
                $arFields['LIST_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['LIST_PAGE_URL'].'?MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];          
            }  
                    
            // ��������� ������
            if(strpos($arFields['DETAIL_PAGE_URL'],'?')) {
                $arFields['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['DETAIL_PAGE_URL'].'&MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];        
            } else {
                $arFields['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['DETAIL_PAGE_URL'].'?MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];          
            }       
            //������� ��������
            //START
            if($arFields['PREVIEW_PICTURE']){
                $arFields['PICTURE'] = $arFields['PREVIEW_PICTURE'];    
            } 
            elseif($arFields['DETAIL_PICTURE']) {
                $arFields['PICTURE'] = $arFields['DETAIL_PICTURE'];              
            }      
            //END
            $arFields['PICTURE_SRC'] = '';
            $arFields['PICTURE_WIDTH'] = '';   
            $arFields['PICTURE_HEIGHT'] = '';         
            if($arFields['PICTURE']) {
                $fileImg = CFile::ResizeImageGet($arFields['PICTURE'], array('width'=> $arParams['IMG_WIDTH_VIEWED'], 'height'=> $arParams['IMG_HEIGHT_VIEWED'] ), BX_RESIZE_IMAGE_PROPORTIONAL, true);  
                $arFields['PICTURE_SRC'] = $arParams['MAILING_SITE_URL'].$fileImg["src"];   
                $arFields['PICTURE_WIDTH'] = $fileImg["width"];  
                $arFields['PICTURE_HEIGHT'] = $fileImg["height"];                              
            } 
        
            //������� ��������
            $arFields['PROPERTIES'] = $ob->GetProperties();
            foreach($arFields['PROPERTIES'] as $kprop => $vprop) {
                $display_value = CIBlockFormatProperties::GetDisplayValue($arFields, $vprop, "news_out");   
                
                if(is_array($display_value['DISPLAY_VALUE'])){
                    $display = '';
                    foreach($display_value['DISPLAY_VALUE'] as $v) {
                        $display .= $v.' ';    
                    }    
                    $display_value['DISPLAY_VALUE'] = $display;    
                }  
                $arFields["PROP_".$kprop] = $display_value['DISPLAY_VALUE'];      
            }        
            unset($arFields['PROPERTIES']);  
            
            
            
            //������� ���� ������
            //START
            if(CModule::IncludeModule("catalog") && CModule::IncludeModule("sale")) {
                $priceProduct = array();
                $ID_IBLOCK_PRODUCT = $arFields['IBLOCK_ID'];
                $ID_PRODUCT = $arFields['ID'];

                //���� ���� ������ ������� ����
                if(CCatalogSKU::IsExistOffers($ID_PRODUCT)){ 
                    if(empty($arInfo)) {
                        $arInfo = CCatalogSKU::GetInfoByProductIBlock($ID_IBLOCK_PRODUCT);
                    }
                    //FILLTER
                    $arOffersViewedFillter = array(
                        'IBLOCK_ID' => $arInfo['IBLOCK_ID'], 
                        'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $ID_PRODUCT                
                    );  
                    //SELECT              
                    $arOffersViewedSelect = array(
                        'ID',
                        'IBLOCK_ID'  
                    );
                    foreach($arParams['VIEWED_PRICES'] as $value)
                    {
                        $arOffersViewedSelect[] = $value["SELECT"];
                    }      
                          
                
                    $rsOffers = CIBlockElement::GetList(array('SORT'=>'ASC'), $arOffersViewedFillter,false,false,$arOffersViewedSelect); 
                    while ($arOffer = $rsOffers->GetNext()) 
                    { 
                        $arrSiteId = CSotbitMailingHelp::GetSiteId();
                        $priceProduct = CIBlockPriceTools::GetItemPrices($ID_IBLOCK_PRODUCT, $arParams['VIEWED_PRICES'], $arOffer, true, array(), 0, $arrSiteId);
                        if($priceProduct) { 
                            break;
                        }
                    } 
                           
                } else {
                    $arrSiteId = CSotbitMailingHelp::GetSiteId();
                    $priceProduct = CIBlockPriceTools::GetItemPrices($ID_IBLOCK_PRODUCT, $arParams['VIEWED_PRICES'], $arFields, true, array(), 0, $arrSiteId); 
                }
                
            
                foreach($arParams['VIEWED_PRICES'] as $k=>$v) {
                    //������������ ����
                    $arFields['PRICE_OPTIMAL'] = floor($priceProduct[$k]['DISCOUNT_VALUE']);
                    $arFields['PRICE_OPTIMAL_CURRENCY'] = $priceProduct[$k]['PRINT_DISCOUNT_VALUE'];  
                    //���� �� �������
                    $arFields['PRINT_NO_DISCOUNT_PRICE'] = ''; 
                    $arFields['NO_DISCOUNT_PRICE'] = '';  
                    $arFields['PRINT_DISCOUNT_DIFF'] = ''; 
                    $arFields['DISCOUNT_DIFF'] = ''; 
                                       
                    if($priceProduct[$k]['VALUE'] > $priceProduct[$k]['DISCOUNT_VALUE']) {
                        $arFields['PRINT_NO_DISCOUNT_PRICE'] = $priceProduct[$k]['PRINT_VALUE']; 
                        $arFields['NO_DISCOUNT_PRICE'] = $priceProduct[$k]['VALUE'];  
                      
                        $arFields['PRINT_DISCOUNT_DIFF'] = $priceProduct[$k]['PRINT_VALUE']; 
                        $arFields['DISCOUNT_DIFF'] = $priceProduct[$k]['VALUE'];                                            
                    }
                    // ���� ��� ������
                    $arFields['PRINT_PRICE'] = $priceProduct[$k]['PRINT_DISCOUNT_VALUE'];  
                    $arFields['PRICE'] = $priceProduct[$k]['DISCOUNT_VALUE'];           
                } 
        
            }  
            //END          
            
            
            $phpIncludeFunction = array();
            include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_VIEWED_FILLTER_WHILE_AFTER.php");  
            
            if($phpIncludeFunction['isContinue']){
                continue;    
            }  
            if($phpIncludeFunction['isBreak']){
                break;    
            }          
                        
            if($arFields){
                $arrViewedProduct[$arFields['ID']] = $arFields;           
            }             
            
            
            
        } 
        
    }     
    // END
    
    //�������� ������ ��� �������������
    //START
    if(count($arrViewedProduct) > 0) {
        $VIEWED_PRODUCT_USER = array();
        foreach($arr_user_product_id as $kuser => $vproduct) {
            $VIEWED_ITEM_CONTENT = '';
            $i = 0;  
            foreach($vproduct as $viewed_product_id) {
                if($arrViewedProduct[$viewed_product_id]){
                    $arrViewedProduct[$viewed_product_id]['BORDER_TABLE_STYLE'] = '';
                    if($i > 0){
                        $arrViewedProduct[$viewed_product_id]['BORDER_TABLE_STYLE'] = ' border-top: 1px solid #E6EAEC; ';  
                    }                     
                    $VIEWED_ITEM_CONTENT .= CSotbitMailingHelp::ReplaceVariables($arParams["TEMP_LIST_VIEWED"] , $arrViewedProduct[$viewed_product_id]);   
                    $i++;                          
                }        
            } 
            if($i>0) {
                $VIEWED_PRODUCT_USER[$kuser] = $arParams['TEMP_TOP_VIEWED'].$VIEWED_ITEM_CONTENT.$arParams['TEMP_BOTTOM_VIEWED'];                    
            }
                       
               
        }
                
    }     
    //END
}  
// END


//������� ������� �����, ������������ email, ������ �������� ��������  - ������� ����� ������  �������
//START
$arrEmailSend = CSotbitMailingHelp::MessageCheck(array('arParams'=>$arParams,'arrEmailSend'=>$arrEmailSend));   
//END


// ������� ����������, �������� ������
// START
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_MESSAGE_FOREACH_BEFORE.php"); 
$i=0;
foreach($arrEmailSend as $k => $ItemEmailSend) {   
    $EmailSend = array();
    $ItemEmailSend['RECOMMEND_PRODUCT'] = $RECOMMEND_PRODUCT ;
    //���������� ������
    //START
    if($arParams['COUPON_ADD'] == 'Y' && CModule::IncludeModule("catalog")) {
                
        //�������� �����
        //START
        $COUPON = CatalogGenerateCoupon();
        $arCouponFields = array(
            "DISCOUNT_ID" => $arParams['COUPON_DISCOUNT_ID'],
            "ACTIVE" => "Y",
            "ONE_TIME" => $arParams['COUPON_ONE_TIME'],
            "COUPON" => $COUPON,
            "DATE_APPLY" => false
        ); 
        if(CCatalogDiscountCoupon::Add($arCouponFields)) {
            $ItemEmailSend['COUPON'] = $COUPON;   
            $ItemEmailSend['PARAM_2'] = $COUPON;
            $ItemEmailSend['PARAM_MESSEGE']['COUPON'] = $COUPON;              
        }           
        //END  
                                                     
    }
    //END      
    //������������� ������
    $ItemEmailSend['VIEWED_PRODUCT'] = ''; 
    if($ItemEmailSend['USER_ID']){
        $ItemEmailSend['VIEWED_PRODUCT'] = $VIEWED_PRODUCT_USER[$ItemEmailSend['USER_ID']];         
    }
    
        
    //������� ��������� php ���
    //START
    $phpIncludeFunction = array();
    include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_MESSAGE_FOREACH_ITEM_BEFORE.php");   
            
    if($phpIncludeFunction['isContinue']){
        continue;    
    }  
    if($phpIncludeFunction['isBreak']){
        break;    
    } 
    //END   
        
    
    //��������� ������
    $EmailSend['SUBJECT'] = CSotbitMailingHelp::ReplaceVariables($arParams["SUBJECT"] , $ItemEmailSend);
    // ����
    $EmailSend['EMAIL_TO'] = CSotbitMailingHelp::ReplaceVariables($arParams["EMAIL_TO"] , $ItemEmailSend);   
    //�� ����
    $EmailSend['EMAIL_FROM'] = CSotbitMailingHelp::ReplaceVariables($arParams["EMAIL_FROM"] , $ItemEmailSend);   
    if(empty($EmailSend['EMAIL_FROM'])) {
        $EmailSend['EMAIL_FROM'] = COption::GetOptionString('sotbit.mailing', 'EMAIL_FROM');
    }    
    elseif(empty($EmailSend['EMAIL_FROM'])) {
        $EmailSend['EMAIL_FROM'] = COption::GetOptionString('main', 'email_from') ;
    }
    $EmailSend['BCC'] = CSotbitMailingHelp::ReplaceVariables($arParams["BCC"] , $EmailSendItem);  
    // ����� ���������
    //$EmailSend['MESSEGE'] = CSotbitMailingHelp::ReplaceVariables($arParams["MESSAGE"] , $ItemEmailSend);   
    $EmailSend['MESSEGE_PARAMETR'] = array();
    $needVariables = CSotbitMailingHelp::GetNeedVariablesTemplate($arParams["MESSAGE"]);
    foreach($ItemEmailSend as $k=>$v){
        if($needVariables[$k]){
            $EmailSend['MESSEGE_PARAMETR'][$k] = $v;        
        }        
    }     
    

    
    // �������� ���������
    $addMessageFields = array(
        'MAILING_ID' => $arParams['MAILING_EVENT_ID'],
        'MAILING_COUNT_RUN' => $arParams['MAILING_COUNT_RUN'], 
    );
    $EmailSend['PARAM_1'] =  $ItemEmailSend['PARAM_1'];
    $EmailSend['PARAM_2'] =  $ItemEmailSend['PARAM_2'];
    $EmailSend['PARAM_3'] =  $ItemEmailSend['PARAM_3'];  
    //����� ������ ��� ����������
    //START
    $EmailSend['PARAM_MESSEGE'] =  $ItemEmailSend['PARAM_MESSEGE'];
    if($ItemEmailSend['USER_ID']){
        $EmailSend['PARAM_MESSEGE']['USER_ID'] = $ItemEmailSend['USER_ID'];    
    }
    if($RECOMMEND_PRODUCT_ID_ARRAY){
        $EmailSend['PARAM_MESSEGE']['RECOMMEND_PRODUCT_ID'] = $RECOMMEND_PRODUCT_ID_ARRAY;    
    }   
    if($arr_user_product_id[$ItemEmailSend['USER_ID']]){
        $EmailSend['PARAM_MESSEGE']['VIEWED_PRODUCT_ID'] = $arr_user_product_id[$ItemEmailSend['USER_ID']];    
    }     
    //END   
    $AddAnswerMessage = CSotbitMailingTools::AddMailingMessage($addMessageFields, $EmailSend);  
       

    $i++; 
    //�������� �������� ���������        
    //START
    $arParams['MAILING_MAILING_WORK_COUNT'] = $arParams['MAILING_MAILING_WORK_COUNT']+1;
    if($i>=$arParams['MAILING_PACKAGE_COUNT']){
        CSotbitMailingEvent::Update($arParams['MAILING_EVENT_ID'], array(
            "MAILING_WORK_COUNT" => $arParams['MAILING_MAILING_WORK_COUNT']
        ));
        $arrProgress = CSotbitMailingHelp::ProgressFileGetArray($arParams['MAILING_EVENT_ID'], $arParams['MAILING_COUNT_RUN']);
        $arrProgress['MAILING_WORK'] = 'N';
        CSotbitMailingHelp::ProgressFile($arParams['MAILING_EVENT_ID'], $arParams['MAILING_COUNT_RUN'], $arrProgress);          
        die;    
    }    
    //END 
           
                      
} 
 



// END 

   
       

?>
