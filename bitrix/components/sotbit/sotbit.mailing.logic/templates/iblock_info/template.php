<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $DB;  


// ������������ ������ ��������� 
if(!CModule::IncludeModule("iblock"))
{
    return;
}



//�������� �� ������ �� ���������
//START
    //������� ������ �� ��������� 
    
    $arrRecommendProduct = array();
    
    $arSortIblock = array();
    $arSelectIblock = Array(
        "ID", 
        "NAME",
        "IBLOCK_ID", 
        "PREVIEW_TEXT",
        'DETAIL_TEXT',
        'PREVIEW_PICTURE',
        'DETAIL_PICTURE',
        'LIST_PAGE_URL',
        'DETAIL_PAGE_URL'
    );
    
    $arFilterIblock = Array(
        "IBLOCK_ID"=> $arParams["IBLOCK_ID_INFO"], 
        "ACTIVE" => 'Y'    
    );
    if($arParams['IBLOCK_INFO_PROPERTY_FILLTER_LIST'] &&  $arParams['IBLOCK_INFO_PROPERTY_FILLTER_LIST_VALUE']) {
         $arFilterIblock["PROPERTY_".$arParams["IBLOCK_INFO_PROPERTY_FILLTER_LIST"]]  = $arParams["IBLOCK_INFO_PROPERTY_FILLTER_LIST_VALUE"];   
    }
    
    if($arParams['IBLOCK_INFO_PROPERTY_FILLTER_STRING'] &&  $arParams['IBLOCK_INFO_PROPERTY_FILLTER_STRING_VALUE']) {
         $arFilterIblock["PROPERTY_".$arParams["IBLOCK_INFO_PROPERTY_FILLTER_STRING"]] = $arParams["IBLOCK_INFO_PROPERTY_FILLTER_STRING_VALUE"];   
    }
    
    
    include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_FILLTER_IBLOCK_PARAM_BEFORE.php");
    $res = CIBlockElement::GetList($arSortIblock, $arFilterIblock, false, false, $arSelectIblock);
    while($ob = $res->GetNextElement())
    {
        $EmailSend = array();


        $arFields = $ob->GetFields();

        //������ �������
        if(strpos($arFields['LIST_PAGE_URL'],'?')) {
            $arFields['LIST_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['LIST_PAGE_URL'].'&MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];        
        } else {
            $arFields['LIST_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['LIST_PAGE_URL'].'?MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];          
        }  
                
        // ��������� ������
        if(strpos($arFields['DETAIL_PAGE_URL'],'?')) {
            $arFields['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['DETAIL_PAGE_URL'].'&MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];        
        } else {
            $arFields['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['DETAIL_PAGE_URL'].'?MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];          
        }         
        //������� ��������
        //START
        if($arFields['PREVIEW_PICTURE']){
            $arFields['PICTURE'] = $arFields['PREVIEW_PICTURE'];    
        } 
        elseif($arFields['DETAIL_PICTURE']) {
            $arFields['PICTURE'] = $arFields['DETAIL_PICTURE'];              
        }      
        //END
        $arFields['PICTURE_SRC'] = '';
        $arFields['PICTURE_WIDTH'] = '';   
        $arFields['PICTURE_HEIGHT'] = '';         
        if($arFields['PICTURE']) {
            $fileImg = CFile::ResizeImageGet($arFields['PICTURE'], array('width'=> $arParams['IMG_WIDTH_RECOMMEND'], 'height'=> $arParams['IMG_HEIGHT_RECOMMEND'] ), BX_RESIZE_IMAGE_PROPORTIONAL, true);  
            $arFields['PICTURE_SRC'] = $arParams['MAILING_SITE_URL'].$fileImg["src"];   
            $arFields['PICTURE_WIDTH'] = $fileImg["width"];  
            $arFields['PICTURE_HEIGHT'] = $fileImg["height"];                              
        } 
        
        //������� ��������
        $arFields['PROPERTIES'] = $ob->GetProperties();
        foreach($arFields['PROPERTIES'] as $kprop => $vprop) { 
            $display_value = CIBlockFormatProperties::GetDisplayValue($arFields, $vprop, "news_out");   
            if(is_array($display_value['DISPLAY_VALUE'])){
                $display = '';
                foreach($display_value['DISPLAY_VALUE'] as $v) {
                    $display .= $v.' ';    
                }    
                $display_value['DISPLAY_VALUE'] = $display;    
            }  
            $arFields["PROP_".$kprop] = $display_value['DISPLAY_VALUE'];   
            
            if($vprop['PROPERTY_TYPE']=='F') {
                $file = CFile::GetFileArray($vprop['VALUE']);  
                $arFields["PROP_".$kprop."_PATH"] = $arParams['MAILING_SITE_URL'].$file['SRC'];                       
            }
               
        }        
        unset($arFields['PROPERTIES']);

        
        $EmailSend = $arFields;
        $EmailSend['PARAM_1'] =  $arFields['ID'];
        $EmailSend['PARAM_MESSEGE']['ELEMENT_ID'] = $arFields['ID'];
        
        
 
        $phpIncludeFunction = array();
        include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_FILLTER_IBLOCK_PARAM_AFTER.php");  
                    
        if($phpIncludeFunction['isContinue']){
            continue;    
        }  
        if($phpIncludeFunction['isBreak']){
            break;    
        }         
                
        
        if($EmailSend) {
            $arrEmailSend[] = $EmailSend;             
        }

        
          
    }
                              
//END




//������������� ������
// START
if($arParams["RECOMMEND_SHOW"] == 'Y' && CModule::IncludeModule("iblock")) {
    
    
    //������� ���������� �� �������
    //START
    
    $arrRecommendProduct = array();
    $arSelectRecommend = Array(
        "ID", 
        "NAME",
        "IBLOCK_ID", 
        "PREVIEW_TEXT",
        'DETAIL_TEXT',
        'PREVIEW_PICTURE',
        'DETAIL_PICTURE',
        'LIST_PAGE_URL',
        'DETAIL_PAGE_URL',
    );
    $arFilterRecommend = Array(
        "ACTIVE" => 'Y' 
    );
    if($arParams["IBLOCK_ID_RECOMMEND"]) {
        $arFilterRecommend['IBLOCK_ID'] = $arParams["IBLOCK_ID_RECOMMEND"];    
    }
    if($arParams['PROPERTY_FILLTER_1_RECOMMEND'] &&  $arParams['PROPERTY_FILLTER_1_VALUE_RECOMMEND']) {
         $arFilterRecommend["PROPERTY_".$arParams["PROPERTY_FILLTER_1_RECOMMEND"]]  = $arParams["PROPERTY_FILLTER_1_VALUE_RECOMMEND"];   
    }
    
    if($arParams['TOP_COUNT_FILLTER_RECOMMEND']){
        $arNavStartParams = array('nTopCount'=>$arParams['TOP_COUNT_FILLTER_RECOMMEND']);    
    }  else {
        $arNavStartParams = false;              
    }
    
    $arSortRecpmmend = Array(
        $arParams["SORT_BY_RECOMMEND"] => $arParams["SORT_ORDER_RECOMMEND"]
    );   
    
    
    
    //������� ����
    //START
    if(empty($arParams['PRICE_TYPE_RECOMMEND']) && CModule::IncludeModule("catalog")){
        $dbPrice = CCatalogGroup::GetList(array(),array('BASE'=>'Y'));
        while($arPrice = $dbPrice->Fetch()) {
            $arParams['PRICE_TYPE_RECOMMEND'] = array($arPrice['NAME']);        
        }      
    }
    elseif(!is_array($arParams['PRICE_TYPE_RECOMMEND'])){
        $arParams['PRICE_TYPE_RECOMMEND'] = array($arParams['PRICE_TYPE_RECOMMEND']);        
    }   
    $arParams['RECOMMEND_PRICES'] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID_RECOMMEND"], $arParams['PRICE_TYPE_RECOMMEND']);
    
    foreach($arParams['RECOMMEND_PRICES'] as $value)
    {
        $arSelectRecommend[] = $value["SELECT"];
    }    
    
    //END
        
    include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_RECOMMEND_FILLTER_BEFORE.php");        
                 
    $res = CIBlockElement::GetList($arSortRecpmmend, $arFilterRecommend, false, $arNavStartParams, $arSelectRecommend);
    while($ob = $res->GetNextElement())
    {      
        $arFields = $ob->GetFields();

 
        //������ �������
        if(strpos($arFields['LIST_PAGE_URL'],'?')) {
            $arFields['LIST_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['LIST_PAGE_URL'].'&MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];        
        } else {
            $arFields['LIST_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['LIST_PAGE_URL'].'?MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];          
        }  
                
        // ��������� ������
        if(strpos($arFields['DETAIL_PAGE_URL'],'?')) {
            $arFields['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['DETAIL_PAGE_URL'].'&MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];        
        } else {
            $arFields['DETAIL_PAGE_URL'] = $arParams['MAILING_SITE_URL'].$arFields['DETAIL_PAGE_URL'].'?MAILING_MESSAGE=#MAILING_MESSAGE#&utm_source=newsletter&utm_medium=email&utm_campaign=sotbit_mailing_'.$arParams['MAILING_EVENT_ID'];          
        }       
        //������� ��������
        //START
        if($arFields['PREVIEW_PICTURE']){
            $arFields['PICTURE'] = $arFields['PREVIEW_PICTURE'];    
        } 
        elseif($arFields['DETAIL_PICTURE']) {
            $arFields['PICTURE'] = $arFields['DETAIL_PICTURE'];              
        }      
        //END
        $arFields['PICTURE_SRC'] = '';
        $arFields['PICTURE_WIDTH'] = '';   
        $arFields['PICTURE_HEIGHT'] = '';         
        if($arFields['PICTURE']) {
            $fileImg = CFile::ResizeImageGet($arFields['PICTURE'], array('width'=> $arParams['IMG_WIDTH_RECOMMEND'], 'height'=> $arParams['IMG_HEIGHT_RECOMMEND'] ), BX_RESIZE_IMAGE_PROPORTIONAL, true);  
            $arFields['PICTURE_SRC'] = $arParams['MAILING_SITE_URL'].$fileImg["src"];   
            $arFields['PICTURE_WIDTH'] = $fileImg["width"];  
            $arFields['PICTURE_HEIGHT'] = $fileImg["height"];                              
        } 
    
        //������� ��������
        $arFields['PROPERTIES'] = $ob->GetProperties();
        foreach($arFields['PROPERTIES'] as $kprop => $vprop) {
            $display_value = CIBlockFormatProperties::GetDisplayValue($arFields, $vprop, "news_out");   
            
            if(is_array($display_value['DISPLAY_VALUE'])){
                $display = '';
                foreach($display_value['DISPLAY_VALUE'] as $v) {
                    $display .= $v.' ';    
                }    
                $display_value['DISPLAY_VALUE'] = $display;    
            }  
            $arFields["PROP_".$kprop] = $display_value['DISPLAY_VALUE'];      
        }        
        unset($arFields['PROPERTIES']);  
        
        
        
        //������� ���� ������
        //START
        if(CModule::IncludeModule("catalog") && CModule::IncludeModule("sale")) {
            $priceProduct = array();
            $ID_IBLOCK_PRODUCT = $arFields['IBLOCK_ID'];
            $ID_PRODUCT = $arFields['ID'];

            //���� ���� ������ ������� ����
            if(CCatalogSKU::IsExistOffers($ID_PRODUCT)){ 
                if(empty($arInfo)) {
                    $arInfo = CCatalogSKU::GetInfoByProductIBlock($ID_IBLOCK_PRODUCT);
                }
                //FILLTER
                $arOffersRecommendFillter = array(
                    'IBLOCK_ID' => $arInfo['IBLOCK_ID'], 
                    'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $ID_PRODUCT                
                );  
                //SELECT              
                $arOffersRecommendSelect = array(
                    'ID',
                    'IBLOCK_ID'  
                );
                foreach($arParams['RECOMMEND_PRICES'] as $value)
                {
                    $arOffersRecommendSelect[] = $value["SELECT"];
                } 
                              
                $rsOffers = CIBlockElement::GetList(array('SORT'=>'ASC'), $arOffersRecommendFillter,false,false,$arOffersRecommendSelect); 
                while ($arOffer = $rsOffers->GetNext()) 
                { 
                    $arrSiteId = CSotbitMailingHelp::GetSiteId();
                    $priceProduct = CIBlockPriceTools::GetItemPrices($ID_IBLOCK_PRODUCT, $arParams['RECOMMEND_PRICES'], $arOffer, true, array(), 0, $arrSiteId);
                    if($priceProduct) { 
                        break;
                    }
                } 
                       
            } else {
                $arrSiteId = CSotbitMailingHelp::GetSiteId();
                $priceProduct = CIBlockPriceTools::GetItemPrices($ID_IBLOCK_PRODUCT, $arParams['RECOMMEND_PRICES'], $arFields, true, array(), 0, $arrSiteId); 
            }
            
             
            //
            foreach($arParams['RECOMMEND_PRICES'] as $k=>$v) {
                //������������ ����
                $arFields['PRICE_OPTIMAL'] = floor($priceProduct[$k]['DISCOUNT_VALUE']);
                $arFields['PRICE_OPTIMAL_CURRENCY'] = $priceProduct[$k]['PRINT_DISCOUNT_VALUE'];  
                //���� �� �������
                $arFields['PRINT_NO_DISCOUNT_PRICE'] = ''; 
                $arFields['NO_DISCOUNT_PRICE'] = '';  
                $arFields['PRINT_DISCOUNT_DIFF'] = ''; 
                $arFields['DISCOUNT_DIFF'] = ''; 
                                   
                if($priceProduct[$k]['VALUE'] > $priceProduct[$k]['DISCOUNT_VALUE']) {
                    $arFields['PRINT_NO_DISCOUNT_PRICE'] = $priceProduct[$k]['PRINT_VALUE']; 
                    $arFields['NO_DISCOUNT_PRICE'] = $priceProduct[$k]['VALUE'];  
                  
                    $arFields['PRINT_DISCOUNT_DIFF'] = $priceProduct[$k]['PRINT_VALUE']; 
                    $arFields['DISCOUNT_DIFF'] = $priceProduct[$k]['VALUE'];                                            
                }
                // ���� ��� ������
                $arFields['PRINT_PRICE'] = $priceProduct[$k]['PRINT_DISCOUNT_VALUE'];  
                $arFields['PRICE'] = $priceProduct[$k]['DISCOUNT_VALUE'];           
            } 
    
        }  
        //END          

        $phpIncludeFunction = array();
        include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_RECOMMEND_FILLTER_WHILE_AFTER.php");  
        
        if($phpIncludeFunction['isContinue']){
            continue;    
        }  
        if($phpIncludeFunction['isBreak']){
            break;    
        }          
                    
        if($arFields){
            $arrRecommendProduct[] = $arFields;             
        }           
        
        
        
    }  
    
    //END   
    // �������� ��������������� ������ 
    if(count($arrRecommendProduct) > 0) {
        $i = 0;  
        $RECOMMEND_PRODUCT_ID_ARRAY = array();
        $RECOMMEND_PRODUCT = $arParams['TEMP_TOP_RECOMMEND'];         
        foreach($arrRecommendProduct as $ItemRecommendProduct) {
            $ItemRecommendProduct['BORDER_TABLE_STYLE'] = '';
            $RECOMMEND_PRODUCT_ID_ARRAY[] = $ItemRecommendProduct['ID'];
            if($i > 0){
                $ItemRecommendProduct['BORDER_TABLE_STYLE'] = ' border-top: 1px solid #E6EAEC; ';  
            }             
            $RECOMMEND_PRODUCT .= CSotbitMailingHelp::ReplaceVariables($arParams["TEMP_LIST_RECOMMEND"] , $ItemRecommendProduct);   
            $i++;           
        }
        $RECOMMEND_PRODUCT .= $arParams['TEMP_BOTTOM_RECOMMEND'];        
    } 
    

    
    
}     
// END






// ���������� �� ��������
// START
    //������������ �����
    if($arParams['EMAIL_DUBLICATE'] == 'Y'){ 
        $emailSendCount = array();
        foreach($arrEmailSend as $k => $ItemEmailSend) { 
            $EmailSend = array();
            $EmailSend['EMAIL_TO'] = CSotbitMailingHelp::ReplaceVariables($arParams["EMAIL_TO"] , $ItemEmailSend);  
            if(empty($emailSendCount[$EmailSend['EMAIL_TO']])) {
                $emailSendCount[$EmailSend['EMAIL_TO']] = $k;           
            } else {
                unset($arrEmailSend[$k]);
            }           
        }            
    }
// END


//������� ������� �����, ������������ email, ������ �������� ��������  - ������� ����� ������  �������
//START
$arrEmailSend = CSotbitMailingHelp::MessageCheck(array('arParams'=>$arParams,'arrEmailSend'=>$arrEmailSend));      
//END


// ������� ����������, �������� ������
// START
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_MESSAGE_FOREACH_BEFORE.php"); 
$i=0;
foreach($arrEmailSend as $k => $ItemEmailSend) {  
    $EmailSend = array();
    $ItemEmailSend['RECOMMEND_PRODUCT'] = $RECOMMEND_PRODUCT;
    //���������� ������
    //START
    if($arParams['COUPON_ADD'] == 'Y' && CModule::IncludeModule("catalog")) {
                
        //�������� �����
        //START
        $COUPON = CatalogGenerateCoupon();
        $arCouponFields = array(
            "DISCOUNT_ID" => $arParams['COUPON_DISCOUNT_ID'],
            "ACTIVE" => "Y",
            "ONE_TIME" => $arParams['COUPON_ONE_TIME'],
            "COUPON" => $COUPON,
            "DATE_APPLY" => false
        ); 
        if(CCatalogDiscountCoupon::Add($arCouponFields)) {
            $ItemEmailSend['COUPON'] = $COUPON;   
            $ItemEmailSend['PARAM_2'] = $COUPON; 
            $ItemEmailSend['PARAM_MESSEGE']['COUPON'] = $COUPON; 
        }           
        //END  
                                                     
    }
    //END      
    
    //������� ��������� php ���
    //START
    $phpIncludeFunction = array();
    include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sotbit.mailing/php_files_mailing/".$arParams['MAILING_EVENT_ID']."/PHP_MESSAGE_FOREACH_ITEM_BEFORE.php");   
            
    if($phpIncludeFunction['isContinue']){
        continue;    
    }  
    if($phpIncludeFunction['isBreak']){
        break;    
    } 
    //END        
    
    //������� ����������
    //START
    //��������� ������
    $EmailSend['SUBJECT'] = CSotbitMailingHelp::ReplaceVariables($arParams["SUBJECT"] , $ItemEmailSend);
    // ����
    $EmailSend['EMAIL_TO'] = CSotbitMailingHelp::ReplaceVariables($arParams["EMAIL_TO"] , $ItemEmailSend);   
    //�� ����
    $EmailSend['EMAIL_FROM'] = CSotbitMailingHelp::ReplaceVariables($arParams["EMAIL_FROM"] , $ItemEmailSend);   
    if(empty($EmailSend['EMAIL_FROM'])) {
        $EmailSend['EMAIL_FROM'] = COption::GetOptionString('sotbit.mailing', 'EMAIL_FROM');
    }    
    elseif(empty($EmailSend['EMAIL_FROM'])) {
        $EmailSend['EMAIL_FROM'] = COption::GetOptionString('main', 'email_from') ;
    }
    $EmailSend['BCC'] = CSotbitMailingHelp::ReplaceVariables($arParams["BCC"] , $EmailSendItem);  
    // ����� ���������
    //$EmailSend['MESSEGE'] = CSotbitMailingHelp::ReplaceVariables($arParams["MESSAGE"] , $ItemEmailSend);   
    $EmailSend['MESSEGE_PARAMETR'] = array();
    $needVariables = CSotbitMailingHelp::GetNeedVariablesTemplate($arParams["MESSAGE"]);
    foreach($ItemEmailSend as $k=>$v){
        if($needVariables[$k]){
            $EmailSend['MESSEGE_PARAMETR'][$k] = $v;        
        }        
    }  
    //END   
  
    
    // �������� ���������
    $addMessageFields = array(
        'MAILING_ID' => $arParams['MAILING_EVENT_ID'],
        'MAILING_COUNT_RUN' => $arParams['MAILING_COUNT_RUN'],
    );
    $EmailSend['PARAM_1'] = $ItemEmailSend['PARAM_1'];
    $EmailSend['PARAM_2'] = $ItemEmailSend['PARAM_2'];
    $EmailSend['PARAM_3'] =  $ItemEmailSend['PARAM_3'];  
    //����� ������ ��� ����������
    //START
    $EmailSend['PARAM_MESSEGE'] =  $ItemEmailSend['PARAM_MESSEGE'];
    if($ItemEmailSend['USER_ID']){
        $EmailSend['PARAM_MESSEGE']['USER_ID'] = $ItemEmailSend['USER_ID'];    
    }
    if($RECOMMEND_PRODUCT_ID_ARRAY){
        $EmailSend['PARAM_MESSEGE']['RECOMMEND_PRODUCT_ID'] = $RECOMMEND_PRODUCT_ID_ARRAY;    
    }   
    if($arr_user_product_id[$ItemEmailSend['USER_ID']]){
        $EmailSend['PARAM_MESSEGE']['VIEWED_PRODUCT_ID'] = $arr_user_product_id[$ItemEmailSend['USER_ID']];    
    }     
    //END 
    
  
    $AddAnswerMessage = CSotbitMailingTools::AddMailingMessage($addMessageFields, $EmailSend);   
    
    if(is_numeric($AddAnswerMessage['ID']) && $arParams["IBLOCK_INFO_PROPERTY_FINISH_LIST"] && $arParams["IBLOCK_INFO_PROPERTY_FINISH_LIST_VALUE"]) {
        CIBlockElement::SetPropertyValuesEx($ItemEmailSend['ID'], $ItemEmailSend['IBLOCK_ID'], array($arParams["IBLOCK_INFO_PROPERTY_FINISH_LIST"] => $arParams["IBLOCK_INFO_PROPERTY_FINISH_LIST_VALUE"]));       
    } 
 
    
    $i++; 
    //�������� �������� ���������        
    //START
    $arParams['MAILING_MAILING_WORK_COUNT'] = $arParams['MAILING_MAILING_WORK_COUNT']+1;
    if($i>=$arParams['MAILING_PACKAGE_COUNT']){
        CSotbitMailingEvent::Update($arParams['MAILING_EVENT_ID'], array(
            "MAILING_WORK_COUNT" => $arParams['MAILING_MAILING_WORK_COUNT']
        ));
        $arrProgress = CSotbitMailingHelp::ProgressFileGetArray($arParams['MAILING_EVENT_ID'], $arParams['MAILING_COUNT_RUN']);
        $arrProgress['MAILING_WORK'] = 'N';
        CSotbitMailingHelp::ProgressFile($arParams['MAILING_EVENT_ID'], $arParams['MAILING_COUNT_RUN'], $arrProgress);          
        die;    
    }    
    //END

                      
}  
//END 

       

?>
