<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// ������� ������ ����������
if(!CModule::IncludeModule("iblock")) return;

// ������ �� ���������
// START
    // ������� ��� ���������
    $arIBlockTypeInfo = CIBlockParameters::GetIBlockTypes();
    if(empty($arCurrentValues["IBLOCK_TYPE_INFO"])){
        foreach($arIBlockTypeInfo as $key => $value) {
           $arCurrentValues["IBLOCK_TYPE_INFO"] = $key;
           break;
        } 
    }
    // ������� ��������
    $arIBlockInfo = array('' => GetMessage('SELECT_CHANGE'));
    if($arCurrentValues["IBLOCK_TYPE_INFO"]) {
        $rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE_INFO"], "ACTIVE"=>"Y"));
        while($arr=$rsIBlock->Fetch())
        {
            $arIBlockInfo[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
        }    
        
    }

    //������� �������� ����������
    $arIBlockInfoPropertyList = array('' => GetMessage('SELECT_CHANGE'));
    $arIBlockInfoPropertyString = array('' => GetMessage('SELECT_CHANGE'));
    $textPeremenInfo = '';  
    if($arCurrentValues["IBLOCK_ID_INFO"]) { 
        $rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID_INFO"]));
        while($arr = $rsProp->Fetch())
        {
            $textPeremenInfo .=  '#PROP_'.$arr["CODE"].'# - '.$arr["NAME"].'<br />'; 
            if($arr["PROPERTY_TYPE"] == "L") {   
                $arIBlockInfoPropertyList[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
            }
            if($arr["PROPERTY_TYPE"] == "S") {   
                $arIBlockInfoPropertyString[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
            }            
        } 
    } 
    
    //�������� �������� ����������
    $arIBlockInfoPropertyListValue = array('' => GetMessage('SELECT_CHANGE'));
    if($arCurrentValues["IBLOCK_ID_INFO"] && $arCurrentValues["IBLOCK_INFO_PROPERTY_FILLTER_LIST"]) { 
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID_INFO"], "CODE"=>$arCurrentValues["IBLOCK_INFO_PROPERTY_FILLTER_LIST"]));
        while($enum_fields = $property_enums->GetNext())
        {      
            $arIBlockInfoPropertyListValue[$enum_fields["ID"]] = "[".$enum_fields["XML_ID"]."] ".$enum_fields["VALUE"];   
        }            
    } 
    
    
    //�������� �������� ����� �������� ���������
    $arIBlockInfoPropertyListFinishValue = array('' => GetMessage('SELECT_CHANGE'));
    if($arCurrentValues["IBLOCK_ID_INFO"] && $arCurrentValues["IBLOCK_INFO_PROPERTY_FINISH_LIST"]) { 
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID_INFO"], "CODE"=>$arCurrentValues["IBLOCK_INFO_PROPERTY_FINISH_LIST"]));
        while($enum_fields = $property_enums->GetNext())
        {      
            $arIBlockInfoPropertyListFinishValue[$enum_fields["ID"]] = "[".$enum_fields["XML_ID"]."] ".$enum_fields["VALUE"];   
        }            
    }         
       
        
        
        
// END





// ��������������� ������
// START

    $arIBlockType = CIBlockParameters::GetIBlockTypes();
    if(empty($arCurrentValues["IBLOCK_TYPE_RECOMMEND"])){
        foreach($arIBlockType as $key => $value) {
           $arCurrentValues["IBLOCK_TYPE_RECOMMEND"] = $key;
           break;
        } 
    }    
    // ������� ��������
    $arIBlockRecommend = array('' => GetMessage('SELECT_CHANGE'));
    if($arCurrentValues["IBLOCK_TYPE_RECOMMEND"]) {
        

        $rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE_RECOMMEND"], "ACTIVE"=>"Y"));
        while($arr=$rsIBlock->Fetch())
        {
            $arIBlockRecommend[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
        }    
        
    }
    
    
    //�������� ���������� 
    $arPropertyListRecommend = array('' => GetMessage('SELECT_CHANGE'));
    $textPeremenRecommend = '';  
    if($arCurrentValues["IBLOCK_ID_RECOMMEND"]) { 

        $rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID_RECOMMEND"]));
        while($arr = $rsProp->Fetch())
        {
            $textPeremenRecommend .=  '#PROP_'.$arr["CODE"].'# - '.$arr["NAME"].'<br />'; 
            if($arr["PROPERTY_TYPE"] == "L") {   
                $arPropertyListRecommend[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
            }
        }
        
    } 
    //�������� �������� ����������
    $arRecommend_fillter_1_list = array('' => GetMessage('SELECT_CHANGE'));

    if($arCurrentValues["IBLOCK_ID_RECOMMEND"] && $arCurrentValues["PROPERTY_FILLTER_1_RECOMMEND"]) { 

        
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID_RECOMMEND"], "CODE"=>$arCurrentValues["PROPERTY_FILLTER_1_RECOMMEND"]));
        while($enum_fields = $property_enums->GetNext())
        {      
            $arRecommend_fillter_1_list[$enum_fields["ID"]] = "[".$enum_fields["XML_ID"]."] ".$enum_fields["VALUE"];   
        }    
            
    }    

    //������� ���� ���
    if(CModule::IncludeModule("catalog")) {
        $arPriceRecommend = array();
        $dbPrice = CCatalogGroup::GetList();
        while($arPrice = $dbPrice->Fetch()) {
            $arPriceRecommend[$arPrice["NAME"]] = "[".$arPrice["ID"]."] ".$arPrice["NAME"];
        }         
    }    
    $sort_by_iblock_list = array(
        'SORT' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_SORT'),
        'ID' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_ID'),   
        'NAME' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_NAME'),
        'CREATED' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_CREATED'),    
        'SHOWS' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_SHOWS'),  
        'RAND' => GetMessage('SORT_BY_IBLOCK_LIST_VALUE_RAND'),                                    
    );  
    $sort_order_iblock_list = array(
        'ASC' => GetMessage('SORT_ORDER_IBLOCK_LIST_VALUE_ASC'),
        'DESC' => GetMessage('SORT_ORDER_IBLOCK_LIST_VALUE_DESC'),        
    );         
        
// END


// ������� ������
// START
if(CModule::IncludeModule("catalog")) {
    $arDiscount_list = array('' => GetMessage('SELECT_CHANGE'));
    $rsDiscount = CCatalogDiscount::GetList(
        array(), 
        Array(),
        false,
        false,
        array('ID','NAME','SITE_ID')
    );
    while($arrDiscount = $rsDiscount->Fetch())
    {    
        $arDiscount_list[$arrDiscount["ID"]] = "[".$arrDiscount["ID"]."] ".$arrDiscount["NAME"];
    }
}    
// END

    $arDiscount_Type = array(
        'Y' => GetMessage('COUPON_ONE_TIME_VALUE_Y'),
        'O' => GetMessage('COUPON_ONE_TIME_VALUE_O'),
        'N' => GetMessage('COUPON_ONE_TIME_VALUE_N')
    );

    $arDiscount_life_time_action = array(
        'DELETE' => GetMessage('COUPON_TIME_LIFE_ACTION_VALUE_DELETE'),    
        'DEACTION' => GetMessage('COUPON_TIME_LIFE_ACTION_VALUE_DEACTION')    
    );
  
  
//���������� ����� �� ���������
//START  
$tpl_mailing = QueryGetData(
    $_SERVER['HTTP_HOST'],
    80,
    COption::GetOptionString('sotbit.mailing','TEMPLATE_MAILING_DEF'),
    "", 
    $error_number, 
    $error_text
);
if(empty($tpl_mailing)){
    $tpl_mailing = QueryGetData(
        $_SERVER['HTTP_HOST'],
        80,
        '/bitrix/components/sotbit/sotbit.mailing.logic/lang/ru/tpl_mailing/default.php',
        "", 
        $error_number, 
        $error_text
    );          
}           
$MESSAGE_DEFAULT = CSotbitMailingHelp::ReplaceVariables($tpl_mailing, array('SITE_URL' => $_SERVER['HTTP_HOST'], 'MESSAGE_TEXT'=> GetMessage("MESSAGE_DEFAULT")));
//END

//��������  ���������� �������
// TABS - ��� ���� � ����
// TABS_NAME - ��� ����
// PARENT - ��� ������ � ���� 
// PARENT_NAME - ��� ������ � ����
// NAME - ��� ��������
// TYPE -  ���  ��������   
// ------STRING - ������
// ------INT - �������� ���� 
// ------CHECKBOX - ������������� ��/���
// ------TEXT - ����� html ���������
// ------TEXTAREA - ����� ���������� ����
// ------TABS_INFO - ��� � ������, ��� ���������� ��������
// ------PHP - ������� php ����  � ������ ��������
// ------DATE_PERIOD - ��������� � �����
// ------USER_ID - ����� id ������������

  
$arTemplateParameters = array(); 
	
  // ������� ��������� ��� ��������       
$arTemplateParameters["IBLOCK_TYPE_INFO"] = array(    
    "PARENT" => "PARAM_IBLOCK",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_NAME"),
    "NAME" => GetMessage('IBLOCK_TYPE_INFO_TITLE'),
    "TYPE" => "LIST",
    "VALUES" => $arIBlockTypeInfo,
    "REFRESH" => "Y",
    "SORT" => "10",
);
$arTemplateParameters["IBLOCK_ID_INFO"] = array(
    "PARENT" => "PARAM_IBLOCK",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_NAME"),
    "NAME" => GetMessage('IBLOCK_ID_INFO_TITLE'),
    "TYPE" => "LIST",
    "ADDITIONAL_VALUES" => "Y",
    "VALUES" => $arIBlockInfo,
    "REFRESH" => "Y",
    "SORT" => "30",
); 

  
$arTemplateParameters["IBLOCK_INFO_PROPERTY_FILLTER_LIST"] = array(
    "PARENT" => "PARAM_IBLOCK_FILLTER_LIST",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_FILLTER_LIST"),
    "NAME" => GetMessage('IBLOCK_INFO_PROPERTY_FILLTER_LIST_TITLE'),
    "TYPE" => "LIST",
    "VALUES" => $arIBlockInfoPropertyList,
    "REFRESH" => "Y",
    "SORT" => "40",
); 
$arTemplateParameters["IBLOCK_INFO_PROPERTY_FILLTER_LIST_VALUE"] = array(
    "PARENT" => "PARAM_IBLOCK_FILLTER_LIST",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_FILLTER_LIST"),
    "NAME" => GetMessage('IBLOCK_INFO_PROPERTY_FILLTER_LIST_VALUE_TITLE'),
    "TYPE" => "LIST",
    "VALUES" => $arIBlockInfoPropertyListValue,
    "REFRESH" => "N",
    "SORT" => "50",
    "NOTES" => GetMessage('IBLOCK_INFO_PROPERTY_FILLTER_LIST_VALUE_NOTES')
);    
      
      
$arTemplateParameters["IBLOCK_INFO_PROPERTY_FILLTER_STRING"] = array(
    "PARENT" => "PARAM_IBLOCK_FILLTER_STRING",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_FILLTER_STRING"),
    "NAME" => GetMessage('IBLOCK_INFO_PROPERTY_FILLTER_STRING_TITLE'),
    "TYPE" => "LIST",
    "VALUES" => $arIBlockInfoPropertyString,
    "REFRESH" => "Y",
    "SORT" => "60",
); 
$arTemplateParameters["IBLOCK_INFO_PROPERTY_FILLTER_STRING_VALUE"] = array(
    "PARENT" => "PARAM_IBLOCK_FILLTER_STRING",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_FILLTER_STRING"),
    "NAME" => GetMessage('IBLOCK_INFO_PROPERTY_FILLTER_STRING_VALUE_TITLE'),
    "TYPE" => "STRING",
    "SORT" => "70",
    "NOTES" => GetMessage('IBLOCK_INFO_PROPERTY_FILLTER_STRING_VALUE_NOTES')
);          
      
      
      
$arTemplateParameters["IBLOCK_INFO_PROPERTY_FINISH_LIST"] = array(
    "PARENT" => "PARAM_IBLOCK_FINISH_LIST",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_FINISH_LIST"),
    "NAME" => GetMessage('IBLOCK_INFO_PROPERTY_FINISH_LIST_TITLE'),
    "TYPE" => "LIST",
    "VALUES" => $arIBlockInfoPropertyList,
    "REFRESH" => "Y",
    "SORT" => "40",
); 
$arTemplateParameters["IBLOCK_INFO_PROPERTY_FINISH_LIST_VALUE"] = array(
    "PARENT" => "PARAM_IBLOCK_FINISH_LIST",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_FINISH_LIST"),
    "NAME" => GetMessage('IBLOCK_INFO_PROPERTY_FINISH_LIST_VALUE_TITLE'),
    "TYPE" => "LIST",
    "VALUES" => $arIBlockInfoPropertyListFinishValue,
    "REFRESH" => "N",
    "SORT" => "50",
    "NOTES" => GetMessage('IBLOCK_INFO_PROPERTY_FINISH_LIST_VALUE_NOTES')
);     
   
   
   
// �������������� ������
// START
$arTemplateParameters["RECOMMEND_SHOW"] =  array(
    "TABS" => 'TABS_RECOMMEND',
    "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
    "PARENT" => "PARAM_RECOMMEND_SETTING",
    "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_SETTING_NAME"),
    "NAME" => GetMessage("RECOMMEND_SHOW_TITLE"),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "4",  
    "SORT" => "7",
    "REFRESH" => "Y",
);  

if($arCurrentValues["RECOMMEND_SHOW"]=='Y'){

    $arTemplateParameters["IBLOCK_TYPE_RECOMMEND"] =  array(   
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),    
        "PARENT" => "PARAM_IBLOCK_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_FILLTER_NAME"),
        "NAME" => GetMessage('IBLOCK_TYPE_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arIBlockType,
        "REFRESH" => "Y",
        "SORT" => "10",
    );
    $arTemplateParameters["IBLOCK_ID_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_IBLOCK_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_FILLTER_NAME"),
        "NAME" => GetMessage('IBLOCK_ID_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arIBlockRecommend,
        "REFRESH" => "Y",
        "SORT" => "20",
    ); 
    $arTemplateParameters["PROPERTY_FILLTER_1_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_IBLOCK_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_FILLTER_NAME"),
        "NAME" => GetMessage('PROPERTY_FILLTER_1_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arPropertyListRecommend,
        "REFRESH" => "Y",
        "SORT" => "40",
    ); 
    $arTemplateParameters["PROPERTY_FILLTER_1_VALUE_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_IBLOCK_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_RECOMMEND_NAME"),
        "NAME" => GetMessage('PROPERTY_FILLTER_1_VALUE_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arRecommend_fillter_1_list,
        "REFRESH" => "N",
        "SORT" => "50",
        "NOTES" => GetMessage('PROPERTY_FILLTER_1_VALUE_RECOMMEND_NOTES'),
    );   
      
    $arTemplateParameters["TOP_COUNT_FILLTER_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_IBLOCK_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_IBLOCK_RECOMMEND_NAME"),
        "NAME" => GetMessage('TOP_COUNT_FILLTER_RECOMMEND_TITLE'),
        "TYPE" => "INT",
        "SORT" => "50",
        "DEFAULT" => "4"
    );     
         
           
    $arTemplateParameters["SORT_BY_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_SORT_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_SORT_NAME"),
        "NAME" => GetMessage('SORT_BY_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $sort_by_iblock_list,
        "REFRESH" => "N",
        "SORT" => "20",
    );      
    $arTemplateParameters["SORT_ORDER_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_SORT_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_SORT_NAME"),
        "NAME" => GetMessage('SORT_ORDER_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $sort_order_iblock_list,
        "REFRESH" => "N",
        "SORT" => "40",
    );  
     
     
      
     //��� ���� 

    $arTemplateParameters["PRICE_TYPE_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_PRICE_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_RECOMMEND_PRICE_NAME"),
        "NAME" => GetMessage('PRICE_TYPE_RECOMMEND_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arPriceRecommend,
        "REFRESH" => "N",
        "SORT" => "40",
    ); 

      
      //������� ��� ������ �������
    $arTemplateParameters["IMG_WIDTH_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_TEMP_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_RECOMMEND_NAME"),
        "NAME" => GetMessage("IMG_WIDTH_RECOMMEND_TITLE"),
        "TYPE" => "INT",
        "DEFAULT" => "100" ,
        "SORT" => "10",
    );    

    $arTemplateParameters["IMG_HEIGHT_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),    
        "PARENT" => "PARAM_TEMP_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_RECOMMEND_NAME"),
        "NAME" => GetMessage("IMG_HEIGHT_RECOMMEND_TITLE"),
        "TYPE" => "INT",
        "DEFAULT" => "200" ,
        "SORT" => "20",
    );        
    $arTemplateParameters["TEMP_TOP_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),  
        "PARENT" => "PARAM_TEMP_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_RECOMMEND_NAME"),
        "NAME" => GetMessage("TEMP_TOP_RECOMMEND_TITLE"),
        "COLS" => 40,
        "HEIGHT" => 160,
        "TYPE" => "TEXT",
        "DEFAULT" => GetMessage("TEMP_TOP_RECOMMEND_DEFAULT"), 
        "NOTES"  =>  GetMessage("TEMP_TOP_RECOMMEND_NOTES"),
        "SORT" => "40",
    );  
    $arTemplateParameters["TEMP_LIST_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"), 
        "PARENT" => "PARAM_TEMP_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_RECOMMEND_NAME"),
        "NAME" => GetMessage("TEMP_LIST_RECOMMEND_TITLE"),
        "COLS" => 40,
        "HEIGHT" => 240,
        "TYPE" => "TEXT",
        "DEFAULT" => GetMessage("TEMP_LIST_RECOMMEND_DEFAULT"), 
        "NOTES"  =>  GetMessage("TEMP_LIST_RECOMMEND_NOTES", array('#IBLOCK_PROP#'=>$textPeremenRecommend)),
        "SORT" => "50",
    );    
    $arTemplateParameters["TEMP_BOTTOM_RECOMMEND"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),   
        "PARENT" => "PARAM_TEMP_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_TEMP_RECOMMEND_NAME"),
        "NAME" => GetMessage("TEMP_BOTTOM_RECOMMEND_TITLE"),
        "COLS" => 40,
        "HEIGHT" => 160,
        "TYPE" => "TEXT",
        "DEFAULT" => GetMessage("TEMP_BOTTOM_RECOMMEND_DEFAULT"), 
        "NOTES"  =>  GetMessage("TEMP_BOTTOM_RECOMMEND_NOTES"),
        "SORT" => "60",
    );    
      
      // PHP �����������
    $arTemplateParameters["PHP_RECOMMEND_FILLTER_BEFORE"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),     
        "PARENT" => "PHP_MODIF_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_RECOMMEND_NAME"),
        "NAME" => GetMessage("PHP_RECOMMEND_FILLTER_BEFORE_TITLE"),
        "HEIGHT" => 250,
        "WIDTH" => 1000,
        "TYPE" => "PHP",
        "SORT" => "70",
        "NOTES" => GetMessage("PHP_RECOMMEND_FILLTER_BEFORE_NOTES"),
    );      
        
    $arTemplateParameters["PHP_RECOMMEND_FILLTER_WHILE_AFTER"] =  array(
        "TABS" => 'TABS_RECOMMEND',
        "TABS_NAME" => GetMessage("TABS_RECOMMEND_NAME"),     
        "PARENT" => "PHP_MODIF_RECOMMEND",
        "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_RECOMMEND_NAME"),
        "NAME" => GetMessage("PHP_RECOMMEND_FILLTER_WHILE_AFTER_TITLE"),
        "HEIGHT" => 250,
        "WIDTH" => 1000,
        "TYPE" => "PHP",
        "SORT" => "90",
        "NOTES" => GetMessage("PHP_RECOMMEND_FILLTER_WHILE_AFTER_NOTES"),
    );     
       
}
  //END
     
     
     

//������ �� ������   


 
$arTemplateParameters["COUPON_ADD"] =  array(
    "TABS" => 'TABS_DISCOUNT',
    "TABS_NAME" => GetMessage("TABS_DISCOUNT_NAME"), 
    "PARENT" => "PARAM_COUPON_ADD",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_COUPON_ADD"),
    "NAME" => GetMessage("COUPON_ADD_TITLE"),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "4" ,
    "SORT" => "10",
    "REFRESH" => "Y",
); 


if($arCurrentValues["COUPON_ADD"]=='Y'){

    $arTemplateParameters["COUPON_DISCOUNT_ID"] =  array(
        "TABS" => 'TABS_DISCOUNT',
        "TABS_NAME" => GetMessage("TABS_DISCOUNT_NAME"), 
        "PARENT" => "PARAM_COUPON_ADD",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_COUPON_ADD"),
        "NAME" => GetMessage("COUPON_DISCOUNT_ID_TITLE"),
        "TYPE" => "LIST",
        "VALUES" => $arDiscount_list,
        "SORT" => "20",
    );     
              
    $arTemplateParameters["COUPON_ONE_TIME"] =  array(
        "TABS" => 'TABS_DISCOUNT',
        "TABS_NAME" => GetMessage("TABS_DISCOUNT_NAME"),   
        "PARENT" => "PARAM_COUPON_ADD",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_COUPON_ADD"),
        "NAME" => GetMessage("COUPON_ONE_TIME_TITLE"),
        "TYPE" => "LIST",
        "VALUES" => $arDiscount_Type,
        "SORT" => "30",
    ); 
        
    $arTemplateParameters["COUPON_TIME_LIFE"] =  array(
        "TABS" => 'TABS_DISCOUNT',
        "TABS_NAME" => GetMessage("TABS_DISCOUNT_NAME"),   
        "PARENT" => "PARAM_COUPON_ADD",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_COUPON_ADD"),
        "NAME" => GetMessage("COUPON_TIME_LIFE_TITLE"),
        "TYPE" => "INT",
        "DEFAULT" => "48" ,
        "SORT" => "40",
    );     
         
    $arTemplateParameters["COUPON_TIME_LIFE_ACTION"] =  array(
        "TABS" => 'TABS_DISCOUNT',
        "TABS_NAME" => GetMessage("TABS_DISCOUNT_NAME"),   
        "PARENT" => "PARAM_COUPON_ADD",
        "PARENT_NAME" => GetMessage("GROUP_PARAM_COUPON_ADD"),
        "NAME" => GetMessage("COUPON_TIME_LIFE_ACTION_TITLE"),
        "TYPE" => "LIST",
        "VALUES" => $arDiscount_life_time_action,
        "SORT" => "50",
    );  
  
}
 
        
  //���������� �� ��������
$arTemplateParameters["EMAIL_DUBLICATE"] = array(
    "PARENT" => "PARAM_EXCEPTIONS",
    "PARENT_NAME" => GetMessage("GROUP_PARAM_EXCEPTIONS"),
    "NAME" => GetMessage("EMAIL_DUBLICATE_TITLE"),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "" ,
    "SORT" => "10",
    "NOTES" => GetMessage("EMAIL_DUBLICATE_NOTES"),
);         
        
        
 
  //������ ������
$arTemplateParameters["SUBJECT"] = array(
    "PARENT" => "MESSAGE_INFO",
    "PARENT_NAME" => GetMessage("GROUP_MESSAGE_INFO_NAME"),
    "NAME" => GetMessage("SUBJECT_TITLE"),
    "TYPE" => "STRING",
    "DEFAULT" => GetMessage("SUBJECT_DEFAULT"),
    "SORT" => "10",
    "SIZE" => '50'
); 
$arTemplateParameters["EMAIL_FROM"] = array(
    "PARENT" => "MESSAGE_INFO",
    "PARENT_NAME" => GetMessage("GROUP_MESSAGE_INFO_NAME"),
    "NAME" => GetMessage("EMAIL_FROM_TITLE"),
    "TYPE" => "STRING",
    "DEFAULT" => COption::GetOptionString('main', 'email_from'),
    "SORT" => "20",
    "SIZE" => '50'
);    
$arTemplateParameters["EMAIL_TO"] = array(
    "PARENT" => "MESSAGE_INFO",
    "PARENT_NAME" => GetMessage("GROUP_MESSAGE_INFO_NAME"),
    "NAME" => GetMessage("EMAIL_TO_TITLE"),
    "TYPE" => "STRING",
    "DEFAULT" => "#PROP_EMAIL#" ,
    "SORT" => "30",
    "SIZE" => '50'
); 

$arTemplateParameters["BCC"] = array(
    "PARENT" => "MESSAGE_INFO",
    "PARENT_NAME" => GetMessage("GROUP_MESSAGE_INFO_NAME"),
    "NAME" => GetMessage("BCC_TITLE"),
    "TYPE" => "STRING",
    "DEFAULT" => "" ,
    "SORT" => "40",
    "SIZE" => '50'
); 
      
$arTemplateParameters["MESSAGE"] = array(
    "PARENT" => "MESSAGE_INFO",
    "PARENT_NAME" => GetMessage("GROUP_MESSAGE_INFO_NAME"),
    "TYPE" => "TEXT",
    "DEFAULT" => $MESSAGE_DEFAULT, 
    "NOTES"  =>  GetMessage("GROUP_MESSAGE_NOTES", array("#IBLOCK_PROP#"=>$textPeremenInfo)),
    "SORT" => "60",
);


  
  // PHP ����������� 
  // START 
$arTemplateParameters["PHP_FILLTER_IBLOCK_PARAM_BEFORE"] = array(
    "PARENT" => "PHP_MODIF_MAILING",                                      
    "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_MAILING_NAME"),
    "NAME" => GetMessage("PHP_FILLTER_IBLOCK_PARAM_BEFORE_TITLE"),
    "HEIGHT" => 200,
    "WIDTH" => 1000,
    "TYPE" => "PHP",
    "SORT" => "40",
    "NOTES" => GetMessage("PHP_FILLTER_IBLOCK_PARAM_BEFORE_NOTES"),
);
$arTemplateParameters["PHP_FILLTER_IBLOCK_PARAM_AFTER"] = array(
    "PARENT" => "PHP_MODIF_MAILING",                                      
    "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_MAILING_NAME"),
    "NAME" => GetMessage("PHP_FILLTER_IBLOCK_PARAM_AFTER_TITLE"),
    "HEIGHT" => 200,
    "WIDTH" => 1000,
    "TYPE" => "PHP",
    "SORT" => "50",
    "NOTES" => GetMessage("PHP_FILLTER_IBLOCK_PARAM_AFTER_NOTES"),
);    
         
$arTemplateParameters["PHP_MESSAGE_FOREACH_BEFORE"] = array(
    "PARENT" => "PHP_MODIF_MAILING",
    "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_MAILING_NAME"),
    "NAME" => GetMessage("PHP_MESSAGE_FOREACH_BEFORE_TITLE"),
    "HEIGHT" => 200,
    "WIDTH" => 1000,
    "TYPE" => "PHP",
    "SORT" => "70",
    "NOTES" => GetMessage("PHP_MESSAGE_FOREACH_BEFORE_NOTES"),
);    
$arTemplateParameters["PHP_MESSAGE_FOREACH_ITEM_BEFORE"] = array(
    "PARENT" => "PHP_MODIF_MAILING",                                      
    "PARENT_NAME" => GetMessage("GROUP_PHP_MODIF_MAILING_NAME"),
    "NAME" => GetMessage("PHP_MESSAGE_FOREACH_ITEM_BEFORE_TITLE"),
    "HEIGHT" => 200,
    "WIDTH" => 1000,
    "TYPE" => "PHP",
    "SORT" => "80",
    "NOTES" => GetMessage("PHP_MESSAGE_FOREACH_ITEM_BEFORE_NOTES"),
);    
  // END    
  
  
$arTemplateParameters["TABS_SOTBIT_MAILING_INSTRUCTION"] = array(
    "TABS" => 'TABS_SOTBIT_MAILING_INSTRUCTION',
    "TABS_NAME" => GetMessage("TABS_SOTBIT_MAILING_INSTRUCTION_TITLE"),   
    "PARENT" => "PARAM_TABS_SOTBIT_MAILING_INSTRUCTION",
    "PARENT_NAME" => GetMessage("GROUP_TABS_SOTBIT_MAILING_INSTRUCTION_NAME"),    
    "TYPE" => "TABS_INFO",
    "DEFAULT" => GetMessage("TABS_SOTBIT_MAILING_INSTRUCTION_DEFAULT"),    
);     
  
  



?>
