<?

$MESS['GROUP_PARAM_IBLOCK_NAME'] = 'Выберем инфоблок'; 

$MESS['GROUP_PARAM_IBLOCK_FILLTER_LIST'] = 'Отфильтруем по свойству список'; 
$MESS['GROUP_PARAM_IBLOCK_FILLTER_STRING'] = 'Отфильтруем по свойству строка'; 


$MESS['GROUP_PARAM_IBLOCK_FILLTER_LIST'] = 'Отфильтруем по свойству список'; 
$MESS['GROUP_PARAM_IBLOCK_FILLTER_STRING'] = 'Отфильтруем по свойству строка'; 





$MESS['IBLOCK_TYPE_INFO_TITLE'] = 'Тип инфоблока';
$MESS['IBLOCK_ID_INFO_TITLE'] = 'Код инфоблока'; 
$MESS['IBLOCK_INFO_PROPERTY_FILLTER_LIST_TITLE'] = 'Фильтровать товары по свойству типа список'; 
$MESS['IBLOCK_INFO_PROPERTY_FILLTER_LIST_VALUE_TITLE'] = 'Значение фильтра свойства типа список';          
$MESS['IBLOCK_INFO_PROPERTY_FILLTER_LIST_VALUE_NOTES'] = 'Оставьте пустыми значения если не нужно фильтровать';


$MESS['IBLOCK_INFO_PROPERTY_FILLTER_STRING_TITLE'] = 'Фильтровать товары по свойству типа строка'; 
$MESS['IBLOCK_INFO_PROPERTY_FILLTER_STRING_VALUE_TITLE'] = 'Значение фильтра свойства типа строка';   
$MESS['IBLOCK_INFO_PROPERTY_FILLTER_STRING_VALUE_NOTES'] = 'Оставьте пустыми значения если не нужно фильтровать';  
   
   
$MESS['GROUP_PARAM_IBLOCK_FINISH_LIST'] = 'Значение свойства список после отправки сообщения';    
$MESS['IBLOCK_INFO_PROPERTY_FINISH_LIST_TITLE'] = 'Свойство'; 
$MESS['IBLOCK_INFO_PROPERTY_FINISH_LIST_VALUE_TITLE'] = 'Значение';          
$MESS['IBLOCK_INFO_PROPERTY_FINISH_LIST_VALUE_NOTES'] = 'Используйте если необходимо поменять значение свойства, у элемента по которому провели отправку сообщения. Выберите свойства типа "список" и его значение после отправки.';                 
         
         
$MESS['TABS_DISCOUNT_NAME'] = 'Скидки';
$MESS["COUPON_ADD_TITLE"] = 'Создавать скидочный купон';
$MESS["COUPON_DISCOUNT_ID_TITLE"] = 'Скидка, в которой создавать купон (заранее создайте)';
$MESS["COUPON_ONE_TIME_TITLE"] = 'Тип купона';       
    $MESS["COUPON_ONE_TIME_VALUE_Y"] = 'Купон на одну позицию заказ';    
    $MESS["COUPON_ONE_TIME_VALUE_O"] = 'Купон на один заказ';  
    $MESS["COUPON_ONE_TIME_VALUE_N"] = 'Многоразовый купон';            
$MESS["COUPON_TIME_LIFE_TITLE"] = 'Время возможности использования купона в часах'; 
$MESS["COUPON_TIME_LIFE_ACTION_TITLE"] = 'Не использованные купоны необходимо';  
    $MESS["COUPON_TIME_LIFE_ACTION_VALUE_DELETE"] = 'Удалить';  
    $MESS["COUPON_TIME_LIFE_ACTION_VALUE_DEACTION"] = 'Деактивировать'; 
      


// рекомендованные товары
// START   
$MESS['TABS_RECOMMEND_NAME'] = 'Рекомендуемые товары';  
$MESS["GROUP_RECOMMEND_SETTING_NAME"] = 'Настройка рекомендуемых товаров';
$MESS["GROUP_RECOMMEND_FILLTER_NAME"] = 'Выберем и отфильтруем';  
     
$MESS['RECOMMEND_SHOW_TITLE'] = 'Выводить рекомендованные товары';         
$MESS['IBLOCK_TYPE_RECOMMEND_TITLE'] = 'Тип инфоблока';
$MESS['IBLOCK_ID_RECOMMEND_TITLE'] = 'Код инфоблока';     
$MESS['PROPERTY_FILLTER_1_RECOMMEND_TITLE'] = 'Фильтровать товары по свойству список';  
$MESS['PROPERTY_FILLTER_1_VALUE_RECOMMEND_TITLE'] = 'Значение фильтра свойства';      
$MESS['PROPERTY_FILLTER_1_VALUE_RECOMMEND_NOTES'] = 'Если не задан фильтр "Фильтровать товары по свойству список", то будут в отбор будут идти все товары из инфоблока';
$MESS["TOP_COUNT_FILLTER_RECOMMEND_TITLE"] = 'Количество выводимых элементов'; 
   
   
   
$MESS["GROUP_RECOMMEND_SORT_NAME"] = 'Сортировка'; 
$MESS['SORT_BY_RECOMMEND_TITLE'] ="Сортировать";
    $MESS['SORT_BY_IBLOCK_LIST_VALUE_SORT'] ="индекс сортировки (sort)";
    $MESS['SORT_BY_IBLOCK_LIST_VALUE_ID'] ="ID элемента (id)";
    $MESS['SORT_BY_IBLOCK_LIST_VALUE_NAME'] ="название (name)";        
    $MESS['SORT_BY_IBLOCK_LIST_VALUE_CREATED'] ="время создания (created)";          
    $MESS['SORT_BY_IBLOCK_LIST_VALUE_SHOWS'] ="усредненное количество показов (shows)";    
    $MESS['SORT_BY_IBLOCK_LIST_VALUE_RAND'] ="случайный порядок (rand)";            
   
        
$MESS['SORT_ORDER_RECOMMEND_TITLE'] ="По";
    $MESS['SORT_ORDER_IBLOCK_LIST_VALUE_ASC'] ="Возрастанию (asc)";
    $MESS['SORT_ORDER_IBLOCK_LIST_VALUE_DESC'] ="Убыванию (desc)";
    
  
$MESS["GROUP_RECOMMEND_PRICE_NAME"] = 'Тип цены';   
$MESS["PRICE_TYPE_RECOMMEND_TITLE"] = 'Показывать цену';  
  
   
$MESS['GROUP_TEMP_RECOMMEND_NAME'] = 'Шаблон списка товаров (переменная #RECOMMEND_PRODUCT#)';  
$MESS['TABS_FORGET_BASKET_NAME'] = 'Список товаров';
$MESS["IMG_WIDTH_RECOMMEND_TITLE"] = "Ширина изображения товара";
$MESS["IMG_HEIGHT_RECOMMEND_TITLE"] = "Высота изображения товара"; 
$MESS["TEMP_TOP_RECOMMEND_TITLE"] = "Верх списка товаров";
$MESS["TEMP_TOP_RECOMMEND_DEFAULT"] = '
    <div style="background-color: #89cbf5; padding: 5px 10px 5px 10px;color: #FFFFFF; margin-bottom: 10px;"><b>Рекомендуем посмотреть:</b></div> 
    <div style="background-color: #fafbfb; border: 1px solid #d3dcdd; padding: 0px 20px 0px 20px;">         
    <table width="100%" cellpadding="0" cellspacing="0">
        <tbody>
';
$MESS["TEMP_LIST_RECOMMEND_TITLE"] = "Внешний вид товара, вывод в цикле";
$MESS["TEMP_LIST_RECOMMEND_DEFAULT"] = '
                <tr>
                    <td width="110px" style="vertical-align: top; padding-bottom: 20px;padding-top: 20px; #BORDER_TABLE_STYLE#">
                        <a href="#DETAIL_PAGE_URL#"><img src="#PICTURE_SRC#" width="#PICTURE_WIDTH#" height="#PICTURE_HEIGHT#" /></a>     
                    </td>
                    <td style="vertical-align: top; padding-bottom: 20px;padding-top: 20px; #BORDER_TABLE_STYLE#">
                        <a href="#DETAIL_PAGE_URL#" style="color: #00b6f4; font-size: 14px"; >#NAME#</a> <br />
                        <br />
                         #PREVIEW_TEXT#
                    </td>
                    <td  style="vertical-align: top; padding-bottom: 20px;padding-top: 20px; #BORDER_TABLE_STYLE#">
                        <s style="color:red;display:block;">#PRINT_NO_DISCOUNT_PRICE#</s>
                        <b style="white-space: nowrap">#PRINT_PRICE#</b>
                    </td>                    
                </tr>   
'; 
$MESS["TEMP_LIST_RECOMMEND_NOTES"] = '
    Переменные элемента инфоблока:   <br />
    #ID# - ID элементы <br />   
    #NAME# - Имя товара<br />   
    #PREVIEW_TEXT# - анонс описания<br />   
    #DETAIL_TEXT#  - детальное описание<br />   
    #LIST_PAGE_URL#  - ссылка на товары из категории <br />   
    #DETAIL_PAGE_URL#  - ссылка на товар <br />     
    #PICTURE_SRC# - пусть до изображения <br /> 
    #PICTURE_WIDTH# - ширина изображения <br />  
    #PICTURE_HEIGHT# - высота изображения <br />

    <br />
    Цена товара:  <br />
    #PRINT_PRICE# - цена товара со валютой<br />
    #PRICE# - цена товара без валюты<br />    
    #PRINT_NO_DISCOUNT_PRICE# - цена товара без скидки с валютой<br />
    #NO_DISCOUNT_PRICE# - цена товара без скидки без валюты<br />    
    #PRINT_DISCOUNT_DIFF# - величина скидки с валютой<br />
    #DISCOUNT_DIFF# - величина скидки без валюты<br />      
    <br />
    Переменные свойств инфоблока: <br />
    #IBLOCK_PROP#          
    <br />
    Дополнительно:  <br />
    #BORDER_TABLE_STYLE# - стиль border-top: 1px solid #E6EAEC; кроме первого элемента<br />
';  
$MESS["TEMP_BOTTOM_RECOMMEND_TITLE"] = "Низ списка товаров";
$MESS["TEMP_BOTTOM_RECOMMEND_DEFAULT"] = '
        </tbody>
    </table>
</div>
'; 
// PHP модификация  
$MESS["GROUP_PHP_MODIF_RECOMMEND_NAME"] = 'Модификация рекомендумых товаров (для разработчиков)';


$MESS["PHP_RECOMMEND_FILLTER_BEFORE_TITLE"] = 'PHP: Перед выборкой товаров';
$MESS["PHP_RECOMMEND_FILLTER_BEFORE_NOTES"] = '<span style="color:red">ОСТРОЖНО: Необходимы знания php, использовать при необходимости расширения функционала.</span><br />
Подлючается до выборки товаров с помощью функции <a target="_blank" href="http://dev.1c-bitrix.ru/api_help/iblock/classes/ciblockelement/getlist.php">CIBlockElement::GetList</a>.<br/>
Вы можете переопределить значения выборкм, либо объединить свой с существующими  <a href="http://www.php.su/array_merge" target="_blank">array_merge($arFilterRecommend, $MyArray)</a>. <br/>
<b>Переменные:</b> <br />
$arSortRecpmmend - сортировка<br />
$arFilterRecommend - фильтрация <br />
$arNavStartParams - навигация  <br />
$arSelectRecommend - выбор данных<br />

<br />
<br />
<b>Отладку</b> можно вести через <a href="/bitrix/admin/php_command_line.php?lang=ru" target="_blank">коммандную PHP-строку</a> в тестовом режиме рассылки.<br />
';    
 
 
$MESS["PHP_RECOMMEND_FILLTER_WHILE_AFTER_TITLE"] = 'PHP: В конце цикла выборки';
$MESS["PHP_RECOMMEND_FILLTER_WHILE_AFTER_NOTES"] = '<span style="color:red">ОСТРОЖНО: Необходимы знания php, использовать при необходимости расширения функционала.</span><br />
Подключается в конце цикла while, позволяет модифицировать полученные данные, добавить новые.<br />
Массив $arFields, хранит в себе данные конкретного товара.<br />
<br />
<b>Функции:</b> <br />
$phpIncludeFunction["isContinue"]="Y" - пропустить итерацию <br /> 
$phpIncludeFunction["isBreak"]="Y" - прервать работу цикла<br />   
<br />
<br />
<b>Отладку</b> можно вести через <a href="/bitrix/admin/php_command_line.php?lang=ru" target="_blank">коммандную PHP-строку</a> в тестовом режиме рассылки.<br />
';    
    
          
// END          
      
      
// исключения из рассылки
// START  
$MESS['GROUP_PARAM_EXCEPTIONS'] = 'Исключить из рассылки';  
$MESS['EMAIL_DUBLICATE_TITLE'] = 'Дублирующие e-mail';
$MESS['EMAIL_DUBLICATE_NOTES'] = 'При обнаружение e-mail по которому отсылали сообщение, не производить отправку.';     
// END  
      
$MESS["SUBJECT_TITLE"] = 'Тема письма';
$MESS["SUBJECT_DEFAULT"] = 'Важная информация';
   
   
$MESS["EMAIL_FROM_TITLE"] = 'От кого';
$MESS["EMAIL_TO_TITLE"] = 'Кому';
$MESS["BCC_TITLE"] = 'Копия';       

     
$MESS["GROUP_PARAM_USER_FILLTER"] = 'Выбираем пользователей';
$MESS["GROUP_PARAM_COUPON_ADD"] = 'Параметры скидочного купона'; 
         
  
$MESS["GROUP_MESSAGE_INFO_NAME"] = 'Параметры почтового шаблона';   
$MESS["GROUP_MESSAGE_NAME"] = 'Шаблон письма';  
$MESS["MESSAGE_DEFAULT"] = '
    <p><b>Добрый день !</b></p>
    <br />
    <p>Вы находитесь у нас в базе данных, поэтому вы информируем вас о новых акциях и скидках.</p>              
    #RECOMMEND_PRODUCT#
';   
  
   
  
$MESS["GROUP_MESSAGE_NOTES"] = '
   <b>Статистика и отписка от рассылки: </b>  <br />
   #MAILING_MESSAGE# - для сбора статистики необходимо ссылки ставить с переменной ?MAILING_MESSAGE=#MAILING_MESSAGE# <br />
   #MAILING_UNSUBSCRIBE# - для возможности отписаться от рассылки ставите ссылку с переменной ?MAILING_UNSUBSCRIBE=#MAILING_UNSUBSCRIBE# <br />  
   #MAILING_EVENT_ID# - ID рассылки<br />   
   <br />

   Переменные элемента инфоблока:   <br />
   #ID# - ID элемента <br />   
   #NAME# - Имя товара<br />   
   #PREVIEW_TEXT# - анонс описания<br />   
   #DETAIL_TEXT#  - детальное описание<br />   
   #LIST_PAGE_URL#  - ссылка на товары из категории <br />   
   #DETAIL_PAGE_URL#  - ссылка на товар <br />     
   #PICTURE_SRC# - пусть до изображения <br /> 
   #PICTURE_WIDTH# - ширина изображения <br />  
   #PICTURE_HEIGHT# - высота изображения <br />

   <br />
   Переменные свойств инфоблока: <br />
   #IBLOCK_PROP#  
            
   <br />   
   
   
   Общие  переменные: <br />
   #COUPON# - купон на скидку   <br />
   #RECOMMEND_PRODUCT# - рекомендованные товары<br />
    
   <br />
   <br />
   При использовании сервиса unisender для отправки писем обязательна переменная {{UnsubscribeUrl}} для отписки от рассылки.
   <br />    
'; 


// дополнительные данные и логика рассылки
// START
$MESS["GROUP_PHP_MODIF_MAILING_NAME"] = 'Модификация рассылки (для разработчиков)';

// Перед выборкой элементов инфоблока
$MESS["PHP_FILLTER_IBLOCK_PARAM_BEFORE_TITLE"] = 'PHP: Перед выборкой элементов инфоблока';
$MESS["PHP_FILLTER_IBLOCK_PARAM_BEFORE_NOTES"] = '<span style="color:red">ОСТРОЖНО: Необходимы знания php, использовать при необходимости расширения функционала.</span><br />
Подлючается до выборки элементов инфоблока с помощью функции <a target="_blank" href="http://dev.1c-bitrix.ru/api_help/iblock/classes/ciblockelement/getlist.php">CIBlockElement::GetList</a>.<br/>
Вы можете переопределить значения выборки, либо объединить свой с существующим  <a href="http://www.php.su/array_merge" target="_blank">array_merge($arFilter, $MyArray)</a>. <br/>
<b>Переменные:</b> <br />
$arSortIblock - сортировка<br />
$arFilterIblock - фильтрация <br />
$arSelectIblock - выбор данных<br />

<br />
<br />
<b>Отладку</b> можно вести через <a href="/bitrix/admin/php_command_line.php?lang=ru" target="_blank">коммандную PHP-строку</a> в тестовом режиме рассылки.<br />
';

// В конце выборки инфоблока
$MESS["PHP_FILLTER_IBLOCK_PARAM_AFTER_TITLE"] = 'PHP: В конце цикла выборки элементов инфоблока';
$MESS["PHP_FILLTER_IBLOCK_PARAM_AFTER_NOTES"] = '<span style="color:red">ОСТРОЖНО: Необходимы знания php, использовать при необходимости расширения функционала.</span><br />
Подключается в конце цикла while, позволяет модифицировать полученные данные, добавить новые.<br />
Массив $EmailSend, хранит в себе данные конкретного сообщения.<br />
<br />
<b>Функции:</b> <br />
$phpIncludeFunction["isContinue"]="Y" - пропустить итерацию <br /> 
$phpIncludeFunction["isBreak"]="Y" - прервать работу цикла<br />   
<br />
<br />
<b>Отладку</b> можно вести через <a href="/bitrix/admin/php_command_line.php?lang=ru" target="_blank">коммандную PHP-строку</a> в тестовом режиме рассылки.<br />
';    



   
//перед циклом
$MESS["PHP_MESSAGE_FOREACH_BEFORE_TITLE"] = 'PHP: До цикла отправки сообщений';
$MESS["PHP_MESSAGE_FOREACH_BEFORE_NOTES"] = '<span style="color:red">ОСТРОЖНО: Необходимы знания php, использовать при необходимости расширения функционала.</span><br />
Подключается перед циклом отправки сообщений рассылки, можно использовать для выборки различных данных и использования их в процессе рассылки.<br />
Массив $arrEmailSend, хранит в себе данные отправляемых сообщений.  <br />
<br />
<br />
<b>Отладку</b> можно вести через <a href="/bitrix/admin/php_command_line.php?lang=ru" target="_blank">коммандную PHP-строку</a> в тестовом режиме рассылки.<br />
';

// в цикле
$MESS["PHP_MESSAGE_FOREACH_ITEM_BEFORE_TITLE"] = 'PHP: В начале цикла отправки сообщения';
$MESS["PHP_MESSAGE_FOREACH_ITEM_BEFORE_NOTES"] = '<span style="color:red">ОСТРОЖНО: Необходимы знания php, использовать при необходимости расширения функционала.</span><br />
Подключается в начале цикла foreach, позволяет модифицировать отправку добавив новые данные, массив с данными отправки.<br />
Массив $ItemEmailSend, хранит в себе данные конкретного сообщения которое будет отправлено.<br />
<br />
<b>Функции:</b> <br />
$phpIncludeFunction["isContinue"]="Y" - пропустить итерацию <br /> 
$phpIncludeFunction["isBreak"]="Y" - прервать работу цикла<br />   
<br />
<br />
<b>Отладку</b> можно вести через <a href="/bitrix/admin/php_command_line.php?lang=ru" target="_blank">коммандную PHP-строку</a> в тестовом режиме рассылки.<br />
';
// END



$MESS['SELECT_CHANGE'] = '--Выберите--';
  
  
  
$MESS["TABS_SOTBIT_MAILING_INSTRUCTION_TITLE"] = "Видео-инструкции"; 
$MESS["GROUP_TABS_SOTBIT_MAILING_INSTRUCTION_NAME"] = "Видео-инструкции"; 
$MESS["TABS_SOTBIT_MAILING_INSTRUCTION_DEFAULT"] = '
    <div style="text-align:center">
        <h3>Видео-урок: Маркетинговые рассылки - рекомендованные товары    </h3> 
        <iframe width="800" height="500" src="//www.youtube.com/embed/FQtNjHpho3U?list=PL2fR59TvIPXe8-iafhCqcLK4r1RxzqhST" frameborder="0" allowfullscreen></iframe>
        <br />
        <br />
        <br />
        <h3>Видео-урок: Маркетинговые рассылки - скидки  </h3> 
        <iframe width="800" height="500" src="//www.youtube.com/embed/EGtwhublnU4?list=PL2fR59TvIPXe8-iafhCqcLK4r1RxzqhST" frameborder="0" allowfullscreen></iframe>
        <br />  
        <br /> 
        <br />     
        <h3>Видео-урок: Маркетинговые рассылки общие настройки, расписание и исключения  </h3> 
        <iframe width="800" height="500" src="//www.youtube.com/embed/ygSlr97rlDo?list=PL2fR59TvIPXe8-iafhCqcLK4r1RxzqhST" frameborder="0" allowfullscreen></iframe>  
        <br />
        <br />  
        <br />      
        <h3>Видео-обзор: Маркетинговые рассылки</h3> 
        <iframe width="800" height="500" src="//www.youtube.com/embed/DYTnKHJAr70?list=PL2fR59TvIPXe8-iafhCqcLK4r1RxzqhST" frameborder="0" allowfullscreen></iframe>
        <br />
        <br />  
        <br />
        <br />       
        <h3>Если у вас возникли проблемы или вопросы с настройкой рассылок, смело пишите в <a href="http://www.sotbit.ru/support/" target="_blank">техническую поддержку</a> компании «Сотбит», мы обязательно вам поможем.</h3>
                                  
    </div>

';        
     
     
?>
