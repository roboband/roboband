<?
$module_id = 'sotbit.mailing'; 

$MESS[$module_id."_PAGE_TITLE"] = "Настройка рассылки #ID#";  


$MESS[$module_id."_edit10"] = "Общие настройки";  
$MESS[$module_id."_edit20"] = "Расписание и исключения";
$MESS[$module_id."_edit30"] = "Параметры рассылки";
                                                    
$MESS[$module_id."_OPTION_DEF_NANE"] = "Настройки";
 
$MESS[$module_id."_OPTION_10"] = "Общие настройки"; 

$MESS[$module_id."_OPTION_40"] = "Отправка писем по расписанию"; 
$MESS[$module_id."_OPTION_50"] = "Ограничение времени рассылки по расписанию"; 

$MESS[$module_id."_OPTION_60"] = "Исключать email из рассылки";


$MESS[$module_id.'_SELECT_PARAM_ALL'] = 'Все';
$MESS[$module_id.'_SELECT_PARAM_Y'] = 'Да';
$MESS[$module_id.'_SELECT_PARAM_N'] = 'Нет';


$MESS[$module_id."_ID_TITLE"] = "ID"; 
$MESS[$module_id."_DATE_LAST_RUN_TITLE"] = "Последний запуск рассылки"; 
$MESS[$module_id."_COUNT_RUN_TITLE"] = "Выпуск"; 
$MESS[$module_id."_TEMPLATE_TITLE"] = "Сценарий рассылки"; 
$MESS[$module_id."_NAME_TITLE"] = "Название рассылки"; 
$MESS[$module_id."_ACTIVE_TITLE"] = "Активность"; 
$MESS[$module_id."_DESCRIPTION_TITLE"] = "Описание рассылки"; 
$MESS[$module_id."_MODE_TITLE"] = "Режим работы";  
$MESS[$module_id."_MODE_NOTES"] = "При работе в тестовом режиме сообщения будут создаваться в системе, но без отправления к пользователю. <br />
Отправить их можно будет вручную на странице списка сообщений, либо удалить и сделать отправку рассылки в рабочем режиме.";  
$MESS[$module_id."_SITE_URL_TITLE"] = "URL сайта";  
$MESS[$module_id."_SITE_URL_NOTES"] = "Необходимо ввести ссылку на ваш сайт в формате http;//ВАШ_САЙТ.РУ <br />
 Этот адрес будет использоваться в ссылках.
"; 

$MESS[$module_id."_USER_AUTH_TITLE"] = "Авторизовать пользователя";  
$MESS[$module_id."_USER_AUTH_NOTES"] = "При включенном значение, пользователь будет авторизован при переходе по ссылке из  рассылки,  если будет  известен USER_ID в сценарии и MAILING_MESSAGE.
<br />Полезно при многих сценариях, позволяет облегчить путь пользователя до покупки..
"; 
    
$MESS[$module_id."_EVENT_SEND_SYSTEM_TITLE"] = "Система отправки сообщений";  

$MESS[$module_id."_AGENT_ID_TITLE"] = "ID агента";  
$MESS[$module_id."_AGENT_ACTIVE_TITLE"] = "Отправка писем по расписанию";  
$MESS[$module_id."_AGENT_INTERVAL_TITLE"] = "Интервал запуска";

$MESS[$module_id."_AGENT_INTERVAL_SELECT_300"] = "Раз 5 минут";  
$MESS[$module_id."_AGENT_INTERVAL_SELECT_900"] = "Раз 15 минут"; 
$MESS[$module_id."_AGENT_INTERVAL_SELECT_1800"] = "Раз 30 минут"; 
$MESS[$module_id."_AGENT_INTERVAL_SELECT_3600"] = "Раз в час"; 
$MESS[$module_id."_AGENT_INTERVAL_SELECT_7200"] = "Раз в 2 часа"; 
$MESS[$module_id."_AGENT_INTERVAL_SELECT_14400"] = "Раз в 4 часа"; 
$MESS[$module_id."_AGENT_INTERVAL_SELECT_28800"] = "Раз в 8 часов";
$MESS[$module_id."_AGENT_INTERVAL_SELECT_43200"] = "Раз в 12 часов";
$MESS[$module_id."_AGENT_INTERVAL_SELECT_86400"] = "Раз в день";
$MESS[$module_id."_AGENT_INTERVAL_SELECT_172800"] = "Раз в 2 дня";
$MESS[$module_id."_AGENT_INTERVAL_SELECT_259200"] = "Раз в 3 дня";
$MESS[$module_id."_AGENT_INTERVAL_SELECT_345600"] = "Раз в 4 дня";
$MESS[$module_id."_AGENT_INTERVAL_SELECT_604800"] = "Раз в неделю";
$MESS[$module_id."_AGENT_INTERVAL_SELECT_1209600"] = "Раз в 2 недели";
$MESS[$module_id."_AGENT_INTERVAL_SELECT_2678400"] = "Раз в месяц";
  
$MESS[$module_id."_AGENT_LAST_EXEC_TITLE"] = "Дата последнего запуска"; 
$MESS[$module_id."_AGENT_NEXT_EXEC_TITLE"] = "Дата и время следующего запуска"; 

$MESS[$module_id."_AGENT_TIME_START_TITLE"] = "Время рассылки сообщений с";         
$MESS[$module_id."_AGENT_TIME_END_TITLE"] = "Время рассылки сообщений до";         
        
       
$MESS[$module_id."_EXCLUDE_UNSUBSCRIBED_USER_TITLE"] = "Исключить email";       
$MESS[$module_id."_EXCLUDE_UNSUBSCRIBED_USER_NOTES"] = "Из рассылки будут исключены email:<br /> 
<b>Отписавшихся с общего списка</b> - все email адреса, которые есть на странице  <a target='_blank' href='sotbit_mailing_unsubscribed.php'>Отписавшиеся email</a>  <br />
<b>Отписавшихся по этой рассылке</b> - email адреса, связанные с данной рассылкой  <a target='_blank' href='sotbit_mailing_unsubscribed.php'>Отписавшиеся email</a>    <br /> 
<b>Не исключать</b> - email адреса исключены не будут <br /> 
<br />
Для того, чтобы у пользователей была возможность отписаться от рассылки, необходимо в шаблоне письма добавить к ссылке переменную ?MAILING_UNSUBSCRIBE=#MAILING_UNSUBSCRIBE#. <br />
Ссылка может вести на любую страницу вашего сайта.
";
     
$MESS[$module_id."_EXCLUDE_UNSUBSCRIBED_USER_MORE_TITLE"] = "Дополнительно Исключить email";      
$MESS[$module_id."_EXCLUDE_UNSUBSCRIBED_USER_MORE_NOTES"] = 'Выберите рассылки, отписавшиеся email от которых, должны быть исключены из данной рассылки.<br />
Действительно только при выбранном параметре  - "Отписавшихся по этой рассылке".
';         
       
$MESS[$module_id."_EXCLUDE_UNSUBSCRIBED_USER_VALUE_NO"] = "Не исключать";   
$MESS[$module_id."_EXCLUDE_UNSUBSCRIBED_USER_VALUE_ALL"] = "Отписавшихся с общего списка";  
$MESS[$module_id."_EXCLUDE_UNSUBSCRIBED_USER_VALUE_THIS"] = "Отписавшихся по этой рассылке";         
     
$MESS[$module_id."_EXCLUDE_DAYS_HOUR_TITLE"] = "Исключить дублирование письма (часов)";       
$MESS[$module_id."_EXCLUDE_DAYS_HOUR_NOTES"] = "Исключить из рассылки email, которые получали письма в течение (*) часов.<br />
Данный параметр поможет не надоедать пользователям частыми сообщениями с вашего сайта. <br />
При нулевом значение данный параметр игнорируется, письма будут идти без ограничений.
";       
     
  
$MESS[$module_id."_EVENT_SEND_SYSTEM_VALUE_BITRIX"] = "1С-Bitrix";  
$MESS[$module_id."_EVENT_SEND_SYSTEM_VALUE_UNISENDER"] = "Сервис UniSender";   
        
$MESS[$module_id."_MODE_VALUE_TEST"] = "Тестовый режим";  
$MESS[$module_id."_MODE_VALUE_WORK"] = "Рабочий режим"; 
 
$MESS[$module_id."_TEMPLATE_TYPE_SYSTEM"] = "Системный"; 
$MESS[$module_id."_TEMPLATE_TYPE_MY"] = "Собственный"; 

 
$MESS[$module_id."_PANEL_TOP_BACK_TITLE"] = "Список рассылок";  
$MESS[$module_id."_PANEL_TOP_ADD_NEW_TITLE"] = "Создать новую рассылку"; 
$MESS[$module_id."_PANEL_TOP_ADD_NEW_ALT"] = "Создать новую рассылку"; 

$MESS[$module_id."_PANEL_TOP_DELETE_TITLE"] = "Удалить"; 
$MESS[$module_id."_PANEL_TOP_DELETE_ALT"] = "Удалить рассылку"; 
$MESS[$module_id."_PANEL_TOP_DELETE_CONFORM"] = "Рассылка №#ID# будет полностью удалена из системы. Данные по отправленным письмам по данной рассылке будут удалены.";
    

$MESS[$module_id."_PANEL_TOP_START_TITLE"] = "Запустить рассылку"; 
$MESS[$module_id."_PANEL_TOP_START_CONFORM"] = "Вы уверены, что хотите запустить рассылку?";

$MESS[$module_id."_PANEL_TOP_STOP_TITLE"] = "Остановить рассылку";

$MESS[$module_id."_JS_OJIDANIE"] = "Загрузка..."; 

$MESS[$module_id."_JS_OTPRAV"] = "Отправлено"; 
$MESS[$module_id."_JS_IZ"] = "из"; 
$MESS[$module_id."_JS_PISEM"] = "писем"; 
$MESS[$module_id."_JS_SEND_MAILING"] = "<b>Отправлено:</b>"; 
$MESS[$module_id."_JS_NO_SEND_MAILING"] = "<b>Не отправлено - находятся в списках отписавшихся</b>";   
$MESS[$module_id."_JS_NO_SEND_UNSCRIBLE_MAILING"] = "<b>Не отправлено - исключены по дублированию письма:</b> ";  
$MESS[$module_id."_JS_SEND_END"] = "Рассылка завершена";  


$MESS[$module_id."_JS_MORE_INFO"] = " подробнее";

?>
