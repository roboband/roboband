<?
$iModuleID = 'sotbit.mailing';
$MESS[$iModuleID."_MESSAGE_LIST_TITLE"] = "Отписавшиеся email";

$MESS[$iModuleID."_list_title_ID"] = "ID";
$MESS[$iModuleID."_list_title_ACTIVE"] = "Активность"; 
$MESS[$iModuleID."_list_title_DATE_CREATE"] = "Дата создания";  
$MESS[$iModuleID."_list_title_ID_MESSEGE"] = "ID сообщения";  
$MESS[$iModuleID."_list_title_ID_EVENT"] = "Рассылка";  
$MESS[$iModuleID."_list_title_EMAIL_TO"] = "E-mail"; 


  
$MESS[$iModuleID."_list_title_NO_INFO"] = "Неизвестно";  
  
  
$MESS[$iModuleID."_ANY"] = "Любой"; 
$MESS[$iModuleID."_YES"] = "Да";  
$MESS[$iModuleID."_NO"] = "Нет";  

$MESS[$iModuleID."_list_title_TEMPLATE_DELETE"] = "Тип рассылки был удален из системы.<br />Действия по данной рассылке будут запрещены до восстановления шаблона ";  


$MESS[$iModuleID."_ACTION_EDIT"] = "Изменить";
$MESS[$iModuleID."_ACTION_DELETE"] = "Удалить";


$MESS[$iModuleID."_ACTION_DELETE_CONFORM"] = "Вы уверены, что хотите удалить данные об этом сообщение.";


   
$MESS[$iModuleID."_MENU_TOP_MAILING_ADD"] = "Добавить email"; 
$MESS[$iModuleID."_MENU_TOP_MAILING_ADD_ALT"] = "Добавить email, отписавшийся от рассылки";



$MESS[$iModuleID."_FOOTER_ACTION_ACTIVATE"] = "Активировать"; 
$MESS[$iModuleID."_FOOTER_ACTION_DEACTIVATE"] = "Деактивировать"; 
$MESS[$iModuleID."_FOOTER_ACTION_DEL"] = "Удалить"; 

  
?>