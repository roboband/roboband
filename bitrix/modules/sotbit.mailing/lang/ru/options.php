<?
$module_id = 'sotbit.mailing'; 
$MESS[$module_id."_edit10"] = "Общие настройки"; 
$MESS[$module_id."_edit20"] = "Шаблон письма";

$MESS[$module_id."_OPTION_10"] = "Настройки отправки писем"; 
$MESS[$module_id."_OPTION_20"] = "Интеграци с UniSender.com"; 
$MESS[$module_id."_OPTION_30"] = "Интеграци с Mailchimp.com"; 
$MESS[$module_id."_OPTION_40"] = "Кэширование модуля";
$MESS[$module_id."_OPTION_50"] = "Шаблон письма по умолчанию";
$MESS[$module_id."_OPTION_60"] = "Оптимизация отправки сообщений";

$MESS[$module_id."_OPTION_100"] = "Шаблон шапки письма по умолчанию";
$MESS[$module_id."_OPTION_110"] = "Шаблон подвала письма по умолчанию";

$MESS[$module_id."_EMAIL_FROM_TITLE"] = "E-mail, от которого будут отсылаться письма"; 
$MESS[$module_id."_UNSUBSCRIBE_PAGE_TITLE"] = "Страница отписаться от рассылки"; 
$MESS[$module_id."_UNSENDER_API_KEY_TITLE"] = "API ключ к сервису <a target='_blank' href='http://www.unisender.com'>UniSender</a>"; 
$MESS[$module_id."_UNSENDER_SENDER_NAME_TITLE"] = "Имя отправителя"; 
$MESS[$module_id."_UNSENDER_SENDER_EMAIL_TITLE"] = "E-mail отправителя"; 
$MESS[$module_id."_UNSENDER_SENDER_EMAIL_NOTES"] = "E-mail адрес отправителя. <br />
Этот e-mail должен быть проверен (для этого надо создать вручную хотя бы одно письмо с этим обратным адресом через веб-интерфейс, затем нажать на ссылку «отправьте запрос подтверждения» и перейти по ссылке из письма)."; 
             
$MESS[$module_id."_UNSENDER_WRAP_TYPE_TITLE"] = "Выравнивать письма"; 

$MESS[$module_id."_UNSENDER_WRAP_TYPE_VALUE_center"] = "Выравнивание по центру"; 
$MESS[$module_id."_UNSENDER_WRAP_TYPE_VALUE_right"] = "Выравнивание по правому краю"; 
$MESS[$module_id."_UNSENDER_WRAP_TYPE_VALUE_left"] = "Выравнивание по левому краю"; 


$MESS[$module_id."_MAILCHIMP_API_KEY_TITLE"] = "API ключ к сервису <a target='_blank' href='http://kb.mailchimp.com/article/where-can-i-find-my-api-key'>mailchimp</a>"; 

$MESS[$module_id."_MANAGED_CACHE_ON_TITLE"] = "Включить кеширование модуля"; 

$MESS[$module_id."_TEMPLATE_MAILING_DEF_TITLE"] = "Путь до шаблона"; 
$MESS[$module_id."_TEMPLATE_MAILING_DEF_NOTES"] = "Необходимо для задания шаблона по умолчанию в создаваемые сценарии, задается от корня сайта<br />
Путь по умолчанию: /bitrix/components/sotbit/sotbit.mailing.logic/lang/ru/tpl_mailing/default.php. 
"; 


$MESS[$module_id."_MAILING_PACKAGE_COUNT_TITLE"] = "Количество сообщений в пакете"; 
$MESS[$module_id."_MAILING_PACKAGE_COUNT_NOTES"] = "Настройка определяет сколько сообщений вы будете отправлять с сервера за один сеанс, по умолчанию 1000 сообщений."; 

$MESS[$module_id."_MAILING_MESSAGE_SLAAP_TITLE"] = "Время задержки между сообщениями"; 
$MESS[$module_id."_MAILING_MESSAGE_SLAAP_NOTES"] = "Настройка определяет время задержки между отправки сообщений, по умолчанию 0.001. <br /> Позволяет снизить нагрузку на сервер.";
?>