<?IncludeModuleLangFile(__FILE__);     
class CSotbitMailingHelp { 
 
    CONST EVENT_ADD_MAILING = "SOTBIT_MAILING_EVENT_SEND";
    CONST MODULE_ID = "sotbit.mailing";
    CONST CACHE_TIME_TOOLS = 1800; 
   
 
    function CacheConstantCheck() { 
        if(COption::GetOptionString("sotbit.mailing", "MANAGED_CACHE_ON", "Y") == "Y")
        {
            define("BX_COMP_MANAGED_CACHE", true);
        } 
        elseif(COption::GetOptionString("sotbit.mailing", "MANAGED_CACHE_ON") == "N") 
        {
            define("BX_COMP_MANAGED_CACHE", false);    
        }        
    }
  
    //������� ������ �� ��������
    //START
    function GetMailingInfo() {
       
        CSotbitMailingHelp::CacheConstantCheck();
        
        $obCache = new CPHPCache();
        $cache_dir = '/'.self::MODULE_ID.'_GetMailingInfo';
        //$cache_dir = '/'.self::MODULE_ID;  
        $cache_id = self::MODULE_ID.'|GetMailingInfo|';  
        if($obCache->InitCache(self::CACHE_TIME_TOOLS,$cache_id,$cache_dir))// ���� ��� �������
        {
           $vars = $obCache->GetVars();// ���������� ���������� �� ����   
        }
        elseif($obCache->StartDataCache())// ���� ��� ���������
        {     
           global $CACHE_MANAGER;
           $CACHE_MANAGER->StartTagCache($cache_dir);             
           $vars = array();

           $select = array(
                'ID',
                'ACTIVE',
                'NAME',
                'MODE',
                'EVENT_TYPE',
                'COUNT_RUN',
                'SITE_URL',
                'USER_AUTH',
                'EVENT_SEND_SYSTEM',
                'EVENT_SEND_SYSTEM_CODE',
                'EXCLUDE_HOUR_AGO',
                'EXCLUDE_UNSUBSCRIBED_USER',
                'EXCLUDE_UNSUBSCRIBED_USER_MORE'
           );
           
           $resData = CSotbitMailingEvent::GetList(array('ID'=>'ASC'),array(),false,$select);
           while($arrData = $resData->Fetch()) {
                $vars[$arrData['ID']] = $arrData;                
           }           
           
                     
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID.'_GetMailingInfo');  
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID);  
           $CACHE_MANAGER->EndTagCache();           
               
           $obCache->EndDataCache($vars);// ��������� ���������� � ���.   
        }      
        return $vars;      
    }
    //END      
   
    //������� ������ �� ���������� ��������
    //START
    function GetCategoriesInfo() {
        CSotbitMailingHelp::CacheConstantCheck();       
        $obCache = new CPHPCache();
        $cache_dir = '/'.self::MODULE_ID.'_GetCategoriesInfo';
        //$cache_dir = '/'.self::MODULE_ID;  
        $cache_id = self::MODULE_ID.'|GetCategoriesInfo|';  
        if( $obCache->InitCache(self::CACHE_TIME_TOOLS,$cache_id,$cache_dir))// ���� ��� �������
        {
           $vars = $obCache->GetVars();// ���������� ���������� �� ����   
        }
        elseif($obCache->StartDataCache())// ���� ��� ���������
        {     
           global $CACHE_MANAGER;
           $CACHE_MANAGER->StartTagCache($cache_dir);             
           $vars = array();

           $resData = CSotbitMailingCategories::GetList(array('ID'=>'ASC'),array(),false, array());
           while($arrData = $resData->Fetch()) {
                $vars[$arrData['ID']] = $arrData;                
           }           
           
                     
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID.'_GetCategoriesInfo');  
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID);  
           $CACHE_MANAGER->EndTagCache();           
               
           $obCache->EndDataCache($vars);// ��������� ���������� � ���.   
        }      
        return $vars;      
    }
    //END    
   
    //������� ������� ��������
    //START
    function GetEventTemplate($ID_EVENT) {
        CSotbitMailingHelp::CacheConstantCheck();       
        $obCache = new CPHPCache();
        $cache_dir = '/'.self::MODULE_ID.'_GetEventTemplate_'.$ID_EVENT;
        //$cache_dir = '/'.self::MODULE_ID;  
        $cache_id = self::MODULE_ID.'|GetEventTemplate|'.$ID_EVENT;  
        if( $obCache->InitCache(self::CACHE_TIME_TOOLS,$cache_id,$cache_dir))// ���� ��� �������
        {
           $vars = $obCache->GetVars();// ���������� ���������� �� ����   
        }
        elseif($obCache->StartDataCache())// ���� ��� ���������
        {     
           global $CACHE_MANAGER;
           $CACHE_MANAGER->StartTagCache($cache_dir);             
           $vars = array();

           $resData = CSotbitMailingMessageTemplate::GetList(array('COUNT_START'=>'DESC'),array('ID_EVENT' => $ID_EVENT) ,false,array()); 
           while($arrData = $resData->Fetch()) {
                $vars[$arrData['ID']] = $arrData;                
           }          
                  
                      
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID.'_GetEventTemplate_'.$ID_EVENT);  
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID);  
           $CACHE_MANAGER->EndTagCache();           
               
           $obCache->EndDataCache($vars);// ��������� ���������� � ���.   
        }      
        return $vars;      
    }
    //END       

    //������� ������ email ��������� �� ������� ���� ���������� � ������������ ����������
    function GetEmailSendMessageTimeNoCache($SETTING=array()) { 
        
        $ID_EVENT = $SETTING['ID_EVENT'];
        $COUNT_RUN = $SETTING['COUNT_RUN'];
        $EXCLUDE_HOUR_AGO = $SETTING['EXCLUDE_HOUR_AGO'];
        
        $vars = array();
        //������� ����
        $mkNow = mktime(date('H'),date('i')+20,date('s'),date('m'),date('j'),date('Y'));  
        $mkAgo = mktime(date('H')-$EXCLUDE_HOUR_AGO,date('i')-20,date('s'),date('m'),date('j'),date('Y'));  

        $FillterMessage = array(
            'ID_EVENT' => $ID_EVENT,
            ">=DATE_CREATE" => date('d.m.Y H:i:s', $mkAgo),     
            "<=DATE_CREATE" => date('d.m.Y H:i:s', $mkNow)
        );  
        
        if($COUNT_RUN){
            $FillterMessage['<=COUNT_RUN'] = $COUNT_RUN;    
        }                                                   
           
        $resData = CSotbitMailingMessage::GetList(array(),$FillterMessage,false,array('ID','EMAIL_TO','DATE_SEND'));
        while($arrData = $resData->Fetch()) {
            $arrData['EMAIL_TO'] = trim($arrData['EMAIL_TO']);
            $vars[$arrData['EMAIL_TO']] = $arrData['ID'];                
        }      
          
        return $vars;                  
    }
    
    //������� ������ email ��������� �� ������� ���� ���������� � ������������ ���������� � �����
    //START
    function GetEmailSendMessageTime($SETTING=array()) {
        
        $ID_EVENT = $SETTING['ID_EVENT'];
        $COUNT_RUN = $SETTING['COUNT_RUN'];
        $EXCLUDE_HOUR_AGO = $SETTING['EXCLUDE_HOUR_AGO'];
        
        CSotbitMailingHelp::CacheConstantCheck();
        $obCache = new CPHPCache();
        $cache_dir = '/'.self::MODULE_ID.'_GetEmailSendMessageTime_'.$ID_EVENT.'_'.$COUNT_RUN;
        //$cache_dir = '/'.self::MODULE_ID;  
        $cache_id = self::MODULE_ID.'|GetEmailSendMessageTime|'.$ID_EVENT.'|'.$COUNT_RUN;  
        if( $obCache->InitCache(self::CACHE_TIME_TOOLS,$cache_id,$cache_dir))// ���� ��� �������
        {
           $vars = $obCache->GetVars();// ���������� ���������� �� ����   
        }
        elseif($obCache->StartDataCache())// ���� ��� ���������
        {     
           global $CACHE_MANAGER;
           $CACHE_MANAGER->StartTagCache($cache_dir);  
                
           $vars = array();
           $vars = CSotbitMailingHelp::GetEmailSendMessageTimeNoCache($SETTING);
                   
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID.'_GetEmailSendMessageTime_'.$ID_EVENT.'_'.$COUNT_RUN);  
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID);  
           $CACHE_MANAGER->EndTagCache();           
               
           $obCache->EndDataCache($vars);// ��������� ���������� � ���.   
        }    
   
          
        return $vars;      
        
    }
    //END 
         
   
    //������� �������� �� ID
    //START
    function GetSiteId() {
        CSotbitMailingHelp::CacheConstantCheck();       
        $obCache = new CPHPCache();
        $cache_dir = '/'.self::MODULE_ID.'_GetSiteId';
        //$cache_dir = '/'.self::MODULE_ID;  
        $cache_id = self::MODULE_ID.'|GetSiteId|';  
        if( $obCache->InitCache(self::CACHE_TIME_TOOLS,$cache_id,$cache_dir))// ���� ��� �������
        {
           $vars = $obCache->GetVars();// ���������� ���������� �� ����   
        }
        elseif(CModule::IncludeModule("iblock") && $obCache->StartDataCache())// ���� ��� ���������
        {     
           global $CACHE_MANAGER;
           $CACHE_MANAGER->StartTagCache($cache_dir);             
           $vars = array();

           $rsSites = CSite::GetList($by="sort", $order="desc", Array());
           while ($arS = $rsSites->Fetch())
           {
                $vars[] = $arS['LID'];  
           }              
           
           
                     
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID.'_GetSiteId');  
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID);  
           $CACHE_MANAGER->EndTagCache();           
               
           $obCache->EndDataCache($vars);// ��������� ���������� � ���.   
        }      
        return $vars;      
    }
    //END     
   
    //������� ������ ������������ �� �������� ��� ����
    //START
    function GetUnsubscribedAllMailingNoCache() { 

        $vars = array();
        $resSearch = CSotbitMailingUnsubscribed::GetList(array(), array("ACTIVE" => "Y"), false, array('ID','EMAIL_TO'));
        while ($arrSearch = $resSearch->Fetch())
        {
            $arrSearch['EMAIL_TO'] = trim($arrSearch['EMAIL_TO']);               
            $vars[$arrSearch['EMAIL_TO']] = $arrSearch['ID'];  
        } 
        
        return $vars;
     
         
        
    }    
    //END       
   
   
    // ������� ������ ������������ ���� ��������� - ��� ����
    //START
    function GetUnsubscribedByMailing_NoCache($ID_EVENT=false) { 

        $vars = array();
        $resSearch = CSotbitMailingUnsubscribed::GetList(array(), array('ID_EVENT' => $ID_EVENT, "ACTIVE" => "Y"), false, array('ID','EMAIL_TO'));
        while ($arrSearch = $resSearch->Fetch())
        {
            $arrSearch['EMAIL_TO'] = trim($arrSearch['EMAIL_TO']);
            $vars[$arrSearch['EMAIL_TO']] = $arrSearch['ID'];  
        }   
        
        return $vars;

    }    
    //END   
       
   
    // ������� ������ ������������ �� ��������
    // START
    function GetUnsubscribedByMailing($ID_EVENT=false) {
        CSotbitMailingHelp::CacheConstantCheck();                       
        $obCache = new CPHPCache();
        $cache_dir = '/'.self::MODULE_ID.'_GetUnsubscribedByMailing_'.$ID_EVENT;
        //$cache_dir = '/'.self::MODULE_ID;  
        $cache_id = self::MODULE_ID.'|GetUnsubscribedByMailing|'.$ID_EVENT;  
        if( $obCache->InitCache(self::CACHE_TIME_TOOLS,$cache_id,$cache_dir))// ���� ��� �������
        {
           $vars = $obCache->GetVars();// ���������� ���������� �� ����   
        }
        elseif($obCache->StartDataCache())// ���� ��� ���������
        {     
           global $CACHE_MANAGER;
           $CACHE_MANAGER->StartTagCache($cache_dir);             
 
           $vars = array();
           $vars = CSotbitMailingHelp::GetUnsubscribedByMailing_NoCache($ID_EVENT);            
                   
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID.'_GetUnsubscribedByMailing_'.$ID_EVENT);  
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID);  
           $CACHE_MANAGER->EndTagCache();           
               
           $obCache->EndDataCache($vars);// ��������� ���������� � ���.   
        }      
        return $vars;      
    } 
    // END   
        
    //������� ������ ������������ ���� ��������� - ��� ����
    //START
    function GetUnsubscribedAllMailing_NoCache() { 

        $vars = array();
        $resSearch = CSotbitMailingUnsubscribed::GetList(array(), array("ACTIVE" => "Y"), false, array('ID','EMAIL_TO'));
        while ($arrSearch = $resSearch->Fetch())
        {
            $arrSearch['EMAIL_TO'] = trim($arrSearch['EMAIL_TO']);               
            $vars[$arrSearch['EMAIL_TO']] = $arrSearch['ID'];  
        }   
        
        return $vars;

    }    
    //END    
        
                  
    // ������� ������ ������������ ���� ���������
    // START
    function GetUnsubscribedAllMailing() {
        CSotbitMailingHelp::CacheConstantCheck();       
        $obCache = new CPHPCache();
        $cache_dir = '/'.self::MODULE_ID.'_GetUnsubscribedAllMailing';
        //$cache_dir = '/'.self::MODULE_ID;  
        $cache_id = self::MODULE_ID.'|GetUnsubscribedAllMailing|';  
        if( $obCache->InitCache(self::CACHE_TIME_TOOLS,$cache_id,$cache_dir))// ���� ��� �������
        {
           $vars = $obCache->GetVars();// ���������� ���������� �� ����   
        }
        elseif($obCache->StartDataCache())// ���� ��� ���������
        {     
           global $CACHE_MANAGER;
           $CACHE_MANAGER->StartTagCache($cache_dir);             

           
           $vars = array();
           $vars = CSotbitMailingHelp::GetUnsubscribedAllMailing_NoCache();           
                      
                   
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID.'_GetUnsubscribedAllMailing');  
           $CACHE_MANAGER->RegisterTag(self::MODULE_ID);  
           $CACHE_MANAGER->EndTagCache();           
               
           $obCache->EndDataCache($vars);// ��������� ���������� � ���.   
        }      
        return $vars;      
    } 
    // END

    //������� ��� ��������� ���� �� ��������� �������
    function GetDateAgoNow($arFields) {
        
        if(!isset($arFields['from']) || !isset($arFields['to']) || !isset($arFields['type'])){
            return false;    
        }
        global $DB; 
        $result = array(); 
        if($arFields['type']=='MIN'){
            $result['to'] = date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")),  mktime(date("H"), date("i")-$arFields['from'], 0,  date("n"), date("d"), date("Y")));    
            $result['from'] = date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")),  mktime(date("H"), date("i")-$arFields['to'], 0,  date("n"), date("d"), date("Y")));    
        }        
        elseif($arFields['type']=='HOURS'){
            $result['to'] = date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")),  mktime(date("H")-$arFields['from'], date("i"), 0,  date("n"), date("d"), date("Y")));    
            $result['from'] = date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")),  mktime(date("H")-$arFields['to'], date("i"), 0,  date("n"), date("d"), date("Y")));             
        }   
        elseif($arFields['type']=='DAYS'){
            $result['to'] = date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")),  mktime(date("H"), date("i"), 0,  date("n"), date("d")-$arFields['from'], date("Y")));    
            $result['from'] = date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")),  mktime(date("H"), date("i"), 0,  date("n"), date("d")-$arFields['to'], date("Y")));              
        }  
        elseif($arFields['type']=='MONTH'){
            $result['to'] = date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")),  mktime(date("H"), date("i"), 0,  date("n")-$arFields['from'], date("d"), date("Y")));    
            $result['from'] = date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")),  mktime(date("H"), date("i"), 0,  date("n")-$arFields['to'], date("d"), date("Y")));               
        }    
        
        return $result;              
                        
    }   
    
    
   
    // �������� � �������� ��� ��������� �������
    // START
    function EventMessageCheck($EVENT_TYPE=false) {
        
        if(empty($EVENT_TYPE)){
            $EVENT_TYPE = self::EVENT_ADD_MAILING;    
        }
        
        $arFilter = array(
            "TYPE_ID" => $EVENT_TYPE,
            "LID"     => "ru"
        );
        $arET = CEventType::GetList($arFilter)->Fetch();

        if(!$arET)
        {
            
            $arrSite = array();
            $rsSites = CSite::GetList($by="sort", $order="desc", Array());
            while ($arS = $rsSites->Fetch())
            {
                $arrSite[] = $arS['LID'];  
            }            
            
            $et = new CEventType;
            $et->Add(array(
                "LID"           => "ru",
                "EVENT_NAME"    => $EVENT_TYPE,
                "NAME"          => GetMessage(self::MODULE_ID.'_EVENT_NAME'),
                "DESCRIPTION"   => ''
            ));
            $arFields = array(
                "ACTIVE" => "Y",
                "EVENT_NAME" => $EVENT_TYPE,
                "LID" => $arrSite,
                "EMAIL_FROM" => "#EMAIL_FROM#",
                "EMAIL_TO" => "#EMAIL_TO#",
                "BCC" => "",
                "SUBJECT" => "#SUBJECT#",
                "BODY_TYPE" => "html",
                "MESSAGE" => "#MESSEGE#"
            );//return;
            $mes = new CEventMessage;
            $mesID = $mes->Add($arFields);
            
            return $EVENT_TYPE;
        }  
        return $arET['EVENT_NAME'];      
    }
    // END
    
    //������� ������� ������ ��������� �� �������
    //START    
    /* */
    function MessageCheck($SETTING) {
        
        $arParams = $SETTING['arParams'];
        $arrEmailSend = $SETTING['arrEmailSend'];     
        $COUNT_ALL = count($arrEmailSend); 
                           
           
        if(empty($arParams["MAILING_EVENT_ID"]) && empty($arParams['MAILING_COUNT_RUN'])){
             $arrEmailSend = array();  
             return $arrEmailSend;                
        }
        
        $arrProgress = CSotbitMailingHelp::ProgressFileGetArray($arParams["MAILING_EVENT_ID"], $arParams['MAILING_COUNT_RUN']);   
        $arrProgress['COUNT_ALL'] = $COUNT_ALL;
       
        // ���� �� ���������� ����� �� ��������
        // START 
            //�� ������� ������������ � ���������� ��������
            if($arParams['MAILING_EXCLUDE_HOUR_AGO'] != 0) { 
                $setting_HourAgoEmail = array(
                    'ID_EVENT'=> $arParams['MAILING_EVENT_ID'],
                    'EXCLUDE_HOUR_AGO'=> $arParams['MAILING_EXCLUDE_HOUR_AGO'],
                    'COUNT_RUN' => $arParams['MAILING_COUNT_RUN']
                );
                $HourAgoEmail = CSotbitMailingHelp::GetEmailSendMessageTimeNoCache($setting_HourAgoEmail);   
                $arrProgress_EMAIL_TO_EXCLUDE_HOUR_AGO = 0;
                $EMAIL_TO_EXCLUDE_HOUR_AGO_INFO = '';
                foreach($arrEmailSend as $k => $ItemEmailSend) { 
                    $EmailSend = array();
                    $EmailSend['EMAIL_TO'] = CSotbitMailingHelp::ReplaceVariables($arParams["EMAIL_TO"] , $ItemEmailSend);  
                    $EmailSend['EMAIL_TO'] = trim($EmailSend['EMAIL_TO']);
                    if($HourAgoEmail[$EmailSend['EMAIL_TO']]){
                        $EMAIL_TO_EXCLUDE_HOUR_AGO_INFO .=  $EmailSend['EMAIL_TO']."\n";
                        $arrProgress_EMAIL_TO_EXCLUDE_HOUR_AGO++;
                        //unset($arrEmailSend[$k]); 
                        $arrEmailSend[$k]['isContinue'] = 'Y';  
                    }

                } 
                
                if($EMAIL_TO_EXCLUDE_HOUR_AGO_INFO){
                    $arrProgress_more['EMAIL_TO_EXCLUDE_HOUR_AGO_INFO'] = $EMAIL_TO_EXCLUDE_HOUR_AGO_INFO;                    
                }
                $arrProgress['EMAIL_TO_EXCLUDE_HOUR_AGO'] =  $arrProgress_EMAIL_TO_EXCLUDE_HOUR_AGO;        
            }
        //END   
 
 
        //�������� �������� �� ���������� ������ ������ ���� ����� ���� � ������
        //START   
        if($arParams['MAILING_EXCLUDE_UNSUBSCRIBED_USER'] == 'ALL') {
            $UnsubscribeEmail = CSotbitMailingHelp::GetUnsubscribedAllMailing_NoCache();  
        } 
        elseif($arParams['MAILING_EXCLUDE_UNSUBSCRIBED_USER'] == 'THIS') {
            $UnsubscribeEmail = CSotbitMailingHelp::GetUnsubscribedByMailing_NoCache($arParams["MAILING_EVENT_ID"]);  
            //������� ������ �� ������� 
            foreach($arParams['MAILING_EXCLUDE_UNSUBSCRIBED_USER_MORE'] as $event_id) {
                $UnsubscribeEmailMore = CSotbitMailingHelp::GetUnsubscribedByMailing_NoCache($event_id);        
                $UnsubscribeEmail = array_merge($UnsubscribeEmail,$UnsubscribeEmailMore); 
            }
                 
        }
        //�������� �������� ������
        $arrProgress_EMAIL_TO_EXCLUDE_UNSUBSCRIBED = 0;
        $EMAIL_TO_EXCLUDE_UNSUBSCRIBED_INFO = '';
        foreach($arrEmailSend as $k => $ItemEmailSend) { 
            $EmailSend = array();
            $EmailSend['EMAIL_TO'] = CSotbitMailingHelp::ReplaceVariables($arParams["EMAIL_TO"] , $ItemEmailSend);  
            $EmailSend['EMAIL_TO'] = trim($EmailSend['EMAIL_TO']);
            if($UnsubscribeEmail[$EmailSend['EMAIL_TO']]){
                $EMAIL_TO_EXCLUDE_UNSUBSCRIBED_INFO .=  $EmailSend['EMAIL_TO']."\n";
                $arrProgress_EMAIL_TO_EXCLUDE_UNSUBSCRIBED++;
                //unset($arrEmailSend[$k]);   
                $arrEmailSend[$k]['isContinue'] = 'Y'; 
            }
        }  
        
        if($EMAIL_TO_EXCLUDE_UNSUBSCRIBED_INFO){
            $arrProgress_more['EMAIL_TO_EXCLUDE_UNSUBSCRIBED_INFO'] = $EMAIL_TO_EXCLUDE_UNSUBSCRIBED_INFO;            
        }
        $arrProgress['EMAIL_TO_EXCLUDE_UNSUBSCRIBED'] =  $arrProgress_EMAIL_TO_EXCLUDE_UNSUBSCRIBED;                   
        //END         
        
        

        
        //�������� ��������
        //START   
            
        //������ �� ������ �� ������� ����������
        foreach($arrEmailSend as $k => $ItemEmailSend){ 
            if($arrEmailSend[$k]['isContinue']=='Y') {
                unset($arrEmailSend[$k]); 
                $arrProgress['COUNT_ALL'] = $arrProgress['COUNT_ALL']-1;   
            }    
        }
                    
            
        //������ ������ �� ������� ��� ���������     
        $c=0;
        foreach($arrEmailSend as $k => $ItemEmailSend){
            $c++; 
            if($c>=$arParams['MAILING_MAILING_WORK_COUNT']){
                break;    
            } else{
                unset($arrEmailSend[$k]);
            }           
        } 
        
         
        //END          
        
        if($arrProgress['COUNT_ALL']==0){
            $arrProgress['COUNT_ALL'] = 0;
            $arrProgress['COUNT_NOW'] = 0;
            $arrProgress['COUNT_SEND'] = 0;   
        }        
        
        
        CSotbitMailingHelp::ProgressFile($arParams["MAILING_EVENT_ID"], $arParams['MAILING_COUNT_RUN'], $arrProgress, $arrProgress_more);     
                
        
        return $arrEmailSend;  
        
    }   
    //END
    
    //������� ��� ������� � UniSender
    //START
    function QueryUniSender($Metod=false, $Fields=array())
    {
        
       // �������� ����
       $data = array(
          'api_key' => COption::GetOptionString('sotbit.mailing', 'UNSENDER_API_KEY'),
          'format' => 'json'
       );
       // ��������� ������
       if(is_array($Fields)) {
           $data = array_merge($data,$Fields);           
       }       
       // ������� ������ 
       $postdata = http_build_query($data);   
       $Response = QueryGetData(
            'api.unisender.com',
            80,
            '/ru/api/'.$Metod.'?',
            $postdata,
            $error_number,
            $error_text ,
            'POST'
        );       
        $arrResponse = json_decode($Response,true);       
        return $arrResponse;    
    }      
    //END
    
    
    //������� ��������� �� Uniseder
    //START
    function UniSenderExportContact($CATEGORIES_ID, $LIST_UNISENDER)
    {
        
        $arrayUser = array();
        $iteration = 500;
        $d = 0;
        //������� ������
        while($d <= 1000000)
        {
            
             $arrUniseder = array(
                'list_id' => $LIST_UNISENDER,
                'limit' => $iteration,
                'offset' => $d
            );
            $ExportList = CSotbitMailingHelp::QueryUniSender('exportContacts',$arrUniseder);    
            foreach($ExportList['result']['data'] as $item) {
                
                $arrayUser[] = array(
                    'EMAIL_TO' => $item[0],
                    'CATEGORIES_ID' => array($CATEGORIES_ID)
                );   
                $d++;         
            } 
            if(count($ExportList['result']['data']) < $iteration) {
                break;    
            }
                   
        }
        
        $count=0;
        foreach($arrayUser as $exportInfo) {
            
            $exportInfo['SOURCE'] = 'UNISENDER_EXPORT';
                     
            $ANSWER = CsotbitMailingSubTools::AddSubscribers($exportInfo, array('UNISENDER_EXPORT'=>'Y'));
            if(is_numeric($ANSWER['ID_SUBSCRIBERS'])) {
                $count++;    
            }    
        }
         
        $result = array(
            'COUNT' => $count,
            'STATUS' => 'OK'
        ); 
        return $result;     
           
    }      
    //END    
    
   
    function ProgressFile($EVENT_ID=false, $EVENT_COUNT_RUN=0,$SETTING=array(), $MORE_INFO=array()) {    
        $file_path =  $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/sotbit_mailing_progress_".$EVENT_ID."_".$EVENT_COUNT_RUN.".json";
        // �������� �����
        $file_path_dir = $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/";
        if(!file_exists($file_path_dir)) {
            mkdir($file_path_dir, 0777, true);
        }       
        
        
        
        
        
        
        if(!$EVENT_ID && !$EVENT_COUNT_RUN) {
            return false;    
        }
        $json = json_encode($SETTING);
        
        $f = fopen($file_path, "w");
        fwrite($f, $json); 
        // ������� ��������� ����
        fclose($f);     
        
        
        //������� � ���� ����������
        //START
        if($MORE_INFO['EMAIL_TO_SEND_INFO']){
       
            $file = $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/sotbit_mailing_progress_".$EVENT_ID."_".$EVENT_COUNT_RUN."_EMAIL_TO_SEND.txt";
            $current = file_get_contents($file);
            $current .= $MORE_INFO['EMAIL_TO_SEND_INFO']."\n";
            // ����� ���������� ������� � ����
            file_put_contents($file, $current);          
        }
        
        if($MORE_INFO['EMAIL_TO_EXCLUDE_HOUR_AGO_INFO']){
       
            $file = $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/sotbit_mailing_progress_".$EVENT_ID."_".$EVENT_COUNT_RUN."_EMAIL_TO_EXCLUDE_HOUR_AGO.txt";
            $current = file_get_contents($file);
            $current .= $MORE_INFO['EMAIL_TO_EXCLUDE_HOUR_AGO_INFO']."\n";
            // ����� ���������� ������� � ����
            file_put_contents($file, $current);          
        }  
        
        if($MORE_INFO['EMAIL_TO_EXCLUDE_UNSUBSCRIBED_INFO']){
       
            $file = $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/sotbit_mailing_progress_".$EVENT_ID."_".$EVENT_COUNT_RUN."_EMAIL_TO_EXCLUDE_UNSUBSCRIBED.txt";
            $current = file_get_contents($file);
            $current .= $MORE_INFO['EMAIL_TO_EXCLUDE_UNSUBSCRIBED_INFO']."\n";
            // ����� ���������� ������� � ����
            file_put_contents($file, $current);          
        }               
        //END           
                              
    }    
       
   
    function ProgressFileGetArray($EVENT_ID=0, $EVENT_COUNT_RUN=0) {    
        
        $file_path =  $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/sotbit_mailing_progress_".$EVENT_ID."_".$EVENT_COUNT_RUN.".json";
            
        if(!$EVENT_ID && !$EVENT_COUNT_RUN) {
            return false;    
        }    
        $f = fopen($file_path, "r+");
        $arr = fgets($f); 
        $info = json_decode($arr);
        // ������� ��������� ����
        fclose($f);   
        $info = (array)$info;
        return $info ;                                        
    }    
   
    function ProgressFileDelete($EVENT_ID=0, $EVENT_COUNT_RUN=0) {    
        $file_path = $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/sotbit_mailing_progress_".$EVENT_ID."_".$EVENT_COUNT_RUN.".json";                    
        unlink($file_path);   
        
        //������ �������������� ����������
        $file_path = $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/sotbit_mailing_progress_".$EVENT_ID."_".$EVENT_COUNT_RUN."_EMAIL_TO_SEND.txt";    
        unlink($file_path);      
        
        $file_path = $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/sotbit_mailing_progress_".$EVENT_ID."_".$EVENT_COUNT_RUN."_EMAIL_TO_EXCLUDE_HOUR_AGO.txt";    
        unlink($file_path); 
        
        $file_path = $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/sotbit_mailing_progress_".$EVENT_ID."_".$EVENT_COUNT_RUN."_EMAIL_TO_EXCLUDE_UNSUBSCRIBED.txt";    
        unlink($file_path);  
                 
    }    
    
    

    
    function ReplaceVariables($string=false, $variables=array()) {    
            
        if(isset($string))
        {     
            if($variables!==false && is_array($variables))  {
                foreach($variables as $search => $replace) {
                    $string = str_replace('#'.$search.'#', $replace, $string);                       
                }
             
            }
            return $string;
        }        
        
    }
    
    function multineedle_stripos($haystack, $needles, $offset=0) {
        
        foreach($needles as $needle) {
            if(stripos($haystack, $needle)!=false){
                return true;
            }
        }
        return false;
        
    }
            
    
    function GetNeedVariablesTemplate($TEMPLATE){
        
        $simvolNone = array(
            '"',
            ';',
            '&',
            '<',
            '>',
            '='
        );

        $templateArr = array();
        $chars = preg_split('/[\##]/', $TEMPLATE, -1, PREG_SPLIT_DELIM_CAPTURE);

        foreach($chars as $kte => $vte) {
            $countVte = strlen($vte);
            if($countVte < 150 && CSotbitMailingHelp::multineedle_stripos($vte,$simvolNone)==false){
               $templateArr[$vte] = $vte;
            }
        }

        
        return $templateArr; 
        
        
    }
    
    
    function slaap($seconds=0)
    {
        $seconds = abs($seconds);
        if ($seconds < 1):
           usleep($seconds*1000000);
        else:
           sleep($seconds);
        endif;   
    }     
       
    function OnPageStart() {
        // ������� �������� ������� ���������
        // START
        //������� ������� � ���� event.php   
        $arrEventLogicFile = array();          
        $templComponentMailing = CComponentUtil::GetTemplatesList('sotbit:sotbit.mailing.logic',true);
    
        if(is_array($templComponentMailing)) { 
            foreach($templComponentMailing as $valTemp){    
                if($valTemp['NAME'] != '.default') {   
                    if($valTemp['TEMPLATE']) {
                        $fileEventName = '/bitrix/templates/'.$valTemp['TEMPLATE'].'/components/sotbit/sotbit.mailing.logic/'.$valTemp['NAME'].'/event.php';
                        if(file_exists($_SERVER["DOCUMENT_ROOT"].$fileEventName)) {
                            $arrEventLogicFile[$valTemp['NAME']] = $fileEventName;    
                        }                                 
                    } 
                    else {
                        $fileEventName = '/bitrix/components/sotbit/sotbit.mailing.logic/templates/'.$valTemp['NAME'].'/event.php';
                        if(file_exists($_SERVER["DOCUMENT_ROOT"].$fileEventName)) {
                            $arrEventLogicFile[$valTemp['NAME']] = $fileEventName;    
                        } 
                    }                
                }
            }  
        }          
        //��������� ��� ����� � ���������
        //������� �������� ��������
        
        $ar_Temp_id = array();
        $ar_activeTemp = array();
        $db_activeTemp = CSotbitMailingEvent::GetList(array(),array('ACTIVE'=>'Y'),false,array('ID','TEMPLATE','TEMPLATE_PARAMS'));
        while($res_activeTemp = $db_activeTemp->Fetch()) {
            $ar_activeTemp[] = $res_activeTemp['TEMPLATE'];   
            $ar_Temp_id[$res_activeTemp['TEMPLATE']][$res_activeTemp['ID']] = array('ID'=>$res_activeTemp['ID'],'TEMPLATE_PARAMS'=>unserialize($res_activeTemp['TEMPLATE_PARAMS']));
            //$ar_Temp_id[$res_activeTemp['TEMPLATE']][] = $res_activeTemp['ID'];
        }        
        foreach($arrEventLogicFile as $k=>$EventLogicFile) {
            if(in_array($k, $ar_activeTemp)){
                $arResult['MAILING_INFO'] = $ar_Temp_id[$k];
                include($_SERVER["DOCUMENT_ROOT"].$EventLogicFile);
            }
        }
        // END 
                
        // ���������� ������������ �� �������� ����  ���� ���������
        // START  
        if($_GET['USER_AUTH'] && $_GET['MAILING_MESSAGE']) { 
            //������� ������ ������������
            global $USER;
            if(!is_object($USER)){
                $USER = new CUser();
            }
            //�������� ����������� �� ������������  
            if(!$USER->IsAuthorized()) {
                //�������� ������ �� ���� ��� ����������� ������������
                $paramMessage = CSotbitMailingMessage::GetByIDInfoParamMessage($_GET['MAILING_MESSAGE']); 
                if($paramMessage['USER_AUTH']==$_GET['USER_AUTH'] && !empty($paramMessage['USER_ID'])){
                    $USER->Authorize($paramMessage['USER_ID']);    
                }               
            }
                 
                
        }  
        // END        
    }   
       
       
    function OnAfterEpilog(){
        //���� ������������ �������� �� ��������� �������
        // START  
        if($_GET['MAILING_MESSAGE']) {
            
            $mailing_message = CSotbitMailingMessage::GetByIDInfoStatic($_GET['MAILING_MESSAGE']);    
            if($mailing_message) {
                global $DB;
                global $USER;
                if(!is_object($USER)){
                    $USER = new CUser();
                }                
 
                // �������� ������ ��� ���������� ����������
                $arrFields = array();
                if($mailing_message['STATIC_USER_BACK'] == 'N'){
                    $arrFields['STATIC_USER_BACK'] = 'Y';    
                }
                
                // ������� ������� 
                $arrFields['STATIC_USER_BACK_DATE'] = unserialize($mailing_message['STATIC_USER_BACK_DATE']);
                $arrFields['STATIC_USER_ID'] = unserialize($mailing_message['STATIC_USER_ID']);
                $arrFields['STATIC_SALE_UID'] = unserialize($mailing_message['STATIC_SALE_UID']);
                $arrFields['STATIC_GUEST_ID'] = unserialize($mailing_message['STATIC_GUEST_ID']);
                $arrFields['STATIC_PAGE_START'] = unserialize($mailing_message['STATIC_PAGE_START']);     
                
                // ������� � ������� ����� ����������
                //START
                
                //������� ����                                                                                              
                $arrFields['STATIC_USER_BACK_DATE'][] = Date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL", SITE_ID)));
                
                //������� ������������
                $USER_ID = $USER->GetID();
                if($USER_ID) {
                    $arrFields['STATIC_USER_ID'][] = $USER_ID;                    
                } else {
                    $arrFields['STATIC_USER_ID'][] = 'N';                      
                }
                
                //������� id ������������ �������
               // $SALE_UID = $_COOKIE[COption::GetOptionString("main", "cookie_name", "0").'_SALE_UID'];
                CModule::IncludeModule('sale');
                $SALE_UID = CSaleBasket::GetBasketUserID();
                if($SALE_UID) {
                    $arrFields['STATIC_SALE_UID'][] = $SALE_UID;                    
                } else {
                    $arrFields['STATIC_SALE_UID'][] = 'N';                      
                }                 
                //������� id ����� �� ���-���������
                //$GUEST_ID = $_COOKIE[COption::GetOptionString("main", "cookie_name", "0").'_GUEST_ID'];
                $GUEST_ID = intval($_SESSION["SESS_GUEST_ID"]);
                if($GUEST_ID) {
                    $arrFields['STATIC_GUEST_ID'][] = $GUEST_ID;                    
                } else {
                    $arrFields['STATIC_GUEST_ID'][] = 'N';                      
                }   
                
                //������� ���� ������� ������������
                $user_came_where = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];  
                $arrFields['STATIC_PAGE_START'][] = $user_came_where;           
                //END
                

                //���������� ������ ��� ����
                $arrFields['STATIC_USER_BACK_DATE'] = serialize($arrFields['STATIC_USER_BACK_DATE']);
                $arrFields['STATIC_USER_ID'] = serialize($arrFields['STATIC_USER_ID']);
                $arrFields['STATIC_SALE_UID'] = serialize($arrFields['STATIC_SALE_UID']);
                $arrFields['STATIC_GUEST_ID'] = serialize($arrFields['STATIC_GUEST_ID']);
                $arrFields['STATIC_PAGE_START'] = serialize($arrFields['STATIC_PAGE_START']); 
                
                
                CSotbitMailingMessage::Update($_GET['MAILING_MESSAGE'],$arrFields);
                
                
                //������� �������� ����� ������
                //START
                $user_came_where_redirect = $user_came_where;
                $strReplaceLink = array(
                    'MAILING_MESSAGE='.$_GET['MAILING_MESSAGE'].'&',
                    'MAILING_MESSAGE='.$_GET['MAILING_MESSAGE'],
                    'USER_AUTH='.$_GET['USER_AUTH'].'&',
                    'USER_AUTH='.$_GET['USER_AUTH']                                    
                );                
                foreach($strReplaceLink as $replace){
                    $user_came_where_redirect = str_replace($replace, "",  $user_came_where_redirect);                                
                }   
                LocalRedirect($user_came_where_redirect);            
                //END

                   
            }  
                     
        }
        // END
        
        // ���� ������������ � ������
        // START
        if($_GET['MAILING_FILE_REDURECT']) { 
            LocalRedirect($_GET['MAILING_FILE_REDURECT']);                   
        }      
        // END
        
        
        // ���� ������������ ��������� �� ��������
        // START
        if($_GET['MAILING_UNSUBSCRIBE']) { 
         
            $arrUnscrible = explode('||', $_GET['MAILING_UNSUBSCRIBE']); 
            $EMAIL_TO = $arrUnscrible[0];
            $ID_MESSEGE = $arrUnscrible[1];
            $ID_EVENT = $arrUnscrible[2];
            if($EMAIL_TO && $ID_MESSEGE && $ID_EVENT) {
              
                $MessageInfo = CSotbitMailingMessage::GetByID($ID_MESSEGE);
                if($MessageInfo['EMAIL_TO'] == $EMAIL_TO) {
                    global $DB;
                    
         
                    $resSearch = CSotbitMailingUnsubscribed::GetList(array(), array('ID_EVENT' => $ID_EVENT, 'EMAIL_TO' => $EMAIL_TO));
                    $arrSearch = $resSearch->Fetch();
                    if(empty($arrSearch)) {
                     
                        CSotbitMailingUnsubscribed::Add(array(
                            'DATE_CREATE' => Date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL", SITE_ID))),
                            'ID_MESSEGE' => $ID_MESSEGE,
                            'ID_EVENT' => $ID_EVENT,
                            'EMAIL_TO' => $EMAIL_TO
                        ));                        
                        
                        global $CACHE_MANAGER;       
                        $CACHE_MANAGER->ClearByTag(self::MODULE_ID.'_GetUnsubscribedByMailing_'.$ID_EVENT);  
                        $CACHE_MANAGER->ClearByTag(self::MODULE_ID.'_GetUnsubscribedAllMailing');                             
                    }                  
                }              
            }                   
        }
        // END
        
        //������ ��� ����������
        //START
        //������� � cookies ������ ������ ������������ 
        $MAILING_USER_CAME = '';
        if($_COOKIE["MAILING_USER_CAME"]){
            $MAILING_USER_CAME = $_COOKIE["MAILING_USER_CAME"];            
        }
        if(empty($MAILING_USER_CAME)) {
            $MAILING_USER_CAME = getenv("HTTP_REFERER");  
            if($MAILING_USER_CAME) {     
                while (preg_match('%([0-9A-F]{2})',$MAILING_USER_CAME)){
                    $val=preg_replace('.*%([0-9A-F]{2}).*','\1',$MAILING_USER_CAME); 
                    $newval=chr(hexdec($val));
                    $MAILING_USER_CAME=str_replace('%'.$val,$newval,$MAILING_USER_CAME); 
                }  
                SetCookie("MAILING_USER_CAME", $MAILING_USER_CAME, time()+7*24*60*60, '/', $_SERVER['SERVER_NAME']);          
            }
            
        } 

            
    }    
              
}?>