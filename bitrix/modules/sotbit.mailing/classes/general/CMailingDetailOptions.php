<?
IncludeModuleLangFile(__FILE__);    
class CMailingDetailOptions        
{
    public $arCurOptionValues = array();
    
    private $module_id = '';
    private $arTabs = array();
    private $arGroups = array();
    private $arOptions = array();
    private $need_access_tab = false;
    
    public function CMailingDetailOptions($module_id, $arTabs, $arGroups, $arOptions)
    {
        $this->module_id = $module_id;
        $this->arTabs = $arTabs;
        $this->arGroups = $arGroups;
        $this->arOptions = $arOptions;
        

        $this->GetCurOptionValues();
    }
    
    private function SaveOptions()
    {
        global $APPLICATION;
        $CONS_RIGHT = $APPLICATION->GetGroupRight($this->module_id); 
        if($CONS_RIGHT <= "R") {
            echo CAdminMessage::ShowMessage(GetMessage($this->module_id.'_ERROR_RIGTH'));
            return false;    
        }
        
        foreach($this->arOptions as $opt => $arOptParams)
        {
            
            
            
            /*
            if($arOptParams['TYPE'] != 'CUSTOM')
            {
                $val = $_REQUEST[$opt];
    
                if($arOptParams['TYPE'] == 'CHECKBOX' && $val != 'Y')
                    $val = 'N';
                elseif(is_array($val))
                    $val = serialize($val);

               // COption::SetOptionString($this->module_id, $opt, $val);
            }
            */
            
        }
        
    }
        
    
    private function GetCurOptionValues()
    {
        foreach($this->arOptions as $opt => $arOptParams)
        {
            
            
            $this->arCurOptionValues[$opt] = $arOptParams['DEFAULT'];
            
            
        }
    }
    
    public function ShowHTML()
    {
        global $APPLICATION;

        $arP = array();
          
        foreach($this->arGroups as $group_id => $group_params)
            $arP[$group_params['TAB']][$group_id] = array();        
        
        if(is_array($this->arOptions))
        {
            foreach($this->arOptions as $option => $arOptParams)
            {

                
                $val = $this->arCurOptionValues[$option];
                

                

                if($arOptParams['SORT'] < 0 || !isset($arOptParams['SORT']))
                    $arOptParams['SORT'] = 0;
                
                $label = (isset($arOptParams['TITLE']) && $arOptParams['TITLE'] != '') ? $arOptParams['TITLE'] : '';
                $opt = htmlspecialcharsEx($option);

                $submit_refresh = '';
                if($arOptParams['REFRESH'] == 'Y') {
                     $submit_refresh = 'onchange=\'$(this).parents("form").submit();\' ';    
                }
                
                
                        // ���� ����� ������� ������� ��������
                        if($arOptParams['DEFAULT_TYPE']=='DELIVERY_ID' && CModule::IncludeModule("sale")) {
                             
                              
                            if($arOptParams['TYPE']=='SELECT') {
                                $arOrderDeliveryId = array('' => GetMessage($this->module_id.'_SELECT_PARAM_ALL'));                                 
                            }
                               
                            
 
                            //������� ������������������ ������ ��������       
                            $rsDeliveryServicesList = CSaleDeliveryHandler::GetList(array(), array('ACTIVE'=>'Y')); 
                            while ($arDeliveryService = $rsDeliveryServicesList->GetNext())
                            {    
                                if (!is_array($arDeliveryService) || !is_array($arDeliveryService["PROFILES"])) continue;
                                foreach ($arDeliveryService["PROFILES"] as $profile_id => $arDeliveryProfile)
                                {
                                    $delivery_id = $arDeliveryService["SID"].":".$profile_id;
                                    $arOrderDeliveryId[$delivery_id] = '['.$delivery_id.'] '.$arDeliveryService["NAME"].": ".$arDeliveryProfile["TITLE"];     
                                }  
                            } 
                            //������� ������� ������ ��������   
                            $dbDelivery = CSaleDelivery::GetList(
                                array("SORT"=>"ASC", "NAME"=>"ASC"),
                                array(
                                    "ACTIVE" => "Y",
                                )
                            );
                            while ($arDelivery = $dbDelivery->GetNext())
                            {
                                $arOrderDeliveryId[$arDelivery["ID"]] = '['.$arDelivery["ID"].'] '.$arDelivery["NAME"];
                            } 
                            
                            foreach($arOrderDeliveryId as $k => $v) {
                                $arOptParams['VALUES']['REFERENCE_ID'][] = $k;
                                $arOptParams['VALUES']['REFERENCE'][] = $v;          
                            }                                  
                        } 
                                       
                
                switch($arOptParams['TYPE'])
                {
                    case 'CHECKBOX':
                        $input = '
                        <input type="hidden" name="'.$opt.'" value="N" />  
                        <input  type="checkbox" name="'.$opt.'" id="'.$opt.'" value="Y"'.($val == 'Y' ? ' checked' : '').' '.$submit_refresh.' />';
                        break;                                                                                                                                  
                    case 'TEXT':
                        if(!isset($arOptParams['COLS']))
                            $arOptParams['COLS'] = 25;
                        if(!isset($arOptParams['ROWS'])) {
                            $arOptParams['ROWS'] = 5;                            
                        }
                        $input = '<textarea cols="'.$arOptParams['COLS'].'" rows="'.$arOptParams['ROWS'].'" name="'.$opt.'">'.htmlspecialcharsEx($val).'</textarea>';
                        break;
                    case 'HTML':    
                              $input = '';
                         break;
                    case 'INT_FROM_TO':
                         $input = '';
                         break;                         
                    case 'USER_ID':    
                              $input = '';  
                         break;                            
                    case 'TABS_INFO':  
                         $input = '';        
                        break;
                    case 'DATE':    
                        $input = CalendarDate($opt, htmlspecialcharsbx($val), $this->module_id ,20);
                        break;                        
                    case 'SELECT':
                        $input = SelectBoxFromArray($opt, $arOptParams['VALUES'], $val, '', $submit_refresh, false );
                        break;
                    case 'MSELECT':
                        if($arOptParams['WIDTH']) {
                           $selHTML =  'style="width: '.$arOptParams['WIDTH'].'px"';
                        }
                        if(empty($arOptParams['VALUES'])){
                            $arOptParams['VALUES'] = array();        
                        }
                        
                        $input = SelectBoxMFromArray($opt.'[]', $arOptParams['VALUES'], $val,'',false,$arOptParams['SIZE'], $selHTML);

                        break;
                    case 'COLORPICKER':
                        if(!isset($arOptParams['FIELD_SIZE']))
                            $arOptParams['FIELD_SIZE'] = 25;
                        ob_start();
                        echo     '<input id="__CP_PARAM_'.$opt.'" name="'.$opt.'" size="'.$arOptParams['FIELD_SIZE'].'" value="'.htmlspecialcharsEx($val).'" type="text" style="float: left;" '.($arOptParams['FIELD_READONLY'] == 'Y' ? 'readonly' : '').' />
                                <script>
                                    function onSelect_'.$opt.'(color, objColorPicker)
                                    {
                                        var oInput = BX("__CP_PARAM_'.$opt.'");
                                        oInput.value = color;
                                    }
                                </script>';
                        $APPLICATION->IncludeComponent('bitrix:main.colorpicker', '', Array(
                                'SHOW_BUTTON' => 'Y',
                                'ID' => $opt,
                                'NAME' => GetMessage("sns.tools1c_choice_color"),
                                'ONSELECT' => 'onSelect_'.$opt
                            ), false
                        );
                        $input = ob_get_clean();
                        if($arOptParams['REFRESH'] == 'Y')
                            $input .= '<input type="submit" name="refresh" value="OK" />';
                        break;
                    case 'FILE':
                        if(!isset($arOptParams['FIELD_SIZE']))
                            $arOptParams['FIELD_SIZE'] = 25;
                        if(!isset($arOptParams['BUTTON_TEXT']))
                            $arOptParams['BUTTON_TEXT'] = '...';
                        CAdminFileDialog::ShowScript(Array(
                            'event' => 'BX_FD_'.$opt,
                            'arResultDest' => Array('FUNCTION_NAME' => 'BX_FD_ONRESULT_'.$opt),
                            'arPath' => Array(),
                            'select' => 'F',
                            'operation' => 'O',
                            'showUploadTab' => true,
                            'showAddToMenuTab' => false,
                            'fileFilter' => '',
                            'allowAllFiles' => true,
                            'SaveConfig' => true
                        ));
                        $input =     '<input id="__FD_PARAM_'.$opt.'" name="'.$opt.'" size="'.$arOptParams['FIELD_SIZE'].'" value="'.htmlspecialcharsEx($val).'" type="text" style="float: left;" '.($arOptParams['FIELD_READONLY'] == 'Y' ? 'readonly' : '').' />
                                    <input value="'.$arOptParams['BUTTON_TEXT'].'" type="button" onclick="window.BX_FD_'.$opt.'();" />
                                    <script>
                                        setTimeout(function(){
                                            if (BX("bx_fd_input_'.strtolower($opt).'"))
                                                BX("bx_fd_input_'.strtolower($opt).'").onclick = window.BX_FD_'.$opt.';
                                        }, 200);
                                        window.BX_FD_ONRESULT_'.$opt.' = function(filename, filepath)
                                        {
                                            var oInput = BX("__FD_PARAM_'.$opt.'");
                                            if (typeof filename == "object")
                                                oInput.value = filename.src;
                                            else
                                                oInput.value = (filepath + "/" + filename).replace(/\/\//ig, \'/\');
                                        }
                                    </script>';
                        if($arOptParams['REFRESH'] == 'Y')
                            $input .= '<input type="submit" name="refresh" value="OK" />';
                        break;
                    case 'CUSTOM':
                        $input = $arOptParams['VALUE'];
                        break;
                    default:
                        if(!isset($arOptParams['SIZE']))
                            $arOptParams['SIZE'] = 25;
                        if(!isset($arOptParams['MAXLENGTH']))
                            $arOptParams['MAXLENGTH'] = 255;
                            
                        if($arOptParams['TYPE'] == 'INT') {
                            $type = 'number';    
                        } 
                        elseif($arOptParams['TYPE'] == 'HIDDEN') {
                            $type = 'hidden';                              
                        }
                        else {
                            $type = 'text';                                 
                        }   
                            
                        $input = '<input type="'.$type.'" size="'.$arOptParams['SIZE'].'" maxlength="'.$arOptParams['MAXLENGTH'].'" value="'.htmlspecialcharsEx($val).'" name="'.htmlspecialcharsEx($option).'" />';
                        
                        
                        if($arOptParams['TYPE'] == 'HIDDEN' && $arOptParams['VALUE_SHOW']) {
                            $input .= $arOptParams['VALUE_SHOW'];    
                        }                        
                        elseif($arOptParams['TYPE'] == 'HIDDEN') {
                            $input .= $val;    
                        }
                        
                        if($arOptParams['REFRESH'] == 'Y')
                            $input .= '<input type="submit" name="refresh" value="OK" />';
                        break;
                }



                
                                    

                if($arOptParams['TYPE'] == 'HTML' && $arOptParams['TYPE'] == 'PHP') {
                    $arP[$this->arGroups[$arOptParams['GROUP']]['TAB']][$arOptParams['GROUP']]['OPTIONS'][$option] = '';
                }
                else {    
    
                    $arP[$this->arGroups[$arOptParams['GROUP']]['TAB']][$arOptParams['GROUP']]['OPTIONS'][$option] =  '
                        <tr>
                            <td width="50%">'.$label.':</td>
                            <td width="50%">'.$input.'</td>
                        </tr>
                    ';           

                }                  

                 
                $arP[$this->arGroups[$arOptParams['GROUP']]['TAB']][$arOptParams['GROUP']]['OPTIONS_SORT'][$option] = $arOptParams['SORT'];     
            }
               
            

            echo '<form name="'.str_replace(".", "", $this->module_id).'" method="POST" action="'.$APPLICATION->GetCurPageParam().'&mid='.$this->module_id.'&lang='.LANGUAGE_ID.'#form" enctype="multipart/form-data">'.bitrix_sessid_post();
             
              
            $tabControl = new CAdminTabControl('tabControl', $this->arTabs);
            $tabControl->Begin();  
                 
            foreach($arP as $tab => $groups)
            {
                $tabControl->BeginNextTab();
                 
                foreach($groups as $group_id => $group)
                {
                    if(sizeof($group['OPTIONS_SORT']) > 0)
                    {
                        echo '<tr class="heading"><td colspan="2">'.$this->arGroups[$group_id]['TITLE'].'</td></tr>';
                        
                        array_multisort($group['OPTIONS_SORT'], $group['OPTIONS']);
                        foreach($group['OPTIONS'] as $key => $opt) {
                            
                            if($this->arOptions[$key]['TYPE'] == 'HTML'):?>
                            
                            <tr>
                                <td colspan="2" width="100%">
                                <?if($this->arOptions[$key]['TITLE']):?>
                                <p><b><?=$this->arOptions[$key]['TITLE']?>:</b></p>  
                                <?endif;?>
                                <? 
                                $val = $this->arCurOptionValues[$key];

                                if($this->arOptions[$key]['HEIGHT']) {
                                    $HEIGHT = $this->arOptions[$key]['HEIGHT'];    
                                } else {
                                    $HEIGHT = 450;        
                                }
                              
                                CFileMan::AddHTMLEditorFrame(
                                    $key,
                                    $val,
                                    $key.'_TYPE',
                                    "html",
                                    array(
                                        'height' => $HEIGHT,
                                        'width' => '100%'
                                    ),
                                    "N",
                                    0,
                                    "",
                                    "",
                                    SITE_ID      
                                );                                
                                ?>
                                </td>
                            </tr>                            
                            
                            <?elseif($this->arOptions[$key]['TYPE'] == 'PHP'): ?>
                            
                            
                            <?
                            $val = $this->arCurOptionValues[$key];
                            if(empty($val)){
                                $val = "<?

?>";    
                            }
                 
                            //c������� ���� php
                            //START
                            if($_GET['ID']) {
                                $name_file_php =  str_replace("TEMPLATE_PARAMS_", "", $key);
                                RewriteFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->module_id."/php_files_mailing/".$_GET['ID']."/".$name_file_php.".php", $val);        
                            }
                            //END
                            ?>
                            <tr>
                                <td colspan="2" width="100%" >  
                                    <div style="width:1000px; margin: 0px auto">
                                                          
                                    <?if($this->arOptions[$key]['TITLE']):?>
                                    <p><b><?=$this->arOptions[$key]['TITLE']?>:</b></p>  
                                    <?endif;?>  
                                    
                                    <textarea id="<?=$key?>" style="width:100%; height: <?=$this->arOptions[$key]['HEIGHT']?>px" name="<?=$key?>"><?=$val?></textarea> 
                                     <?
                                    $ceid = CCodeEditor::Show(array(
                                        'id'=>$key, 
                                        'textareaId' => $key , 
                                        'forceSyntax' => 'php',
                                        'theme' => 'light', 
                                    ));

                                    ?>                                      

                                    <script type="text/javascript">
                                    
                                    <?
                                    if(empty($this->arOptions[$key]['HEIGHT'])){
                                        $this->arOptions[$key]['HEIGHT'] = '300';    
                                    }
                                    if(empty($this->arOptions[$key]['WIDTH'])){   
                                        $this->arOptions[$key]['WIDTH'] = '1000'; 
                                    }                                    
                                    ?> 

                                    // ��� ���� ����� �� ������������� ��� ������������ � ����� ��������
                                    $(function(){
                                        $('body,html').animate({scrollTop: 130}, 1);    
                                    });
      
                                    $('#tabControl_tabs span').click(function(){
                                        var CE_<?=$ceid?> = window.BXCodeEditors['<?=$ceid?>'];
                                        CE_<?=$ceid?>.Resize('<?=$this->arOptions[$key]['WIDTH']?>', '<?=$this->arOptions[$key]['HEIGHT']?>');     
                                    }); 
                                    </script>                         
                                    </div>                             
                          
                                </td>
                            </tr> 
                            <?elseif($this->arOptions[$key]['TYPE'] == 'HTML_SHOW'): ?>                            
                             
                            <tr>
                                <td colspan="2" width="100%" bgcolor="white" align="center">
                                <br />
                                <?if($this->arOptions[$key]['TITLE']):?>
                                <p><b><?=$this->arOptions[$key]['TITLE']?>:</b></p>  
                                <?endif;?>
                                <? 
                                $val = $this->arCurOptionValues[$key];
                                
                                echo $val;                                
                                ?>
                                <br />
                                </td>
                            </tr>                              
                             
                                                            
                            <?elseif($this->arOptions[$key]['TYPE'] == 'TABS_INFO'):
                                $val = $this->arCurOptionValues[$key];
                            ?>
                            <tr>
                                <td colspan="2" width="100%">                                
                                    <?=$val?>
                                </td>
                            </tr> 
                                                            
                            <?
                            elseif($this->arOptions[$key]['TYPE'] == 'USER_ID'):
                            $str_USER_ID = $this->arCurOptionValues[$key];  
                            ?>
                            <tr>                           
                            
                                <td><?=$this->arOptions[$key]['TITLE']?></td>
                                <td>
                                <?
                                $sUser = "";
                                if($str_USER_ID > 0)
                                {
                                    $rsUser = CUser::GetByID($str_USER_ID);
                                    $arUser = $rsUser->GetNext();
                                    if($arUser)
                                        $sUser = "[<a href=\"user_edit.php?ID=".$arUser["ID"]."&amp;lang=".LANG."\">".$arUser["ID"]."</a>] (".$arUser["LOGIN"].") ".$arUser["NAME"]." ".$arUser["LAST_NAME"];
                                }
                                echo FindUserID($key, $str_USER_ID, $sUser, str_replace(".", "", $this->module_id), "10", "", " ... ", "", "");

                                if((integer)$str_USER_ID==0):
                                ?><script language="JavaScript">document.subscrform.USER_ID.disabled=document.subscrform.FindUser.disabled=true;</script><?
                                endif;                            
                                ?>
                                </td>  
                            </tr>                                                                 
                            <?elseif($this->arOptions[$key]['TYPE'] == 'DATE_PERIOD'):?>
                            <?$val = $this->arCurOptionValues[$key];?>
                            
                            <tr>   
                                <td>
                                    <?=$this->arOptions[$key]['TITLE']?>:                               
                                </td>                         
                                <td>
                                    <div style="max-width: 400px;">     
                                        <?echo CalendarPeriod($key."_from", $val['from'], $key."_to", $val['to'], str_replace(".", "", $this->module_id), "N")?>                                    
                                    </div>
  
                                </td>                 
                            </tr>     
                            <?elseif($this->arOptions[$key]['TYPE'] == 'DATE_PERIOD_AGO'):?>
                            <?
                            $val = $this->arCurOptionValues[$key];   
                            ?>
                            
                            <tr>   
                                <td>
                                    <?=$this->arOptions[$key]['TITLE']?>:                             
                                </td>                         
                                <td>
                                    <?echo GetMessage($this->module_id."_FROM");?>
                                    <input type="int" name="<?=$key?>_from" value="<?=$val['from']?>" size="20">

                                    <?echo GetMessage($this->module_id."_TO");?>
                                    <input type="int" name="<?=$key?>_to" value="<?=$val['to']?>" size="20">

                                    <?
                                    $select_type = array(
                                        '' =>  GetMessage($this->module_id."_DATE_PERIOD_AGO_SELECT_TYPE"),
                                        'MIN' => GetMessage($this->module_id."_DATE_PERIOD_AGO_SELECT_TYPE_MIN"),
                                        'HOURS' => GetMessage($this->module_id."_DATE_PERIOD_AGO_SELECT_TYPE_HOURS"),
                                        'DAYS' => GetMessage($this->module_id."_DATE_PERIOD_AGO_SELECT_TYPE_DAYS"),
                                        'MONTH' => GetMessage($this->module_id."_DATE_PERIOD_AGO_SELECT_TYPE_MONTH"),                                                                                                                                                                   
                                    );
                                    ?>
                                    
                                    <select name="<?=$key?>_type" >
                                        <?foreach($select_type as $k => $v):?> 
                                        <option value="<?=$k?>" <?if($k==$val['type']):?>selected="selected"<?endif;?>><?=$v?></option>   
                                        <?endforeach;?>                                                                                                                                                            
                                    </select>    
                                </td>                 
                            </tr>                                
                                        
                            
                            <?elseif($this->arOptions[$key]['TYPE'] == 'INT_FROM_TO'):?>
                            
                            <?$val = $this->arCurOptionValues[$key];?>                            
                            <tr>   
                                <td>
                                    <?=$this->arOptions[$key]['TITLE']?>:                                
                                </td>                         
                                <td>
                                    <?echo GetMessage($this->module_id."_FROM");?>
                                    <input type="text" name="<?=$key?>_from" value="<?=(floatval($val['from'])>0)?floatval($val['from']):""?>" size="20">

                                    <?echo GetMessage($this->module_id."_TO");?>
                                    <input type="text" name="<?=$key?>_to" value="<?=(floatval($val['to'])>0)?floatval($val['to']):""?>" size="20">
                                </td>                 
                            </tr>                                 
                                 
                                 
            
                            <?else:?>
                            
                                <?echo $opt;?>
                            
                            <?endif;?>
                            
                            <?if($this->arOptions[$key]['NOTES'] != ''):?>
                            <tr>
                                <td align="center" colspan="2">           
                                    <div  class="adm-info-message-wrap">
                                        <div class="adm-info-message" align="left">
                                            <?=$this->arOptions[$key]['NOTES']?>
                                        </div>
                                    </div>
                                </td>
                            </tr> 
                            <?endif;?>
                            
                            
                            
                            <?    
                        }

                    }
                }
            }
            
             if($this->need_access_tab)
            {
                $tabControl->BeginNextTab();
                $module_id = $this->module_id;
                require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
            }
            
            $tabControl->Buttons();
            
            echo     '<input type="hidden" name="update" value="Y" />
                    <input type="submit" name="save" value="' .GetMessage($this->module_id."_submit_save"). '"  class="adm-btn-save"/>
                    <input type="reset" name="reset" value="' .GetMessage($this->module_id."_submit_cancel"). '" />
                    ';

     
            $tabControl->End();    
            echo '</form>';
            
        }
    }
}
?>
