<?require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');   
$module_id = "sotbit.mailing";
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/include.php");    
IncludeModuleLangFile(__FILE__);
CSotbitMailingHelp::CacheConstantCheck();
//�������� ����
//�������� ����
$POST_RIGHT = $APPLICATION->GetGroupRight($module_id);   
if ($POST_RIGHT <= "D") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));         
} 
//����� �������� ����  




//������� ������ � ��������
//START
$arrTempl = array();
$templComponent = CComponentUtil::GetTemplatesList('sotbit:sotbit.mailing.logic',true);

if(is_array($templComponent)) {
  
    foreach($templComponent as $valTemp){
        if($valTemp['NAME'] != '.default') {
           
            if($valTemp['TEMPLATE']) {
                $valTemp["TITLE_CUSTOM"] = $valTemp["TITLE"]. ' ['.GetMessage($module_id.'_TEMPLATE_TYPE_MY').' - '.$valTemp['NAME'].']';   
            } else {
                $valTemp["TITLE_CUSTOM"] = $valTemp["TITLE"]. ' ['.GetMessage($module_id.'_TEMPLATE_TYPE_SYSTEM').' - '.$valTemp['NAME'].']';                 
            }        
            
            $arrTempl[$valTemp['NAME']] = $valTemp;          
        }
       
    }
    
}

//END


$sTableID = "tbl_sotbit_mailing_event"; // ID �������
$oSort = new CAdminSorting($sTableID, "ID", "asc"); // ������ ����������
$lAdmin = new CAdminList($sTableID, $oSort); // �������� ������ ������


// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
  global $FilterArr, $lAdmin;
  if(is_array($FilterArr)) {
    foreach ($FilterArr as $f) global $$f;      
  }


  // � ������ ������ ��������� ������. 
  // � ����� ������ ����� ��������� �������� ���������� $find_���
  // � � ������ �������������� ������ ���������� �� ����������� 
  // ����������� $lAdmin->AddFilterError('�����_������').
  
  return count($lAdmin->arFilterErrors)==0; // ���� ������ ����, ������ false;
}
// *********************** /CheckFilter ******************************* //

// ������ �������� �������
$FilterArr = Array(
  "find_ID", 
  "find_NAME", 
  "find_ACTIVE",  
  "find_TEMPLATE",  
  "find_MODE",  
);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

// ���� ��� �������� ������� ���������, ���������� ���
if (CheckFilter())
{
  // �������� ������ ���������� ��� ������� CRubric::GetList() �� ������ �������� �������
  $arFilter = Array(
    "ID"  => $find_ID,    
    "NAME" => $find_NAME,  
    "ACTIVE" => $find_ACTIVE,  
    "TEMPLATE" => $find_TEMPLATE,
    "MODE" => $find_MODE    
  );
}

$cData = new CSotbitMailingEvent; 

$cDataMessage = new CSotbitMailingMessage; 
// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //



// ���������� ����������������� ���������  
if($POST_RIGHT=="W")
{
  $lAdmin->EditAction();  
  // ������� �� ������ ���������� ���������  
  if(is_array($FIELDS)) { 
  
      foreach($FIELDS as $ID=>$arFields)
      {
        
        if(!$lAdmin->IsUpdated($ID))
          continue;
        
        // �������� ��������� ������� ��������
        $DB->StartTransaction();
        $ID = IntVal($ID);  
     
        if(!$cData->Update($ID, $arFields))
        {
            $lAdmin->AddGroupError(GetMessage($module_id.'_ERROR_DB').' '.$cData->LAST_ERROR, $ID);
            $DB->Rollback();
        }

        
        if($res = CSotbitMailingEvent::GetByID($ID)) { 
     
            //������ � �������� UniSender
            //START
            if($res['EVENT_SEND_SYSTEM'] == 'UNISENDER') {
                
                //���������� ��������
                //START
                $getListUniSender = CSotbitMailingHelp::QueryUniSender('getLists'); 
                $arrListUniSender = array();
                if(is_array($getListUniSender)) {
               
                    foreach($getListUniSender['result'] as $k=>$v) {
                        $arrListUniSender[$v['id']] =  $v['title'];    
                    }               
                    
                }

                // ���� ��� ��������� ��������, �������� �����
                if(empty($arrListUniSender[$res['EVENT_SEND_SYSTEM_CODE']])){
                    
                    
                    $createListUniSender = CSotbitMailingHelp::QueryUniSender('createList', array('title'=>$res['NAME'])); 
                    if($createListUniSender['result']['id']){
                         CSotbitMailingEvent::Update($res['ID'] , array('EVENT_SEND_SYSTEM_CODE' => $createListUniSender['result']['id']));                           
                    }             
                }
                //���� ���������� ��������, �������
                if($arrListUniSender[$res['EVENT_SEND_SYSTEM_CODE']] && $arrListUniSender[$res['EVENT_SEND_SYSTEM_CODE']] != $res['NAME']) {
                    $updateListUniSender = CSotbitMailingHelp::QueryUniSender('updateList', array('list_id'=>$res['EVENT_SEND_SYSTEM_CODE'],'title'=>$res['NAME']));             
                }    
                //END
                        
            }
            //END 
                    
        } 
        
        
        global $CACHE_MANAGER; 
        $CACHE_MANAGER->ClearByTag($module_id);       
        $DB->Commit();
      } 
  
      
  }       

    
  
}
 
 


    

 
 
// ��������� ��������� � ��������� ��������
if($POST_RIGHT=="W")
{
    
        // ������� ����������� ������
      $arrInfo = array();
      $rsDataInfo = $cData->GetList(array($by=>$order), $arFilter, false, array('ID', 'AGENT_ID') );
      while($ResInfo = $rsDataInfo->Fetch()) {
        $arrInfo[$ResInfo['ID']] = $ResInfo;    
      }        
    
    
      $arID = $lAdmin->GroupAction(); 
      // ���� ������� "��� ���� ���������"
      if($_REQUEST['action_target']=='selected')
      {

        $rsData = $cData->GetList(array($by=>$order), $arFilter, false ,array('ID'));
        while($arRes = $rsData->Fetch()) {
            $arID[] = $arRes['ID'];            
        }

          
      }
      
      if(is_array($arID)) { 
       
          // ������� �� ������ ���������
          foreach($arID as $ID)
          {            
                if(strlen($ID)<=0){
                  continue;            
                }
                
                $ID = IntVal($ID);
                
                

                // ��� ������� �������� �������� ��������� ��������
                switch($_REQUEST['action'])
                {
                                 
                    // ��������
                   case "delete":
                   
                        // ������ ��� ��������� ��������      
                        $resEventMessage = CSotbitMailingMessage::GetList(array(),array('ID_EVENT'=>$ID),false,array('ID'));
                        while($arrEventMessage = $resEventMessage->Fetch()) {
                            CSotbitMailingMessage::Delete($arrEventMessage['ID']);
                        }                                          
                        //������ ��� ������� ��������    
                        $resEventTemplate = CSotbitMailingMessageTemplate::GetList(array(),array('ID_EVENT'=>$ID),false,array('ID','ID_EVENT'));
                        while($arrEventTemplate = $resEventTemplate->Fetch()) {
                            CSotbitMailingMessageTemplate::Delete($arrEventTemplate['ID']);
                        }                     
                   
                   
                        $cData->Delete($ID); 
                        if($arrInfo[$ID]['AGENT_ID']) {
                            CAgent::Delete($arrInfo[$ID]['AGENT_ID']);                         
                        }
                        
                        //������ ��� ��������� ������ ��������
                        

                        
                        //������ ����� ��� ��������
                        DeleteDirFilesEx("/bitrix/modules/".$module_id ."/php_files_mailing/".$ID.'/');
                   break;                    
                   case "copy":
                        $CopyElement = $cData->GetByID($ID);
                        unset($CopyElement['ID']);
                        $cData->Add($CopyElement);
                   break;
                    // ���������/�����������
                   case "activate":
                        $cData->Update($ID, array("ACTIVE" => "Y"));
                   break;       
                   case "deactivate":
                        $cData->Update($ID, array("ACTIVE" => "N"));
                   break;    
                } 
                 
          }       
          
      }
      
      // ������� ��� ��� �������
      // START
      global $CACHE_MANAGER; 
      $CACHE_MANAGER->ClearByTag($module_id.'_GetMailingInfo');
      // END   
  
  
}
 

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //


$cData = new CSotbitMailingEvent;
$rsData = $cData->GetList(array($by=>$order), $arFilter);


// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage($module_id.'_NAV_TITLE')));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$lAdmin->AddHeaders(array(
 
  array("id"    =>"ID",
    "content"  =>'ID',
    "sort"    =>"ID",
    "align"    =>"right",
    "default"  =>true,
  ),
  array(  "id"    =>"ACTIVE",
    "content"  => GetMessage($module_id.'_list_title_ACTIVE'),  
    "sort"    =>"ACTIVE",
    "default"  =>true,    
  ),   
  array("id" =>"NAME",
    "content"  =>  GetMessage($module_id.'_list_title_NAME'),   
    "sort"    =>"NAME",
    "default"  =>true,    
  ), 
  array(  "id"    =>"DESCRIPTION",
    "content"  => GetMessage($module_id.'_list_title_DESCRIPTION'),  
    "sort"    =>"ACTIVE",
    "default"  =>true,    
  ),      
  array(  "id"    =>"TEMPLATE",
    "content"  => GetMessage($module_id.'_list_title_TEMPLATE'),  
    "sort"    =>"TEMPLATE",
    "default"  =>true,    
  ), 
  array(  "id"    =>"MODE",
    "content"  => GetMessage($module_id.'_list_title_MODE'),  
    "sort"    =>"MODE",
    "default"  =>true,    
  ),    
  array(  "id"    =>"EVENT_SEND_SYSTEM",
    "content"  => GetMessage($module_id.'_list_title_EVENT_SEND_SYSTEM'),  
    "sort"    =>"EVENT_SEND_SYSTEM",
    "default"  =>true,    
  ),  
  array(  "id"    =>"DATE_LAST_RUN",
    "content"  => GetMessage($module_id.'_list_title_DATE_LAST_RUN'),  
    "sort"    =>"DATE_LAST_RUN",
    "default"  =>true,    
  ), 
  array(  "id"    =>"COUNT_RUN",
    "content"  => GetMessage($module_id.'_list_title_COUNT_RUN'),  
    "sort"    =>"COUNT_RUN",
    "default"  =>true,    
  ),   
  array(  "id"    =>"EXCLUDE_UNSUBSCRIBED_USER",
    "content"  => GetMessage($module_id.'_list_title_EXCLUDE_UNSUBSCRIBED_USER'),  
    "sort"    =>"EXCLUDE_UNSUBSCRIBED_USER",
    "default"  =>true,    
  ),    
  
  
));





while($arRes = $rsData->NavNext(true, "f_")) {
    $editUrl = "sotbit_mailing_detail.php?ID=".$f_ID."&SOTBIT_MAILING_DETAIL=Y&lang=".LANG;  
    
    
    $row =& $lAdmin->AddRow($f_ID, $arRes);    
    // ����� �������� ����������� �������� ��� ��������� � �������������� ������         
    if($POST_RIGHT>="W") {           
          //�������� NAME ����� ��������������� ��� �����, � ������������ �������
         // $row->AddInputField("NAME_1C", array("size"=>20));    
         $row->AddInputField("NAME", array("size"=>40));
         $row->AddCheckField("ACTIVE");
       
       

         if($arrTempl[$f_TEMPLATE]) {
            $row_TEMPLATE = $arrTempl[$f_TEMPLATE]['TITLE_CUSTOM'];             
         } 
         else {
            $row_TEMPLATE = "<span style='color:red'>".GetMessage($module_id.'_list_title_TEMPLATE_DELETE')." ".$f_TEMPLATE."</span>";                    
         }
         $row->AddViewField("TEMPLATE", $row_TEMPLATE);

       
       
         $ar_MODE = array(
            'TEST' => GetMessage($module_id."_list_title_MODE_VALUE_TEST"),
            'WORK' => GetMessage($module_id."_list_title_MODE_VALUE_WORK"),                      
         );       
       
         //$row_MODE = GetMessage($module_id."_list_title_MODE_VALUE_".$f_MODE);                    
         //$row->AddViewField("MODE", $row_MODE);
         $row->AddSelectField(
            "MODE", 
             $ar_MODE 
         );          

      
         $ar_EXCLUDE_UNSUBSCRIBED_USER = array(
            'NO' => GetMessage($module_id."_list_title_EXCLUDE_UNSUBSCRIBED_USER_VALUE_NO"),
            'ALL' => GetMessage($module_id."_list_title_EXCLUDE_UNSUBSCRIBED_USER_VALUE_ALL"),
            'THIS' => GetMessage($module_id."_list_title_EXCLUDE_UNSUBSCRIBED_USER_VALUE_THIS"),                        
         );
         $row->AddSelectField(
            "EXCLUDE_UNSUBSCRIBED_USER", 
             $ar_EXCLUDE_UNSUBSCRIBED_USER 
         );      
       
       
         $ar_EVENT_SEND_SYSTEM = array(
            'BITRIX' => GetMessage($module_id."_list_title_EVENT_SEND_SYSTEM_VALUE_BITRIX"),
            'UNISENDER' => GetMessage($module_id."_list_title_EVENT_SEND_SYSTEM_VALUE_UNISENDER"),                    
         );
         $row->AddSelectField(
            "EVENT_SEND_SYSTEM", 
             $ar_EVENT_SEND_SYSTEM 
         );         
       
       
        
      
        
         
       
         $sHTML = '<textarea rows="5" cols="30" name="FIELDS['.$f_ID.'][DESCRIPTION]">'.$f_DESCRIPTION.'</textarea>';
         $row->AddEditField("DESCRIPTION",$sHTML);
          // �������� ����������� ���� � ������
                                       
    } 
      
      
    if($POST_RIGHT>="W") { 
   
           $arActions = array();
   
                     
           $ACTION_EDIT =  array(
                "ICON"=>"edit",
                "TEXT"=> GetMessage($module_id."_ACTION_EDIT"),
                "ACTION"=>$lAdmin->ActionRedirect($editUrl),
           );  
           /* 
           $ACTION_COPY =  array(
                "ICON"=>"copy",
                "TEXT"=> GetMessage($module_id."_ACTION_COPY"),
                "ACTION"=> $lAdmin->ActionDoGroup($f_ID, "copy"),
           );  */                  
           $ACTION_DELETE =  array(
                "ICON"=>"delete",
                "TEXT"=> GetMessage($module_id."_ACTION_DELETE"),
                "ACTION"=>"if(confirm('".GetMessage($module_id."_ACTION_DELETE_CONFORM", array('#ID#' =>$f_ID))."?')) ".$lAdmin->ActionDoGroup($f_ID, "delete"),
           );
       
            
           if($arrTempl[$f_TEMPLATE]) {
                $arActions[] = $ACTION_EDIT; 
              //  $arActions[] = $ACTION_COPY;
           }    
                   
         
           $arActions[] = $ACTION_DELETE;          
          
    }   
         
        
    $row->AddActions($arActions); 

  

}

// ������ �������
$lAdmin->AddFooter(
  array(
    array(
        "title"=> GetMessage("MAIN_ADMIN_LIST_SELECTED"), 
        "value"=>$rsData->SelectedRowsCount()), // ���-�� ���������
        array(
            "counter"=>true, 
            "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), 
            "value"=>"0"
        ), // ������� ��������� ���������
  )
);

// ��������� ��������
if($POST_RIGHT>="W") {

    $GroupActionTable = array(
        "delete"=>GetMessage($module_id."_FOOTER_ACTION_DEL"),   
        "activate"=>GetMessage($module_id."_FOOTER_ACTION_ACTIVATE"),
        "deactivate"=>GetMessage($module_id."_FOOTER_ACTION_DEACTIVATE")      
    ); 
       
}


$lAdmin->AddGroupActionTable($GroupActionTable);

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //
if($POST_RIGHT>="W") {
  
    // ���������� ���� �������� ��������
    $arDDMenu = array();
    if(is_array($arrTempl)) {
   
        foreach($arrTempl as $kres => $arRes) {
           /* 
            if(!CModule::IncludeModule("catalog") && 
            !CModule::IncludeModule("sale") && 
            in_array($arRes['NAME'],array(
                'user_not_buy_anything',
                'order_fillter_many_param',
                'order_thank_status_change',
                'forgotten_basket_discount',
                'order_ask_for_review',
                ))
            ){
                continue;    
            }
             */
            
            $arDDMenu[] = array(
                "TEXT" => $arRes['TITLE_CUSTOM'],
                "TITLE" => '['.$arRes["NAME"].'] '.$arRes['DESCRIPTION'],
                "ACTION" => "window.location = 'sotbit_mailing_detail.php?lang=".LANG."&TEMPLATE=".$arRes["NAME"]."';"
            ); 
                       
        }   
        
    }

    
    $aContext = array(
        array(
            "TEXT" => GetMessage($module_id."_MENU_TOP_MAILING_ADD"),
            "ICON" => "btn_new",
            "TITLE" => GetMessage($module_id."_MENU_TOP_MAILING_ADD_ALT"),
            "MENU" => $arDDMenu
        ),
    );
    $lAdmin->AddAdminContextMenu($aContext);      
    
    
}    

 


// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

// �������������� �����
$lAdmin->CheckListMode();

// ��������� ��������� ��������
$APPLICATION->SetTitle(GetMessage($module_id.'_MAILING_LIST_TITLE'));  

// �� ������� ��������� ���������� ������ � �����
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //
 
// �������� ������ �������
$oFilter = new CAdminFilter(
  $sTableID."_filter",
  array(
    GetMessage($module_id.'_list_title_ID'),
    GetMessage($module_id.'_list_title_NAME'),  
    GetMessage($module_id.'_list_title_ACTIVE'),
    GetMessage($module_id.'_list_title_TEMPLATE'),
    GetMessage($module_id.'_list_title_MODE'), 
  )   
);?>

<form name="find_form" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
<?$oFilter->Begin();?>

    <tr>
        <td><?=GetMessage($module_id.'_list_title_ID')?>:</td>
        <td>
            <input type="text" name="find_ID" size="20" value="<?echo htmlspecialchars($find_ID)?>">
        </td>
    </tr>


    <tr>
        <td><?=GetMessage($module_id.'_list_title_NAME')?>:</td>
        <td>
            <input type="text" name="find_NAME" size="47" value="<?echo htmlspecialchars($find_NAME)?>">
        </td>
    </tr>

    <tr>
        <td><?echo GetMessage($module_id."_list_title_ACTIVE")?>:</td>
        <td>
            <select name="find_ACTIVE">
                <option value=""><?=htmlspecialcharsex(GetMessage($module_id.'_ANY'))?></option>
                <option value="Y"<?if($find_ACTIVE=="Y")echo " selected"?>><?=htmlspecialcharsex(GetMessage($module_id."_YES"))?></option>
                <option value="N"<?if($find_ACTIVE=="N")echo " selected"?>><?=htmlspecialcharsex(GetMessage($module_id."_NO"))?></option>
            </select>
        </td>
    </tr>

    <tr>
        <td><?echo GetMessage($module_id."_list_title_TEMPLATE")?>:</td>
        <td>
            <select name="find_TEMPLATE">
                <option value=""><?=htmlspecialcharsex(GetMessage($module_id.'_ANY'))?></option>
            <?foreach($arrTempl as $kres => $arRes):?>    
                <option value="<?=$kres?>"<?if($find_MODE==$kres)echo " selected"?>><?=htmlspecialcharsex($arRes['TITLE_CUSTOM'])?></option>
            <?endforeach;?>

            </select>
        </td>
    </tr>    

    
    <tr>
        <td><?echo GetMessage($module_id."_list_title_MODE")?>:</td>
        <td>
            <select name="find_MODE">
                <option value=""><?=htmlspecialcharsex(GetMessage($module_id.'_ANY'))?></option>
                <option value="TEST" <?if($find_MODE=="TEST")echo " selected"?>><?=htmlspecialcharsex(GetMessage($module_id."_list_title_MODE_VALUE_TEST"))?></option>
                <option value="WORK" <?if($find_MODE=="WORK")echo " selected"?>><?=htmlspecialcharsex(GetMessage($module_id."_list_title_MODE_VALUE_WORK"))?></option>
            </select>
        </td>
    </tr>

    
    
    
<?
$oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
$oFilter->End(); 
?>
</form>


<?// ������� ������� ������ ���������
$lAdmin->DisplayList();
?>


<?
//���������� ��������
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>