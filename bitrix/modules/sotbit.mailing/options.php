<? 
$module_id = "sotbit.mailing";

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$module_id.'/include.php');
IncludeModuleLangFile(__FILE__);    


$arTabs = array(
   array(
      'DIV' => 'edit10',
      'TAB' => GetMessage($module_id.'_edit10'),
      'ICON' => '',
      'TITLE' => GetMessage($module_id.'_edit10'),
      'SORT' => '10'
   ),
             
);


$arGroups = array(
   'OPTION_10' => array('TITLE' => GetMessage($module_id.'_OPTION_10'), 'TAB' => 0),    
   'OPTION_20' => array('TITLE' => GetMessage($module_id.'_OPTION_20'), 'TAB' => 0),   
   'OPTION_30' => array('TITLE' => GetMessage($module_id.'_OPTION_30'), 'TAB' => 0), 
   'OPTION_40' => array('TITLE' => GetMessage($module_id.'_OPTION_40'), 'TAB' => 0),   
   'OPTION_50' => array('TITLE' => GetMessage($module_id.'_OPTION_50'), 'TAB' => 0),   
   'OPTION_60' => array('TITLE' => GetMessage($module_id.'_OPTION_60'), 'TAB' => 0),   
);

$values_arr_unsender_wrap_type = array(
    'REFERENCE_ID' => array(
        'center',
        'right',
        'left',
    ),
    'REFERENCE' => array(
        GetMessage($module_id.'_UNSENDER_WRAP_TYPE_VALUE_center'),
        GetMessage($module_id.'_UNSENDER_WRAP_TYPE_VALUE_right'),
        GetMessage($module_id.'_UNSENDER_WRAP_TYPE_VALUE_left'),
    )
);
       

$arOptions = array( 

   'EMAIL_FROM' => array(
      'GROUP' => 'OPTION_10',
      'TITLE' => GetMessage($module_id."_EMAIL_FROM_TITLE"),
      'TYPE' => 'STRING',  
      'SORT' => '10',
      'SIZE' => '50',
      'DEFAULT' => COption::GetOptionString('main', 'email_from')
   ), 
   
   'UNSENDER_API_KEY' => array(
      'GROUP' => 'OPTION_20',
      'TITLE' => GetMessage($module_id."_UNSENDER_API_KEY_TITLE"),
      'TYPE' => 'STRING',  
      'SORT' => '10',
      'SIZE' => '50',
      'DEFAULT' => ''
   ),    
   'UNSENDER_SENDER_NAME' => array(
      'GROUP' => 'OPTION_20',
      'TITLE' => GetMessage($module_id."_UNSENDER_SENDER_NAME_TITLE"),
      'TYPE' => 'STRING',  
      'SORT' => '20',
      'SIZE' => '50',
      'DEFAULT' => COption::GetOptionString('main', 'site_name')
   ),  
   'UNSENDER_SENDER_EMAIL' => array(
      'GROUP' => 'OPTION_20',
      'TITLE' => GetMessage($module_id."_UNSENDER_SENDER_EMAIL_TITLE"),
      'TYPE' => 'STRING',  
      'SORT' => '30',
      'SIZE' => '50',
      'DEFAULT' => COption::GetOptionString('main', 'email_from') ,
      'NOTES' => GetMessage($module_id."_UNSENDER_SENDER_EMAIL_NOTES"),
   ),          
   'UNSENDER_WRAP_TYPE' => array(
      'GROUP' => 'OPTION_20',
      'TITLE' => GetMessage($module_id."_UNSENDER_WRAP_TYPE_TITLE"),
      'TYPE' => 'STRING',  
      'SORT' => '40',
      'SIZE' => '50',
      'DEFAULT' => COption::GetOptionString('main', 'email_from') ,
      'NOTES' => GetMessage($module_id."_UNSENDER_WRAP_TYPE_NOTES"),
   ),       
     
   'UNSENDER_WRAP_TYPE' => array(
      'GROUP' => 'OPTION_20',
      'TITLE' =>  GetMessage($module_id.'_UNSENDER_WRAP_TYPE_TITLE'),
      'TYPE' => 'SELECT', 
      'SORT' => '70',
      'VALUES' => $values_arr_unsender_wrap_type,
      'NOTES' => GetMessage($module_id.'_UNSENDER_WRAP_TYPE_NOTES'),
      'DEFAULT' => 'center',
   ),    
 
 
   'MAILCHIMP_API_KEY' => array(
      'GROUP' => 'OPTION_30',
      'TITLE' => GetMessage($module_id."_MAILCHIMP_API_KEY_TITLE"),
      'TYPE' => 'STRING',  
      'SORT' => '10',
      'SIZE' => '50',
      'DEFAULT' => ''
   ),         
     
    
   'MANAGED_CACHE_ON' => array(
      'GROUP' => 'OPTION_40',
      'TITLE' => GetMessage($module_id."_MANAGED_CACHE_ON_TITLE"),
      'TYPE' => 'CHECKBOX',  
      'SORT' => '10',
      'DEFAULT' => 'Y'
   ),      
       
       
   'TEMPLATE_MAILING_DEF' => array(
      'GROUP' => 'OPTION_50',
      'TITLE' => GetMessage($module_id."_TEMPLATE_MAILING_DEF_TITLE"),
      'TYPE' => 'STRING',  
      'SORT' => '10',
      'SIZE' => '70',
      'NOTES' => GetMessage($module_id."_TEMPLATE_MAILING_DEF_NOTES"),
      'DEFAULT' => '/bitrix/components/sotbit/sotbit.mailing.logic/lang/ru/tpl_mailing/default.php'
   ),          
      
      
   'MAILING_PACKAGE_COUNT' => array(
      'GROUP' => 'OPTION_60',
      'TITLE' => GetMessage($module_id."_MAILING_PACKAGE_COUNT_TITLE"),
      'TYPE' => 'INT',  
      'SORT' => '10',
      'SIZE' => '50',
      'DEFAULT' => '3000',
      'NOTES' => GetMessage($module_id."_MAILING_PACKAGE_COUNT_NOTES"),      
   ),        
  
   'MAILING_MESSAGE_SLAAP' => array(
      'GROUP' => 'OPTION_60',
      'TITLE' => GetMessage($module_id."_MAILING_MESSAGE_SLAAP_TITLE"),
      'TYPE' => 'STRING',  
      'SORT' => '20',
      'SIZE' => '50',
      'DEFAULT' => '0.001',
      'NOTES' => GetMessage($module_id."_MAILING_MESSAGE_SLAAP_NOTES"),      
   ),      
     
   
       
); 
?>
<?  
$showRightsTab = true;
$opt = new CModuleOptions($module_id, $arTabs, $arGroups, $arOptions, $showRightsTab);
$opt->ShowHTML();      
?>