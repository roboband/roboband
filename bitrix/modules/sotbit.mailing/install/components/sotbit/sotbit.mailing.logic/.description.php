<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arComponentDescription = array(
    "NAME" => GetMessage("SOTBIT_MAILING_LOGIC_TEMPLATE_NAME"),
    "DESCRIPTION" => GetMessage("SOTBIT_MAILING_LOGIC_TEMPLATE_DESCRIPTION"),
    "ICON" => "/images/menu_ext.gif",
    "CACHE_PATH" => "Y",
    "SORT" => 40,
    "PATH" => array(
        "ID" => "sotbit",
        "NAME" => GetMessage("SECTION_NAME")
    ),
);


?>