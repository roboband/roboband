<?
if(!$USER->IsAdmin())
	return;

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");
IncludeModuleLangFile(__FILE__);

$aTabs = array(
	array(
		"DIV"=>"show",
		"TAB"=>GetMessage("MAIN_TAB_SET"),
		"ICON"=>"ib_settings",
		"TITLE"=>GetMessage("MAIN_TAB_TITLE_SET")
	),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$site_id = COption::GetOptionString("simai.catalogyandex", "site_id");
$path = COption::GetOptionString("simai.catalogyandex", "path");
$filename = COption::GetOptionString("simai.catalogyandex", "filename");

$res = CSite::GetByID($site_id);
$arSite = $res->Fetch();

$path = "//".implode("/", array_filter(array(trim($arSite["SERVER_NAME"], "/"), trim($arSite["DIR"], "/"), trim($path, "/"), $filename)));
?>
<?$tabControl->Begin();?>
<?$tabControl->BeginNextTab();?>
	<tr>
		<td valign="top" width="50%"><?=GetMessage("SIMAI_EDIT_LINK")?></td>
		<td valign="top" width="50%"><a href="<?=$path?>"><?="http:".$path?></a></td>
	</tr>
<?$tabControl->Buttons();?>
	<?=bitrix_sessid_post();?>
<?$tabControl->End();?>

