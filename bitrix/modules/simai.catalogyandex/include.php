<?
IncludeModuleLangFile(__FILE__);

CModule::IncludeModule("iblock");

class CSimaiCatalogYandex
{
	public static $DEBUG;
	protected static $force = true;

	protected static function GetMultipleIBlockIDs($arIBlockIDs)
	{
		if(!$arIBlockIDs || !is_array($arIBlockIDs))
			return;

		$arTemp = array();
		foreach($arIBlockIDs as $v)
			$arTemp = array_merge($arTemp, explode(",", substr($v, 1)));

		$arIBlockIDs = array_filter(array_unique($arTemp));

		return $arIBlockIDs;
	}

	protected static function GetSectionIBlockIDs($arSectionIDs)
	{
		$arIBlockIDs = array();

		foreach($arSectionIDs as $v)
		{
			list($iBlockID, $depthLevel, $sectionID) = explode(",", $v);
			if($iBlockID && !in_array($iBlockID, $arIBlockIDs))
				$arIBlockIDs[] = $iBlockID;
		}

		return $arIBlockIDs;
	}

	protected static function GetIBlockTypes()
	{
		$arIBlockType = array();

		$arCatalogID = array();
		if(CModule::IncludeModule("catalog") && !self::$force)
		{
			$arIBlockType = array(""=>"");
			$res = CCatalog::GetList(array("sort"=>"asc"), array("ACTIVE"=>"Y"));
			while($arr = $res->Fetch())
				if(!$arr["PRODUCT_IBLOCK_ID"])
					$arCatalogID[] = $arr["IBLOCK_TYPE_ID"];
		}

		$res = CIBlockType::GetList(array("sort"=>"asc"), array("ACTIVE"=>"Y"));
		while($arr = $res->Fetch())
			if(!$arCatalogID || in_array($arr["ID"], $arCatalogID))
				if($arr1 = CIBlockType::GetByIDLang($arr["ID"], LANGUAGE_ID))
					$arIBlockType[$arr["ID"]] = array_merge($arr, $arr1);

		return $arIBlockType;
	}

	protected static function GetIBlocks($arIBlockTypes=null, $arIBlockIDs=null)
	{
		$arIBlocks = array();

		$arFilter = array("ACTIVE"=>"Y");

		if($arIBlockTypes)
			$arFilter = array("TYPE"=>$arIBlockTypes);

		if($arIBlockIDs)
			$arFilter = array("ID"=>$arIBlockIDs);

		$arCatalogIDs = array();
		if(CModule::IncludeModule("catalog") && !self::$force)
		{
			$res = CCatalog::GetList(array("sort"=>"asc"), $arFilter);
			while($arr = $res->Fetch())
				if(!$arr["PRODUCT_IBLOCK_ID"])
					$arCatalogIDs[] = $arr["IBLOCK_ID"];
		}

		$res = CIBlock::GetList(array("sort"=>"asc"), $arFilter);
		while($arr = $res->Fetch())
			if(!$arCatalogIDs || in_array($arr["ID"], $arCatalogIDs))
				$arIBlocks[$arr["ID"]] = $arr;

		return $arIBlocks;
	}

	protected static function GetSections($arIBlockIDs, $maxDepthLevel=null, $forceSingle=false)
	{
		$arSections = array();

		if(!$arIBlockIDs)
			return $arSections;

		$arSort = array();
		$arFilter = array("ACTIVE"=>"Y");

		$singleRoot = $arIBlockIDs && !is_array($arIBlockIDs) || is_array($arIBlockIDs) && count($arIBlockIDs) == 1;
		if($forceSingle)
			$singleRoot = false;

		if($singleRoot)
			$arSort["left_margin"] = "asc";

		if($arIBlockIDs)
			$arFilter["IBLOCK_ID"] = $arIBlockIDs;

		if($maxDepthLevel)
			$arFilter["<=DEPTH_LEVEL"] = $maxDepthLevel - (!$singleRoot ? 1 : 0);

		$arTemp = array();
		$res = CIBlockSection::GetList($arSort, $arFilter);
		while($arr = $res->Fetch())
			$arTemp[$arr["IBLOCK_ID"]][$arr["LEFT_MARGIN"]] = $arr;

		$arIBlock = array();
		if(!$singleRoot)
			$arIBlock = self::GetIBlocks(null, $arIBlockIDs);

		if(!is_array($arIBlockIDs))
			$arIBlockIDs = array($arIBlockIDs);

		foreach($arIBlockIDs as $v)
		{
			if(!$singleRoot)
			{
				$arSections[] = $arIBlock[$v];
				ksort($arTemp[$v]);
			}

			foreach($arTemp[$v] as $arr)
			{
				if($singleRoot)
					$arr["DEPTH_LEVEL"]--;

				$arr["IS_SECTION"] = true;

				$arSections[] = $arr;
			}
		}

		return $arSections;
	}

	protected static function GetCurrencies($force=false)
	{
		$arCurrencies = array();

		$arSortedCurrencyCodes = array("RUB", "USD", "EUR", "BYR", "UAH", "KZT");

		$arTemp = array();

		if(!CModule::IncludeModule("currency") || $force)
		{
			// ������ ������ �� ����� (������ ����, �����, ��������)
			foreach($arSortedCurrencyCodes as $k=>$v)
				if($k == 0)
					$arCurrencies[] = array("CODE"=>$v, "RATE"=>1);
				else
					$arCurrencies[] = array("CODE"=>$v, "RATE"=>"CBRF");

			return $arCurrencies;
		}

		$factor = 1;
		$res = CCurrency::GetList($by="name", $order="asc", LANGUAGE_ID);
		while($arr = $res->Fetch())
		{
			$res1 = CCurrencyRates::GetList($by="date", $order="desc", array("CURRENCY"=>$arr["CURRENCY"]));
			if($arr1 = $res1->Fetch())
			{
				$arr["AMOUNT"] = $arr1["RATE"];
				$arr["AMOUNT_CNT"] = $arr1["RATE_CNT"];
			}

			if($arr["AMOUNT"] == 1 && $arr["AMOUNT_CNT"] != 1)
				$factor = $arr["AMOUNT_CNT"];

			$arr["AMOUNT"] /= $arr["AMOUNT_CNT"]*$factor;

			$arTemp[] = $arr;
		}

		// Sort by codes. With rate == 1 will be first.
		foreach($arTemp as $k=>$arr)
		{
			if($arr["AMOUNT"] == 1)
			{
				$arCurrencies[] = array("CODE"=>$arr["CURRENCY"], "RATE"=>$arr["AMOUNT"], "NAME"=>$arr["FULL_NAME"]);
				unset($arTemp[$k], $arSortedCurrencyCodes[$arr["CURRENCY"]]);
				break;
			}
		}
		foreach($arSortedCurrencyCodes as $code)
		{
			foreach($arTemp as $arr)
			{
				if($code == $arr["CURRENCY"] || ($code == "RUB" && $arr["CURRENCY"] == "RUR"))
				{
					$arCurrencies[] = array("CODE"=>$arr["CURRENCY"], "RATE"=>$arr["AMOUNT"], "NAME"=>$arr["FULL_NAME"]);
					break;
				}
			}
		}

		return $arCurrencies;
	}
}

class CSimaiCatalogYandexParameters extends CSimaiCatalogYandex
{
	protected static function GetIBlockTypes()
	{
		$arIBlockTypes = array(""=>"");

		foreach(parent::GetIBlockTypes() as $v)
			$arIBlockTypes[$v["ID"]] = "[".$v["ID"]."] ".$v["NAME"];

		return $arIBlockTypes;
	}

	protected static function GetIBlocks($arIBlockTypes=array())
	{
		$arIBlocks = array(""=>"");

		foreach(parent::GetIBlocks($arIBlockTypes) as $v)
			$arIBlocks[$v["ID"]] = "[".$v["ID"]."] ".$v["NAME"];

		return $arIBlocks;
	}

	protected static function GetMultipleIBlocks()
	{
		$arMultipleIBlocks = array();

		$arIBlockTypes = parent::GetIBlockTypes();

		$arIBlocks = parent::GetIBlocks();

		$arTemp = array();
		foreach($arIBlocks as $v)
			$arTemp[$v["IBLOCK_TYPE_ID"]][$v["ID"]] = $v["NAME"];

		$arID = array();
		foreach($arIBlockTypes as $k=>$v)
		{
			if(!$k || !$arTemp[$k])
				continue;

			$keys = array_keys($arTemp[$k]);
			$arID = array_merge($arID, $keys);

			$arMultipleIBlocks["#".implode(",", $keys)] = $v["NAME"];
			foreach($arTemp[$k] as $k1=>$v1)
				$arMultipleIBlocks["_".$k1] = "-- [".$k1."] ".$v1;
		}
		if($arMultipleIBlocks)
			$arMultipleIBlocks = array_merge(array("$".implode(",", $arID)=>GetMessage("ALL_CHOICES")), $arMultipleIBlocks);

		foreach($arIBlockTypes as &$v)
			$v = $v["NAME"];

		foreach($arIBlocks as &$v)
			$v = $v["NAME"];

		return array($arIBlockTypes, $arIBlocks, $arMultipleIBlocks);
	}

	private static function GetSectionDepths($arIBlockIDs)
	{
		global $DB;

		$arSectionDepths = array("");

		if(!$arIBlockIDs)
			return $arSectionDepths;

		if(!is_array($arIBlockIDs))
			$arIBlockIDs = array($arIBlockIDs);

		$arIBlockIDs = array_filter($arIBlockIDs);

		$where = "";
		if($arIBlockIDs)
			$where = $DB->ForSql(" WHERE IBLOCK_ID=".implode(" OR IBLOCK_ID=", $arIBlockIDs));

		$res = $DB->Query("SELECT MAX(DEPTH_LEVEL) AS MAX_DEPTH FROM b_iblock_section".$where);
		if($arr = $res->Fetch())
		{
			$max_depth = $arr["MAX_DEPTH"] + (count($arIBlockIDs) > 1 ? 1 : 0);
			$arSectionDepths = array_merge($arSectionDepths, array_combine(range(1, $max_depth), range(1, $max_depth)));
		}

		return $arSectionDepths;
	}

	protected static function GetSections($arIBlockIDs, $maxDepthLevel=null, $includeSubsections=false)
	{
		$arSections = array();

		foreach(parent::GetSections($arIBlockIDs, $maxDepthLevel, !$includeSubsections) as $v)
		{
			if($v["IS_SECTION"])
			{
				$sign = str_repeat("--", $v["DEPTH_LEVEL"]);
				$arSections[$v["IBLOCK_ID"].",".$v["DEPTH_LEVEL"].",".$v["ID"]] = $sign." [".$v["ID"]."] ".$v["NAME"];
			}
			else
			{
				$arSections[$v["ID"].",0,0"] = $v["NAME"];
			}
		}

		if(!$arSections)
			return array(",,"=>GetMessage("IBLOCK_SECTION_NO_SECTIONS"));

		$arSections = array_merge(array(",,"=>GetMessage("IBLOCK_SECTION_ALL_SECTIONS")), $arSections);

		return $arSections;
	}

	private static function GetProperties($arIBlockIDs, $force=false)
	{
		$arPropertyTypes = array("ALL", "LNS", "F", "E", "S_LNS", "S_LS", "S_NS", "S_N", "S_C", "S_S", "M_LNS");

		$arProperties = array(array_fill_keys($arPropertyTypes, array()));

		if(!$arIBlockIDs)
			return $arProperties;

		if(!is_array($arIBlockIDs))
			$arIBlockIDs = array($arIBlockIDs);

		foreach($arIBlockIDs as $v)
		{
			// PROPERTY_TYPE: S - ������, N - �����, F - ����, L - ������, E - �������� � ���������, G - �������� � �������
			// MULTIPLE: ��������������� (Y|N)
			// LIST_TYPE: ����� ���� "L" - ���������� ������ ��� "C" - ������

			$arProperties[$v] = array_fill_keys($arPropertyTypes, array());

			$res = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("ACTIVE"=>"Y", "IBLOCK_ID"=>$v));
			while($arr = $res->Fetch())
			{
				$key = $arr["CODE"] ? $arr["CODE"] : "id".$arr["ID"];
				$value = "[".$arr["CODE"]."] ".$arr["NAME"];
				$type = $arr["PROPERTY_TYPE"];
				$list = $arr["LIST_TYPE"];
				$multiple = $arr["MULTIPLE"];

				$arProperties[$v]["ALL"][$key] = $value;

				if(in_array($type, array("N", "S")) || ($type == "L" && $list == "L"))
				{
					$arProperties[$v]["LNS"][$key] = $value;

					if($multiple == "Y")
						$arProperties[$v]["S_LNS"][$key] = $value;
					else
						$arProperties[$v]["M_LNS"][$key] = $value;
				}
				if($multiple == "N")
				{
					if($type == "S" || ($type == "L" && $list == "L"))
						$arProperties[$v]["S_LS"][$key] = $value;

					if(in_array($type, array("N", "S")))
						$arProperties[$v]["S_NS"][$key] = $value;

					if($type == "N")
						$arProperties[$v]["S_N"][$key] = $value;

					if($type == "L" && $list == "C")
						$arProperties[$v]["S_C"][$key] = $value;

					if($type == "S")
						$arProperties[$v]["S_S"][$key] = $value;
				}
				if($type == "F")
					$arProperties[$v]["F"][$key] = $value;

				if($type == "E")
					$arProperties[$v]["E"][$key] = $value;
			}

			if($force)
			{
				foreach($arPropertyTypes as $k)
				{
					if($k == "ALL")
						continue;

					$diff = array_diff_key($arProperties[$v]["ALL"], $arProperties[$v][$k]);
					if($diff)
						$arProperties[$v][$k] = array_merge($arProperties[$v][$k], array("-"=>"---"), $diff);

				}
			}
			foreach($arPropertyTypes as $k)
				$arProperties[$v][$k] = array_merge(array(""=>""), $arProperties[$v][$k]);
		}

		return $arProperties;
	}

	private static function GetCatalogIBlockIDs()
	{
		$arCatalogIBlockIDs = array();
		
		if(!CModule::IncludeModule("catalog"))
			return $arCatalogIBlockIDs;
		
		//$res = CCatalog::GetList(array(), array(">PRODUCT_IBLOCK_ID"=>0));
		$res = CCatalog::GetList();
		while($arr = $res->Fetch())
			$arCatalogIBlockIDs[] = $arr["IBLOCK_ID"];

		return $arCatalogIBlockIDs;
	}

	private static function GetPrices()
	{
		$arPrices = array();

		if(!CModule::IncludeModule("catalog"))
			return $arPrices;

		$arPrices = array(""=>"");
		$res = CCatalogGroup::GetList($v1="sort", $v2="asc");
		while($arr = $res->Fetch())
			$arPrices[$arr["NAME"]] = "[".$arr["NAME"]."] ".$arr["NAME_LANG"];

		return $arPrices;
	}

	protected static function GetCurrencies($force=false)
	{
		$arCurrencies = parent::GetCurrencies($force);

		$arTemp = array();
		foreach($arCurrencies as $v)
			$arTemp[$v["CODE"]] = "[".$v["CODE"]."] ".(isset($v["NAME"]) ? $v["NAME"] : GetMessage("OFFER_CURRENCY_".$v["CODE"]));

		return $arTemp;
	}

	private static function GetModuleCurrencies()
	{
		$arCurrencies = parent::GetCurrencies();

		$str = "";

		if(count($arCurrencies) == 1)
		{
			$str = $arCurrencies[0]["CODE"]." = ".$arCurrencies[0]["RATE"]."<br>";
		}
		elseif(count($arCurrencies) > 1)
		{
			foreach($arCurrencies as $k=>$v)
				if($k != 0)
					$str .= "1 ".$arCurrencies[0]["CODE"]." = ".$v["RATE"]." ".$v["CODE"]."<br>";
		}

		return $str;
	}

	private static function GetDefault($arChoices, $k1="", $k2="")
	{
		if(count($arChoices) == 2)
			return end(array_keys($arChoices));

		if($k1 && array_key_exists($k1, $arChoices))
			return $k1;

		if($k2 && array_key_exists($k2, $arChoices))
			return $k2;

		return "";
	}

	public static function Add(&$arComponentParameters, &$arCurrentValues)
	{
		$multiple = $arCurrentValues["USE_MULTIPLE_DATA_SOURCES"] == "Y";
		$includeSubsections = $arCurrentValues["INCLUDE_SUBSECTIONS"] == "Y";
		$maxDepthLevel = $arCurrentValues["SECTION_SHOW_DEPTH"] > 0 ? $arCurrentValues["SECTION_SHOW_DEPTH"] : null;
		$useExtraShopSettings = $arCurrentValues["USE_EXTRA_SHOP_SETTINGS"] == "Y";

		$arMultipleIBlocks = array();

		if(!$multiple)
		{
			$arIBlockTypes = self::GetIBlockTypes();
			$arIBlocks = self::GetIBlocks($arCurrentValues["IBLOCK_TYPE"]);
			if(is_array($arCurrentValues["IBLOCK_ID"]))
				unset($arCurrentValues["IBLOCK_ID"]);
		}
		else
		{
			list($arIBlockTypes, $arIBlocks, $arMultipleIBlocks) = self::GetMultipleIBlocks();
			$arCurrentValues["IBLOCK_ID"] = self::GetMultipleIBlockIDs($arCurrentValues["IBLOCK_ID"]);
		}

		$arSectionDepths = self::GetSectionDepths($arCurrentValues["IBLOCK_ID"]);

		$arSections = self::GetSections($arCurrentValues["IBLOCK_ID"], $maxDepthLevel, $includeSubsections);

		$arIBlockIDs = self::GetSectionIBlockIDs($arCurrentValues["SECTION_IDS"]);
		if($multiple && $arIBlockIDs)
			$arCurrentValues["IBLOCK_ID"] = $arIBlockIDs;

		$arProperties = self::GetProperties($arCurrentValues["IBLOCK_ID"], true);

		$arCatalogIBlockIDs = self::GetCatalogIBlockIDs();

		$arPrices = self::GetPrices();

		// currencies from valute module or predefined.
		$arCurrencies = self::GetCurrencies();
		$arCurrenciesWithNoModule = self::GetCurrencies(true);

		$moduleCurrencies = self::GetModuleCurrencies();

		$arGroups = array(
			"CATEGORY_SETTINGS" => array(
				"NAME" => GetMessage("CATEGORY_SETTINGS"),
				"SORT" => 300
			),
		);
		$arParameters = array(
			"USE_MULTIPLE_DATA_SOURCES" => array(
				"PARENT" => "DATA_SOURCE",
				"NAME" => GetMessage("USE_MULTIPLE_DATA_SOURCES"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
				"REFRESH" => "Y",
			),
		);

		if(!$multiple || ($multiple && count($arMultipleIBlocks) <= 3))
		{
			$arParameters = array_merge($arParameters, array(
				"IBLOCK_TYPE" => array(
					"PARENT" => "DATA_SOURCE",
					"NAME" => GetMessage("IBLOCK_TYPE"),
					"TYPE" => "LIST",
					"VALUES" => $arIBlockTypes,
					"REFRESH" => "Y",
					"DEFAULT" => self::GetDefault($arIBlockTypes)
				),
				"IBLOCK_ID" => array(
					"PARENT" => "DATA_SOURCE",
					"NAME" => GetMessage("IBLOCK_ID"),
					"TYPE" => "LIST",
					"VALUES" => $arIBlocks,
					"REFRESH" => "Y",
					"DEFAULT" => self::GetDefault($arIBlocks)
				),
			));
		}
		else
		{
			$arParameters = array_merge($arParameters, array(
				"IBLOCK_ID" => array(
					"PARENT" => "DATA_SOURCE",
					"NAME" => GetMessage("IBLOCK_IDS"),
					"TYPE" => "LIST",
					"VALUES" => $arMultipleIBlocks,
					"MULTIPLE" => "Y",
					"REFRESH" => "Y",
				),
			));
		}
		$arParameters = array_merge($arParameters, array(
			"SECTION_SHOW_DEPTH" => array(
				"PARENT" => "CATEGORY_SETTINGS",
				"NAME" => GetMessage("SECTION_SHOW_DEPTH"),
				"TYPE" => "LIST",
				"VALUES" => $arSectionDepths,
				"REFRESH" => "Y",
			),
			"SECTION_IDS" => array(
				"PARENT" => "CATEGORY_SETTINGS",
				"NAME" => GetMessage("SECTION_IDS"),
				"TYPE" => "LIST",
				"MULTIPLE" => "Y",
				"VALUES" =>$arSections,
				"REFRESH" => "Y",
			),
			"INCLUDE_SUBSECTIONS" => Array(
				"PARENT" => "CATEGORY_SETTINGS",
				"NAME" => GetMessage("INCLUDE_SUBSECTIONS"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "Y",
				"REFRESH" => "Y"
			),
		));

		if(!$arCurrentValues["IBLOCK_ID"])
			$arCurrentValues["IBLOCK_ID"] = array(0);

		if(!is_array($arCurrentValues["IBLOCK_ID"]))
			$arCurrentValues["IBLOCK_ID"] = array($arCurrentValues["IBLOCK_ID"]);

		$sort = 301;
		$singleIBlock = count($arCurrentValues["IBLOCK_ID"]) == 1;
		foreach($arCurrentValues["IBLOCK_ID"] as $i=>$v)
		{
			$k = ($i ? "_".$i : "");

			$useExtraOfferSettings = $arCurrentValues["USE_EXTRA_OFFER_SETTINGS".$k] == "Y";

			// Offer, delivery and other groups
			$arGroups = array_merge($arGroups, array(
				"OFFER_SETTINGS".$k => array(
					"NAME" => $singleIBlock ? GetMessage("OFFER_SETTINGS") : GetMessage("OFFER_SETTINGS_MULTIPLE").$arIBlocks[$v],
					"SORT" => $sort++,
				),
				"OFFER_DELIVERY_SETTINGS".$k => array(
					"NAME" => $singleIBlock ? GetMessage("OFFER_DELIVERY_SETTINGS") : GetMessage("OFFER_DELIVERY_SETTINGS_MULTIPLE").$arIBlocks[$v],
					"SORT" => $sort++,
				),
				"OFFER_OTHER_SETTINGS".$k => array(
					"NAME" => $singleIBlock ? GetMessage("OFFER_OTHER_SETTINGS") : GetMessage("OFFER_OTHER_SETTINGS_MULTIPLE").$arIBlocks[$v],
					"SORT" => $sort++,
				),
			));

			if(!$useExtraOfferSettings)
				unset($arGroups["OFFER_DELIVERY_SETTINGS".$k], $arGroups["OFFER_OTHER_SETTINGS".$k]);

			// Offer parameters
			if(in_array($v, $arCatalogIBlockIDs))
			{
				if(!empty($arCurrentValues["OFFER_PRICE".$k]) && !array_key_exists($arCurrentValues["OFFER_PRICE".$k], $arPrices))
					$arCurrencies = $arCurrenciesWithNoModule;

				$arPrices = array_merge($arPrices, array("-"=>"---"), $arProperties[$v]["S_NS"]);
			}
			else
			{
				$arPrices = $arProperties[$v]["S_NS"];
			}
			$arParameters = array_merge($arParameters, array(
				"USE_EXTRA_OFFER_SETTINGS".$k => array(
					"PARENT" => "OFFER_SETTINGS".$k,
					"NAME" => GetMessage("USE_EXTRA_OFFER_SETTINGS"),
					"TYPE" => "CHECKBOX",
					"DEFAULT" => "N",
					"REFRESH" => "Y",
				),
				"OFFER_TYPE".$k => array(
					"PARENT" => "OFFER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_TYPE"),
					"TYPE" => "LIST",
					"VALUES" => array(
						"" => GetMessage("OFFER_TYPE_SIMPLE"),
						"vendor.model" => GetMessage("OFFER_TYPE_VENDOR_SIMPLE"),
						"book" => GetMessage("OFFER_TYPE_BOOK"),
						"audiobook" => GetMessage("OFFER_TYPE_AUDIOBOOK"),
						"artist.title" => GetMessage("OFFER_TYPE_ARTIST_TITLE"),
						"tour" => GetMessage("OFFER_TYPE_TOUR"),
						"event-ticket" => GetMessage("OFFER_TYPE_EVENT_TICKET"),
					),
					"DEFAULT" => "",
					"REFRESH" => "Y",
				),
				"OFFER_PRICE".$k => array(
					"PARENT" => "OFFER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_PRICE"),
					"TYPE" => "LIST",
					"VALUES" => $arPrices,
					"DEFAULT" => self::GetDefault($arPrices, "BASE", "PRICE"),
					"REFRESH" => "Y",
				),
				"OFFER_CURRENCY".$k => array(
					"PARENT" => "OFFER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_CURRENCY"),
					"TYPE" => "LIST",
					"VALUES" => $arCurrencies,
					"DEFAULT" => self::GetDefault($arCurrencies, "RUB"),
				),
			));
			if($arPrices[$v])
				unset($arParameters["OFFER_CURRENCY".$k]);

			if(!$arCurrentValues["OFFER_TYPE".$k])
			{
				// name, vendor?, vendorCode?
				$arParameters = array_merge($arParameters, array(
					"OFFER_NAME".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_NAME"),
						"TYPE" => "LIST",
						"VALUES" => array("NAME"=>GetMessage("OFFER_NAME_NAME")),
						"DEFAULT" => "NAME",
					),
					"OFFER_VENDOR".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_VENDOR_SIMPLE"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => self::GetDefault($arProperties[$v]["LNS"], "MANUFACTURER"),
					),
					"OFFER_VENDOR_CODE".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_VENDOR_CODE"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_MARKET_CATEGORY".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_MARKET_CATEGORY"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
				));
				if(!$useExtraOfferSettings)
					foreach(array("VENDOR", "VENDOR_CODE") as $k1)
						unset($arParameters["OFFER_".$k1.$k]);
			}
			elseif($arCurrentValues["OFFER_TYPE".$k] == "vendor.model")
			{
				// typePrefix?, vendor, vendorCode?, model, (provider, tarifplan?)?
				$arParameters = array_merge($arParameters, array(
					"OFFER_TYPE_PREFIX".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_TYPE_PREFIX"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_VENDOR".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_VENDOR"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["LNS"],
						"DEFAULT" => self::GetDefault($arProperties[$v]["LNS"], "MANUFACTURER"),
					),
					"OFFER_VENDOR_CODE".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_VENDOR_CODE"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["LNS"],
						"DEFAULT" => "",
					),
					"OFFER_MODEL".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_MODEL"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["LNS"],
					),
					"OFFER_PROVIDER".$k => array(
						"PARENT" => "OFFER_SETTINGS",
						"NAME" => GetMessage("OFFER_PROVIDER"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["LNS"],
						"DEFAULT" => "",
					),
					"OFFER_TARIFPLAN".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_TARIFPLAN"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["LNS"],
						"DEFAULT" => "",
					),
				));
				if(!$useExtraOfferSettings)
					foreach(array("TYPE_PREFIX", "VENDOR_CODE", "PROVIDER", "TARIFPLAN") as $k1)
						unset($arParameters["OFFER_".$k1.$k]);

			}
			elseif($arCurrentValues["OFFER_TYPE".$k] == "book")
			{
				// author?, name, publisher?, series?, year?, ISBN?, volume?, part?, language?, binding?, page_extent?, table_of_contents?
				$arParameters = array_merge($arParameters, array(
					"OFFER_AUTHOR".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_AUTHOR"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_NAME".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_BOOK_NAME"),
						"TYPE" => "LIST",
						"VALUES" => array("NAME"=>"��������"),
						"DEFAULT" => "NAME",
					),
					"OFFER_PUBLISHER".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_PUBLISHER"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_SERIES".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_SERIES"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_YEAR".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_YEAR"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_ISBN".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_ISBN"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_VOLUME".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_VOLUME"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_PART".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_PART"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_LANGUAGE".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_LANGUAGE"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_BINDING".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_BINDING"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_PAGE_EXTENT".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_PAGE_EXTENT"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_TABLE_OF_CONTENTS".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_TABLE_OF_CONTENTS"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					)
				));
				if(!$useExtraOfferSettings)
					foreach(array("AUTHOR", "PUBLISHER", "SERIES", "YEAR", "ISBN", "VOLUME", "PART", "LANGUAGE", "BINDING", "PAGE_EXTENT", "TABLE_OF_CONTENTS") as $k1)
						unset($arParameters["OFFER_".$k1.$k]);
			}
			elseif($arCurrentValues["OFFER_TYPE".$k] == "audiobook")
			{
				// author?, name, publisher?, series?, year?, ISBN?, volume?, part?, language?, table_of_contents?, performed_by?,
				// performance_type?, storage?, format?, recording_length?
				$arParameters = array_merge($arParameters, array(
					"OFFER_AUTHOR".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_AUTHOR"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_NAME".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_NAME"),
						"TYPE" => "LIST",
						"VALUES" => array("NAME"=>GetMessage("OFFER_NAME_NAME")),
						"DEFAULT" => "NAME",
					),
					"OFFER_PUBLISHER".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_PUBLISHER"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_SERIES".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_SERIES"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_YEAR".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_YEAR"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_ISBN".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_ISBN"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_VOLUME".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_VOLUME"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_PART".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_PART"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_LANGUAGE".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_LANGUAGE"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_TABLE_OF_CONTENTS".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_TABLE_OF_CONTENTS"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_PERFORMED_BY".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_PERFORMED_BY"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_PERFORMANCE_TYPE".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_PERFORMANCE_TYPE"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_STORAGE".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_STORAGE"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_FORMAT".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_FORMAT"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_RECORDING_LENGTH".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_RECORDING_LENGTH"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),

				));
				if(!$useExtraOfferSettings)
					foreach(array("AUTHOR", "PUBLISHER", "SERIES", "YEAR", "ISBN", "VOLUME", "PART", "LANGUAGE", "TABLE_OF_CONTENTS", "PERFORMED_BY", "PERFORMANCE_TYPE", "STORAGE", "FORMAT", "RECORDING_LENGTH") as $k1)
						unset($arParameters["OFFER_".$k1.$k]);
			}
			elseif($arCurrentValues["OFFER_TYPE".$k] == "artist.title")
			{
				// artist?, title, year?, media?, starring?, director?, originalName?, country?
				$arParameters = array_merge($arParameters, array(
					"OFFER_ARTIST".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_ARTIST"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_TITLE".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_TITLE"),
						"TYPE" => "LIST",
						"VALUES" => array("NAME"=>GetMessage("OFFER_NAME_NAME")),
						"DEFAULT" => "NAME",
					),
					"OFFER_YEAR".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_YEAR"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_MEDIA".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_MEDIA"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_STARRING".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_STARRING"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_DIRECTOR".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_DIRECTOR"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_ORIGINAL_NAME".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_ORIGINAL_NAME"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_COUNTRY".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_COUNTRY"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
				));
				if(!$useExtraOfferSettings)
					foreach(array("ARTIST", "YEAR", "MEDIA", "STARRING", "DIRECTOR", "ORIGINAL_NAME", "COUNTRY") as $k1)
						unset($arParameters["OFFER_".$k1.$k]);
			}
			elseif($arCurrentValues["OFFER_TYPE".$k] == "tour")
			{
				// worldRegion?, country?, region?, days, dataTour*, name, hotel_stars?, room?, meal?, included, transport, price_min?, price_max?, options?
				$arParameters = array_merge($arParameters, array(
					"OFFER_WORLD_REGION".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_WORLD_REGION"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_COUNTRY".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_COUNTRY"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_REGION".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_REGION"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
						"DEFAULT" => "",
					),
					"OFFER_DAYS".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_DAYS"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_DATA_TOUR".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_DATA_TOUR"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_NAME".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_NAME"),
						"TYPE" => "LIST",
						"VALUES" => array("NAME"=>GetMessage("OFFER_NAME_NAME")),
					),
					"OFFER_HOTEL_STARS".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_HOTEL_STARS"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_ROOM".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_ROOM"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_MEAL".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_MEAL"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_INCLUDED".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_INCLUDED"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_C"],
					),
					"OFFER_TRANSPORT".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_TRANSPORT"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_PRICE_MIN".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_PRICE_MIN"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_PRICE_MAX".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_PRICE_MAX"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_OPTIONS".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_OPTIONS"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
				));
				if(!$useExtraOfferSettings)
					foreach(array("WORLD_REGION", "COUNTRY", "REGION", "HOTEL_STARS", "ROOM", "MEAL", "PRICE_MIN", "PRICE_MAX", "OPTIONS") as $k1)
						unset($arParameters["OFFER_".$k1.$k]);
			}
			elseif($arCurrentValues["OFFER_TYPE".$k] == "event-ticket")
			{
				// name, place, hall?, hall_part?, date, is_premiere?, is_kids?
				$arParameters = array_merge($arParameters, array(
					"OFFER_NAME".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_EVENT_TICKET_NAME"),
						"TYPE" => "LIST",
						"VALUES" => array("NAME"=>GetMessage("OFFER_NAME_NAME")),
						"DEFAULT" => "NAME",
					),
					"OFFER_PLACE".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_PLACE"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_HALL".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_HALL"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_HALL_PART".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_HALL_PART"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_DATE".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_DATE"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_IS_PREMIERE".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_IS_PREMIERE"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					),
					"OFFER_IS_KIDS".$k => array(
						"PARENT" => "OFFER_SETTINGS".$k,
						"NAME" => GetMessage("OFFER_IS_KIDS"),
						"TYPE" => "LIST",
						"VALUES" => $arProperties[$v]["S_LS"],
					)
				));
				if(!$useExtraOfferSettings)
					foreach(array("HALL", "HALL_PART", "IS_PREMIERE", "IS_KIDS") as $k1)
						unset($arParameters["OFFER_".$k1.$k]);
			}
			// -url?-, -buyurl?-, price, -wprice?-, -currencyId-, -xCategory?-, -categoryId+-, -market_category?-,
			// picture*, store?, pickup?, delivery?, -deliveryIncluded?-, local_delivery_cost?,-orderingTime?-
			$arParameters = array_merge($arParameters, array(
				"OFFER_PICTURE".$k => array(
					"PARENT" => "OFFER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_PICTURE"),
					"TYPE" => "LIST",
					"VALUES" => array(
						"0" => "",
						"PREVIEW_PICTURE" => GetMessage("OFFER_PICTURE_PREVIEW"),
						"DETAIL_PICTURE" => GetMessage("OFFER_PICTURE_DETAIL"),
						"DETAIL_PICTURE,PREVIEW_PICTURE" => GetMessage("OFFER_PICTURE_DETAIL_PREVIEW"),
						"PREVIEW_PICTURE,DETAIL_PICTURE" => GetMessage("OFFER_PICTURE_PREVIEW_DETAIL"),
						"MORE_PHOTO" => GetMessage("OFFER_PICTURE_MORE_PHOTO"),
						"PREVIEW_PICTURE,MORE_PHOTO" => GetMessage("OFFER_PICTURE_PREVIEW_MORE_PHOTO"),
						"DETAIL_PICTURE,MORE_PHOTO" => GetMessage("OFFER_PICTURE_DETAIL_MORE_PHOTO"),
						"PREVIEW_PICTURE,DETAIL_PICTURE,MORE_PHOTO" => GetMessage("OFFER_PICTURE_PREVIEW_DETAIL_MORE_PHOTO"),
						"DETAIL_PICTURE,PREVIEW_PICTURE,MORE_PHOTO" => GetMessage("OFFER_PICTURE_DETAIL_PREVIEW_MORE_PHOTO"),
					),
					"DEFAULT" => "PREVIEW_PICTURE,DETAIL_PICTURE,MORE_PHOTO",
				),
				"OFFER_DESCRIPTION".$k => array(
					"PARENT" => "OFFER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_DESCRIPTION_".($arCurrentValues["OFFER_TYPE".$k] == "tour" ? "TOUR" : (in_array($arCurrentValues["OFFER_TYPE".$k], array("book", "audiobook")) ? "BOOK" : ""))),
					"TYPE" => "LIST",
					"VALUES" => array(
						"0"=>"",
						"PREVIEW_TEXT" => GetMessage("OFFER_DESCRIPTION_PREVIEW"),
						"DETAIL_TEXT" => GetMessage("OFFER_DESCRIPTION_DETAIL"),
						"PREVIEW_TEXT,DETAIL_TEXT" => GetMessage("OFFER_DESCRIPTION_PREVIEW_DETAIL"),
						"DETAIL_TEXT,PREVIEW_TEXT" => GetMessage("OFFER_DESCRIPTION_DETAIL_PREVIEW"),
					),
					"DEFAULT" => "PREVIEW_TEXT,DETAIL_TEXT",
				),
				"OFFER_AVAILABLE".$k => array(
					"PARENT" => "OFFER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_AVAILABLE"),
					"TYPE" => "LIST",
					"VALUES" => in_array($v, $arCatalogIBlockIDs) ? array_merge(array("QUANTITY"=>GetMessage("OFFER_AVAILABLE_FROM_SALE")), $arProperties[$v]["S_C"]) : $arProperties[$v]["S_C"],
					"DEFAULT" => "PREVIEW_TEXT,DETAIL_TEXT",
				),
				"OFFER_STORE".$k => array(
					"PARENT" => "OFFER_DELIVERY_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_STORE"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_C"],
				),
				"OFFER_PICKUP".$k => array(
					"PARENT" => "OFFER_DELIVERY_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_PICKUP"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_C"],
				),
				"OFFER_DELIVERY".$k => array(
					"PARENT" => "OFFER_DELIVERY_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_DELIVERY"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_C"],
				),
				"OFFER_LOCAL_DELIVERY_COST".$k => array(
					"PARENT" => "OFFER_DELIVERY_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_LOCAL_DELIVERY_COST"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_NS"],
				),
			));
			if(!$useExtraOfferSettings)
				foreach(array("STORE", "PICKUP", "DELIVERY", "LOCAL_DELIVERY_COST") as $k1)
					unset($arParameters["OFFER_".$k1.$k]);

			// aliases?, additional*, description?, sales_notes?, promo?, manufacturer_warranty?, country_of_origin?, downloadable?,
			// adult?, age?, barcode*, param*, related_offer*
			$arParameters = array_merge($arParameters, array(
				/*"OFFER_ALIASES" => array(
					"PARENT" => "OFFER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_ALIASES"),
					"TYPE" => "LIST",
					"VALUES" => $arProperty_LS[$v],
					"DEFAULT" => "",
				),
				"OFFER_ADDITIONAL" => array(
					"PARENT" => "OFFER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_ADDITIONAL"),
					"TYPE" => "LIST",
					"VALUES" => $arProperty_LS[$v],
					"DEFAULT" => "",
				),*/
				"OFFER_BID".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_BID"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_NS"],
				),
				"OFFER_CBID".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_CBID"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_NS"],
				),
				"OFFER_SALES_NOTES".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_SALES_NOTES"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_S"],
				),
				"OFFER_PROMO".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_PROMO"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["LNS"],
				),
				"OFFER_MANUFACTURER_WARRANTY".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_MANUFACTURER_WARRANTY"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_C"],
				),
				"OFFER_SELLER_WARRANTY".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_SELLER_WARRANTY"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_C"],
				),
				"OFFER_COUNTRY_OF_ORIGIN".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_COUNTRY_OF_ORIGIN"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_LS"],
				),
				"OFFER_DOWNLOADABLE".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_DOWNLOADABLE"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_C"],
				),
				"OFFER_ADULT".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_ADULT"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_C"],
				),
				"OFFER_AGE".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_AGE"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["S_LNS"],
				),
				"OFFER_BARCODE".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_BARCODE"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["LNS"],
				),
				"OFFER_PARAM".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_PARAM"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["LNS"],
				),
				"OFFER_RELATED_OFFER".$k => array(
					"PARENT" => "OFFER_OTHER_SETTINGS".$k,
					"NAME" => GetMessage("OFFER_RELATED_OFFER"),
					"TYPE" => "LIST",
					"VALUES" => $arProperties[$v]["E"],
					"ADDITIONAL_VALUES" => "Y",
				),
				"USE_OFFER_SETTINGS_NOTES".$k => array(
					"PARENT" => "OFFER_SETTINGS".$k,
					"TYPE" => "CUSTOM",
					"JS_FILE" => "/bitrix/js/main/comp_props.js",
					"JS_EVENT" => "BxShowComponentNotes",
					"JS_DATA" => GetMessage("USE_OFFER_SETTINGS_NOTES"),
				),
			));

			if(!$useExtraOfferSettings)
				foreach(array("ALIASES", "ADDITIONAL", "BID", "CBID", "SALES_NOTES", "PROMO", "MANUFACTURER_WARRANTY", "COUNTRY_OF_ORIGIN", "DOWNLOADABLE", "ADULT", "AGE", "BARCODE", "PARAM", "RELATED_OFFER") as $k1)
					unset($arParameters["OFFER_".$k1.$k]);

		}

		foreach($arParameters as $key=>&$k1)
			if(!strncmp($key, "OFFER_", 6) && strncmp($key, "OFFER_TYPE", 10))
				$k1["ADDITIONAL_VALUES"] = $arCurrentValues["USE_OFFER_ADDITIONAL_VALUES"];

		// Shop
		$arGroups = array_merge($arGroups, array(
			"SHOP_SETTINGS" => array(
				"NAME" => GetMessage("SHOP_SETTINGS"),
				"SORT" => 500,
			),
		));

		$arParameters = array_merge($arParameters, array(
			"USE_EXTRA_SHOP_SETTINGS" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("USE_EXTRA_SHOP_SETTINGS"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
				"REFRESH" => "Y",
			),
			"SHOP_NAME" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_NAME"),
				"TYPE" => "STRING",
			),
			"SHOP_COMPANY" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_COMPANY"),
				"TYPE" => "STRING",
			),
			"SHOP_URL" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_URL"),
				"TYPE" => "STRING",
				"DEFAULT" => "http://".$_SERVER["SERVER_NAME"],
			),
			"SHOP_PHONE" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_PHONE"),
				"TYPE" => "STRING",
			),
			"SHOP_PLATFORM" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_PLATFORM"),
				"TYPE" => "STRING",
				"DEFAULT" => "Simai.CatalogYandex (1�-Bitrix)",
			),
			"SHOP_VERSION" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_VERSION"),
				"TYPE" => "STRING",
				"DEFAULT" => "2.0.0",
			),
			"SHOP_AGENCY" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_AGENCY"),
				"TYPE" => "STRING",
			),
			"SHOP_EMAIL" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_EMAIL"),
				"TYPE" => "STRING",
				"MULTIPLE" => "Y",
			),
			
			"SHOP_STORE" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_STORE"),
				"TYPE" => "CHECKBOX",
			),
			"SHOP_PICKUP" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_PICKUP"),
				"TYPE" => "CHECKBOX",
			),
			"SHOP_DELIVERY" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_DELIVERY"),
				"TYPE" => "CHECKBOX",
			),
			"SHOP_LOCAL_DELIVERY_COST" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_LOCAL_DELIVERY_COST"),
				"TYPE" => "STRING",
			),
			"SHOP_ADULT" => array(
				"PARENT" => "SHOP_SETTINGS",
				"NAME" => GetMessage("SHOP_ADULT"),
				"TYPE" => "CHECKBOX",
			),
			"USE_SHOP_SETTINGS_NOTES" => array(
				"PARENT" => "SHOP_SETTINGS",
				"TYPE" => "CUSTOM",
				"JS_FILE" => "/bitrix/js/main/comp_props.js",
				"JS_EVENT" => "BxShowComponentNotes",
				"JS_DATA" => GetMessage("USE_SHOP_SETTINGS_NOTES"),
			),
		));
		//echo $useExtraShopSettings;
		if(!$useExtraShopSettings)
			foreach(array("PHONE", "PLATFORM", "VERSION", "AGENCY", "EMAIL", "STORE", "PICKUP", "DELIVERY", "LOCAL_DELIVERY_COST", "ADULT") as $v)
			{
				//echo "SHOP_".$v;
				unset($arParameters["SHOP_".$v]);
			}

		// Currency
		$arGroups = array_merge($arGroups, array(
			"CURRENCY_SETTINGS" => array(
				"NAME" => GetMessage("CURRENCY_SETTINGS"),
				"SORT" => 501,
			),
		));

		$arParameters = array_merge($arParameters, array(
			"CURRENCY_BANK" => array(
				"PARENT" => "CURRENCY_SETTINGS",
				"NAME" => GetMessage("CURRENCY_BANK"),
				"TYPE" => "LIST",
				"VALUES" => array(
					"" => "",
					"MODULE" => GetMessage("CURRENCY_BANK_MODULE"),
					"CBRF" => GetMessage("CURRENCY_BANK_CBRF"),
					"NBU" => GetMessage("CURRENCY_BANK_NBU"),
					"NBK" => GetMessage("CURRENCY_BANK_NBK"),
					"��" => GetMessage("CURRENCY_BANK_��"),
				),
				"DEFAULT" => $moduleCurrencies ? "MODULE" : "",
			),
			"CURRENCY_BANK_RATE_PLUS" => array(
				"PARENT" => "CURRENCY_SETTINGS",
				"NAME" => GetMessage("CURRENCY_BANK_RATE_PLUS"),
				"TYPE" => "LIST",
				"VALUES" => array_merge(array(""=>""), array_combine(range(1, 29), range(1, 29))),
			),
			"USE_CURRENCY_SETTINGS_NOTES" => array(
				"PARENT" => "CURRENCY_SETTINGS",
				"TYPE" => "CUSTOM",
				"JS_FILE" => "/bitrix/js/main/comp_props.js",
				"JS_EVENT" => "BxShowComponentNotes",
				"JS_DATA" => GetMessage("USE_CURRENCY_SETTINGS_NOTES".($moduleCurrencies ? "" : "_NO_MODULE"), array("#CURRENCIES#"=>$moduleCurrencies)),
			),
		));
		if(!$moduleCurrencies)
			unset($arParameters["CURRENCY_BANK"]["VALUES"]["MODULE"]);

		// Cache parameters
		$arParameters = array_merge($arParameters, array(
			"CACHE_TIME" => array("DEFAULT"=>3600),
		));

		// Additional parameters
		$filename = COption::GetOptionString("simai.catalogyandex", "filename");
		$filenameWithoutExtension = GetFileNameWithoutExtension($filename);
		$extension = $arCurrentValues["COMPRESS"] == "Y" ? ".zip" : ".xml";
		$arParameters = array_merge($arParameters, array(
			"IN_UTF8" => array(
				"PARENT" => "ADDITIONAL_SETTINGS",
				"NAME" => GetMessage("IN_UTF8"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
			),		
			"NOT_SHOW_NA" => array(
				"PARENT" => "ADDITIONAL_SETTINGS",
				"NAME" => GetMessage("NOT_SHOW_NA"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
			),
			"USE_VAT_PRICES" => array(
				"PARENT" => "ADDITIONAL_SETTINGS",
				"NAME" => GetMessage("USE_VAT_PRICES"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
			),
			"COMPRESS" => array(
				"PARENT" => "ADDITIONAL_SETTINGS",
				"NAME" => GetMessage("COMPRESS"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
				"REFRESH" => "Y",
			),
			"USE_OUTPUT_FILE" => array(
				"PARENT" => "ADDITIONAL_SETTINGS",
				"NAME" => GetMessage("USE_OUTPUT_FILE"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
				"REFRESH" => "Y",
			),
			"OUTPUT_PATH" => array(
				"PARENT" => "ADDITIONAL_SETTINGS",
				"NAME" => GetMessage("OUTPUT_PATH"),
				"TYPE" => "STRING",
				"DEFAULT" => "/upload/".$filenameWithoutExtension ? $filenameWithoutExtension.$extension : "/upload/yandex_market.xml",
			),
			"COMPRESS_FILE" => array(
				"PARENT" => "ADDITIONAL_SETTINGS",
				"NAME" => GetMessage("COMPRESS_FILE"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
				"REFRESH" => "Y",
			),
			/*"USE_OFFER_ADDITIONAL_VALUES" => array(
				"PARENT" => "ADDITIONAL_SETTINGS",
				"NAME" => GetMessage("USE_OFFER_ADDITIONAL_VALUES"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
				"REFRESH" => "Y",
			),*/
			"USE_PRODUCT_IDS" => array(
				"PARENT" => "ADDITIONAL_SETTINGS",
				"NAME" => GetMessage("USE_PRODUCT_IDS"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
				"REFRESH" => "Y",
			),
			"DEL_ZEROS" => array(
				"PARENT" => "ADDITIONAL_SETTINGS",
				"NAME" => GetMessage("DEL_ZEROS"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
				"REFRESH" => "Y",
			),
			"USE_URL_YMARKER" => array(
				"PARENT" => "ADDITIONAL_SETTINGS",
				"NAME" => GetMessage("USE_URL_YMARKER"),
				"TYPE" => "STRING",
				"DEFAULT" => "r1=yandext",
			),	
		));
		if($arCurrentValues["USE_OUTPUT_FILE"] != "Y")
			unset($arParameters["OUTPUT_PATH"], $arParameters["COMPRESS_FILE"]);

		// Debug
		if(self::$DEBUG)
			$arParameters = array_merge($arParameters, array(
				"DEBUG" => array(
					"PARENT" => "BASE",
					"TYPE" => "CUSTOM",
					"JS_FILE" => "/bitrix/js/main/comp_props.js",
					"JS_EVENT" => "BxShowComponentNotes",
					"JS_DATA" => "<pre style=\"font-size:9px;line-height:10px;\">".print_r(self::$DEBUG, true)."</pre>",
				),
			));

		$arComponentParameters = array(
			"GROUPS" => $arGroups,
			"PARAMETERS" => $arParameters,
		);

	}
}

class CSimaiCatalogYandexComponent extends CSimaiCatalogYandex
{
	private static function IsEdit()
	{
		return $GLOBALS["USER"]->isAdmin() && !(strtoupper($_GET["edit"]) == "N" || strtoupper($_GET["EDIT"]) == "N");
	}

	private static function GetSectionIDs($arSectionIDs)
	{
		$arTemp = array();

		foreach($arSectionIDs as $v)
		{
			list($iBlockID, $depthLevel, $sectionID) = explode(",", $v);
			if($sectionID > 0)
				$arTemp[] = $sectionID;
		}

		return $arTemp;
	}

	private static function FormatOfferTagName($tag, $len=null)
	{
		$tag = strtolower($tag);

		if($len > 0)
			$tag = substr($tag, 0, -$len);

		if(in_array($tag, array("delivery_included", "ordering_time", "delivery_time", "type_prefix", "original_name", "x_category", "world_region", "data_tour", "vendor_code")))
		{
			$parts = explode("_", $tag);
			$tag = array_shift($parts);
			foreach($parts as $part)
				$tag .= ucfirst($part);
		}
		elseif($tag == "isbn")
			$tag = strtoupper($tag);

		return $tag;
	}

	private static function GetOfferProperties($arIBlockIDs, $arParams)
	{
		$arTagsOrder = array(
			"type",
			"url",
			"price",
			//"oldprice",
			"currencyId",
			"categoryId",
			"market_category",
			"picture",
			"store",
			"pickup",
			"delivery",
			"local_delivery_cost",
			"available",
			"author",
			"typePrefix",
			"artist",
			"title",			
			"worldRegion",
			"country",
			"region",
			"days",
			"dataTour",	
			"name",
			"publisher",
			"series",
			"year",
			"ISBN",
			"volume",
			"part",
			"language",
			"binding",
			"page_extent",
			"table_of_contents",
			"performed_by",
			"performance_type",
			"storage",
			"format",
			"media",
			"starring",
			"director",
			"originalName",			
			"hotel_stars",
			"room",
			"meal",
			"included",
			"transport",
			"price_min",
			"price_max",
			"options",
			"place",
			"hall",
			"hall_part",
			"date",
			"is_premiere",
			"is_kids",			
			"vendor",
			"model",
			"provider",
			"tarifplan",
			"vendorCode",
			"description",
			"downloadable",		
			"bid",
			"cbid",
			"promo",
			"sales_notes",
			"manufacturer_warranty",
			"seller_waranty",
			"country_of_origin",
			"adult",
			"age",
			"barcode",
			"cpa",
			"param",			
			"rec",
			"expiry",
			"weight",
			"dimensions",
			"related_offer"
		);
		
		$arTemp = array();
		$arProperties = array();

		foreach($arParams as $k=>$v)
			if(!strncmp("OFFER_", $k, 6))
				$arTemp[substr($k, 6)] = trim($v);

		foreach($arIBlockIDs as $k=>$v)
		{
			if(!$k) continue;

			foreach($arTemp as $k1=>$v1)
			{
				$len = strlen("_".$k);
				if(substr($k1, -$len) == "_".$k)
				{
					if($v1)
					{
						$tag = self::FormatOfferTagName($k1, $len);
						$arr = explode(",", $v1);
						$arProperties[$v][$tag] = count($arr) > 1 ? $arr : $v1;
					}

					unset($arTemp[$k1]);
				}
			}
		}
		if(!isset($arIBlockIDs[0]))
			return $arProperties;

		foreach($arTemp as $k=>$v)
		{
			if(!$v) continue;

			$tag = self::FormatOfferTagName($k);
			$arr = explode(",", $v);
			
			$arTempFormatted[$tag] = count($arr) > 1 ? $arr : $v;			
		}
		
		foreach ($arTagsOrder as $tag_o)
		{
			foreach ($arTempFormatted as $tag=>$v)
			{
				if ($tag == $tag_o)
					$arProperties[$arIBlockIDs[0]][$tag] = $v;
			}
		}
		return $arProperties;
	}

	private static function GetOfferValues($arOfferProperties, $arFields, $arProperties, $arOffer=array())
	{
		foreach($arOfferProperties as $tag=>$property)
		{
			if($property == "NAME")
			{
				$value = trim($arFields[$property]);

				if(strlen($value) > 0)
				{					
					$value_h = htmlspecialcharsbx($value);
					$cut = "";
					while(strlen($value_h) > 251)
					{
						$value = substr($value,0,strlen($value)-1);
						$cut = "...";
						$value_h = htmlspecialcharsbx($value);
					}					
					$value = $value_h.$cut;
					$value = str_replace("&amp;#40;","(",$value);
					$value = str_replace("&amp;#41;",")",$value);
				}

				if($tag == "title")
					$tag = "name";

				if(strlen($value) > 0)
					$arOffer[$tag] = $value;
			}			
			elseif($tag == "type" && $property)
			{
				$arOffer[$tag] = $property;
			}
			elseif($tag == "picture")
			{
				if(!is_array($property))
					$property = array($property);

				$first = true;
				foreach($property as $v)
				{
					if(in_array($v, array("PREVIEW_PICTURE", "DETAIL_PICTURE")) && $first)
					{
						$value = $arFields[$v];
						if($value > 0)
						{
							$arFile = CFile::GetFileArray($value);
							$arOffer[$tag][] = $arFile["SRC"];
							$first = false;
						}
					}
					elseif(isset($arProperties[$v]) && $arProperties[$v]["PROPERTY_TYPE"] == "F")
					{
						$arValues = $arProperties[$v]["VALUE"];
						if(!is_array($arValues))
							$arValues = array($arValues);

						foreach($arValues as $value)
						{
							if($value > 0)
							{
								$arFile = CFile::GetFileArray($value);
								$arOffer[$tag][] = $arFile["SRC"];
							}
						}
					}
				}
			}
			elseif($tag == "description")
			{
				if(!is_array($property))
					$property = array($property);

				foreach($property as $v)
				{
					$value = "";
					if(in_array($v, array("PREVIEW_TEXT", "DETAIL_TEXT")))
					{
						$value = $arFields[$v];
						$value = strip_tags($value);
						$value = str_replace("\r\n",' ',$value);
						$value = str_replace("\n",' ',$value);
						$value = str_replace("
",' ',$value);
					}
					elseif(isset($arProperties[$v]))
					{
						if($arProperties[$v]["MULTIPLE"] == "Y")
							$value = $arProperties[$v]["VALUE"][0];
						else
							$value = $arProperties[$v]["VALUE"];
					}
					$value = trim($value);
					if(strlen($value) > 0)
					{
						$value_h = htmlspecialcharsbx($value);
						$cut = "";
						while(strlen($value_h) > 508)
						{
							$value = substr($value,0,strlen($value)-1);
							$cut = "...";
							$value_h = htmlspecialcharsbx($value);
						}					
						$value = $value_h.$cut;
						$value = str_replace("&amp;#40;","(",$value);
						$value = str_replace("&amp;#41;",")",$value);
					}
						
					if(strlen($value) > 0)
						$arOffer[$tag] = $value;

					break;
				}
			}
			elseif($tag == "available")
			{
				// http://dev.1c-bitrix.ru/api_help/catalog/fields.php
				// QUANTITY > 0 (QUANTITY_TRACE not checked!)
				if(isset($arFields[$property]))
				{
					if($arFields[$property] > 0 || $arFields[$property] == "Y" || $arFields[$property] === true)
						$arOffer[$tag] = "true";
					elseif($arFields[$property] <= 0 || $arFields[$property] == "N" || $arFields[$property] === false)
						$arOffer[$tag] = "false";
				}
				elseif(isset($arProperties[$property]))
				{
					if($arProperties[$property]["VALUE"] == "N" || empty($arProperties[$property]["VALUE"]))
						$arOffer[$tag] = "false";
					else
						$arOffer[$tag] = "true";
				}
			}
			elseif(isset($arProperties[$property]))
			{
				if(!is_array($property))
					$property = array($property);

				foreach($property as $v)
				{
					$arValues = $arProperties[$v]["VALUE"];
					if(!is_array($arValues))
						$arValues = array($arValues);

					foreach($arValues as $k1=>$v1)
					{
						$v1 = trim($v1);

						if(in_array($tag, array("barcode", "related_offer")) && strlen($v1) > 0)
						{
							$arOffer[$tag][] = htmlspecialcharsbx($v1);
						}
						elseif($tag == "param" && isset($v1))
						{
							$arOffer[$tag][] = array(
								"name" => $arProperties[$v]["DESCRIPTION"][$k1],
								//"unit" => $arProperties[$v]["UNIT"][$k1], // TODO: probably use regexp to catch units from "10 (cm)"
								"value" => $v1,
							);
						}
						else
						{
							if($tag == "sales_notes")
							{
								/*$v1 = htmlspecialcharsbx($v1);
								if (strlen($v1) > 50)							
									$v1 = substr($v1, 0, 47)."...";*/
								if (strlen($v1))
								{
									$value_h = htmlspecialcharsbx($v1);
									$cut = "";
									while(strlen($value_h) > 46)
									{
										$v1 = substr($v1,0,strlen($v1)-1);
										$cut = "...";
										$value_h = htmlspecialcharsbx($v1);
									}
									$v1 = $value_h.$cut;
									$v1 = str_replace("&amp;#40;","(",$v1);
									$v1 = str_replace("&amp;#41;",")",$v1);
								}
							}								
							elseif(in_array($tag, array("store", "pickup", "delivery", "downloadable", "adult")) && ($v1 != "N" && !empty($v1)))
								$v1 = "true";
							elseif($tag == "age" && !in_array(trim($v1), array(0, 6, 12, 16, 18)))
								break;
							else
								$v1 = htmlspecialcharsbx($v1);

							$sh = false;
							if ($tag == "local_delivery_cost")
							{
								$v1 = trim($v1);
								if ($v1 === "0")
									$sh = true;
							}
							
							if($v1 || $sh)
							{
								$arOffer[$tag] = $v1;
								break;
							}
						}
					}
				}
			}
		}

		return $arOffer;
	}

	private static function GetShop(&$arParams)
	{
		$arShop = array();

		foreach($arParams as $k=>$v)
		{
			if(!strncmp("SHOP_", $k, 5))
			{
				$tag = strtolower(substr($k, 5));
				if (is_array($v))
				{					
					$v_ = Array();
					foreach ($v as $v1)
						if (trim($v1))
							$v_[] = htmlspecialchars($v1);
					$v = $v_;
				}
				elseif($v == "Y") $v = "true";
				elseif($v == "N") $v = "false";
				else $v = htmlspecialchars(trim($v));
				$arShop[$tag] = $v;
			}
		}

		return $arShop;
	}

	protected static function GetSections($arIBlockIDs, $arSectionIDs, $includeSubsections, $real_ids)
	{
		$arSections = array();

		$ID = 1;
		$parentID = null;
		$arIDsTable = array();
		$previousDepthLevel = 0;
		$arLevels = array();
		$parentDepthLevel = null;

		foreach(parent::GetSections($arIBlockIDs, null, $includeSubsections) as $v)
		{
			$depthLevel = $v["DEPTH_LEVEL"] ? $v["DEPTH_LEVEL"] : 0;
			$iBlockID = $v["IS_SECTION"] ? $v["IBLOCK_ID"] : $v["ID"];
			$sectionID = $v["IS_SECTION"] ? $v["ID"] : 0;

			$key = $iBlockID.",".$depthLevel.",".$sectionID;
			$key1 = $iBlockID.",".($depthLevel - 1).",".$sectionID;
			$isKeyInSectionIDs = in_array($key, $arSectionIDs) || in_array($key1, $arSectionIDs);

			$isSubsection = false;
			if(!is_null($parentDepthLevel) && $depthLevel > $parentDepthLevel)
				$isSubsection = true;
			else
				$parentDepthLevel = null;

			if($isSubsection && $isKeyInSectionIDs)
				unset($arSectionIDs);

			if($includeSubsections && $isKeyInSectionIDs && is_null($parentDepthLevel))
				$parentDepthLevel = $depthLevel;
			if(!$arSectionIDs || $isKeyInSectionIDs	|| $isSubsection)
			{
				$i = $depthLevel;
				if($depthLevel != $previousDepthLevel)
					do $parentID = $arLevels[--$i]; while($i > 0 && !isset($arLevels[$i]));
					
				if ($real_ids)
					$arSections[] = array("ID"=>$v["ID"], "PARENT_ID"=>$parentID, "NAME"=>$v["NAME"]);
				else
					$arSections[] = array("ID"=>$ID, "PARENT_ID"=>$parentID, "NAME"=>$v["NAME"]);

				if ($real_ids)
					$arIDsTable[($v["IS_SECTION"] ? "_" : "#").$v["ID"]] = $v["ID"];
				else
					$arIDsTable[($v["IS_SECTION"] ? "_" : "#").$v["ID"]] = $ID;

				if ($real_ids)
					$arLevels[$depthLevel] = $v["ID"];
				else
					$arLevels[$depthLevel] = $ID;

				$previousDepthLevel = $depthLevel;

				$ID++;
			}
		}

		if(!$arSections)
		{
			foreach(parent::GetIBlocks(null, $arIBlockIDs) as $v)
			{
				$arSections[] = array("ID"=>1, "NAME"=>$v["NAME"]);
				$arIDsTable["#".$v["ID"]] = 1;
			}
		}

		return array($arSections, $arIDsTable);
	}

	protected static function GetCurrencies($arParams)
	{
		if(empty($arParams["CURRENCY_BANK"]) || $arParams["CURRENCY_BANK"] == "MODULE")
			return parent::GetCurrencies();

		$arCurrencies = array();

		// TODO: CURRENCY_BANK_RATE_PLUS for each currency code
		foreach(parent::GetCurrencies(true) as $v)
		{
			if($v["RATE"] != 1)
			{
				if($arParams["CURRENCY_BANK_RATE_PLUS"] && intval($arParams["CURRENCY_BANK_RATE_PLUS"]) > 1)
					$v["PLUS"] = $arParams["CURRENCY_BANK_RATE_PLUS"];

				if(in_array($arParams["CURRENCY_BANK"], array("CBRF", "NBU", "NBK", "��")))
					$v["RATE"] = $arParams["CURRENCY_BANK"];
			}

			$arCurrencies[] = $v;
		}

		return $arCurrencies;
	}

	private static function GetPrices($arIBlockIDs, &$arParams)
	{
		$arPrices = array();

		foreach($arIBlockIDs as $k=>$v)
		{
			$k = ($k ? "_".$k : "");
			$priceProperty = trim($arParams["OFFER_PRICE".$k]);
			if(strlen($priceProperty) > 0)
			{
				$arPrice = CIBlockPriceTools::GetCatalogPrices($v, array($priceProperty));
				$arPrice = $arPrice[$priceProperty];
				$arPrice["CODE"] = $priceProperty;
				$currencyProperty = trim($arParams["OFFER_CURRENCY".$k]);
				if(strlen($currencyProperty) > 0)
				{
					$arPrice["CURRENCY"] = $currencyProperty;
					unset($arParams["OFFER_CURRENCY".$k]);
				}
				$arPrices[$v] = $arPrice;
				unset($arParams["OFFER_PRICE".$k]);
			}
		}

		return $arPrices;
	}

	private static function GetOfferIBlockIDs($arIBlockIDs)
	{
		$arChildOfferIBlockIDs = array();

		if(!CModule::IncludeModule("catalog"))
			return $arChildOfferIBlockIDs;

		$res = CCatalog::GetList(array(), array("PRODUCT_IBLOCK_ID"=>$arIBlockIDs));
		while($arr = $res->Fetch())
			$arChildOfferIBlockIDs[$arr["ID"]] = $arr["PRODUCT_IBLOCK_ID"];

		return $arChildOfferIBlockIDs;
	}

	private static function GetOffers($arIBlockIDs, $arOfferIBlockIDs, $arSectionIDs, $includeSubsection, $arSectionIDsTable, $arOfferProperties, $arPrices, $useVatPrices, $del_nulls)
	{
		$arParents = array();

		$arOffers = array();

		$arFilter = array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "CHECK_PERMISSIONS"=>"Y");

		$arSectionIDs = self::GetSectionIDs($arSectionIDs);

		if(!$arSectionIDs)
		{
			$arFilter["IBLOCK_ID"] = $arIBlockIDs;

			if($includeSubsection)
				$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
			else
				$arFilter["SECTION_ID"] = 0;
		}
		else
		{
			$arFilter["SECTION_ID"] = $arSectionIDs;
			if($includeSubsection)
				$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
		}

		$arSelect = array("*");
		foreach($arPrices as $v)
			if(!strncmp($v["SELECT"], "CATALOG_GROUP_", 14))
				$arSelect[] = $v["SELECT"];

		$arCatalogFields = array();
		$priceSArray = false;
		if(CModule::IncludeModule("catalog"))
		{
			$res = CCatalogProduct::GetList(array(), array(), false, false, array("ID", "QUANTITY"));
			while($arr = $res->Fetch())
				$arCatalogFields[$arr["ID"]] = $arr;
			if ($useVatPrices)
			{		
				$priceCodes = Array();
				foreach ($arPrices as $arPrice)
					$priceCodes[] = $arPrice["CODE"];
				$priceSArray = Array();
				foreach ($arIBlockIDs as $ib)
					$priceSArray[$ib] = CIBlockPriceTools::GetCatalogPrices($ib, $priceCodes);
			}
		}
		
		$res = CIBlockElement::GetList(array("iblock_id"=>"asc"), $arFilter, false, false, $arSelect);
		while($obj = $res->GetNextElement())
		{
			$arFields = $obj->GetFields();
			$arProperties = $obj->GetProperties();

			$ID = $arFields["ID"];
			$iBlockID = $arFields["IBLOCK_ID"];
			$sectionID = $arFields["IBLOCK_SECTION_ID"];

			if($arCatalogFields && isset($arCatalogFields[$ID]))
				$arFields = array_merge($arFields, $arCatalogFields[$ID]);

			$arItem = self::GetOfferValues($arOfferProperties[$iBlockID], $arFields, $arProperties);

			$arItem["id"] = $ID;
			$arItem["url"] = $arFields["DETAIL_PAGE_URL"];

			if(isset($arSectionIDsTable["_".$sectionID]) && $arSectionIDsTable["_".$sectionID] > 0)
				$arItem["categoryId"] = $arSectionIDsTable["_".$sectionID];
			elseif(isset($arSectionIDsTable["#".$iBlockID]) && $arSectionIDsTable["#".$iBlockID] > 0)
				$arItem["categoryId"] = $arSectionIDsTable["#".$iBlockID];
			else
				continue;
				
			$oldprice = 0;
			if ($priceSArray)
			{
				$prices = CIBlockPriceTools::GetItemPrices($ID, $priceSArray[$iBlockID], $arFields, true);
				$prices = current($prices);
				$price = $prices["DISCOUNT_VALUE"];
				$oldprice = $prices["VALUE"];
				$currency = $prices["CURRENCY"];
			}
			else
			{
				$price = $arFields["CATALOG_PRICE_".$arPrices[$iBlockID]["ID"]];
				$currency = $arFields["CATALOG_CURRENCY_".$arPrices[$iBlockID]["ID"]];
			}
				
			$price1 = $arProperties[$arPrices[$iBlockID]["CODE"]]["VALUE"];
			$currency1 = $arPrices[$iBlockID]["CURRENCY"];
			
			//print_r($arPrices);

			if($price > 0)
			{
				$arItem["price"] = $price;
				if (round($oldprice,2) > round($price,2))
					$arItem["oldprice"] = $oldprice;
				$arItem["currencyId"] = $currency;
			}
			elseif($price1 > 0)
			{
				$arItem["price"] = $price1;
				$arItem["currencyId"] = $currency1;
			}
			
			if ($del_nulls)
				$arItem["price"] = round($arItem["price"],5);

			if(in_array($iBlockID, $arOfferIBlockIDs))
				$arParents[$ID] = $arItem;
			elseif(isset($arItem["price"]))
				$arOffers[] = $arItem;
		}
		
		if($arParents)
		{
			$arChildOffers = self::GetChildOffers($arOfferIBlockIDs, $arParents, $arCatalogFields, $priceSArray, $arOfferProperties, $arPrices, $del_nulls);
			$arOffers = array_merge($arOffers, $arChildOffers);
		}
		
		/*$uni = Array();
		foreach ($arOffers as $i=>$arItem)
		{
			$u = $arItem["name"]."|".$arItem["url"];
			if (in_array($u,$uni))
				unset($arOffers[$i]);
			$uni[] = $u;
		}*/

		return $arOffers;
	}

	private static function GetChildOffers($arOfferIBlockIDs, $arParents, $arCatalogFields, $priceSArray, $arOfferProperties, $arPriceIDs, $del_nulls)
	{
		$arOffers = array();
		
		$changedByParent = Array();
		$arParentsExt = $arParents;

		if(!$arOfferIBlockIDs)
			return $arOffers;
			
		$arFilter = array(
			"IBLOCK_ID" => array_keys($arOfferIBlockIDs),
			"ACTIVE" => "Y",
			"ACTIVE_DATE" => "Y",
			"CHECK_PERMISSIONS" => "Y",
		);

		$arSelect = array("*");
		foreach($arPriceIDs as $v)
			if(!strncmp("CATALOG_GROUP_", $v["SELECT"], 14))
				$arSelect[] = $v["SELECT"];
				
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while($obj = $res->GetNextElement())
		{
			$arFields = $obj->GetFields();
			$arProperties = $obj->GetProperties();

			$ID = $arFields["ID"];
			
			$iBlockID = $arFields["IBLOCK_ID"];
			$parentIBlockID = $arOfferIBlockIDs[$iBlockID];

			if($arCatalogFields && isset($arCatalogFields[$ID]))
				$arFields = array_merge($arFields, $arCatalogFields[$ID]);

			$arParentID = $arProperties["CML2_LINK"]["VALUE"];
			unset($arProperties["CML2_LINK"]);
			
			unset($arParentsExt[$arParentID]);
			
			if(!isset($arParents[$arParentID]))
				continue;

			$arOffer = $arParents[$arParentID];
			
			$parentName = strtoupper($arOffer["name"]);

			$arOffer = self::GetOfferValues($arOfferProperties[$parentIBlockID], $arFields, $arProperties, $arOffer);

			if($arOffer["type"] != "vendor.model" && strtoupper($arOffer["name"]) == $parentName)
			{
				$arTemp = array();
				foreach($arProperties as $v)
				{
					if(!is_array($v["VALUE"]))
						$v["VALUE"] = array($v["VALUE"]);

					foreach($v["VALUE"] as $v2)
						if(strlen($v2) > 0)
							$arTemp[] .= strtolower($v["NAME"]).": ".strtolower($v2);
				}
				if($arTemp)
				{
					$changedByParent[$arParentID]++;
					$arOffer["~parent"] = $arParentID;
					$arOffer["~name"] = $arOffer["name"];
					$arOffer["name"] .= " (".implode(", ", $arTemp).")";
				}
			}

			$arOffer["id"] = $ID;

			$oldprice = 0;
			if ($priceSArray)
			{
				$prices = CIBlockPriceTools::GetItemPrices($ID, $priceSArray[$parentIBlockID], $arFields, true);
				$prices = current($prices);
				$price = $prices["DISCOUNT_VALUE"];
				$oldprice = $prices["VALUE"];
				$currency = $prices["CURRENCY"];
			}
			else
			{
				$price = $arFields["CATALOG_PRICE_".$arPriceIDs[$parentIBlockID]["ID"]];
				$currency = $arFields["CATALOG_CURRENCY_".$arPriceIDs[$parentIBlockID]["ID"]];
			}

			if(!($price > 0))
				continue;

			$arOffer["price"] = $price;
			if (round($oldprice,2) > round($price,2))
				$arOffer["oldprice"] = $oldprice;
			$arOffer["currencyId"] = $currency;
			
			if ($del_nulls)
				$arOffer["price"] = round($arOffer["price"],5);

			$arOffers[] = $arOffer;			
		}
		
		foreach ($arOffers as $i=>$arOffer)
		{
			if ($arOffer["~parent"])
			{
				if (count($changedByParent[$arOffer["~parent"]]) <= 1)
					$arOffer["name"] = $arOffer["~name"];
				unset($arOffer["~parent"]);
				unset($arOffer["~name"]);
				$arOffers[$i] = $arOffer;
			}
		}
		
		foreach ($arParentsExt as $ID => $arOffer)
		{
			if ($arOffer["price"] > 0)
				$arOffers[] = $arOffer;
		}
		
		return $arOffers;
	}
	
	static private function SetOfferUrlYmarker($arOffers, $ymarker)
	{
		$ymarker_query = false;
		$url_arr = array();
		$ymarker = str_replace("?", "", $ymarker);
		$ymarker = str_replace("&amp;", "&", $ymarker);
		parse_str($ymarker, $url_arr);
		foreach ($url_arr as $k=>$v)
		{
			if (preg_match("/^[a-zA-Z0-9_-]+$/", $k) && preg_match("/^[a-zA-Z0-9_-]+$/", $v))
				$url_arr[$k] = $k."=".$v;
			else
				unset($url_arr);
		}
		if (count($url_arr) > 0)
			$ymarker_query = "?".implode("&amp;", $url_arr);		
		foreach($arOffers as $id=>$arOffer)
			$arOffers[$id]["url"] = $arOffers[$id]["url"].$ymarker_query;
		return $arOffers;
	}
	

	static private function MakeOfferUniqueUrls($arOffers)
	{
		$repeats = Array();
		foreach($arOffers as $id=>$arOffer)
			$repeats[$arOffer["url"]][] = $id;
			
		foreach($repeats as $repeat)
			if (count($repeat) > 1)
				foreach ($repeat as $id)
				{
					if (substr_count($arOffers[$id]["url"],"?") > 0)
						$arOffers[$id]["url"] .= "&amp;u4ym=".$id;
					else
						$arOffers[$id]["url"] .= "?u4ym=".$id;
				}

		return $arOffers;
	}
	
	static private function NormalizeOfferIDs(&$arOffers,$normalize_ids)
	{
		$ID = 1;
		$arIDsTable = array();
		foreach($arOffers as $id=>$arOffer)
		{
			if($arOffer["type"] == "artist.title")
			{
				$arOffers[$id]["title"] = $arOffers[$id]["name"];
				unset($arOffers[$id]["name"]);
			}
			if ($normalize_ids)
			{
				$arIDsTable[$arOffer["id"]] = $ID;
				$arOffers[$id]["id"] = $ID;
				$ID++;
			}
		}
		if ($normalize_ids)
		{
			foreach($arOffers as $id=>$arOffer)
				if(isset($arOffer["related_offer"]))
					foreach($arOffer["related_offer"] as $k=>$v)
						$arOffers[$id]["related_offer"][$k] = $arIDsTable[$v];
		}
	}

	public static function InitParams(&$arParams, &$arErrorMessages)
	{
		$MESS = __IncludeLang(dirname(__FILE__)."/components/simai/catalog.yandex/lang/".LANGUAGE_ID.".parameters.php", true);

		$arErrorMessages = array();

		// In first start $arParams is empty.
		// Auto populated values: $arParams = array(["CACHE_TYPE"]=>"A", ["~CACHE_TYPE"]=>"A")
		if(count($arParams) == 2 && isset($arParams["CACHE_TYPE"]))
		{
			$arErrorMessages[] = GetMessage("SIMAI_ADD_COMPONENT_PARAMS");
			return false;
		}

		// Check sections
		$arParams["INCLUDE_SUBSECTIONS"] = $arParams["INCLUDE_SUBSECTIONS"] == "Y";
		if($arParams["SECTION_IDS"][0] == ",,")
		{
			$arParams["SECTION_IDS"] = array();
			$arParams["INCLUDE_SUBSECTIONS"] = true;
		}

		// Get iblock ids from sections
		$arIBlockIDs = self::GetSectionIBlockIDs($arParams["SECTION_IDS"]);

		// Check iBlockID
		if(!$arParams["IBLOCK_ID"] && !$arIBlockIDs)
		{
			$arErrorMessages[] = $MESS["IBLOCK_ID"];
			return false;
		}

		// Ensure iBlockID is array!
		if($arIBlockIDs)
			$arParams["IBLOCK_ID"] = $arIBlockIDs;
		elseif(!is_array($arParams["IBLOCK_ID"]))
			$arParams["IBLOCK_ID"] = array($arParams["IBLOCK_ID"]);
		else
			$arParams["IBLOCK_ID"] = parent::GetMultipleIBlockIDs($arParams["IBLOCK_ID"]);

		// Check required fields
		$arIBlocks = parent::GetIBlocks(null, $arParams["IBLOCK_ID"]);

		foreach($arParams["IBLOCK_ID"] as $i=>$v)
		{
			$k = ($i ? "_".$i : "");

			// Check price
			if(!$arParams["OFFER_PRICE".$k])
				$arErrorMessages[] = $MESS["OFFER_PRICE"]." (".$arIBlocks[$v]["NAME"].")";

			// Check currency if currency module not installed
			if(CModule::IncludeModule("catalog"))
			{
				if($arParams["OFFER_CURRENCY".$k] && !IsModuleInstalled("currency"))
					$arErrorMessages[] = $MESS["OFFER_CURRENCY"]." (".$arIBlocks[$v]["NAME"].")";
			}

			// Check required fields by offer type
			$arRequiredFieldsByOfferType = array(
				"" => "OFFER_NAME",
				"book" => "OFFER_NAME",
				"audiobook" => "OFFER_NAME",
				"vendor.model" => "OFFER_VENDOR",
				"artist.title" => "OFFER_TITLE",
				"tour" => "OFFER_DAYS,OFFER_DATA_TOUR,OFFER_NAME,OFFER_INCLUDED,OFFER_TRANSPORT",
				"event-ticket" => "OFFER_NAME,OFFER_PLACE,OFFER_DATE"
			);

			$offer_type = $arParams["OFFER_TYPE"].$k;
			foreach($arRequiredFieldsByOfferType as $type=>$fields)
				if($offer_type == $type || (!$type && !$offer_type))
					foreach(explode(",", $fields) as $field)
						if(!$arParams[$field.$k])
							$arErrorMessages[] = $MESS[$field]." - ".$field." (".$arIBlocks[$v]["NAME"].")";
		}

		// Shop name and description is required
		foreach(array("SHOP_NAME", "SHOP_COMPANY") as $v)
			if(!$arParams[$v])
				$arErrorMessages[] = $MESS[$v];

		if($arErrorMessages)
			return false;

		// Cache
		if(!isset($arParams["CACHE_TIME"]))
			$arParams["CACHE_TIME"] = 3600;

		// Shop URL
		if(!$arParams["SHOP_URL"])
			$arParams["SHOP_URL"] = "http://".$_SERVER["SERVER_NAME"];

		// File output
		$arParams["USE_OUTPUT_FILE"] = $arParams["USE_OUTPUT_FILE"] == "Y";
		$arParams["OUTPUT_FILE"] = trim($arParams["OUTPUT_FILE"]);
		if(!$arParams["USE_OUTPUT_FILE"] || !(strlen($arParams["OUTPUT_FILE"]) > 0))
			unset($arParams["OUTPUT_FILE"]);

		// Compress
		$arParams["COMPRESS"] = $arParams["COMPRESS"] == "Y";

		return true;
	}

	public static function ShowError($strError, $arErrorParams)
	{
		foreach($arErrorParams as $k=>$v)
			$arErrorParams[$k] = trim(str_replace("*", "", $arErrorParams[$k]));

		$strError .= $arErrorParams ? ": ".implode(", ", $arErrorParams) : "";

		ShowError($strError);
	}

	public static function GetResult($arParams)
	{
		//print_r($arParams);
		
		$arResult = array();

		$arIBlockIDs = $arParams["IBLOCK_ID"];
		$arSectionIDs = $arParams["SECTION_IDS"];
		$includeSubsection = $arParams["INCLUDE_SUBSECTIONS"];

		$arResult["SHOP"] = self::GetShop($arParams);

		$arResult["CURRENCIES"] = self::GetCurrencies($arParams);

		list($arResult["CATEGORIES"], $arSectionIDsTable) = self::GetSections($arIBlockIDs, $arSectionIDs, $includeSubsection, ($arParams["USE_PRODUCT_IDS"] == "Y"));

		$arPrices = self::GetPrices($arIBlockIDs, $arParams);

		$arOfferProperties = self::GetOfferProperties($arIBlockIDs, $arParams);

		$arOfferIBlockIDs = self::GetOfferIBlockIDs($arIBlockIDs);

		$arResult["OFFERS"] = self::GetOffers($arIBlockIDs, $arOfferIBlockIDs, $arSectionIDs, $includeSubsection, $arSectionIDsTable, $arOfferProperties, $arPrices, ($arParams["USE_VAT_PRICES"] == "Y"), ($arParams["DEL_ZEROS"] == "Y"));
		
		if (trim($arParams["USE_URL_YMARKER"]))
			$arResult["OFFERS"] = self::SetOfferUrlYmarker($arResult["OFFERS"], trim($arParams["USE_URL_YMARKER"]));
	
		$arResult["OFFERS"] = self::MakeOfferUniqueUrls($arResult["OFFERS"], trim($arParams["USE_URL_YMARKER"]));
	
		self::NormalizeOfferIDs($arResult["OFFERS"],($arParams["USE_PRODUCT_IDS"] != "Y"));
		
		$arResult["inUtf8"] = ($arParams["IN_UTF8"] == "Y");
		
		$arResult["notShowNA"] = ($arParams["NOT_SHOW_NA"] == "Y");

		return $arResult;
	}

	public static function StartBuffer($arParams)
	{
		global $APPLICATION;

		if($arParams["OUTPUT_PATH"])
			$GLOBALS["SIMAI_CATALOGYANDEX_PATH"] = $arParams["OUTPUT_PATH"];

		if($arParams["COMPRESS_FILE"] == "Y")
			$GLOBALS["SIMAI_CATALOGYANDEX_COMPRESS_FILE"] = $arParams["COMPRESS_FILE"];
			
		Define(IN_UTF8,($arParams["IN_UTF8"] == "Y"));

		if(self::IsEdit())
		{
			function simai_catalogyandex_buffer($buffer)
			{
				global $APPLICATION;
				
				$path = $GLOBALS["SIMAI_CATALOGYANDEX_PATH"];
				$compress = $GLOBALS["SIMAI_CATALOGYANDEX_COMPRESS_FILE"];
				if($path)
				{
					$buffer_f = $buffer;
					if (IN_UTF8)
					{
						if(substr_count(LANG_CHARSET,"UTF") <= 0 && substr_count(LANG_CHARSET,"utf") <= 0)
							$buffer_f = $APPLICATION->ConvertCharset($buffer_f, "windows-1251", "UTF-8");
					}
					else
					{
						if(substr_count(LANG_CHARSET,"UTF") > 0 || substr_count(LANG_CHARSET,"utf") > 0)
							$buffer_f = $APPLICATION->ConvertCharset($buffer_f, "UTF-8", "windows-1251");
					}
					if ($path[0] != "/")
						$path = "/".$path;
					$abs_path = $_SERVER["DOCUMENT_ROOT"].$path;
					$arr = explode("/",$abs_path);
						$filename = array_pop($arr);
					$arr = explode(".",$abs_path);
					if (count($arr) > 1)
						array_pop($arr);
					$abs_path = implode(".",$arr);
					if(!$compress)
					{
						$abs_path .= ".xml";
						RewriteFile($abs_path, $buffer_f);
					}
					else
					{
						$abs_path .= ".zip";
						CheckDirPath($abs_path);
						if(file_exists($abs_path) && !is_writable($abs_path))
							@chmod($abs_path, BX_FILE_PERMISSIONS);
						@chmod($abs_path, BX_FILE_PERMISSIONS);
						$zip = new ZipArchive();
						if ($zip->open($abs_path, ZipArchive::OVERWRITE) === true)
						{
							$zip->addFromString($filename, $buffer_f);
							$zip->close();
						}
					}
				}
				$str = '<pre style="background-color:#FEFDEA;border:1px solid #D7D6BA;padding:5px; margin:5px;">';
				$str .= htmlspecialcharsEx($buffer);
				if($abs_path)
					$str .= "\n\n Output path: $abs_path";
				$str .= "</pre>";
				return $str;
			}
		}
		elseif(!$arParams["COMPRESS"])
		{
			function simai_catalogyandex_buffer($buffer)
			{
				global $APPLICATION;
				
				$encoding = (IN_UTF8 ? "charset=utf-8" : "charset=windows-1251");

				header("Content-Type: text/xml; ".$encoding);
				header("Pragma: no-cache");

				if (IN_UTF8)
				{
					if(substr_count(LANG_CHARSET,"UTF") <= 0 && substr_count(LANG_CHARSET,"utf") <= 0)
						$buffer = $APPLICATION->ConvertCharset($buffer, "windows-1251", "UTF-8");
				}
				else
				{
					if(substr_count(LANG_CHARSET,"UTF") > 0 || substr_count(LANG_CHARSET,"utf") > 0)
						$buffer = $APPLICATION->ConvertCharset($buffer, "UTF-8", "windows-1251");
				}
				return $buffer;
			}
		}
		else
		{
			function simai_catalogyandex_buffer($buffer)
			{
				global $APPLICATION;
				
				$filename = COption::GetOptionString("simai.catalogyandex", "filename");
				if($filename)
				{
					if (IN_UTF8)
					{
						if(substr_count(LANG_CHARSET,"UTF") <= 0 && substr_count(LANG_CHARSET,"utf") <= 0)
							$buffer = $APPLICATION->ConvertCharset($buffer, "windows-1251", "UTF-8");
					}
					else
					{
						if(substr_count(LANG_CHARSET,"UTF") > 0 || substr_count(LANG_CHARSET,"utf") > 0)
							$buffer = $APPLICATION->ConvertCharset($buffer, "UTF-8", "windows-1251");
					}
					$filenameWithoutExtension = GetFileNameWithoutExtension($filename);
					$file = tempnam("tmp", "zip");
					$zip = new ZipArchive();
					$zip->open($file, ZipArchive::OVERWRITE);
					$zip->addFromString($filenameWithoutExtension.".xml", $buffer);
					$zip->close();
					header('Content-Type: application/zip');
					header('Content-Length: '.filesize($file));
					header('Content-Disposition: attachment; filename="'.$filenameWithoutExtension.'.zip"');
					$buffer = @file_get_contents($file);
					unlink($file);
					return $buffer;
				}
			}
		}
		if(!self::IsEdit())
			$APPLICATION->RestartBuffer();

		ob_start("simai_catalogyandex_buffer");
		
	}

	public static function EndBuffer($arParams)
	{
		ob_end_flush();
		if(!self::IsEdit())
			die();
	}
}

class CSimaiCatalogYandexTemplate
{
	private static function ShowShop($arShop)
	{
		foreach($arShop as $tag=>$v)
		{
			if ($tag == "store")
				break;
			if (is_array($v))
				foreach ($v as $v1)
					printf('<%s>%s</%s>%s', $tag, $v1, $tag, PHP_EOL);
			elseif (strlen($v) > 0)
				printf('<%s>%s</%s>%s', $tag, $v, $tag, PHP_EOL);
		}
	}
	
	private static function ShowShop2($arShop)
	{
		$shop2 = false;
		foreach($arShop as $tag=>$v)
		{
			if ($tag == "store")
				$shop2 = true;
			if ($shop2)
			{
				if (is_array($v))
					foreach ($v as $v1)
						printf('<%s>%s</%s>%s', $tag, $v1, $tag, PHP_EOL);
				elseif (strlen($v) > 0)
					printf('<%s>%s</%s>%s', $tag, $v, $tag, PHP_EOL);
			}
		}
	}

	private static function ShowCurrencies($arCurrencies)
	{
		printf('<currencies>%s', PHP_EOL);
		foreach($arCurrencies as $v)
		{
			$extra = $v["PLUS"] ? sprintf(' plus="%s"', $v["PLUS"]) : "";
			printf('%s<currency id="%s" rate="%s"%s/>%s', "\t", str_replace("RUR","RUB",$v["CODE"]), $v["RATE"], $extra, PHP_EOL);
		}

		printf('</currencies>%s', PHP_EOL);
	}

	private static function ShowCategories($arCategories)
	{
		printf('<categories>%s', PHP_EOL);
		foreach($arCategories as $v)
		{
			$extra = $v["PARENT_ID"] ? sprintf(' parentId="%s"', $v["PARENT_ID"]) : "";
			printf('%s<category id="%s"%s>%s</category>%s', "\t", $v["ID"], $extra, htmlspecialcharsbx($v["NAME"]), PHP_EOL);
		}
		printf('</categories>%s', PHP_EOL);
	}

	private static function ShowOffers($arOffers, $url, $notShowNA)
	{
		printf('<offers>%s', PHP_EOL);
		foreach($arOffers as $arOffer)
		{
			if ($notShowNA && $arOffer["available"] == "false")
				continue;
			$extra = $arOffer["type"] ? sprintf(' type="%s"', $arOffer["type"]) : "";
			$extra .= $arOffer["available"] ? sprintf(' available="%s"', $arOffer["available"]) : "";
			$extra .= $arOffer["bid"] ? sprintf(' bid="%s"', $arOffer["bid"]) : "";
			$extra .= $arOffer["cbid"] ? sprintf(' cbid="%s"', $arOffer["cbid"]) : "";
			printf('%s<offer id="%s"%s>%s', "\t", $arOffer["id"], $extra, PHP_EOL);
			printf('%s<url>%s</url>%s', "\t\t", $url.$arOffer["url"], PHP_EOL);
			printf('%s<price>%s</price>%s', "\t\t", $arOffer["price"], PHP_EOL);
			if (round($arOffer["oldprice"]) > 0)
				printf('%s<oldprice>%s</oldprice>%s', "\t\t", $arOffer["oldprice"], PHP_EOL);
			printf('%s<currencyId>%s</currencyId>%s', "\t\t", str_replace("RUR","RUB",$arOffer["currencyId"]), PHP_EOL);
			printf('%s<categoryId>%s</categoryId>%s', "\t\t", $arOffer["categoryId"], PHP_EOL);
			foreach($arOffer["picture"] as $src)
				printf('%s<picture>%s</picture>%s', "\t\t", $url.$src, PHP_EOL);
			$params = $arOffer["param"];
			$barcodes = $arOffer["barcode"];
			$related_offers = $arOffer["related_offer"];
			foreach(array("type", "available", "id", "url", "price", "oldprice", "currencyId", "categoryId", "picture", "param", "barcode", "related_offer", "bid", "cbid") as $v)
				unset($arOffer[$v]);
			foreach($arOffer as $tag=>$value)
				printf('%s<%s>%s</%s>%s', "\t\t", $tag, $value, $tag, PHP_EOL);
			foreach($params as $param)
			{
				$param["value"] = str_replace('&amp;quot;','&quot;',$param["value"]);
				$extra = $param["name"] ? sprintf(' name="%s"', $param["name"]) : "";
				$extra .= $param["unit"] ? sprintf(' unit="%s"', $param["unit"]) : "";
				printf('%s<param%s>%s</param>%s', "\t\t", $extra, $param["value"], PHP_EOL);
			}
			foreach($barcodes as $barcode)
				printf('%s<barcode>%s</barcode>%s', "\t\t", $barcode, PHP_EOL);
			foreach($related_offers as $related_offer)
				printf('%s<related_offer>%s</related_offer>%s', "\t\t", $related_offer, PHP_EOL);
			printf('%s</offer>%s', "\t", PHP_EOL);

		}
		printf('</offers>%s', PHP_EOL);
	}

	public static function ShowTemplate($arResult)
	{
		if(isset(CSimaiCatalogYandex::$DEBUG)) {print_r(CSimaiCatalogYandex::$DEBUG);return;}

		$encoding = ($arResult["inUtf8"] ? "UTF-8" :"windows-1251");
		
		printf('<?xml version="1.0" encoding="'.$encoding.'"?>%s', PHP_EOL);
		printf('<!DOCTYPE yml_catalog SYSTEM "shops.dtd">%s', PHP_EOL);
		printf('<yml_catalog date="%s">%s', date("Y-m-d H:i"), PHP_EOL);
		printf('<shop>%s', PHP_EOL);
		self::ShowShop($arResult["SHOP"]);
		self::ShowCurrencies($arResult["CURRENCIES"]);
		self::ShowCategories($arResult["CATEGORIES"]);
		self::ShowShop2($arResult["SHOP"]);
		self::ShowOffers($arResult["OFFERS"], $arResult["SHOP"]["url"], $arResult["notShowNA"]);
		printf('</shop>%s', PHP_EOL);
		printf('</yml_catalog>%s', PHP_EOL);
	}
}
?>