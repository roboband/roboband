<?
if(!check_bitrix_sessid())
	return;

if($ex = $APPLICATION->GetException())
{
	CAdminMessage::ShowMessage(array(
		"TYPE" => "ERROR",
		"MESSAGE" => GetMessage("MOD_INST_ERR"),
		"DETAILS" => $ex->GetString(),
		"HTML" => true,
	));
}
else
{
	$arSites= array();
	$res = CSite::GetList($by="sort", $order="asc", array("ACTIVE"=>"Y"));
	while($arSite = $res->GetNext())
		$arSites[] = $arSite;


    if(!isset($_REQUEST["path"]))
        $_REQUEST["path"] = "/";

    if(!isset($_REQUEST["filename"]))
        $_REQUEST["filename"] = "yandex_market.php";
?>
<form action="<?= $APPLICATION->GetCurPage()?>" name="simai_catalogyandex_install">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="lang" value="<?=LANG?>">
	<input type="hidden" name="id" value="simai.catalogyandex">
	<input type="hidden" name="install" value="Y">
	<input type="hidden" name="step" value="2">
	<div class="adm-detail-content-wrap">
		<div class="adm-detail-content">
			<div class="adm-detail-title">Настройки установки</div>
			<div class="adm-detail-content-item-block">
				<table class="adm-detail-content-table edit-table">
					<tr>
						<td class="adm-detail-content-cell-l" width="50%"><?=GetMessage("SIMAI_CATALOGYANDEX_MODULE_DEFAULT_SITE")?>:</td>
						<td class="adm-detail-content-cell-r">
							<select name="site_id">
								<?foreach($arSites as $arSite):?>
									<option value="<?=$arSite["ID"]?>"<?=($arSite["DEF"]=="Y"?' selected':"")?>><?=$arSite["NAME"]?></option>
								<?endforeach;?>
							</select>
						</td>
					</tr>
					<tr>
						<td class="adm-detail-content-cell-l"><?=GetMessage("SIMAI_CATALOGYANDEX_MODULE_PATH")?>:</td>
						<td class="adm-detail-content-cell-r"><input size="30" type="text" name="path" id="path" value="<?=CUtil::JSEscape($_REQUEST["path"])?>"></td>
					</tr>
					<tr>
						<td class="adm-detail-content-cell-l"><?=GetMessage("SIMAI_CATALOGYANDEX_MODULE_FILENAME")?>:</td>
						<td class="adm-detail-content-cell-r"><input size="30" type="text" name="filename" id="filename" value="<?=CUtil::JSEscape($_REQUEST["filename"])?>"></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="adm-detail-content-btns-wrap">
			<div class="adm-detail-content-btns">
				<input class="adm-btn-save" type="submit" name="inst" value="<?=GetMessage("MOD_INSTALL")?>">
			</div>
		</div>
	</div>
</form>
<?}?>