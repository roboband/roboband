<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

switch(CModule::IncludeModuleEx("simai.catalogyandex"))
{
	case MODULE_NOT_FOUND: ShowError(GetMessage("SIMAI_MODULE_NOT_FOUND")); return;
	//case MODULE_INSTALLED: ShowError(GetMessage("SIMAI_MODULE_INSTALLED")); break;
	case MODULE_DEMO: ShowError(GetMessage("SIMAI_MODULE_DEMO")); break;
	case MODULE_DEMO_EXPIRED: ShowError(GetMessage("SIMAI_MODULE_DEMO_EXPIRED"));
}

if(!CSimaiCatalogYandexComponent::InitParams($arParams, $arErrorMessages))
{
	CSimaiCatalogYandexComponent::ShowError(GetMessage("SIMAI_REQUIRED_FIELDS_EMPTY"), $arErrorMessages);
	return;
}

CSimaiCatalogYandexComponent::StartBuffer($arParams);

if($this->StartResultCache(false, $USER->GetGroups()))
{
	$arResult = array();
	$arResult = CSimaiCatalogYandexComponent::GetResult($arParams);
	$this->IncludeComponentTemplate();
}

CSimaiCatalogYandexComponent::EndBuffer($arParams);
