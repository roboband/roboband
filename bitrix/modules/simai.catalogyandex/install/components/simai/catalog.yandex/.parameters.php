<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("simai.catalogyandex"))
	return;

$arComponentParameters = array();

CSimaiCatalogYandexParameters::Add($arComponentParameters, $arCurrentValues);
