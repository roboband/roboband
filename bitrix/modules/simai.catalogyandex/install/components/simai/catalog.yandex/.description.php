<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
	"NAME"        => GetMessage("SIMAI_TEMPLATE_NAME"),
	"DESCRIPTION" => GetMessage("SIMAI_TEMPLATE_DESCRIPTION"),
	"ICON"        => "/images/cy_icon.gif",
	"CACHE_PATH"  => "Y",
	"SORT"        => 10,
	"PATH"        => array(
		"ID"          => "simai",
        "NAME"        => GetMessage('SIMAI_COMPONENTS_NAME'),
		"CHILD"       => array(
			"ID"          => "simai_export",
			"NAME"        => GetMessage("SIMAI_EXPORT"),
			"SORT"        => 10,
		)
	),
);
