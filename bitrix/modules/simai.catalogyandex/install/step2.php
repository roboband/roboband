<?
if(!check_bitrix_sessid())
	return;

if($ex = $APPLICATION->GetException())
{
	CAdminMessage::ShowMessage(array(
		"TYPE" => "ERROR",
		"MESSAGE" => GetMessage("MOD_INST_ERR"),
		"DETAILS" => $ex->GetString(),
		"HTML" => true,
	));
}
else
{
    $site_id = COption::GetOptionString("simai.catalogyandex", "site_id");
	$path = COption::GetOptionString("simai.catalogyandex", "path");
	$filename = COption::GetOptionString("simai.catalogyandex", "filename");

    $res = CSite::GetByID($site_id);
    $arSite = $res->Fetch();

    $path = "//".implode("/", array_filter(array(trim($arSite["SERVER_NAME"], "/"), trim($arSite["DIR"], "/"), trim($path, "/"), $filename)));

    echo CAdminMessage::ShowNote(GetMessage("MOD_INST_OK"));
}
?>
<form action="<?=$APPLICATION->GetCurPage()?>">
	<?if(!$ex):?>
        <p><b><?=GetMessage("SIMAI_EDIT_LINK")?>:</b> <a href="<?=$path?>"><?="http:".$path?></a></p>
        <p id="simai_catalogyandex_redirect"></p>
        <script type="text/javascript">
        (function() {
            var timeout = 11; // in seconds
            var timeoutInterval = setInterval(function () {
                timeout--;
                var temp = document.getElementById("simai_catalogyandex_redirect");
                temp.innerHTML = "<?=GetMessage("SIMAI_CATALOGYANDEX_EDIT_REDIRECT")?>".replace(/#TIMEOUT#/, timeout.toString());
                if (timeout == 0) {
                    clearInterval(timeoutInterval);
                    window.location = "<?=$path?>";
                }
            }, 1000);
        })();
        </script>
	<?endif;?>
	<input type="hidden" name="lang" value="<?=LANG?>">
	<input type="submit" name="" value="<?=GetMessage("MOD_BACK")?>">
<form>
