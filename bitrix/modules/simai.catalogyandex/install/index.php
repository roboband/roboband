<?
global $MESS;
IncludeModuleLangFile(__FILE__);

if(class_exists("simai_catalogyandex"))
	return;

class simai_catalogyandex extends CModule
{
	var $MODULE_ID = "simai.catalogyandex";

	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $PARTNER_NAME;
	var $PARTNER_URI;

	function simai_catalogyandex()
	{
		$arModuleVersion = array();

		include(substr(__FILE__, 0,  -10)."/version.php");

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

		$this->MODULE_NAME = GetMessage("SIMAI_CATALOGYANDEX_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("SIMAI_CATALOGYANDEX_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("SIMAI_CATALOGYANDEX_MODULE_PARTNER_NAME");;
		$this->PARTNER_URI = "http://www.simai.ru";
	}

	function InstallDB()
	{
		RegisterModule($this->MODULE_ID);
		return true;
	}

	function UnInstallDB()
	{
		UnRegisterModule($this->MODULE_ID);
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles()
	{
		global $DOCUMENT_ROOT, $site_id, $path, $filename;

		if($_ENV["COMPUTERNAME"] == 'BX')
			return;

		// Install components
		CopyDirFiles($this->ModulePath()."/install/components", $DOCUMENT_ROOT."/bitrix/components", true, true);

		// Install public (copy yandex_market.php with new filename if changed)
        $filename = trim(trim($filename), "/");
        $path = rtrim(trim($path), "/");
        $path = !$path ? "/" : $path;
        $public_path = $this->SitePath($site_id, $path, $filename);
		$full_path = implode("/", array_filter(array(rtrim($public_path), trim($path, "/"), $filename)));
		CopyDirFiles($this->ModulePath()."/install/public/yandex_market.php", $full_path, true, false);

		// Set options
        COption::SetOptionString($this->MODULE_ID, "site_id", $site_id);
		COption::SetOptionString($this->MODULE_ID, "path", $path);
		COption::SetOptionString($this->MODULE_ID, "filename", $filename);

		return true;
	}

	function UnInstallFiles()
	{
		global $DOCUMENT_ROOT, $site_id;

		if($_ENV["COMPUTERNAME"] == 'BX')
			return;

		// Uninstall components
		DeleteDirFiles($this->ModulePath()."/install/components", $DOCUMENT_ROOT."/bitrix/components");

		// Uninstall public
        $site_id = COption::GetOptionString($this->MODULE_ID, "site_id");
		$path = COption::GetOptionString($this->MODULE_ID, "path");
		$filename = COption::GetOptionString($this->MODULE_ID, "filename");
        $public_path = $this->SitePath($site_id);
        $full_path = implode("/", array_filter(array(rtrim($public_path), trim($path, "/"), $filename)));
		if(file_exists($full_path) && is_file($full_path))
			DeleteDirFilesEx($full_path);

		// Delete options
		COption::RemoveOption($this->MODULE_ID);

		return true;
	}

	function DoInstall()
	{
		global $APPLICATION, $step;

		if(!check_bitrix_sessid())
			return false;

		if(!IsModuleInstalled("iblock"))
			$APPLICATION->ThrowException(GetMessage("SIMAI_CATALOGYANDEX_MODULE_ERROR_IBLOCK_MODULE_NOT_FOUND"));

		$step = IntVal($step);
		if($step < 2)
		{
			$APPLICATION->IncludeAdminFile(GetMessage("SIMAI_CATALOGYANDEX_MODULE_TITLE_INST"), $this->ModulePath()."/install/step1.php");
		}
		elseif($step == 2)
		{
			$this->InstallFiles();
			$this->InstallDB();
			$this->InstallEvents();
			$APPLICATION->IncludeAdminFile(GetMessage("SIMAI_CATALOGYANDEX_MODULE_TITLE_INST"), $this->ModulePath()."/install/step2.php");
		}
	}

	function DoUninstall()
	{
		global $APPLICATION;

		if(!check_bitrix_sessid())
			return false;

		$this->UnInstallDB();
		$this->UnInstallFiles();
		$this->InstallEvents();
		$APPLICATION->IncludeAdminFile(GetMessage("SIMAI_CATALOGYANDEX_MODULE_TITLE_UNITST"), $this->ModulePath()."/install/unstep.php");
	}

	function SitePath($site_id)
	{
        global $DOCUMENT_ROOT;

		$res = CSite::GetByID($site_id);
		$arSite = $res->Fetch();

        if(!$arSite["DOC_ROOT"])
            $arSite["DOC_ROOT"] = $DOCUMENT_ROOT;

		return implode("/", array_filter(array(rtrim($arSite["DOC_ROOT"], "/"), trim($arSite["DIR"], "/"))));
	}

	function ModulePath()
	{
		global $DOCUMENT_ROOT;

		return $DOCUMENT_ROOT."/bitrix/modules/".$this->MODULE_ID;
	}
}