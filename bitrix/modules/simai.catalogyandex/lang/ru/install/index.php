<?
$MESS["SIMAI_CATALOGYANDEX_MODULE_NAME"] = "SIMAI Выгрузка в Яндекс.Mаркет";
$MESS["SIMAI_CATALOGYANDEX_MODULE_DESC"] = "Выгрузка товаров в Яндекс.Маркет в формате YML c поддержкой товарных предложений";
$MESS["SIMAI_CATALOGYANDEX_MODULE_TITLE_INST"] = 'Установка модуля "'.$MESS["SIMAI_CATALOGYANDEX_MODULE_NAME"].'"';
$MESS["SIMAI_CATALOGYANDEX_MODULE_TITLE_UNITST"] = 'Удаление модуля "'.$MESS["SIMAI_CATALOGYANDEX_MODULE_NAME"].'"';
$MESS["SIMAI_CATALOGYANDEX_MODULE_PARTNER_NAME"] = "SIMAI";
$MESS["SIMAI_CATALOGYANDEX_MODULE_ERROR_IBLOCK_MODULE_NOT_FOUND"] = 'Модуль "'.$MESS["SIMAI_CATALOGYANDEX_MODULE_NAME"].'" не может работать без модуля "Информационные блоки"';
$MESS["SIMAI_CATALOGYANDEX_EDIT_LINK"] = "Перейдите по ссылке для редактирования настроек компонента";
$MESS["SIMAI_CATALOGYANDEX_EDIT_REDIRECT"] = "Автоматический переход через #TIMEOUT# секунд";
$MESS["SIMAI_CATALOGYANDEX_MODULE_DEFAULT_SITE"] = "Сайт по умолчанию";
$MESS["SIMAI_CATALOGYANDEX_MODULE_PATH"] = "Путь по умолчанию";
$MESS["SIMAI_CATALOGYANDEX_MODULE_FILENAME"] = "Файл выгрузки по умолчанию";