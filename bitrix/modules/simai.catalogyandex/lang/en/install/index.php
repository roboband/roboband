<?
$MESS["SIMAI_INSTALL_NAME"] = "SIMAI Export to Yandex.Market";
$MESS["SIMAI_INSTALL_DESC"] = "Export goods to Yandex.Market in YML format with trade offers support";
$MESS["SIMAI_INSTALL_TITLE_INST"] = 'Module setup "SIMAI Export to Yandex.Market"';
$MESS["SIMAI_INSTALL_TITLE_UNITST"] = 'Uninstall module "SIMAI Export to Yandex.Market"';
$MESS["SIMAI_INSTALL_PARTNER_NAME"] = "SIMAI";
$MESS["SIMAI_INSTALL_ERROR_IBLOCK_MODULE_NOT_FOUND"] = 'Module "SIMAI Export to Yandex.Market" cannot work without info-blocks module';
$MESS["SIMAI_EDIT_LINK"] = "Click link to edit component settings";
$MESS["SIMAI_INSTALL_DEFAULT_SITE"] = "Default site";
$MESS["SIMAI_INSTALL_PATH"] = "Default path";
$MESS["SIMAI_INSTALL_FILENAME"] = "Export default filename";
?>