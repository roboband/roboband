<?
IncludeModuleLangFile(__FILE__);

Class api_orderstatusex extends CModule
{
	const MODULE_ID = 'api.orderstatusex';
	const MYSQL_TABLE = 'api_orderstatusex';
	var $MODULE_ID = 'api.orderstatusex'; 
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("api.orderstatusex_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("api.orderstatusex_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("api.orderstatusex_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("api.orderstatusex_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;

		$errors = null;
		if (!$DB->Query("SELECT 'x' FROM `". self::MYSQL_TABLE ."`", true))
			$errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/db/'.$DBType.'/install.sql');

		if (!empty($errors))
		{
			$APPLICATION->ThrowException(implode("", $errors));
			return false;
		}

		RegisterModule(self::MODULE_ID);
		//General events
		RegisterModuleDependences('sale', 'OnOrderNewSendEmail',            $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		RegisterModuleDependences('sale', 'OnOrderDeliverSendEmail',        $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');//(!)15.0.0
		RegisterModuleDependences('sale', 'OnOrderPaySendEmail',            $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');//(!)15.0.0
		RegisterModuleDependences('sale', 'OnOrderCancelSendEmail',         $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		RegisterModuleDependences('sale', 'OnOrderRemindSendEmail',         $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		RegisterModuleDependences('sale', 'OnOrderRecurringSendEmail',      $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		RegisterModuleDependences('sale', 'OnOrderRecurringCancelSendEmail',$this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		//\\General events

		RegisterModuleDependences('sale', 'OnBeforeOrderUpdate',    $this->MODULE_ID, 'CApiOrderStatusEx', 'OnBeforeOrderUpdateHandler');
		RegisterModuleDependences('sale', 'OnOrderStatusSendEmail', $this->MODULE_ID, 'CApiOrderStatusEx', 'OnOrderStatusSendEmailHandler');
		RegisterModuleDependences('sale', 'OnOrderDelete',          $this->MODULE_ID, 'CApiOrderStatusEx', 'OnOrderDeleteHandler');//(!)15.0.0

		return true;
	}

	function UnInstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;

		$errors = null;
		if(array_key_exists("savedata", $arParams) && $arParams["savedata"] != "Y")
		{
			$errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/db/'.$DBType.'/uninstall.sql');
			$DB->Query("DELETE FROM `b_option` WHERE `MODULE_ID` = '".$this->MODULE_ID."'", true);
			$DB->Query("DELETE FROM `b_event_log` WHERE `MODULE_ID` = '".$this->MODULE_ID."'", true);

			if (!empty($errors))
			{
				$APPLICATION->ThrowException(implode("", $errors));
				return false;
			}
		}

		//General events
		UnRegisterModuleDependences('sale', 'OnOrderNewSendEmail',              $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		UnRegisterModuleDependences('sale', 'OnOrderDeliverSendEmail',          $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		UnRegisterModuleDependences('sale', 'OnOrderPaySendEmail',              $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		UnRegisterModuleDependences('sale', 'OnOrderCancelSendEmail',           $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		UnRegisterModuleDependences('sale', 'OnOrderRemindSendEmail',           $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		UnRegisterModuleDependences('sale', 'OnOrderRecurringSendEmail',        $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		UnRegisterModuleDependences('sale', 'OnOrderRecurringCancelSendEmail',  $this->MODULE_ID, 'CApiOrderStatusEx', 'OnGeneralSendEmailHandler');
		//\\General events
		UnRegisterModuleDependences('sale', 'OnBeforeOrderUpdate',      $this->MODULE_ID, 'CApiOrderStatusEx', 'OnBeforeOrderUpdateHandler');
		UnRegisterModuleDependences('sale', 'OnOrderStatusSendEmail',   $this->MODULE_ID, 'CApiOrderStatusEx', 'OnOrderStatusSendEmailHandler');
		UnRegisterModuleDependences('sale', 'OnOrderDelete',            $this->MODULE_ID, 'CApiOrderStatusEx', 'OnOrderDeleteHandler');
		UnRegisterModule(self::MODULE_ID);

		return true;
	}

	function InstallFiles($arParams = array())
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/". $this->MODULE_ID ."/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", True, True);

		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/admin/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/');

		return true;
	}

	function UnInstallEvents($arParams = array())
	{
		$TYPE_ID  = 'API_ORDERSTATUSEX';

		if(array_key_exists("savedata", $arParams) && $arParams["savedata"] != "Y")
		{
			$et = new CEventType;
			$et->Delete($TYPE_ID);

			$eventM = new CEventMessage;
			$rsMess = $eventM->GetList($by="id", $order="desc", array('TYPE_ID' => $TYPE_ID));
			while($arEM = $rsMess->Fetch()){
				$eventM->Delete($arEM['ID']);
			}
		}
	}


	function DoInstall()
	{
		global $APPLICATION;

		if ($APPLICATION->GetGroupRight('main') < 'W')
			return false;

		$this->InstallDB();
		$this->InstallFiles();
	}

	function DoUninstall()
	{
		global $APPLICATION, $step;

		$step = IntVal($step);
		if($step < 2)
			$APPLICATION->IncludeAdminFile(GetMessage("IBLOCK_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/". $this->MODULE_ID ."/install/unstep1.php");
		else
		{
			$arParams = array(
				"savedata" => $_REQUEST["savedata"],
			);

			$this->UnInstallDB($arParams);
			$this->UnInstallFiles();
			$this->UnInstallEvents($arParams);
		}
	}
}
?>