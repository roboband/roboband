<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

//EVENT_TYPE RU
$MESS['ET_EVENT_NAME']     = 'API_ORDERSTATUSEX';
$MESS['RU_ET_NAME']        = '[API] Расширенные статусы заказов';
$MESS['RU_ET_DESCRIPTION'] = '#ID# - ID заказа
#ORDER_ID# - Номер заказа
#ORDER_DATE# - Дата заказа
#ORDER_DESCRIPTION# - Комментарий к статусу
#EMAIL_TO# - E-mail получателя письма
#SALE_EMAIL# - E-mail отдела продаж

=== Служебные макросы ===
';

$MESS['EN_ET_NAME']        = '[API] Advanced order status';
$MESS['EN_ET_DESCRIPTION'] = '#ID# - order ID
#ORDER_ID# - order Number
#ORDER_DATE# - order date
#ORDER_DESCRIPTION# - order status comment
#EMAIL_TO# - E-mail recipient
#SALE_EMAIL# - E-mail sales

=== System macros ===
';


//EVENT_MESSAGE ADMIN
$MESS['EM_EMAIL_FROM']       = '#SALE_EMAIL#';
$MESS['EM_EMAIL_TO']         = '#EMAIL_TO#';
$MESS['EM_SUBJECT_ADMIN']    = '#SITE_NAME#: Сообщение для заказа №#ORDER_ID#';
$MESS['EM_MESSAGE']          = '#ORDER_DESCRIPTION#<br>
 <br>
 --<br>
 С уважением, <br>
 Интернет-магазин #SITE_NAME#<br>
 <a href="http://#SERVER_NAME#">#SERVER_NAME#</a>
';
