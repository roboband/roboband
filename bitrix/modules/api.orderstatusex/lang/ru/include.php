<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

//DB COLUMNS
$MESS['COLUMN_ID']          = '№';
$MESS['COLUMN_ORDER_ID']    = '№ заказа';
$MESS['COLUMN_STATUS']      = 'Статус';
$MESS['COLUMN_DATE_CREATE'] = 'Дата';
$MESS['COLUMN_DESCRIPTION'] = 'Комментарий';
$MESS['COLUMN_LID']         = 'Сайт';
$MESS['COLUMN_MAIL']        = '(не уведомлен)';

//Exceptions
$MESS['ERROR_STATUS_ADD'] = 'Ошибка добавления статуса';

//Other
$MESS['STATUS_HISTORY']   = 'История статусов';
$MESS['COMMENT']          = 'Комментарий';
$MESS['NOT_NOTIFY']       = 'Не уведомлять';
$MESS['BTN_SEND_COMMENT'] = 'Отправить только комментарий';

//Mess
$MESS['ORDER_STATUS_FALSE'] = 'Статусы заказов не найдены';

//Errors in /tools/ajax.php
$MESS['ORDERSTATUSEX_MODULE_FALSE'] = 'Модуль "Расширенные статусы заказов" не установлен';
$MESS['SALE_MODULE_FALSE']          = 'Модуль "Интернет-магазин" не установлен';
$MESS['ERROR_AJAX_VARS']            = 'Не все необходимые переменные переданы в AJAX';
$MESS['EVENT_MESSAGE_SEND']         = 'Сообщение отправлено';
$MESS['EVENT_MESSAGE_NOT_ADD']      = 'Не смог создать "почтовый шаблон" для комментария';
$MESS['EVENT_MESSAGE_NOT_FOUND']    = 'Не найден "почтовый шаблон" для комментария';
$MESS['EVENT_TYPE_NOT_FOUND']       = 'Не смог создать "тип почтового" события';
$MESS['SOME_ERROR']                 = 'Неизвестная ошибка';
$MESS['SESSION_EXPIRED']            = 'Ваша сессия истекла, обновите страницу';
$MESS['ERROR_EMPTY_COMMENT']        = 'Введите комментарий';