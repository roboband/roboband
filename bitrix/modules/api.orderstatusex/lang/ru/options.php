<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$MESS['SITE_SETTINGS'] = 'Настройки для сайта';
$MESS['SALE_EMAIL'] = 'E-mail магазина';

//GROUP_EMAIL_TEMPLATE
$MESS['GROUP_EMAIL_TPL'] = 'Параметры почтовых шаблонов';
$MESS['ORDER_HEADER'] = 'Верхняя часть почтового шаблона, макрос #ORDER_HEADER#';
$MESS['ORDER_FOOTER'] = 'Нижняя часть почтового шаблона, макрос #ORDER_FOOTER#';
$MESS['MESS_EMAIL_TPL'] = '<div style="text-align:left;">
Все почтовые шаблоны к статусам заказа можете привести к общему виду тремя макросами:<br>
#ORDER_HEADER#<br>
#ORDER_DESCRIPTION#<br>
#ORDER_FOOTER#<br>
</div>';

//Note
$MESS['SETTINGS_SAVED'] = 'Настройки сохранены';