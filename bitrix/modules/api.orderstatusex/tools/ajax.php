<?
define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile(__FILE__);

global $DB, $APPLICATION;
$MODULE_ID = 'api.orderstatusex';

$MODULE_SALE_RIGHT = $APPLICATION->GetGroupRight("sale");
if($MODULE_SALE_RIGHT<="D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

CUtil::JSPostUnescape();
$arResult = array();
$action   = $_REQUEST['action'];
$ajax     = $_REQUEST['ajax'] == 'Y';
$order_id = (int)$_REQUEST['order_id'];
$comment  = $_REQUEST['comment'];


/////////////////////////////////////////////////////////////////
//      Check modules and vars
/////////////////////////////////////////////////////////////////
if(!CModule::IncludeModule('api.orderstatusex'))
{
	$arResult = array(
		'result' => 'error',
		'message'  => GetMessage('ORDERSTATUSEX_MODULE_FALSE'),
	);
}

if(!CModule::IncludeModule('sale'))
{
	$arResult = array(
		'result' => 'error',
		'message'  => GetMessage('SALE_MODULE_FALSE'),
	);
}

if(!$action || !$ajax || !$order_id)
{
	$arResult = array(
		'result' => 'error',
		'message'  => GetMessage('ERROR_AJAX_VARS'),
	);
}

if(!$comment)
{
	$arResult = array(
		'result' => 'error',
		'message'  => GetMessage('ERROR_EMPTY_COMMENT'),
	);

}


/////////////////////////////////////////////////////////////////
//                           EXEC
/////////////////////////////////////////////////////////////////
if(check_bitrix_sessid())
{
	if($order_id && empty($arResult))
	{
		$arOrderFields = CApiOrderStatusEx::getOrderFields($order_id);
		$LID = $arOrderFields['LID'] ? $arOrderFields['LID'] : 's1'; //Current order SITE_ID

		switch($action)
		{
			case 'sendStatusComment':{

				//StatusEX Add fields
				$arAddFields = array(
					'ORDER_ID'    => $arOrderFields['ID'],
					'DATE_CREATE' => Date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL", LANGUAGE_ID))),
					'STATUS'      => $arOrderFields['STATUS_ID'],
					'DESCRIPTION' => $comment,
					'LID'         => $LID,
					'MAIL'        => 'Y',
				);

				foreach($arOrderFields as $key => $val){
					$arAddFields['DESCRIPTION'] = str_replace('#'.$key.'#', $val, $arAddFields['DESCRIPTION']);
				}

				$arOrderFields['ORDER_DESCRIPTION'] = $arAddFields['DESCRIPTION'];


				/////////////////////////////////////////////////////////////////
				//      Check event and event type, if not isset -> Add()
				/////////////////////////////////////////////////////////////////
				$eventType = new CEventType;
				$eventM    = new CEventMessage;

				$TYPE_ID    = 'API_ORDERSTATUSEX';
				$message_id = 0;
				$arET = $arEM = array();

				$arSiteID = array();
				$rs_sites = CSite::GetList($by="sort", $order="desc",array('ACTIVE'=>'Y'));
				while ($ar_site = $rs_sites->Fetch())
					$arSiteID[] = $ar_site['ID'];

				//LANG EVENT TYPE
				$arEventTypeFields = array(
					0 => array(
						'LID'         => 'ru',
						'EVENT_NAME'  => GetMessage('ET_EVENT_NAME'),
						'NAME'        => GetMessage('RU_ET_NAME'),
						'DESCRIPTION' => GetMessage('RU_ET_DESCRIPTION'),
					),
					1 => array(
						'LID'         => 'en',
						'EVENT_NAME'  => GetMessage('ET_EVENT_NAME'),
						'NAME'        => GetMessage('EN_ET_NAME'),
						'DESCRIPTION' => GetMessage('EN_ET_DESCRIPTION'),
					),
				);
				//LANG EVENT MESSAGE
				$arEventMessFields = array(
					'ACTIVE'     => 'Y',
					'EVENT_NAME' => GetMessage('ET_EVENT_NAME'),
					'LID'        => $arSiteID,
					'EMAIL_FROM' => GetMessage('EM_EMAIL_FROM'),
					'EMAIL_TO'   => GetMessage('EM_EMAIL_TO'),
					'SUBJECT'    => GetMessage('EM_SUBJECT_ADMIN'),
					'BODY_TYPE'  => 'html',
					'MESSAGE'    => GetMessage('EM_MESSAGE'),
				);

				$arFilter = array(
					"TYPE_ID" => $TYPE_ID,
					"LID"     => LANGUAGE_ID
				);
				$rsET = $eventType->GetList($arFilter);
				if(!$rsET->Fetch())
				{
					foreach($arEventTypeFields as $arField)
					{
						$dbRes = $eventType->GetByID($arField['EVENT_NAME'], $arField['LID']);
						$arEventType = $dbRes->Fetch();

						if(!$arEventType)
							$eventType->Add($arField);
						else
							$eventType->Update(array('ID'=>$arEventType['ID']),$arField);
					}
				}
				unset($rsET,$dbRes,$arEventType,$arField);

				$rsET = $eventType->GetList($arFilter);
				if($arET = $rsET->Fetch())
				{
					$arFilter = array(
						'TYPE_ID' => $TYPE_ID,
						'LID' => $LID,
					);
					$rsMess = $eventM->GetList($by="id", $order="desc", $arFilter);
					if($arEM = $rsMess->Fetch()){
						$message_id = $arEM['ID'];
					}
					else
					{
						//If not found LID mess, found other LID mess
						$dbRes = $eventM->GetList($by="id", $order="desc", array('TYPE_ID' => $TYPE_ID));
						if($dbRes->Fetch())
							$arEventMessFields['LID'] = $LID;

						if($emID = $eventM->Add($arEventMessFields)){
							$message_id = $emID;
						}
						else
						{
							$arResult = array(
								'result' => 'error',
								'message'  => GetMessage('EVENT_MESSAGE_NOT_ADD'),
							);
						}

					}

					unset($rsET,$arET,$arFilter,$rsMess,$arEM,$dbRes,$emID);
				}
				else
				{
					$arResult = array(
						'result' => 'error',
						'message'  => GetMessage('EVENT_TYPE_NOT_FOUND'),
					);
				}


				/////////////////////////////////////////////////////////////////
				//                      Send message
				/////////////////////////////////////////////////////////////////
				CEvent::SendImmediate('API_ORDERSTATUSEX',$LID,$arOrderFields,'N',$message_id);
				if($message_id)
				{
					//Add StatusEX
					CApiOrderStatusEx::Add($arAddFields);

					//Prepare StatusEx HTML
					$htmlStatusDesc = '
							<tr>
							<td class="api-statusex-count"></td>
							<td>'. $arAddFields['DATE_CREATE'] .'</td>
							<td>'. $arOrderFields['ORDER_STATUS'] .'</td>
							<td>'. (preg_match("/[;&<>\"]/", $arAddFields['DESCRIPTION']) ? $arAddFields['DESCRIPTION'] : nl2br($arAddFields['DESCRIPTION'])) .'</td>
						</tr>
					';

					$arResult = array(
						'result'  => 'ok',
						'message' => GetMessage('EVENT_MESSAGE_SEND'),
						'text'    => $htmlStatusDesc,
					);
				}
				else
				{
					$arResult = array(
						'result' => 'error',
						'message'  => GetMessage('EVENT_MESSAGE_NOT_FOUND'),
					);
				}
			}
			break;

			default:
				$arResult = array(
					'result' => 'error',
					'message'  => GetMessage('SOME_ERROR'),
				);
		}
	}
}
else
{
	$arResult = array(
		'result' => 'error',
		'message'  => GetMessage('SESSION_EXPIRED'),
	);
}

if ($arResult)
{
	$APPLICATION->RestartBuffer();

	$arResult['message'] = $APPLICATION->ConvertCharset($arResult['message'], LANG_CHARSET, 'UTF-8');
	$arResult['text']    = $APPLICATION->ConvertCharset($arResult['text'], LANG_CHARSET, 'UTF-8');

	header('Content-Type: application/json; charset='.LANG_CHARSET);
	//echo CUtil::PhpToJSObject($arResult,false,true);
	echo json_encode($arResult);
	//die();
}
require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");
?>
