<?
global $MESS, $APPLICATION;
IncludeModuleLangFile(__FILE__);
CJSCore::Init(array('jquery'));

Class CApiOrderStatusEx
{
	const MODULE_ID   = 'api.orderstatusex';
	const MYSQL_TABLE = 'api_orderstatusex';
	const MYSQL_ERROR = 'MySQL error in class CApiOrderStatusEx on line ';
	const MESS_ERROR  = 'Can\'t found Message in class CApiOrderStatusEx';

	/**
	 * @var array
	 */
	public $LAST_ERROR = array();
	protected static $allowAdminPages = array(
		'/bitrix/admin/sale_order_new.php',
		'/bitrix/admin/sale_order_detail.php'
	);


	public static function OnBeforeOrderUpdateHandler($ID, $arFields)
	{
		global $DB, $APPLICATION;

		if(!$ID) return;

		//if(substr(php_sapi_name(), 0, 3) == 'cli') return;

		if($ID && $_REQUEST['ID'] && in_array($APPLICATION->GetCurPage(), self::$allowAdminPages))
		{

			$arStatus     = $arStatusEx = $arOrder = array();
			$arOrder      = self::getOrderFields($ID);
			$arStatusLang = self::getFields();

			//Get Status
			$dbRes = CSaleStatus::GetList(
				array(),
				array('LID' => LANGUAGE_ID),
				false,
				false,
				array('ID','NAME','DESCRIPTION')
			);
			while ($ar_res = $dbRes->Fetch())
			{
				$arStatus[$ar_res['ID']] = $ar_res;
			}
			unset($dbRes,$ar_res);


			if($arStatus && $arOrder)
			{
				foreach($arStatus as $key => &$status)
				{
					foreach($arOrder as $k => $v)
					{
						$status['DESCRIPTION'] = str_replace('#'.$k.'#',$v,$status['DESCRIPTION']);
					}
				}

				unset($key,$status,$k,$v);
			}


			//Get StatusEx
			$dbRes = self::getList(array(),array('ORDER_ID' => $ID));
			while($ar_res = $dbRes->Fetch())
			{
				if($ar_res['DATE_CREATE'])
					$ar_res['DATE_CREATE'] = Date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL", LANGUAGE_ID)));

				if($ar_res['STATUS'])
					$ar_res['STATUS'] = $arStatus[$ar_res['STATUS']]['NAME'];

				$arStatusEx[] = $ar_res;
			}
			unset($dbRes,$ar_res);


			//Prepare StatusEx HTML
			$htmlStatusDesc = $htmlStatusBody = '';
			$htmlStatusHeader ='
			<tr class="heading tr_API_ORDER_STATUS_EX"><td colspan="2">'. GetMessage('STATUS_HISTORY') .'</td></tr>
			<tr class="tr_API_ORDER_STATUS_EX">
				<td colspan="2">
					<table class="internal status-history" width="100%">
						<thead>
							<tr class="heading">
								<td>'.$arStatusLang['ID'].'</td>
								<td>'.$arStatusLang['DATE_CREATE'].'</td>
								<td>'.$arStatusLang['STATUS'].'</td>
								<td>'.$arStatusLang['DESCRIPTION'].'</td>
							</tr>
						</thead>
						<tbody>
			';
			$htmlStatusFooter ='
						</tbody>
					</table>
				</td>
			</tr>
		';
			if(is_array($arStatusEx) && $arStatusEx)
			{
				foreach($arStatusEx as $k=>$v)
				{
					$tdStyle = ($v['MAIL'] == 'N' ? ' style="background:#F7F7F7;"' : '');
					$htmlStatusBody .= '
								<tr>
									<td'.$tdStyle.' class="api-statusex-count">'. ++$k .'</td>
									<td'.$tdStyle.'>'. $v['DATE_CREATE'] .'</td>
									<td'.$tdStyle.'>'. $v['STATUS'] .'</td>
									<td'.$tdStyle.'>'. (preg_match("/[;&<>\"]/", $v['DESCRIPTION']) ? $v['DESCRIPTION'] : nl2br($v['DESCRIPTION'])) .'</td>
								</tr>
				';
				}

				$htmlStatusDesc .= $htmlStatusHeader.$htmlStatusBody.$htmlStatusFooter;
			}
			?>
			<style>
				#order_edit_info_form .status-history td{text-align: left}
				#order_edit_info_form .status-history tbody td {background: #FFFFF4;}
			</style>
			<script type="text/javascript">

				var arStatus         = <?=CUtil::PhpToJSObject($arStatus,false,true);?>;
				var htmlStatusHeader = '<?=CUtil::JSEscape($htmlStatusHeader);?>';
				var htmlStatusFooter = '<?=CUtil::JSEscape($htmlStatusFooter);?>';


				function API_SendStatusComment(obj)
				{
					//Show loader
					$(obj).addClass('adm-btn-load').attr('disabled',true).after('<div style="top: 50%; margin-top: -10px; left: 100px;" class="adm-btn-load-img"></div>');

					if($('#ORDER_STATUS_EX').length)
					{
						var API_ORDER_STATUS_EX = $('#ORDER_STATUS_EX').val();

						$.ajax({
							type: 'POST',
							dataType: 'json',
							url: '/bitrix/admin/api_orderstatusex_ajax.php',
							data: {
								ajax: 'Y',
								action: 'sendStatusComment',
								sessid: BX.bitrix_sessid(),
								order_id: '<?=$ID?>',
								comment: API_ORDER_STATUS_EX
							},
							timeout: 6000,
							error: function(request,error) {
								if(error.length)
									alert('Error!' + error);
							},
							success: function(data) {

								//Remove loader
								$(obj).removeClass('adm-btn-load').attr('disabled',false).next('.adm-btn-load-img').detach();

								if(data.text && data.text.length)
								{
									if($('.status-history').length)
									{
										$('.status-history tbody').append(data.text);
									}
									else
									{
										$('.tr_API_ORDER_STATUS_EX').detach();
										$('#tr_NEWO_COMMENTS').before(htmlStatusHeader + data.text + htmlStatusFooter);
									}

									$('.status-history tbody > tr').each(function(index, element){
										$(this).find('.api-statusex-count').text(++index);
									});
								}

								if(data.message.length)
								{
									alert(data.message);
								}
								else
									alert('ERROR_NOT_FOUND_IN_AJAX_RESULT');
							}
						});
					}
				}

				$(function(){

					if($('#STATUS_ID').length && $('#order_edit_info_form').length)
					{
						var obSTATUS_ID = $('#STATUS_ID');
						obSTATUS_ID.after('&nbsp;<input type="checkbox" name="NOT_SEND_EMAIL" value="Y"><?=GetMessage('NOT_NOTIFY')?>');

						$(window).load(function(){
							obSTATUS_ID.change();
						});
					}

					$('#order_edit_info_form').on('change','#STATUS_ID',function(){

						var activeStatusID = $(this).find('option:selected').val();

						if(Object.keys(arStatus).length && activeStatusID.length )
						{
							if(!$(this).parents().find('.ts-sale-custom-desc').length)
							{
								$(this)
									.parents('.adm-detail-required-field')
									.after('' +
									'<tr class="ts-sale-custom-desc">' +
									'<td class="adm-detail-content-cell-l"><?=GetMessage('COMMENT')?></td>' +
									'<td class="adm-detail-content-cell-r">' +
									'<textarea cols="80" rows="6" name="ORDER_STATUS_EX" id="ORDER_STATUS_EX">'+ arStatus[activeStatusID].DESCRIPTION +'</textarea>' +
									'</td>' +
									'<tr class="ts-sale-custom-submit">' +
									'<td class="adm-detail-content-cell-l"></td>' +
									'<td class="adm-detail-content-cell-r"><div style="margin-bottom: 15px; position:relative;"><input type="button" value="<?=GetMessage('BTN_SEND_COMMENT')?>" onclick="API_SendStatusComment(this);" class=""></div></td>' +
									'</tr>');
							}
							else
							{
								$('#ORDER_STATUS_EX').val(arStatus[activeStatusID].DESCRIPTION);
							}
						}
						else
							alert('<?=GetMessage('ORDER_STATUS_FALSE')?>');
					});

					<?if(strlen($htmlStatusDesc) > 0):?>
					$('#tr_NEWO_COMMENTS').before('<?=CUtil::JSEscape($htmlStatusDesc)?>');
					<?endif;?>
				});
			</script>
			<?
		}
	}

	public static function OnOrderStatusSendEmailHandler($ID, &$eventName, &$arFields, $val)
	{
		global $DB, $USER, $APPLICATION;
		$LID = trim($_REQUEST['LID']) ? trim($_REQUEST['LID']) : 's1';

		$sale_email = trim(COption::GetOptionString(self::MODULE_ID, 'sale_email','', $LID));
		if($sale_email)
			$arFields['SALE_EMAIL'] = $sale_email;


		$bNotSendEmail = (isset($_REQUEST['NOT_SEND_EMAIL']) && $_REQUEST['NOT_SEND_EMAIL'] == 'Y');
		if($ID && $_REQUEST['ID'] && in_array($APPLICATION->GetCurPage(), self::$allowAdminPages))
		{
			//StatusEX Add fields
			$arAddFields = array(
				'ORDER_ID'    => $ID,
				'DATE_CREATE' => Date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL", LANGUAGE_ID))),
				'STATUS'      => trim($val),
				'DESCRIPTION' => trim($_REQUEST['ORDER_STATUS_EX']) ? trim($_REQUEST['ORDER_STATUS_EX']) : $arFields['ORDER_DESCRIPTION'],
				'LID'         => $LID,
				'MAIL'        => ($bNotSendEmail ? 'N' : 'Y'),
			);

			//Fill macros in email template and stusEX fields
			if($arOrderFields = self::getOrderFields($ID))
			{
				foreach($arOrderFields as $key => $val)
				{
					$arAddFields['DESCRIPTION'] = str_replace('#'.$key.'#', $val, $arAddFields['DESCRIPTION']);
				}
			}
			$arFields['ORDER_DESCRIPTION'] = $arAddFields['DESCRIPTION'];

			//ORDER MACROS FOR EMAIL TEMPLATES
			$arFields = array_merge($arFields, $arOrderFields);

			//Add StatusEX
			self::Add($arAddFields);

			//Send email or not
			if($bNotSendEmail)
				return false;
		}


		return true;
	}

	public static function OnOrderDeleteHandler($ID,$Success)
	{
		if($ID && $Success)
		{
			self::delete($ID);
		}
	}

	//General events
	public static function OnGeneralSendEmailHandler($ID, &$eventName, &$arFields)
	{
		$arOrderFields = self::getOrderFields($ID);

		$sale_email = trim(COption::GetOptionString(self::MODULE_ID, 'sale_email','', $arOrderFields['LID']));
		if($sale_email)
			$arFields['SALE_EMAIL'] = $sale_email;

		//ORDER MACROS FOR EMAIL TEMPLATES
		$arFields = array_merge($arFields, $arOrderFields);
	}

	public static function getOrderFields($ID)
	{
		global $USER, $SERVER_NAME;

		if(!$ID)
			return array();

		$arOrder = CSaleOrder::GetByID($ID);


		//ORDER PRICES
		if($arOrder['CURRENCY'])
		{
			if($arOrder['PRICE'])
				$arOrder['PRICE'] = CCurrencyLang::CurrencyFormat($arOrder['PRICE'], $arOrder["CURRENCY"], true);

			if($arOrder['PRICE_DELIVERY'])
				$arOrder['PRICE_DELIVERY'] = CCurrencyLang::CurrencyFormat($arOrder['PRICE_DELIVERY'], $arOrder["CURRENCY"], true);

			if($arOrder['DISCOUNT_VALUE'])
				$arOrder['DISCOUNT_VALUE'] = CCurrencyLang::CurrencyFormat($arOrder['DISCOUNT_VALUE'], $arOrder["CURRENCY"], true);

			if($arOrder['PS_SUM'] && $arOrder["PS_CURRENCY"])
				$arOrder['PS_SUM'] = CCurrencyLang::CurrencyFormat($arOrder['PS_SUM'], $arOrder["PS_CURRENCY"], true);

			if($arOrder['TAX_VALUE'])
				$arOrder['TAX_VALUE'] = CCurrencyLang::CurrencyFormat($arOrder['TAX_VALUE'], $arOrder["CURRENCY"], true);

			if($arOrder['SUM_PAID'])
				$arOrder['SUM_PAID'] = CCurrencyLang::CurrencyFormat($arOrder['SUM_PAID'], $arOrder["CURRENCY"], true);
		}


		//ORDER USER
		if($arOrder['USER_ID'])
		{
			$arSelect['FIELDS'] = array('ID','TITLE','LOGIN','EMAIL','NAME','LAST_NAME','SECOND_NAME');
			$arFilter = array('ID' => (int)$arOrder['USER_ID']);
			$dbUser = CUser::GetList(($by="id"), ($order="asc"), $arFilter, $arSelect);
			if($arUser = $dbUser->Fetch())
			{
				unset($arUser['ID']);

				foreach($arUser as $key => $val)
				{
					$arOrder['USER_'.$key] = $val;
				}
				$arOrder['USER_IF']  = trim($arOrder['USER_NAME'] .' '. $arOrder['USER_LAST_NAME']);
				$arOrder['USER_FI']  = trim($arOrder['USER_LAST_NAME'] .' '. $arOrder['USER_NAME']);
				$arOrder['USER_IO']  = trim($arOrder['USER_NAME'] .' '. $arOrder['USER_SECOND_NAME']);
				$arOrder['USER_FIO'] = trim($arOrder['USER_LAST_NAME'] .' '. $arOrder['USER_NAME'] .' '. $arOrder['USER_SECOND_NAME']);
			}

			unset($arSelect, $arFilter, $dbUser, $arUser, $key, $val);
		}


		//SALE USER CHANGED ORDER
		if($SALE_USER_ID = (int)$USER->GetID())
		{
			$arSelect['FIELDS'] = array('ID','TITLE','LOGIN','EMAIL','NAME','LAST_NAME','SECOND_NAME');
			$arFilter = array('ID' => $SALE_USER_ID);
			$dbUser = CUser::GetList(($by="id"), ($order="asc"), $arFilter, $arSelect);
			if($arUser = $dbUser->Fetch())
			{
				unset($arUser['ID']);

				foreach($arUser as $key => $val)
				{
					$arOrder['SALE_USER_'.$key] = $val;
				}
				$arOrder['SALE_USER_IF']  = trim($arOrder['USER_NAME'] .' '. $arOrder['USER_LAST_NAME']);
				$arOrder['SALE_USER_FI']  = trim($arOrder['USER_LAST_NAME'] .' '. $arOrder['USER_NAME']);
				$arOrder['SALE_USER_IO']  = trim($arOrder['USER_NAME'] .' '. $arOrder['USER_SECOND_NAME']);
				$arOrder['SALE_USER_FIO'] = trim($arOrder['USER_LAST_NAME'] .' '. $arOrder['USER_NAME'] .' '. $arOrder['USER_SECOND_NAME']);
			}

			unset($arSelect, $arFilter, $dbUser, $arUser, $key, $val);
		}


		//ORDER PROPS
		$arLocation = array();
		$emailTo = '';
		$dbProps = CSaleOrderPropsValue::GetOrderProps($ID);
		while($arProp = $dbProps->Fetch())
		{
			if($arProp['TYPE'] == 'LOCATION' && $arProp['IS_LOCATION'] == 'Y'){
				$arLocation = $arProp;
			}

			if($arProp['IS_EMAIL'] == 'Y')
				$emailTo = trim($arProp['VALUE']);

			if($arProp['CODE'])
				$arOrder['PROP_'.$arProp['CODE']] = trim($arProp['VALUE']);

			$arOrder['PROP_'.$arProp['ORDER_PROPS_ID']] = trim($arProp['VALUE']);

		}
		unset($dbProps, $arProp);


		//ORDER LOCATION
		if($arLocation['VALUE'] && $arLoc = CSaleLocation::GetByID($arLocation['VALUE']))
		{
			unset($arLoc['ID'], $arLoc['SORT'], $arLoc['CODE'], $arLoc['COUNTRY_ID'], $arLoc['REGION_ID'], $arLoc['CITY_ID']);

			foreach($arLoc as $key => $val)	{
				$arOrder[$key] = trim($val);
			}

			//Generate full location
			//$arOrder['PROP_'.$arLocation['CODE']] = trim($arLoc['COUNTRY_NAME'] .', '. $arLoc['REGION_NAME'] .', '. $arLoc['CITY_NAME']);
			$sLocation = $arLoc['COUNTRY_NAME'] .($arLoc['REGION_NAME'] == ''? '': ', '). $arLoc['REGION_NAME'] .($arLoc['CITY_NAME'] == ''? '': ', '). $arLoc['CITY_NAME'];
			if($arLocation['CODE'])
				$arOrder['PROP_'.$arLocation['CODE']] = trim($sLocation);

		}


		//ORDER PAY_SYSTEM
		if($arOrder['PAY_SYSTEM_ID'])
		{
			$dbRes = CSalePaySystem::GetList(array(),array('ID' => $arOrder['PAY_SYSTEM_ID']), false, false, array('NAME','DESCRIPTION'));
			$arPaySystem = $dbRes->Fetch();
			$arOrder['PAY_SYSTEM_NAME'] = trim($arPaySystem['NAME']);
			$arOrder['PAY_SYSTEM_DESCRIPTION'] = trim($arPaySystem['DESCRIPTION']);

			unset($dbRes, $arPaySystem);
		}


		//ORDER DELIVERY
		if($arOrder['DELIVERY_ID'])
		{
			$arExp = array();
			if(intval($arOrder['DELIVERY_ID']) > 0)
			{
				$dbRes = CSaleDelivery::GetList(array(),array('ID' => $arOrder['DELIVERY_ID']), false, false, array('NAME','DESCRIPTION'));
			}
			else
			{
				$arExp = explode(':',$arOrder['DELIVERY_ID']);
				$dbRes = CSaleDeliveryHandler::GetList(array(),array('SID' => $arExp[0]));
			}

			$arDelivery = $dbRes->Fetch();

			if($arDelivery['NAME'])
				$arOrder['DELIVERY_NAME']	= $arDelivery['NAME'];
			elseif($arExp[1] && $arDelivery['PROFILES'][$arExp[1]]['TITLE'])
				$arOrder['DELIVERY_NAME'] =  $arDelivery['PROFILES'][$arExp[1]]['TITLE'];

			$arOrder['DELIVERY_DESCRIPTION'] = trim($arDelivery['DESCRIPTION']);

			unset($dbRes, $arExp, $arDelivery);
		}

		//ORDER STATUS
		if($arOrder['STATUS_ID'])
		{
			$arFilter = array(
				'ID' => $arOrder['STATUS_ID'],
				'LID' => LANGUAGE_ID,
			);
			$dbRes = CSaleStatus::GetList(array(),$arFilter,false,false,array('ID','NAME'));
			if($arStatus = $dbRes->Fetch())
			{
				if($arStatus['NAME'])
					$arOrder['ORDER_STATUS'] = $arStatus['NAME'];
			}
		}


		//ORDER SITE
		if($arOrder['LID'])
		{
			$rsSites = CSite::GetList($by="sort", $order="desc", Array("LID" => $arOrder['LID']));
			$arSite = $rsSites->Fetch();
			$arOrder['SITE_NAME'] = $arSite['SITE_NAME'];
			$arOrder['SITE_EMAIL'] = $arSite['EMAIL'];
			$arOrder['SITE_SERVER_NAME'] = $arSite['SERVER_NAME'];
			$arOrder['SITE_SERVER_URL']  =  (CMain::IsHTTPS() ? "https://" : "http://") . $arSite['SERVER_NAME'];
		}


		//ORDER USER EMAIL
		$arOrder['ORDER_ID']   = $arOrder['ACCOUNT_NUMBER'] ? $arOrder['ACCOUNT_NUMBER'] : $arOrder['ID'];
		$arOrder['ORDER_DATE'] = $arOrder['DATE_INSERT_FORMAT'] ? $arOrder['DATE_INSERT_FORMAT'] : $arOrder['DATE_INSERT'];

		$arOrder['SALE_EMAIL'] = trim(COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME));
		if($sale_email = trim(COption::GetOptionString(self::MODULE_ID, 'sale_email','', $arOrder['LID'])))
			$arOrder['SALE_EMAIL'] = $sale_email;

		$arOrder['EMAIL_TO']   = $emailTo ? $emailTo : $arOrder['USER_EMAIL'];
		if(!$arOrder['EMAIL_TO'] && $arOrder['SALE_EMAIL'])
			$arOrder['EMAIL_TO'] = $arOrder['SALE_EMAIL'];


		//ORDER HEADER & ORDER FOOTER FOR EMAIL TEMPLATE
		$ORDER_HEADER = trim(COption::GetOptionString(self::MODULE_ID, 'order_header','', $arOrder['LID']));
		$ORDER_FOOTER = trim(COption::GetOptionString(self::MODULE_ID, 'order_footer','', $arOrder['LID']));
		$arOrder['ORDER_HEADER'] = self::replaceMacros($arOrder,$ORDER_HEADER);
		$arOrder['ORDER_FOOTER'] = self::replaceMacros($arOrder,$ORDER_FOOTER);

		return $arOrder;
	}

	public static function replaceMacros($arFields, $strReturn)
	{
		foreach($arFields as $key => $val)
		{
			$strReturn = str_replace('#'.$key.'#', trim($val), $strReturn);
		}

		return $strReturn;
	}

	public static function getList($aSort = array(), $arFilter = array())
	{
		global $DB;

		if($arFilter['ID'])
			$strSql = "SELECT *
						FROM `".self::MYSQL_TABLE."`
						WHERE ID IN(". ($DB->ForSql(is_array($arFilter['ID']) ? implode(',',$arFilter['ID']) : $arFilter['ID'])) .")
						ORDER BY `ID` ASC";
		elseif($arFilter['ORDER_ID'])
			$strSql = "SELECT *
						FROM `".self::MYSQL_TABLE."`
						WHERE ORDER_ID IN(". ($DB->ForSql(is_array($arFilter['ORDER_ID']) ? implode(',',$arFilter['ORDER_ID']) : $arFilter['ORDER_ID'])) .")
						ORDER BY `ID` ASC";
		else
			$strSql = "SELECT *
						FROM `".self::MYSQL_TABLE."`
						ORDER BY `ID` ASC";

		if(!$res = $DB->Query($strSql, false, self::MYSQL_ERROR.__LINE__))
			return false;

		return $res;
	}

	public static function delete($ID)
	{
		global $DB,$USER;

		if(!$ID) return false;

		$strSql = "DELETE FROM `".self::MYSQL_TABLE ."` WHERE `ORDER_ID` = ".(int)$ID;
		if(!$DB->Query($strSql, false, self::MYSQL_ERROR.__LINE__))
			return false;

		$LID = trim($_REQUEST['LID']) ? trim($_REQUEST['LID']) : 's1';
		$log = array(
			"ID" => $ID,
			"USER_ID" => $USER->GetID(),
		);
		CEventLog::Log(
			"INFO",
			"ORDER_STATUS_EX_DELETE",
			self::MODULE_ID,
			$ID,
			serialize($log),
			$LID
		);

		return true;
	}

	public static function add($arFields)
	{
		global $DB, $APPLICATION;

		if (!$arFields || !$arFields['ORDER_ID'])
			return false;

		$strSql = "INSERT INTO `".self::MYSQL_TABLE ."`
				   SET  `ORDER_ID`    = '". (int)$arFields['ORDER_ID'] ."',
						`STATUS`      = '". $DB->ForSql($arFields['STATUS']) ."',
						`DATE_CREATE` = ".  $DB->CharToDateFunction($arFields['DATE_CREATE'], "FULL", LANGUAGE_ID) .",
						`DESCRIPTION` = '". $DB->ForSql($arFields['DESCRIPTION']) ."',
						`LID`         = '". $DB->ForSql($arFields['LID']) ."',
						`MAIL`        = '". $DB->ForSql($arFields['MAIL']) ."'
					";

		if(!$DB->Query($strSql, false, self::MYSQL_ERROR.__LINE__))
			return false;

		$ID = IntVal($DB->LastID());

		if(!$ID)
		{
			$APPLICATION->ThrowException(GetMessage("ERROR_STATUS_ADD"), "ERROR_STATUS_ADD");
			return false;
		}

		return $ID;
	}

	public static function getFields()
	{
		return array(
			'ID'          => self::getMess('ID'),
			'ORDER_ID'    => self::getMess('ORDER_ID'),
			'STATUS'      => self::getMess('STATUS'),
			'DATE_CREATE' => self::getMess('DATE_CREATE'),
			'DESCRIPTION' => self::getMess('DESCRIPTION'),
			'LID'         => self::getMess('LID'),
			'MAIL'        => self::getMess('MAIL'),
		);
	}

	protected static function getMess($mess = '')
	{
		$return = (string)self::MESS_ERROR;
		if ($mess && GetMessage('COLUMN_'.$mess))
			$return = GetMessage('COLUMN_'.$mess);

		return $return;
	}
}
?>