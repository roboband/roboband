<?
global $MESS, $DB, $USER, $APPLICATION, $REQUEST_METHOD;
if(!$USER->IsAdmin())
	return;

IncludeModuleLangFile(__FILE__);
$MODULE_ID = basename(dirname(__FILE__));

CModule::IncludeModule("fileman");
//$isUserHavePhpAccess = $USER->CanDoOperation('edit_php');
//echo CAdminMessage::ShowMessage('aaaa');
//echo CAdminMessage::ShowNote('aaaa');

$arTabs = $arSites = array();
$rs_sites = CSite::GetList($by = "sort", $order = "asc", array('ACTIVE'=>'Y'));
while( $ar_site = $rs_sites->Fetch() )
{
	$arTabs[] = array(
		'DIV'   => 'api_tab_' . $ar_site['ID'],
		'TAB'   => '('. $ar_site['ID'] .')',
		'ICON'  => 'fileman_settings',
		'TITLE' => GetMessage('SITE_SETTINGS') .' «'. $ar_site['SITE_NAME'] .'»',
	);
	$arSites[] = $ar_site;
}

if( $REQUEST_METHOD == 'POST' && strlen($_POST['Apply'].$_POST['Update']) && check_bitrix_sessid() )
{
	if(!empty($arSites))
	{
		foreach( $arSites as $arSite )
		{
			//sale_email
			COption::SetOptionString(
				$MODULE_ID,
				'sale_email',
				trim($_REQUEST['sale_email_'. $arSite['ID']]) ? trim($_REQUEST['sale_email_'. $arSite['ID']]) : '',
				false,
				$arSite['ID']
			);
			//order_header
			COption::SetOptionString(
				$MODULE_ID,
				'order_header',
				trim($_REQUEST['order_header_'. $arSite['ID']]) ? trim($_REQUEST['order_header_'. $arSite['ID']]) : '',
				false,
				$arSite['ID']
			);
			//order_footer
			COption::SetOptionString(
				$MODULE_ID,
				'order_footer',
				trim($_REQUEST['order_footer_'. $arSite['ID']]) ? trim($_REQUEST['order_footer_'. $arSite['ID']]) : '',
				false,
				$arSite['ID']
			);
		}
		echo CAdminMessage::ShowNote(GetMessage('SETTINGS_SAVED'));
	}
}

$tabControl = new CAdminTabControl('tabcontrol', $arTabs);
$tabControl->Begin();
?>
<form method="POST" enctype="multipart/form-data" action="<?=$APPLICATION->GetCurPage()?>?mid=<?=$MODULE_ID?>&lang=<?=LANGUAGE_ID?>">
	<?= bitrix_sessid_post() ?>
	<?
	if( !empty($arSites) )
	{
		foreach( $arSites as $arSite )
		{
			?>
			<? $tabControl->BeginNextTab(); ?>
			<tr>
				<td class="adm-detail-content-cell-l"><?= GetMessage('SALE_EMAIL'); ?>:</td>
				<td class="adm-detail-content-cell-r">
					<?
					$sale_email = COption::GetOptionString($MODULE_ID, 'sale_email','', $arSite['ID']);
					?>
					<input type="text" name="sale_email_<?=$arSite['ID']?>" value="<?=trim($sale_email)?>" id="sale_email_<?=$arSite['ID']?>" size="60" />
				</td>
			</tr>

			<tr class="heading"><td colspan="2"><?=GetMessage('GROUP_EMAIL_TPL')?></td></tr>
			<tr>
				<td colspan="2" align="center">
					<?
					echo BeginNote();
					echo GetMessage('MESS_EMAIL_TPL');
					echo EndNote();
					?>
				</td>
			</tr>
			<tr class="heading"><td colspan="2"><?=GetMessage('ORDER_HEADER')?></td></tr>
			<tr>
				<td colspan="2" align="center">
					<?
					$order_header = COption::GetOptionString($MODULE_ID, 'order_header','', $arSite['ID']);
					?>
					<?CFileMan::AddHTMLEditorFrame(
						"order_header_".$arSite['ID'],
						$order_header,
						"order_header_type_".$arSite['ID'],
						($_REQUEST['order_header_type_'.$arSite['ID']]),
						array(
							'height' => 150,
							'width' => '100%'
						),
						"N",
						0,
						"",
						"",
						$arSite['ID'],//false
						true,//!$isUserHavePhpAccess
						false,
						array(
							//'toolbarConfig' => CFileman::GetEditorToolbarConfig("iblock_".(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1 ? 'public' : 'admin')),
							//'saveEditorKey' => $IBLOCK_ID,
							//'site_template_type' => 'mail',
							//'templateID' => $str_SITE_TEMPLATE_ID,
							//'componentFilter' => array('TYPE' => 'mail'),
							//'limit_php_access' => !$isUserHavePhpAccess
						)
					);?>
				</td>
			</tr>
			<tr class="heading"><td colspan="2"><?=GetMessage('ORDER_FOOTER')?></td></tr>
			<tr>
				<td colspan="2" align="center">
					<?
					$order_footer = COption::GetOptionString($MODULE_ID, 'order_footer','', $arSite['ID']);
					?>
					<?CFileMan::AddHTMLEditorFrame(
						"order_footer_".$arSite['ID'],
						$order_footer,
						"order_footer_type_".$arSite['ID'],
						($_REQUEST['order_footer_type_'.$arSite['ID']]),
						array(
							'height' => 150,
							'width' => '100%'
						),
						"N",
						0,
						"",
						"",
						$arSite['ID'],//false
						true,//!$isUserHavePhpAccess
						false,
						array(
							//'toolbarConfig' => CFileman::GetEditorToolbarConfig("iblock_".(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1 ? 'public' : 'admin')),
							//'saveEditorKey' => $IBLOCK_ID,
							//'site_template_type' => 'mail',
							//'templateID' => $str_SITE_TEMPLATE_ID,
							//'componentFilter' => array('TYPE' => 'mail'),
							//'limit_php_access' => !$isUserHavePhpAccess
						)
					);?>
				</td>
			</tr>
		<?
		}
	}
	?>
	<? $tabControl->Buttons(); ?>
	<input type="hidden" name="Update" value="Y" />
	<input <?if (!$USER->CanDoOperation('edit_other_settings')) echo "disabled" ?>
		type="submit"
		name="Apply"
		value="<?=GetMessage("MAIN_SAVE")?>"
		class="adm-btn-save">
	<? $tabControl->End(); ?>
</form>