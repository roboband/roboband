<?
IncludeModuleLangFile(__FILE__);
class mailorderdriver
{
	static $MODULE_ID = "ipol.mailorder";

	function insertMacrosData(&$event, &$lid, &$arFields){
		$chozen = explode(',',COption::GetOptionString(self::$MODULE_ID,"IPOLMO_OPT_EVENTS","SALE_NEW_ORDER"));
		$manual = unserialize(COption::GetOptionString(self::$MODULE_ID,"IPOLMO_OPT_ADDEVENTS","a:{}"));
		$rightEvent = true;
		if(!in_array($event,$chozen) && !in_array($event,$manual)){
			if(strpos($event,"SALE_STATUS_CHANGED")===0 && in_array("SALE_STATUS_CHANGED",$chozen))
				$rightEvent = true;
			else
				$rightEvent = false;
		}

		if(strlen($arFields['ORDER_ID'])<=0 || !$rightEvent || !CModule::includeModule('sale'))
			return;

		$orderId=$arFields['ORDER_ID'];
		$checkAN=CSaleOrder::GetList(array(),array('ACCOUNT_NUMBER'=>$orderId))->Fetch();
		if($checkAN)
			$orderId=$checkAN['ID'];
		$addedArray=array();
		$addedStr='';
		$arSavedProps=explode('|',COption::GetOptionString(mailorderdriver::$MODULE_ID,'IPOLMO_OPT_PROPS',''));
		$savedProps='';
		foreach($arSavedProps as $propStr)
			$savedProps.=','.substr($propStr,strpos($propStr,'{')+1,strpos($propStr,'}')-strpos($propStr,'{')-1);
		$savedProps=str_replace(',,',',',$savedProps);

		$arSavedProps = explode(',', $savedProps);
		foreach($arSavedProps as $key=>$val) {
			$val = trim($val);
			$val = trim(substr($val, 0, strpos($val, ' ')));
			if(!$val) unset($arSavedProps[$key]); else $arSavedProps[$key] = $val;
		}

		$orderProps = CSaleOrderPropsValue::GetOrderProps($orderId);
		$newStringSign="\n";
		if(COption::GetOptionString(mailorderdriver::$MODULE_ID,'IPOLMO_OPT_TEXTMODE','1')=='2')
			$newStringSign="<br>";
		while($prop=$orderProps->Fetch()){
			if(in_array($prop['CODE'], $arSavedProps)) // == MOD ���� � �������
			{
				if ($prop['TYPE']=="LOCATION"){
					if(method_exists("CSaleLocation","isLocationProMigrated") && CSaleLocation::isLocationProMigrated() && strlen($prop['VALUE']) > 8)
						$prop['VALUE'] = CSaleLocation::getLocationIDbyCODE($prop['VALUE']);
					$location=CSaleLocation::GetByID($prop['VALUE']);
					$strLocation=$location['COUNTRY_NAME_LANG'];
					if($location['CITY_ID'])
						$strLocation=$location['CITY_NAME_LANG'];
					$addedArray['IPOLMO_'.$prop['CODE']]=$strLocation;
					$addedStr.=$prop['NAME']." - ".$strLocation.$newStringSign;
				} 
				else{
					$value = $prop['VALUE'];
					if($prop['TYPE']=="RADIO" || $prop['TYPE']=="SELECT"){
						$value = CSaleOrderPropsVariant::GetByValue($prop['ORDER_PROPS_ID'],$value);
						$value = $value['NAME'];
					}
					$addedArray['IPOLMO_'.$prop['CODE']]=$value;
					if($prop['TYPE']=="CHECKBOX")
						$addedStr.=$prop['NAME'].$newStringSign;
					else
						$addedStr.=$prop['NAME']." - ".$value.$newStringSign;
				}
			}
		}
		foreach($arSavedProps as $val){
			if(!isset($addedArray['IPOLMO_'.$val])) 
				$addedArray['IPOLMO_'.$val] = '';
		}
		$orderHimself=CSaleOrder::GetByID($orderId);
		// ��������� �������
		if(strpos($savedProps,',IMOPAYSYSTEM ( #IPOLMO_IMOPAYSYSTEM# ),')!==false)
		{
			$paySystem=CSalePaySystem::GetByID($orderHimself['PAY_SYSTEM_ID']);
			$addedArray['IPOLMO_IMOPAYSYSTEM']=$paySystem['NAME'];
			$addedStr.=GetMessage("IPOLMO_OPT_PROPS_PAYSYSTEM")." - ".$paySystem['NAME'].$newStringSign;
		}
		// ��������
		if(strpos($savedProps,',IMODELIVERY ( #IPOLMO_IMODELIVERY# ),')!==false)
		{
			$IHopeThatNoOneWouldSeeIt=strval(intval($orderHimself['DELIVERY_ID']));
			if($IHopeThatNoOneWouldSeeIt==$orderHimself['DELIVERY_ID'])
			{
				$deliverySystem=CSaleDelivery::GetByID($orderHimself['DELIVERY_ID']);
				$deliveryName=$deliverySystem['NAME'];
			}
			else
			{
				$deliveryId=explode(':',$orderHimself['DELIVERY_ID']);
				if($deliverySystem=CSaleDeliveryHandler::GetBySID($deliveryId[0])->Fetch())
				{
					$deliveryName=$deliverySystem['NAME'];
					if($deliverySystem['PROFILES'][$deliveryId[1]]['TITLE'])
						$deliveryName.=" (".$deliverySystem['PROFILES'][$deliveryId[1]]['TITLE'].")";
				}
				else
					$deliveryName=false;
			}
			if($deliveryName)
			{
				$addedArray['IPOLMO_IMODELIVERY']=$deliveryName;
				$addedStr.=GetMessage("IPOLMO_OPT_PROPS_DELIVERY")." - ".$deliveryName.$newStringSign;
			}
		}
		// �������� �� ������
		if(strpos($savedProps,',IMOPAYED ( #IPOLMO_IMOPAYED# ),')!==false){
			$strOfPayed=false;
			if($orderHimself['PAY_VOUCHER_NUM']){
				$strOfPayed=GetMessage("IPOLMO_SIGN_PAYDOC").$orderHimself['PAY_VOUCHER_NUM'];
				if(preg_match('/([\d]{4})-([\d]{2})-([\d]{2})/',$orderHimself['PAY_VOUCHER_DATE'],$matches))
					$strOfPayed.=" ".GetMessage("IPOLMO_SIGN_FROM").$matches[3].".".$matches[2].".".$matches[1];
			}
			if($strOfPayed){
				$addedArray['IPOLMO_IMOPAYED']=$strOfPayed;
				$addedStr.=$strOfPayed.$newStringSign;
			}
		}
		// ������������� �����������
		if(strpos($savedProps,',IMOTRACKING ( #IPOLMO_IMOTRACKING# ),')!==false){
			$strOfPayed=false;
			if($orderHimself['TRACKING_NUMBER'])
				$strOfPayed=GetMessage("IPOLMO_SIGN_TRACKING")." - ".$orderHimself['TRACKING_NUMBER'];
			if($strOfPayed){
				$addedArray['IMOTRACKING']=$strOfPayed;
				$addedStr.=$strOfPayed.$newStringSign;
			}
		}		
		// ����� ������
		if(strpos($savedProps,',IMOPRICE ( #IPOLMO_IMOPRICE# ),')!==false){
			$strOfPayed=false;
			$strOfPayed=GetMessage("IPOLMO_SIGN_PRICE")." - ".CCurrencyLang::CurrencyFormat($orderHimself['PRICE'],$orderHimself['CURRENCY'],true);
			$addedArray['IMOTRACKING']=$strOfPayed;
			$addedStr.=$strOfPayed.$newStringSign;
		}
		$mode=COption::GetOptionString(mailorderdriver::$MODULE_ID,'IPOLMO_OPT_WORKMODE','1');
		if($mode=='1')
			$arFields=array_merge($arFields,$addedArray);
		if($mode=='2')
			$arFields['IPOLMOALL_PROPS']=$addedStr;
	}
}
?>