<?
namespace reflection;
/**
 * Description of user
 *
 * @author Andrey
 */
class clsEventsHandler {

    static $MODULE_ID = "reflection.goodsemail";
    static $ORDER_ID = 0;
    static $user_email = "";


    function __construct() {

    }

    /**
     * @param $ID - идентификатор заказа
     * @param $val- ID статуса заказа
     */
    function OnStatusUpdateHandler($ID, $val) {
        global $MESS, $USER;

        $customOrderId = $ID;

        \CModule::IncludeModule("iblock");

        // Получаем опцию, на какоой статус нужно среагировать
        $status_type = \COption::GetOptionString(self::$MODULE_ID, "reflekto_goodsemail_saletype[]");
        $status_type_arr = explode("|",$status_type);

        // Если статус один из тех, на который нужно среагировать, то отправляем почтовое сообщение
        if(in_array($val,$status_type_arr)) {
            // Открываем файл
            $template_id = \COption::GetOptionString(self::$MODULE_ID, "reflekto_goodsemail_template");;
            $template_path = $_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".self::$MODULE_ID."/classes/mail_template/".$template_id.".php";
            $template = file_get_contents($template_path);

            if($template==false) return false;

            // Получаем информацию о заказе пользователя
            if (!($arOrder = \CSaleOrder::GetByID($ID)) || !is_int($ID)) {

                if(!($arOrder = \CSaleOrder::GetList(array(), array('ACCOUNT_NUMBER' => $ID))->Fetch())) {
                    return false;
                } else {
                    $ID = $arOrder['ID'];
                }

            }


            // Получаем ифномрацию о доставке
            $delivery_type = "";
            if($arOrder['DELIVERY_ID']!=NULL) {

                // пробуем вначале автоматическую службу
                $arDeliv = \CSaleDelivery::GetByID($arOrder['DELIVERY_ID']);
                if($arDeliv!=NULL && $arDeliv != false) {
                    $delivery_type = $arDeliv['NAME'];
                } else {


                      // пробуем настраиваемую
                    $arOrder['DELIVERY_ID'] = explode(":",$arOrder['DELIVERY_ID']);
                    //echo $arOrder['DELIVERY_ID'][0];
                    $arDeliv = \CSaleDeliveryHandler::GetBySID($arOrder['DELIVERY_ID'][0]);
                    if ($arResult_elem = $arDeliv->GetNext()) {
                        $delivery_type = $arResult_elem['NAME'];
                        if ($delivery_type =="") $delivery_type = $arResult_elem["PROFILES"][$arOrder['DELIVERY_ID'][1]]["TITLE"];
                    }

                }


            }


            // Подключаем нужный языковой файл
            $rsSites = \CSite::GetByID($arOrder['LID']);
            $arSite = $rsSites->Fetch();
            include($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".self::$MODULE_ID."/classes/mail_template/lang/".$arSite['LANGUAGE_ID']."/".$template_id.".php");


            // подставляем полученные данные в шаблон
            $template = preg_replace("#_YOUR_ORDER_#ims", GetMessage("YOUR_ORDER"), $template);

            // Подставляем часть правой колонки
            $right_block = $_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".self::$MODULE_ID."/classes/mail_template/right_block_info.php";
            $right_block_template = file_get_contents($right_block);


            // Выбираем нужные свойства заказа по соответствующему типу плательщика
            $props_string = self::formOrderOptions('reflekto_goodsemail_orderprop_col_left[]', $arOrder, $ID);
            if($props_string=="") {
                $template = preg_replace("#__RIGHT_BLOCK_INFO__#ims", "", $template);
            } else {
                $template = preg_replace("#__RIGHT_BLOCK_INFO__#ims", $right_block_template, $template);
            }
            $template = preg_replace("#__ORDER_PROPS_COL_LEFT__#ims", $props_string, $template);

            $props_string = self::formOrderOptions('reflekto_goodsemail_orderprop_center[]', $arOrder, $ID);
            $template = preg_replace("#__ORDER_PROPS_CENTER__#ims", $props_string, $template);


            $delivery_info = "";

            if(self::$user_email=="" && $USER->IsAuthorized()) {
                self::$user_email = $USER->GetEmail();
            }


            // Получаем информацию по платежной системе
            $pay_system = "";
            if ($arPaySys = \CSalePaySystem::GetByID($arOrder['PAY_SYSTEM_ID'], $arOrder["PERSON_TYPE_ID"])) {
                $pay_system = $arPaySys["NAME"];

            }

            // Получаем информацию по товарам в заказе
            $arBasketItems = array();

            $dbBasketItems = \CSaleBasket::GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                ),
                array(
                    "ORDER_ID" => $ID
                ),
                false,
                false,
                array("ID", "CALLBACK_FUNC", "MODULE",
                    "PRODUCT_ID", "QUANTITY", "DELAY",
                    "CAN_BUY", "PRICE", "WEIGHT")
            );

            // Опция: показывать ли характеристика товара
            $option_show_props = \COption::GetOptionString(self::$MODULE_ID, "reflekto_goodsemail_show_order_props", "Y");

            // Открыть шаблон вывода блока ифнормации о товаре
            $item_template_path = $_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".self::$MODULE_ID."/classes/mail_template/item_".$template_id.".php";
            $item_template = file_get_contents($item_template_path);

            $all_items = "";

            while ($arItems = $dbBasketItems->Fetch()) {
                $arFile = null;
                // Получаем информацию из инфоблока
                $cur_item = $item_template;

                $res = \CIBlockElement::GetByID($arItems['PRODUCT_ID']);

                $arFilter = Array(
                    "ID" => $arItems['PRODUCT_ID']
                );

                if($ar_res = $res->GetNext()) {
                    $arFilter['IBLOCK_ID'] = $ar_res['IBLOCK_ID'];
                }

                $res = \CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_CML2_LINK", "DETAIL_PAGE_URL"));

                if($ar_res_item = $res->GetNext()) {

                    $item_link = 'http://'.$arSite['SERVER_NAME'].$ar_res_item['DETAIL_PAGE_URL'];
                    // Посмотрим еще на ссылку из основного каталога, возможно она более полная
                    if($ar_res_item['PROPERTY_CML2_LINK_VALUE']!=NULL) {
                        $res_catalog = \CIBlockElement::GetByID($ar_res_item['PROPERTY_CML2_LINK_VALUE']);
                        if($ar_res_catalog = $res_catalog->GetNext()) {
                            if(mb_strlen($ar_res_item['DETAIL_PAGE_URL']) < mb_strlen($ar_res_catalog['DETAIL_PAGE_URL'])) {
                                $item_link = 'http://'.$arSite['SERVER_NAME'].$ar_res_catalog['DETAIL_PAGE_URL'];
                            }
                        }
                    }
                    $cur_item = preg_replace("#_DETAIL_URL_#ims", $item_link, $cur_item);


                    if($arFile==null) {
                        // Тащим фото из торгового каталога предложений

                        // Фотография продукта
                        if($ar_res_item['PREVIEW_PICTURE']!=NULL) {
                            $rsFile = \CFile::GetByID($ar_res_item["PREVIEW_PICTURE"]);
                            $arFile = $rsFile->Fetch();
                        } else if($ar_res_item['DETAIL_PICTURE']!=NULL) {
                            $rsFile = \CFile::GetByID($ar_res_item["DETAIL_PICTURE"]);
                            $arFile = $rsFile->Fetch();
                        }
                    }

                    // Возможно это торговое предложение, тогда поищем изображение в основном каталоге
                    if($ar_res_item['PROPERTY_CML2_LINK_VALUE']!=NULL && ($arFile==null || $arFile==false)) {
                        // Тащим фото из основного каталога товаров
                        $res_catalog = \CIBlockElement::GetByID($ar_res_item['PROPERTY_CML2_LINK_VALUE']);
                        if($ar_res_catalog = $res_catalog->GetNext()) {
                            // Фотография продукта
                            if($ar_res_catalog['PREVIEW_PICTURE']!=NULL) {
                                $rsFile = \CFile::GetByID($ar_res_catalog["PREVIEW_PICTURE"]);
                                $arFile = $rsFile->Fetch();
                            } else if($ar_res_catalog['DETAIL_PICTURE']!=NULL) {
                                $rsFile = \CFile::GetByID($ar_res_catalog["DETAIL_PICTURE"]);
                                $arFile = $rsFile->Fetch();
                            }
                        }

                    }


                    $file = null;
                    if($arFile!=NULL) {
                        $file = \CFile::ResizeImageGet($arFile, array('width' => 120, 'height' => 120), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
                        if(!isset($file['src'])) $file['src'] = '/upload/'.$arFile['SUBDIR'].'/'.$arFile['FILE_NAME'];
                        $cur_item = preg_replace("#_PREVIEW_PICTURE_#ims", 'http://'.$arSite['SERVER_NAME'].$file['src'], $cur_item);
                    }


                    // Характеристики товара
                    $db_res_props = \CSaleBasket::GetPropsList( array("SORT" => "ASC", "NAME" => "ASC"), array("BASKET_ID" => $arItems['ID'], "!~NAME" => "%XML_ID")); // $arItems['ID'] - код корзины

                    $props = "";
                    while($ar_res_props = $db_res_props->Fetch()){
                        $props .= $ar_res_props['NAME'].": ".$ar_res_props["VALUE"]."; ";

                    }

                    $cur_item = preg_replace("#_PRODUCT_NAME_#ims", $ar_res_item['NAME'], $cur_item);
                    $cur_item = preg_replace("#_PRODUCT_ID_#ims", $ar_res_item['ID'], $cur_item);
                    if($option_show_props!="Y") $props = "";
                    $cur_item = preg_replace("#_PRODUCT_PROP_#ims", $props, $cur_item);
                    $cur_item = preg_replace("#_PRODUCT_LABEL_QUANTITY_#ims", GetMessage("QUANTITY_LABEL"), $cur_item);
                    $cur_item = preg_replace("#_PRODUCT_QUANTITY_#ims", (int)$arItems['QUANTITY'], $cur_item);
                    $cur_item = preg_replace("#_PRODUCT_PRICE_VAL_#ims", CurrencyFormat($arItems['PRICE'], $arOrder["CURRENCY"]), $cur_item);
                    $cur_item = preg_replace("#_PRODUCT_PRICE_#ims", GetMessage("PRICE"), $cur_item);
                    $cur_item = preg_replace("#_SHARE_ABOUT_#ims", GetMessage("SHARE_ABOUT"), $cur_item);
                    $cur_item = preg_replace("#_VK_ICON_URL_#ims", 'http://'.$arSite['SERVER_NAME'].'/bitrix/images/'.self::$MODULE_ID.'/vk.png', $cur_item);
                    $cur_item = preg_replace("#_FACEBOOK_ICON_URL_#ims", 'http://'.$arSite['SERVER_NAME'].'/bitrix/images/'.self::$MODULE_ID.'/fb.png', $cur_item);
                    $cur_item = preg_replace("#_TWITTER_ICON_#ims", 'http://'.$arSite['SERVER_NAME'].'/bitrix/images/'.self::$MODULE_ID.'/tw.png', $cur_item);
                    $cur_item = preg_replace("#_GPLUS_ICON_#ims", 'http://'.$arSite['SERVER_NAME'].'/bitrix/images/'.self::$MODULE_ID.'/gp.png', $cur_item);
                    $cur_item = preg_replace("#_YOUTUBE_ICON_#ims", 'http://'.$arSite['SERVER_NAME'].'/bitrix/images/'.self::$MODULE_ID.'/youtube.png', $cur_item);



                }

                $all_items .= $cur_item;
            }


            // Замена опций

            $descr_txt = "";
            $descr_txt = \COption::GetOptionString(self::$MODULE_ID, "reflekto_goodsemail_text", "");
            $template = preg_replace("#_ADD_TEXT_#ims", nl2br($descr_txt), $template);

            // logo
            $logo = \COption::GetOptionString(self::$MODULE_ID, "reflekto_goodsemail_logo", "");
            if($logo!="") {
                $template = preg_replace("#_LOGO_IMG_#ims", "<img src='".$logo."'>", $template);
            } else {
                $logo = $arSite['SERVER_NAME'];
                $template = preg_replace("#_LOGO_IMG_#ims", $logo, $template);
            }

            // phone, email
            $phone = \COption::GetOptionString(self::$MODULE_ID, "reflekto_goodsemail_phone", "");
            $template = preg_replace("#_CONTACTS_PHONE_#ims", $phone, $template);
            $contacts_email = \COption::GetOptionString(self::$MODULE_ID, "reflekto_goodsemail_email", "");
            $template = preg_replace("#_CONTACTS_EMAIL_#ims", $contacts_email, $template);
            if($contacts_email=="") {
                $template = preg_replace("#__MESS_EMAIL__#ims", "", $template);
            } else {
                $template = preg_replace("#__MESS_EMAIL__#ims", GetMessage("MESS_EMAIL"), $template);
            }

            // Price BG color
            $price_bg_color = \COption::GetOptionString(self::$MODULE_ID, "reflekto_goodsemail_price_block_color", "");
            $template = preg_replace("#_PRICE_BLOCK_COLOR_#ims", $price_bg_color, $template);

            // Greetings text
            $greetings = \COption::GetOptionString(self::$MODULE_ID, "reflekto_goodsemail_greetings_textarea", "");
            $greetings = preg_replace("#_FULL_NAME_#ims", $USER->GetFullName(), $greetings);
            $template = preg_replace("#_YOUR_ORDERFROM_OPTION_#ims", nl2br($greetings), $template);

            // Different email templates
            $template = preg_replace("#__MESS_KARAPUZ_1__#ims", GetMessage("KARAPUZ_1")." ".$arSite['NAME'], $template);
            $template = preg_replace("#__MESS_KARAPUZ_2__#ims", GetMessage("KARAPUZ_2"), $template);
            $template = preg_replace("#__MESS_KARAPUZ_3__#ims", GetMessage("KARAPUZ_3"), $template);
            $template = preg_replace("#__MESS_KARAPUZ_4__#ims", GetMessage("KARAPUZ_4"), $template);
            $template = preg_replace("#__MESS_KARAPUZ_5__#ims", GetMessage("KARAPUZ_5"), $template);
            $template = preg_replace("#__MESS_KARAPUZ_6__#ims", GetMessage("KARAPUZ_6"), $template);
            $template = preg_replace("#__MESS_KARAPUZ_7__#ims", GetMessage("KARAPUZ_7")."<a href='http://".$arSite['SERVER_NAME']."'>".$arSite['NAME']."</a>.", $template);
            $template = preg_replace("#__MESS_KARAPUZ_8__#ims", GetMessage("KARAPUZ_8")."<a href='http://".$arSite['SERVER_NAME']."'>".$arSite['NAME']."</a>.", $template);
            $template = preg_replace("#__MESS_KARAPUZ_9__#ims", GetMessage("KARAPUZ_9")."<a href='http://".$arSite['SERVER_NAME']."'>".$arSite['NAME']."</a>.", $template);
            $template = preg_replace("#__MESS_KARAPUZ_10__#ims", GetMessage("KARAPUZ_10")."<a href='http://".$arSite['SERVER_NAME']."'>".$arSite['NAME']."</a> ".GetMessage("KARAPUZ_10_a").$customOrderId.".", $template);
            $template = preg_replace("#__MESS_KARAPUZ_11__#ims", GetMessage("KARAPUZ_11"), $template);
            $template = preg_replace("#__MESS_KARAPUZ_12__#ims", GetMessage("KARAPUZ_12")."<a href='http://".$arSite['SERVER_NAME']."'>".$arSite['NAME']."</a>.", $template);





            $template = preg_replace("#_ORDER_ID_VAL_#ims", $customOrderId, $template);
            $template = preg_replace("#_ORDER_ID_#ims", GetMessage("ORDER_ID"), $template);
            $template = preg_replace("#_SHARE_ABOUT_#ims", GetMessage("SHARE_ABOUT"), $template);
            $template = preg_replace("#_ORDER_DATE_VAL_#ims", $arOrder['DATE_INSERT'], $template);
            $template = preg_replace("#_ORDER_DATE_#ims", GetMessage("ORDER_DATE"), $template);
            $delivery_padding = "0px";
            if($delivery_type!="") {
                $delivery_padding = "10px";
                $delivery_mess = GetMessage("DELIVERY_TYPE");
                $delivery_price = CurrencyFormat($arOrder["PRICE_DELIVERY"], $arOrder["CURRENCY"]);
                $delivery_price_mess = GetMessage("DELIVERY_PRICE");
            } else {
                $delivery_type = "";
                $delivery_mess = "";
                $delivery_price = "";
                $delivery_price_mess = "";

                // Вообще удаляем блок доставки из шаблона
                $template = preg_replace("#<!--DELIVERY_BLOCK-->(.*)<!--DELIVERY_BLOCK_END-->#ims", "", $template);
            }
            // Описание доставки
            $template = preg_replace("#_DELIVERY_PADDING_#ims", $delivery_padding, $template);
            $template = preg_replace("#_DELIVERY_TYPE_VAL_#ims", $delivery_type, $template);
            $template = preg_replace("#_DELIVERY_TYPE_#ims", $delivery_mess, $template);
            $template = preg_replace("#_DELIVERY_PRICE_VAL_#ims", $delivery_price, $template);
            $template = preg_replace("#_DELIVERY_PRICE_#ims", $delivery_price_mess, $template);
            $template = preg_replace("#_SUMM_VAL_#ims", CurrencyFormat($arOrder["PRICE"], $arOrder["CURRENCY"]), $template);
            $template = preg_replace("#_SUMM_#ims", GetMessage("SUMM"), $template);
            $template = preg_replace("#_DELIVERY_ADDRESS_#ims", GetMessage("DELIVERY_ADDRESS"), $template);
            $template = preg_replace("#__DELIVERY_INFO_(.*?)__#ims", "", $template);
            $template = preg_replace("#_PAY_SYSTEM_VAL_#ims", $pay_system, $template);
            $template = preg_replace("#_PAY_SYSTEM_#ims", GetMessage("PAY_SYSTEM"), $template);
            $template = preg_replace("#_COPYRIGHT_#ims", "© ".date("Y")." <a href='http://".$arSite['SERVER_NAME']."'>".$arSite['NAME']."</a>", $template);
            $template = preg_replace("#_SITE_LINK_#ims", 'http://'.$arSite['SERVER_NAME'], $template);
            $template = preg_replace("#_PRODUCTS_LIST_#ims",  $all_items, $template);
            $template = preg_replace("#_VK_ICON_#ims", 'http://'.$arSite['SERVER_NAME'].'/bitrix/images/'.self::$MODULE_ID.'/vk.png', $template);
            $template = preg_replace("#_FACEBOOK_ICON_#ims", 'http://'.$arSite['SERVER_NAME'].'/bitrix/images/'.self::$MODULE_ID.'/fb.png', $template);
            $template = preg_replace("#_TWITTER_ICON_#ims", 'http://'.$arSite['SERVER_NAME'].'/bitrix/images/'.self::$MODULE_ID.'/tw.png', $template);
            $template = preg_replace("#_GPLUS_ICON_#ims", 'http://'.$arSite['SERVER_NAME'].'/bitrix/images/'.self::$MODULE_ID.'/gp.png', $template);
            $template = preg_replace("#_YOUTUBE_ICON_#ims", 'http://'.$arSite['SERVER_NAME'].'/bitrix/images/'.self::$MODULE_ID.'/youtube.png', $template);
            if($arOrder["USER_DESCRIPTION"]!="") {
                $arOrder["USER_DESCRIPTION"] = GetMessage("YOUR_COMMENT_TO_ORDER").': '.nl2br($arOrder["USER_DESCRIPTION"]);
            }
            $template = preg_replace("#_ORDER_COMMENT_TEXT_#ims", $arOrder["USER_DESCRIPTION"], $template);



            // Устанавливаем в шаблоне дополнительные опции
            if(file_exists(__DIR__."/../mail_template/optionsraw_".$template_id.".php")) {
                // Найдены опции шаблона
                require(__DIR__."/../mail_template/optionsraw_".$template_id.".php");

                for ($i = 0; $i < count($options_list); $i++) {
                    $Option = $options_list[$i];
                    $val = \COption::GetOptionString(self::$MODULE_ID, $Option[0], $Option[2]);

                    $template = preg_replace("#_".$Option[0]."_#ims", $val, $template);
                }

            }


            // Генерируем отправку почты
            $arEventFields = array(
                "EMAIL_TO"            => self::$user_email,
                "ORDER_ID"            => $customOrderId,
                "REPLACE_BY_MODULE" => $template

            );



            \CEvent::Send("REFLEKTO_ORDER_LIST", $arOrder['LID'], $arEventFields);

        }


    }

    function OnOrderNewSendEmailHandler($ID, &$event_name, &$arFields) {
        self::OnStatusUpdateHandler($arFields['ORDER_ID'], "N");
    }

    /**
     * @param $option_name string Имя опции модуля
     * @param $option_template_path string Визуальный шаблрн
     */
    static function formOrderOptions($option_name, $arOrder, $ID, $option_template_path = false) {
        $answer = "";
        $option_props = \COption::GetOptionString(self::$MODULE_ID, $option_name);
        if($option_props=="") return "";
        $arr_order_props = explode("|",$option_props);


        $db_props = \CSaleOrderProps::GetList(
            array("SORT" => "ASC"),
            array(
                "PERSON_TYPE_ID" => $arOrder["PERSON_TYPE_ID"]
            ),
            false,
            false,
            array()
        );

        // Открываем шаблок списка свойств заказа
        $template_path = $_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".self::$MODULE_ID."/classes/mail_template/".$option_name.".php";
        $item_template = file_get_contents($template_path);

        while ($props = $db_props->Fetch()) {
            $cur_item = '';
            if(is_array($arr_order_props) && in_array($props['ID'],$arr_order_props)) {

                $db_vals = \CSaleOrderPropsValue::GetList(
                    array("SORT" => "ASC"),
                    array(
                        "ORDER_ID" => $ID,
                        "ORDER_PROPS_ID" => $props['ID']
                    )
                );

                if ($arVals = $db_vals->Fetch()) {
                    if($arVals['VALUE']!="") {

                        $cur_item = $item_template;

                        if($props['CODE']=="EMAIL") self::$user_email = $arVals['VALUE'];

                        // Местоположение
                        if($props['CODE']=="LOCATION" && is_int((int)($arVals['VALUE']))) {

                            $arLocs = \CSaleLocation::GetByID($arVals['VALUE']);
                            $arVals['VALUE'] = $arLocs["CITY_NAME"];
                        }
                        $cur_item = preg_replace("#__OPT__#ims", "<b>".$props['NAME']."</b>: ".$arVals['VALUE'], $cur_item);
                        $answer .= $cur_item;
                    }
                }
            }
        }

        return $answer;
    }

}
?>