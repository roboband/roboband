<table cellspacing="0" cellpadding="0" style="margin:0;width: 528px;">
    <tbody>
    <tr>
        <td style="padding: 15px;background-color: #ffffff;margin:0 0 0 0;width:500px;float:left;border-bottom: 1px solid #dadada;">
            <table style="margin:0;padding:0;" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="height: 105px;margin: 0;padding: 0 15px 0 0;width: 120px;vertical-align: top;">
                        <a href="_DETAIL_URL_" style="text-decoration: none;" target="_blank">
                            <img src="_PREVIEW_PICTURE_" alt="" title="">
                        </a>
                    </td>
                    <td>
                        <table style="width: 375px;word-wrap: break-word;white-space: normal;margin: 0;padding: 0;vertical-align: top;">
                            <tbody>
                            <tr>
                                <td>
                                    <ul style="list-style-type:none;margin: 0;padding: 0;">
                                        <li style="list-style-type:none;display: block;margin-bottom: 5px;">
                                            <span style="font-size: 14px;">_PRODUCT_NAME_</span>
                                        </li>
                                        <li style="list-style-type:none;display: block;margin-bottom: 5px;">
									<span style="font-size: 12px;">
									_PRODUCT_PROP_</span>
                                        </li>
                                    </ul>
                                </td>
                                <td align="right" style="margin:0;padding:0;float: right;vertical-align: top; width: 150px;">
                                    <div style="list-style-type:none;margin: 0;padding: 0;">
                                        <p style="list-style-type:none;display: block;margin:0;padding:0;margin-bottom: 5px;text-align: right;">
								            <span style="font-size: 16px;font-weight: bold;cursor: pointer;">
									        _PRODUCT_PRICE_ _PRODUCT_PRICE_VAL_</span>
                                        </p>

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-top:1px dotted #929294;"></td>
                            </tr>
                            <tr>
                                <td width="200">
                                    <div style="margin: 0;padding: 0;">
                                        <p style="font-size: 13px;font-family:arial;display: block;margin:0 0 5px 0px;padding:0;">
                                            <span>_PRODUCT_LABEL_QUANTITY_:</span>
									   <span style="font-weight: bold;"> _PRODUCT_QUANTITY_</span>
                                        </p>


                                    </div>
                                </td>
                                <td align="right" style="margin:0;padding:0;float: right;vertical-align: top;">
                                    <table style="margin: 0;padding: 0;" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr style="padding:15px 0 0;margin:0;">
                                            <td style="font-family:arial;font-size:13px;font-weight:bold;margin:0 5px 0 0;color: #383939;">
                                                _SHARE_ABOUT_
                                            </td>
                                            <td style="text-decoration: none;margin:0 0 0 5px;pading:0; padding-left: 10px;">
                                                <a style="text-decoration: none;" href="http://vkontakte.ru/share.php?url=_DETAIL_URL_" title="Share this Product on Vk" target="_blank">
                                                    <img style="border:none;" src="_VK_ICON_URL_" alt="Vk">
                                                </a>
                                            </td>

                                            <td style="text-decoration: none;margin:0 0 0 5px;pading:0; padding-left: 10px;">
                                                <a style="text-decoration: none;" href="https://www.facebook.com/sharer.php?u=_DETAIL_URL_" title="Share this Product on Facebook" target="_blank">
                                                    <img style="border:none;" src="_FACEBOOK_ICON_URL_" alt="facebook">
                                                </a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>