<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <style>
        body
        {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 14px;
            color: #000;
        }
    </style>
</head>
<body>
<table cellpadding="0" cellspacing="0" width="850" style="background-color: #d1d1d1; border-radius: 2px; border:1px solid #d1d1d1; margin: 0 auto;" border="1" bordercolor="#d1d1d1">
    <tr>
        <td height="83" width="850" bgcolor="#eaf3f5" style="border: none; padding-top: 23px; padding-right: 17px; padding-bottom: 24px; padding-left: 17px;">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td bgcolor="#ffffff" height="75" style="font-weight: bold; text-align: center; font-size: 26px; color: #0b3961;">__MESS_KARAPUZ_1__</td>
                </tr>
                <tr>
                    <td bgcolor="#bad3df" height="11"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 16px; padding-left: 44px;">
            <p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">_YOUR_ORDERFROM_OPTION_</p>



            <p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">__MESS_KARAPUZ_2__ _ORDER_ID_VAL_ __MESS_KARAPUZ_3__ _ORDER_DATE_VAL_ __MESS_KARAPUZ_4__.<br />
                <br />
                __MESS_KARAPUZ_5__: _SUMM_VAL_<br />
                <br />
                __MESS_KARAPUZ_6__:<br />
            </p>


                <table width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr style="border: none;">
            <td style="border: none;vertical-align: top;border: 1px solid #dadada;padding: 0px;">
                _PRODUCTS_LIST_

                <table idth="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="padding:10px;">
                            __ORDER_PROPS_CENTER__

                            <p style="line-height: 20px;line-height: 20px;margin: 0px;word-wrap: break-word;padding:3px 10px 0px 10px;">
                                _ORDER_COMMENT_TEXT_</p>

                        </td>
                    </tr>
                </table>
            </td>
            <td width="15" style="border: none;vertical-align: top;">&nbsp;</td>
            <td style="border: none;vertical-align: top;float: right;">


            __RIGHT_BLOCK_INFO__

            <table cellspacing="0" cellpadding="0"
                   style="border: 1px solid #dadada;width: 100%;background-color: _PRICE_BLOCK_COLOR_; border-bottom:none;">
                <tbody>
                <tr style="margin: 0;padding: 0;">
                    <td style="margin: 0;padding: 0;">
                        <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                            <tbody>
                            <tr>
                                <td width="3">&nbsp;</td>
                                <td width="100%">
                                    <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                                        <tbody>
                                        <tr>
                                            <td width="5%">&nbsp;</td>
                                            <td width="90%">
                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left"
                                                            style="color: #FFFFFF;font-size: 14px;font-weight: bold;padding: 5px 0px;">
                                                            _PAY_SYSTEM_
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="5%">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="3">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" style="border: 1px solid #dadada;width: 100%;border-top:none;">
                <tbody>
                <tr style="margin: 0;padding: 0;">
                    <td style="margin: 0;padding: 0;">
                        <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                            <tbody>
                            <tr>
                                <td width="3">&nbsp;</td>
                                <td width="100%">
                                    <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                                        <tbody>
                                        <tr>
                                            <td width="5%">&nbsp;</td>
                                            <td width="90%">
                                                <table width="178" cellspacing="0" cellpadding="0">

                                                    <tbody>
                                                    <tr>
                                                        <td height="5"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="margin: 0;padding: 0;line-height: 20px;font-size: 14px;">
                                                            _PAY_SYSTEM_VAL_
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="5"></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="5%">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="3">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tbody>
                <tr style="margin: 0;padding: 0;">
                    <td style="margin: 0;padding: 0;" height="10">

                    </td>
                </tr>
                </tbody>
            </table>

            <!-- БЛОК ОПИСАНИЯ ИТОГОВОЙ СТОИМОСТИ -->
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tbody>
                <tr>
                    <td style="width: 100%;margin:0;padding:3px 0px;background-color: _PRICE_BLOCK_COLOR_;">
                        <!--DELIVERY_BLOCK-->
                        <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                            <tbody>
                            <tr>
                                <td width="3">&nbsp;</td>
                                <td width="100%" bgcolor="#ffffff">
                                    <table width="100%" cellspacing="0" cellpadding="0" style="padding:_DELIVERY_PADDING_;background:#ffffff"
                                           bgcolor="#ffffff">
                                        <tbody>
                                        <tr>
                                            <td width="5%">&nbsp;</td>
                                            <td width="90%">
                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left">
                                                            <span style="font-size: 14px;float: left;">_DELIVERY_TYPE_:</span>
                                                        </td>
                                                        <td align="right">
                                                            <span style="font-size: 14px;float: right;">_DELIVERY_TYPE_VAL_</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <span style="font-size: 14px;float: left;">_DELIVERY_PRICE_:</span>
                                                        </td>
                                                        <td align="right">
                                                            <span style="font-size: 14px;float: right;">_DELIVERY_PRICE_VAL_</span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="5%">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="3">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                        <!--DELIVERY_BLOCK_END-->
                        <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                            <tbody>
                            <tr>
                                <td width="3">&nbsp;</td>
                                <td width="100%">
                                    <table width="100%" cellspacing="0" cellpadding="0" style="padding:10px 0;">
                                        <tbody>
                                        <tr>
                                            <td width="5%">&nbsp;</td>
                                            <td width="90%">
                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left" style="color:#ffffff;font-size:18px;">
                                                            <strong style="color:#ffffff;font-size:18px;">_SUMM_:</strong>
                                                        </td>
                                                        <td align="right">
                                                <span
                                                    style="float: right;font-size: 16px;line-height: 22px;color:#ffffff"><strong>_SUMM_VAL_</strong></span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="5%">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="3">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            </td>
            </tr>

            </tbody>
            </table>

            <p style="line-height: 20px;line-height: 20px;margin: 0 0 20px 0;word-wrap: break-word;padding:0;">_ADD_TEXT_</p>

            <p>
                __MESS_KARAPUZ_7__
                <br />
                <br />
                __MESS_KARAPUZ_8__
                <br />
                <br />
                __MESS_KARAPUZ_9__
                <br />
                <br />
                __MESS_KARAPUZ_10__
                <br />
                <br />
                __MESS_KARAPUZ_11__<br />
            </p>
        </td>
    </tr>
    <tr>
        <td height="40px" width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 30px; padding-left: 44px;">
            <p style="border-top: 1px solid #d1d1d1; margin-bottom: 5px; margin-top: 0; padding-top: 20px; line-height:21px;">__MESS_KARAPUZ_12__<br />
                _CONTACTS_PHONE_<br />
                __MESS_EMAIL__ <a href="mailto:_CONTACTS_EMAIL_">_CONTACTS_EMAIL_</a>
            </p>
        </td>
    </tr>
</table>
</body>
</html>