<table cellspacing="0" cellpadding="0"
       style="border: 1px solid #dadada;width: 100%;background-color: _PRICE_BLOCK_COLOR_;border-bottom:none;">
    <tbody>
    <tr style="margin: 0;padding: 0;">
        <td style="margin: 0;padding: 0;">
            <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                <tbody>
                <tr>
                    <td width="3">&nbsp;</td>
                    <td width="100%">
                        <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                            <tbody>
                            <tr>
                                <td width="5%">&nbsp;</td>
                                <td width="90%">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td align="left"
                                                style="color: #FFFFFF;font-size: 14px;font-weight: bold;padding: 5px 0px;">
                                                _DELIVERY_ADDRESS_
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="5%">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="3">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<table cellspacing="0" cellpadding="0" style="border: 1px solid #dadada;width: 100%;border-top:none;">
    <tbody>
    <tr style="margin: 0;padding: 0;">
        <td style="margin: 0;padding: 0;">
            <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                <tbody>
                <tr>
                    <td width="3">&nbsp;</td>
                    <td width="100%">
                        <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                            <tbody>
                            <tr>
                                <td width="5%">&nbsp;</td>
                                <td width="90%">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td height="5"></td>
                                        </tr>
                                        <tr>
                                            <td style="margin: 0;padding: 0;line-height: 20px;font-size: 14px;">
                                                __ORDER_PROPS_COL_LEFT__
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="5"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="5%">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="3">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<table cellspacing="0" cellpadding="0" style="width: 215px;">
    <tbody>
    <tr style="margin: 0;padding: 0;">
        <td style="margin: 0;padding: 0;" height="10">

        </td>
    </tr>
    </tbody>
</table>