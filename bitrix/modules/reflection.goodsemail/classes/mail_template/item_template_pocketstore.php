<table cellspacing="0" cellpadding="0" style="margin:0;width: 528px;">
    <tbody>
    <tr>
        <td style="padding: 15px;background-color: #ffffff;margin:0 0 0 0;width:500px;float:left;border-bottom: 1px solid #dadada;">
            <table style="margin:0;padding:0;" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="height: 105px;margin: 0;padding: 0 15px 0 0;width: 120px;vertical-align: top;">
                        <a href="_DETAIL_URL_" style="text-decoration: none;" target="_blank">
                            <img src="_PREVIEW_PICTURE_" alt="" title="">
                        </a>
                    </td>
                    <td>
                        <table style="width: 375px;word-wrap: break-word;white-space: normal;margin: 0;padding: 0;vertical-align: top;">
                            <tbody>
                            <tr>
                                <td>
                                    <ul style="list-style-type:none;margin: 0;padding: 0;">
                                        <li style="list-style-type:none;display: block;margin-bottom: 5px;">
                                            <span style="font-size: 14px;">
                                                <a href="_DETAIL_URL_" style="text-decoration: underline;" target="_blank">_PRODUCT_NAME_</a></span>
                                        </li>
                                    </ul>
                                </td>
                                <td align="right" style="margin:0;padding:0;float: right;vertical-align: top; width: 150px;">
                                    <div style="list-style-type:none;margin: 0;padding: 0;">
                                        <p style="list-style-type:none;display: block;margin:0;padding:0;margin-bottom: 5px;text-align: right;">
								            <span style="font-size: 16px;font-weight: bold;cursor: pointer;">
									        _PRODUCT_PRICE_ _PRODUCT_PRICE_VAL_</span>
                                        </p>

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-top:1px dotted #929294;"></td>
                            </tr>
                            <tr>
                                <td width="200" colspan="2">
                                    <div style="margin: 0;padding: 0;">
                                        <p style="font-size: 13px;font-family:arial;display: block;margin:0 0 5px 0px;padding:0;">
                                            <span>_PRODUCT_LABEL_QUANTITY_:</span>
									   <span style="font-weight: bold;"> _PRODUCT_QUANTITY_</span>
                                        </p>

                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>