<?

include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"] . "/bitrix/modules/reflection.goodsemail/classes/mail_template/lang/", "/options_template_2.php"));

require('optionsraw_template_2.php');

for ($i = 0; $i < count($options_list); $i++) {
    $Option = $options_list[$i];
    $val = COption::GetOptionString("reflection.goodsemail", $Option[0], $Option[2]);
    $type = $Option[3];

 if ($type[0] == "text") {
?>
     <p><i><?= $Option[1];?></i></p>
     <input type="text" class="typeinput reflekto_w_300" size="<? echo $type[1] ?>"
            value="<? echo htmlspecialchars($val) ?>"
            name="<? echo htmlspecialchars($Option[0]) ?>" />
<?
 } else if ($type[0] == "textarea") {
     ?>
     <p><i><?= $Option[1];?></i></p>
     <textarea class="typeinput reflekto_w_300" size="<? echo $type[1] ?>"
            name="<? echo htmlspecialchars($Option[0]) ?>"><? echo htmlspecialchars($val) ?></textarea>
 <?
 } else if ($type[0] == "checkbox") {

        ?>
        <p>
            <label><i><?= $Option[1];?></i></label>
            <input value="Y" type="checkbox" name="<? echo htmlspecialchars($Option[0]) ?>" <? if($val=="Y") { echo "checked"; } ?> />
        </p>
        <?
 } else  if ($type[0] == "color") {
     ?>
     <p><i><?= $Option[1];?></i></p>
     <input type="color" class="typeinput reflekto_w_300" size="<? echo $type[1] ?>"
            value="<? echo htmlspecialchars($val) ?>"
            name="<? echo htmlspecialchars($Option[0]) ?>"
            id="<? echo htmlspecialchars($Option[0]) ?>"/>
     <div class="hexcolor"></div>

     <script>
         var color    = $('#<?= $Option[0];?>');
         hexcolor = color.next('.hexcolor');

         hexcolor.html(color.val());
         color.on('change', function() {
             hexcolor.html(this.value);
         });
     </script>
 <?
 }
}


?>