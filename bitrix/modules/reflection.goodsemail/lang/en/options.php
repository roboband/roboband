<?
$MESS['REFLEKTO_SALETYPE_EVENT'] = "Type of order status";
$MESS['REFLEKTO_SALETYPE_EVENT_DESCR'] = "Customer will get an email notification with the report about his order.";
$MESS['REFLEKTO_TEXT'] = "Additional text";
$MESS['REFLEKTO_LOGO'] = "Logotype";
$MESS['REFLEKTO_PHONE'] = "Company phone number";
$MESS['REFLEKTO_EMAIL'] = "Company email";


$MESS['ECHO_MODULE_SAVE'] = "Save";

$MESS['TAB_MAIN'] = "Main settings";
$MESS['TAB_ACCESS'] = "Access rights";
$MESS['REFLEKTO_WRONG_FILE_EXT'] = "Only formats of JPG and PNG are allowed"




?>