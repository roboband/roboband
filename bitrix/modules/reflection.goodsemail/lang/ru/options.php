<?
$MESS['REFLEKTO_SALETYPE_EVENT'] = "Тип статуса заказа";
$MESS['REFLEKTO_ORDERPROP_COL_LEFT'] = "Свойства заказа, выводимые в левой колонке";
$MESS['REFLEKTO_ORDERPROP_CENTER'] = "Свойства заказа, выводимые в центре";
$MESS['REFLEKTO_SALETYPE_EVENT_DESCR'] = "При установке у заказа данного статуса, клиенту будет отправляться почтовое уведомление с отчетом по его заказу.";
$MESS['REFLEKTO_TEXT'] = "Дополнительный текст";
$MESS['REFLEKTO_LOGO'] = "Изображение логотипа";
$MESS['REFLEKTO_PHONE'] = "Номер телефона фирмы";
$MESS['REFLEKTO_EMAIL'] = "Контактный email фирмы";


$MESS['ECHO_MODULE_SAVE'] = "Применить";

$MESS['TAB_MAIN'] = "Главные настройки";
$MESS['TAB_ACCESS'] = "Права доступа к модулю";
$MESS['REFLEKTO_WRONG_FILE_EXT'] = "Для загрузки допускаются только файлы формата jpg,png";

// Наименование шаблонов

$MESS['REFLEKTO_TEMPLATE'] = "Шаблон письма";
$MESS['REFLEKTO_TEMPLATE_NAME_template_1'] = "Базовый шаблон";
$MESS['REFLEKTO_TEMPLATE_NAME_template_2'] = "Базовый шаблон № 2";
$MESS['REFLEKTO_TEMPLATE_NAME_template_pocketstore'] = "Базовый шаблон с доп. настройками";
$MESS['REFLEKTO_TEMPLATE_NAME_template_karapuz'] = "Гибрид со стандартным письмом Битрикс";

$MESS['REFLEKTO_ADDITIONAL_TEMPLATE_OPTIONS'] = "Опции шаблона";


?>