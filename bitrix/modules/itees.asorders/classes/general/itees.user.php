<?
IncludeModuleLangFile(__FILE__);

class IteesASOUser
{
	public $ADV_SERVICE = false;		//	��������� ������, �������� direct.yandex.ru (utm_source ��� openstat_service)
	public $ADV_CAMPAIGN = false;		//	��������� ��������, �������� campaign1 (utm_campaign ��� openstat_campaign)
	public $ADV_AD = false;				//	��������� ���������� (utm_content ��� openstat_ad)
	public $ADV_SOURCE = false;			//	�������� ��������, �������� yandex.ru:premium (utm_medium ��� openstat_source)
	public $ADV_KW = false;				//	�������� ����� (utm_term)
	
	public $SEARCH_ENGINE = false;		//	��������� �������
	public $SEARCH_QUERY = false;		//	��������� ������
	public $SEARCH_REGION = false;		//	������ ������
	public $SEARCH_FLAG = false;		//	���� �������������� ������� � �������
	
	public $REFERER = false;			//	�������� ���������� $_SERVER["HTTP_REFERER"]
	
	function __construct($arParams = array())
	{
		if (count($arParams))
		{
			$this->SetParams($arParams);
		}
	}
	
	public function SetParams($arParams)
	{
		if (count($arParams))
		{
			$this->ADV_SERVICE = $arParams["ADV_SERVICE"];
			$this->ADV_CAMPAIGN = $arParams["ADV_CAMPAIGN"];
			$this->ADV_AD = $arParams["ADV_AD"];
			$this->ADV_SOURCE = $arParams["ADV_SOURCE"];
			$this->ADV_KW = $arParams["ADV_KW"];
			$this->ADV_KEYWORDS = $arParams["ADV_KEYWORDS"];
			$this->ADV_POSITION = $arParams["ADV_POSITION"];
			
			$this->SEARCH_ENGINE = $arParams["SEARCH_ENGINE"];
			$this->SEARCH_QUERY = $arParams["SEARCH_QUERY"];
			$this->SEARCH_REGION = $arParams["SEARCH_REGION"];
			$this->SEARCH_FLAG = $arParams["SEARCH_FLAG"];
			$this->REFERER = $arParams["REFERER"];
		}
	}
	
	public function GetParams()
	{
		return get_object_vars($this);
	}
}
?>