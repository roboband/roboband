<?
$MESS ['ITEES_ADV_SERVICE'] = "Рекламный сервис";
$MESS ['ITEES_ADV_CAMPAIGN'] = "Рекламная кампания";
$MESS ['ITEES_ADV_AD'] = "Рекламное объявление";
$MESS ['ITEES_ADV_SOURCE'] = "Источник перехода";
$MESS ['ITEES_ADV_KW'] = "Ключевые слова";
$MESS ['ITEES_ADV_KEYWORDS'] = "Ключевое слово";
$MESS ['ITEES_ADV_POSITION'] = "Номер позиции объявления в блоке";

$MESS ['ITEES_SEARCH_ENGINE'] = "Поисковая система";
$MESS ['ITEES_SEARCH_QUERY'] = "Поисковый запрос";
$MESS ['ITEES_SEARCH_REGION'] = "Регион";
$MESS ['ITEES_SEARCH_FLAG'] = "Флаг ограничения по региону";
$MESS ['ITEES_REFERER'] = "REFERER";
?>