<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile(__FILE__);
CUtil::InitJSCore(array('ajax'));
$RIGHT = $APPLICATION->GetGroupRight("main");
if ($RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("PTB_NOT_ACCESS"));


function CheckCodeElement($IBLOCK_ID, $CODE, $ID) {
	$count = 0;
	$res = CIBlockElement::GetList(
		Array("ID"=>"ASC"), 
		Array("IBLOCK_ID"=>$IBLOCK_ID, "CODE"=>$CODE, "!ID"=>$ID), 
		false,
		false,
		Array("ID")
	);
	if($item = $res->GetNext()) {
		return 1;
	};
	return 0;
}

function CheckEnd($IBLOCK_ID, $LID) {
	$all_count = CIBlockElement::GetList(
		array("ID" => "ASC"), 
		array("IBLOCK_ID" => $IBLOCK_ID)
	);
	$all = $all_count->SelectedRowsCount();
	
	$last_count = CIBlockElement::GetList(
		array("ID" => "ASC"), 
		array("IBLOCK_ID" => $IBLOCK_ID, "<=ID" => $LID)
	);
	$last = $last_count->SelectedRowsCount();
	if ($last==$all)
		return true;
	else 
		return false;
}

function CheckIblockProperty($IBLOCK_ID, $CODE, $MESS) {
		$properties = CIBlockProperty::GetList(Array(), Array("ACTIVE"=>"Y", "CODE" => $CODE, "IBLOCK_ID"=>$IBLOCK_ID));
		while ($ob = $properties->GetNext())
		{	
			$new_prop[] = $ob;
		}
		if(!$new_prop) {
			$arFields = Array(
				"NAME" => GetMessage($MESS),
				"ACTIVE" => "Y",
				"SORT" => "1000",
				"CODE" => $CODE,
				"PROPERTY_TYPE" => "S",
				"IBLOCK_ID" => $IBLOCK_ID,
				);
      
			$ibp = new CIBlockProperty;
			$PropID = $ibp->Add($arFields);
		}
		return false;
}
function error_message($mess) {
		echo '<script>
			alert("'.$mess.'");
		</script>';
		return false;
}
function get_yandex_model($query,$token,$client,$login) {


		$headr = array();
		$headr[] = 'api.partner.market.yandex.ru';
		$headr[] = 'Accept: */*';
		$headr[] = 'Authorization: OAuth oauth_token="'.$token.'", oauth_client_id="'.$client.'", oauth_login="'.$login.'"';
		
		
		
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => false,    // don't follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_HTTPHEADER => $headr,
		);
		$url = "https://api.partner.market.yandex.ru/v2/models.json?regionId=1&pageSize=2&query=".$query;
		$ch      = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );
		
		
		
		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		
		$header['content'] = json_decode($header['content'], true);
		
		return $header;
			  
}
function get_yandex_reviews ($id) {


		$headr = array();
		$headr[] = 'api.content.market.yandex.ru';
		$headr[] = 'Accept: */*';		
		
		
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => false,    // don't follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_HTTPHEADER => $headr,
		);
		$url = "https://api.content.market.yandex.ru/v1/model/".$id."/opinion.json?geo_id=0&count=30";
		$ch      = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );
		
		
		
		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		
		$header['content'] = json_decode($header['content'], true);
		
		$res = $header['content']['modelOpinions'];
		
		return $res;
			  
}

// SAVE OPTIONS START
if($_POST["save_form"]) {
	COption::SetOptionString("dkst.ymreviews", "PTB_SEL_TIBLOCK", $_POST["ptb_sel_tiblock"]);
	COption::SetOptionString("dkst.ymreviews", "PTB_SEL_IBLOCK", $_POST["ptb_sel_iblock"]);
	
	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_SHAG", $_POST["ptb_gen_element_shag"]);
	
	if(!$_POST["ptb_gen_yid"]) 
		$_POST["ptb_gen_yid"] = "N";		
	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_YID", $_POST["ptb_gen_yid"]);

	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_YID_CODE", $_POST["ptb_gen_yid_code"]);
	
	if(!$_POST["ptb_gen_rating"]) 
		$_POST["ptb_gen_rating"] = "N";	
	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_RATING", $_POST["ptb_gen_rating"]);	
	
	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_RATING_CODE", $_POST["ptb_gen_rating_code"]);
	
	if(!$_POST["ptb_gen_countrev"]) 
		$_POST["ptb_gen_countrev"] = "N";
	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_COUNTREV", $_POST["ptb_gen_countrev"]);

	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_COUNTREV_CODE", $_POST["ptb_gen_countrev_code"]);
	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_REG", $_POST["ptb_gen_reg"]);
	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_SEARCH_CODE", $_POST["ptb_gen_search_code"]);
	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_OAUTH_LOGIN", $_POST["ptb_gen_oauth_login"]);
	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_OAUTH_CLIENT", $_POST["ptb_gen_oauth_client"]);
	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_OAUTH_TOKEN", $_POST["ptb_gen_oauth_token"]);
	
	if(!$_POST["ptb_gen_isset"]) 
		$_POST["ptb_gen_isset"] = "N";
	COption::SetOptionString("dkst.ymreviews", "PTB_GEN_ISSET", $_POST["ptb_gen_isset"]);
}

// SAVE OPTIONS END

if($_SERVER["REQUEST_METHOD"] == "POST" && $_REQUEST["ptb_gener_start"] == "Y") {
	CModule::IncludeModule("iblock");
	CUtil::JSPostUnescape();
	
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");
	
	if(array_key_exists("params", $_REQUEST) && is_array($_REQUEST["params"])){
		foreach ($_REQUEST["params"] as $key=>$value)
			if(!is_array($value))
				$params[$key] = htmlspecialchars($value);
	} 
	else {	
		$params = Array(
		"ptb_gen_element_shag" => $_REQUEST["ptb_gen_element_shag"],
		"ptb_gen_yid" => $_REQUEST["ptb_gen_yid"],
		"ptb_gen_yid_code" => $_REQUEST["ptb_gen_yid_code"],
		"ptb_gen_rating" => $_REQUEST["ptb_gen_rating"],
		"ptb_gen_rating_code" => $_REQUEST["ptb_gen_rating_code"],
		"ptb_gen_countrev" => $_REQUEST["ptb_gen_countrev"],
		"ptb_gen_countrev_code" => $_REQUEST["ptb_gen_countrev_code"],
		"ptb_gen_reg" => $_REQUEST["ptb_gen_reg"],
		"ptb_gen_search_code" => $_REQUEST["ptb_gen_search_code"],
		"ptb_gen_isset" => $_REQUEST["ptb_gen_isset"],
		"ptb_gen_oauth_login" => $_REQUEST["ptb_gen_oauth_login"],
		"ptb_gen_oauth_client" => $_REQUEST["ptb_gen_oauth_client"],
		"ptb_gen_oauth_token" => $_REQUEST["ptb_gen_oauth_token"],
		"ptb_gener_start" => $_REQUEST["ptb_gener_start"],
		"ptb_sel_tiblock" => $_REQUEST["ptb_sel_tiblock"],
		"ptb_sel_iblock" => $_REQUEST["ptb_sel_iblock"],
		"ptb_lid" => $_REQUEST["ptb_lid"],

		"ptb_gen_element_count" => 0,
		"ptb_gen_element_fail" => 0,
		"ptb_errors" => Array(),
		"ptb_reply_code" => Array()
	);
	}
	// ���������, ���� �� �������� � ������
	
	CheckIblockProperty($params["ptb_sel_iblock"], $params["ptb_gen_yid_code"], "PTB_GEN_NEW_NAME");
	
	if($params["ptb_gen_rating"] == "Y") {
		CheckIblockProperty($params["ptb_sel_iblock"], $params["ptb_gen_rating_code"], "PTB_GEN_NEW_RATING");
	}	
	if($params["ptb_gen_countrev"] == "Y") {
		CheckIblockProperty($params["ptb_sel_iblock"], $params["ptb_gen_countrev_code"], "PTB_GEN_NEW_COUNT");
	}
	
	if($params['ptb_gen_reg'] == "PROP") {
		$full_prop_name = "PROPERTY_".$params["ptb_gen_search_code"]."_VALUE";
		$arSelect = Array("ID","NAME","PROPERTY_".$params["ptb_gen_yid_code"],"PROPERTY_".$params["ptb_gen_search_code"]);
	}
	else {
		$full_prop_name = "NAME";
		$arSelect = Array("ID","NAME","PROPERTY_".$params["ptb_gen_yid_code"]);
	}


	$arFilter = Array("IBLOCK_ID"=>$params["ptb_sel_iblock"], ">ID" => $params["ptb_lid"]);

	
	$res = CIBlockElement::GetList(
		Array("ID"=>"ASC"), 
		$arFilter, 
		false,
		array("nTopCount" => $params["ptb_gen_element_shag"]),
		$arSelect
	);
	
	while($item = $res->GetNext()) {
		if(!array_key_exists($full_prop_name, $item) && $params['ptb_gen_reg'] == "PROP") {
			error_message(GetMessage("PTB_ERROR_SEARCH_CODE"));
			$params["ptb_lid"] = 1000000000000;
			break;
			
		}
		if($params["ptb_gen_isset"] == "Y" && $item["PROPERTY_".$params["ptb_gen_yid_code"]."_VALUE"] != null) {
			$params["ptb_lid"] = $item["ID"];
			$params["ptb_gen_element_fail"]++;
			continue;
		}
		if(!isset($item["PROPERTY_".$params["ptb_gen_search_code"]."_VALUE"]) && $params['ptb_gen_reg'] == "PROP") {
			$params["ptb_lid"] = $item["ID"];
			$params["ptb_gen_element_fail"]++;
			continue;			
		}

		if($params["ptb_gen_yid"] == "Y") {
			$arResModel = get_yandex_model(urlencode($item[$full_prop_name]),$params["ptb_gen_oauth_token"],$params["ptb_gen_oauth_client"],$params["ptb_gen_oauth_login"]);
			if($arResModel["content"]["error"]) {
				error_message("Error: ".$arResModel["content"]["error"]["message"]);
				$params["ptb_lid"] = 1000000000000;
				break;
			}		
			if(count($arResModel["content"]["models"]) == 1) {
				CIBlockElement::SetPropertyValuesEx($item["ID"], $params["ptb_sel_iblock"], array($params["ptb_gen_yid_code"] => $arResModel["content"]["models"]["0"]["id"]));

				if($params["ptb_gen_countrev"] == "Y" || $params["ptb_gen_rating"] == "Y") {
					$arReviews = get_yandex_reviews($arResModel["content"]["models"]["0"]["id"]);
				}
				if($params["ptb_gen_countrev"] == "Y") {
					CIBlockElement::SetPropertyValuesEx($item["ID"], $params["ptb_sel_iblock"], array($params["ptb_gen_countrev_code"] => $arReviews["total"]));
				}
				
				if($params["ptb_gen_rating"] == "Y") {
					$rating = 0;
					foreach($arReviews["opinion"] as $opinion) {
						$rating += $opinion["grade"] + 3;
					}
					
					$rating = round($rating/count($arReviews["opinion"]), 2);
					CIBlockElement::SetPropertyValuesEx($item["ID"], $params["ptb_sel_iblock"], array($params["ptb_gen_rating_code"] => $rating));
				}	
				$params["ptb_gen_element_count"]++;
			}
			else {
				$params["ptb_gen_element_fail"]++;
			}
		}
		else {
			if(isset($item["PROPERTY_".$params["ptb_gen_yid_code"]."_VALUE"])) {
				if($params["ptb_gen_countrev"] == "Y" || $params["ptb_gen_rating"] == "Y") {
					$arReviews = get_yandex_reviews($item["PROPERTY_".$params["ptb_gen_yid_code"]."_VALUE"]);
					$params["ptb_gen_element_count"]++;
				}
				else {
					$params["ptb_gen_element_fail"]++;
				}
				if($params["ptb_gen_countrev"] == "Y") {
					CIBlockElement::SetPropertyValuesEx($item["ID"], $params["ptb_sel_iblock"], array($params["ptb_gen_countrev_code"] => $arReviews["total"]));
				}
				if($params["ptb_gen_rating"] == "Y") {
					$rating = 0;
					foreach($arReviews["opinion"] as $opinion) {
						$rating += $opinion["grade"] + 3;
					}				
					$rating = round($rating/count($arReviews["opinion"]), 2);
					CIBlockElement::SetPropertyValuesEx($item["ID"], $params["ptb_sel_iblock"], array($params["ptb_gen_rating_code"] => $rating));
				}	
			}
			else {
				$params["ptb_gen_element_fail"]++;
			}
			
		}
		
		$params["ptb_lid"] = $item["ID"];
	};
	
	$jsParams = CUtil::PhpToJSObject(array("params"=>$params));
	if(CheckEnd($params["ptb_sel_iblock"], $params["ptb_lid"]))  {		
		if(count($params["ptb_errors"])>0)
			CAdminMessage::ShowMessage(array(
				"MESSAGE"=>GetMessage("PTB_ERROR_TEXT"),
				"DETAILS"=>implode("",$params["ptb_errors"]),
				"HTML"=>true,
				"TYPE"=>"ERROR",
			));
		CAdminMessage::ShowMessage(array(
			"MESSAGE"=>GetMessage("PTB_PROGRESS_STATUS_OK"),
			"DETAILS"=>"<b>".GetMessage("PTB_PROPGRESS_ELEMENT")."</b>".$params["ptb_gen_element_count"]."<br/><b>".GetMessage("PTB_PROPGRESS_ELEMENT_FAIL")."</b>".$params["ptb_gen_element_fail"],
			"HTML"=>true,
			"TYPE"=>"OK",
		));
		echo '<script>
			generation_finish();
		</script>';
	} else {				
		CAdminMessage::ShowMessage(array(
			"MESSAGE"=>GetMessage("PTB_PROGRESS_STATUS_ON"),
			"DETAILS"=>"<b>".GetMessage("PTB_PROPGRESS_ELEMENT")."</b>".$params["ptb_gen_element_count"]."<br/><b>".GetMessage("PTB_PROPGRESS_ELEMENT_FAIL")."</b>".$params["ptb_gen_element_fail"],
			"HTML"=>true,
			"TYPE"=>"OK",
		));
		echo '<script>
			generation('.$jsParams.');
		</script>';
	};

	require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin_js.php");
	exit;
}

$APPLICATION->SetTitle(GetMessage("PTB_YMREVIEWS_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
$aTabs = array(
	array(
		"DIV" => "ptb_gen",
		"TAB" => GetMessage("PTB_TAB"),
		"ICON" => "main_user_edit",
		"TITLE" => GetMessage("PTB_TAB")
	)
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if(!CModule::IncludeModule("iblock"))
	echo GetMessage("PTB_ERROR_IBLOCK");
	
$dbIBlockType = CIBlockType::GetList();
$arIBTypes = array();
$arIB = array();
while ($arIBType = $dbIBlockType->Fetch())
{
	if ($arIBTypeData = CIBlockType::GetByIDLang($arIBType["ID"], LANG))
	{
		$arIB[$arIBType['ID']] = array();
		$arIBTypes[$arIBType['ID']] = '['.$arIBType['ID'].'] '.$arIBTypeData['NAME'];
	}
}

$dbIBlock = CIBlock::GetList(array('SORT' => 'ASC'), array('ACTIVE' => 'Y'));
while ($arIBlock = $dbIBlock->Fetch())
{
	$arIB[$arIBlock['IBLOCK_TYPE_ID']][$arIBlock['ID']] = ($arIBlock['CODE'] ? '['.$arIBlock['CODE'].'] ' : '').$arIBlock['NAME'];
};

?>
<style>
	.dkst_descr {
		background: none repeat scroll 0 0 #f5f9f9;
		border: 1px solid #c4ced2;
		border-radius: 5px;
		color: #778;
		display: inline-block;
		font-size: 11px;
		line-height: 18px;
		margin-bottom: 15px;
		padding: 15px 20px;
	}
	.dkst_descr span {
		border-left: 1px solid #c4ced2;
		display: inline-block;
		margin-left: 20px;
		padding-left: 20px;
	}
</style>
<div class="dkst_descr">
	<?=GetMessage("INFORM_BLOCK");?>
</div>
<div id="ptb_window"></div>
<form id="ptb_ymreviews" method="POST" action="<?=$APPLICATION->GetCurPage()?>?lang=<?=LANG?>" name="ptb_ymreviews">
<?
echo bitrix_sessid_post();
$tabControl->Begin();
$tabControl->BeginNextTab();
?>

<input type="hidden" name="save_form" id="save_form" value="1" />
<input type="hidden" name="ptb_lid" id="ptb_lid" value="0" />
		<tr>
			<td width="300"><label for="ptb_sel_tiblock"><?=GetMessage("PTB_SEL_TIBLOCK");?></label></td>
			<td>
				<select name="ptb_sel_tiblock" id="ptb_sel_tiblock" onchange="changeIblockList(this.value)">
					<option value="0"><?=GetMessage('PTB_NOT_SET')?></option>
					<?foreach ($arIBTypes as $ibtype_id => $ibtype_name) {?>
						<option value="<?=$ibtype_id?>"<?=(COption::GetOptionString("dkst.ymreviews", "PTB_SEL_TIBLOCK")== $ibtype_id ?' selected ':'')?>><?=$ibtype_name?></option>
					<?};?>
				</select>
			</td>
		</tr>
		<tr>
			<td><label for="ptb_sel_iblock"><?=GetMessage("PTB_SEL_IBLOCK");?></label></td>
			<td>
				<select id="ptb_sel_iblock" name="ptb_sel_iblock">
					<?if(COption::GetOptionString("dkst.ymreviews", "PTB_SEL_TIBLOCK")):?>
					<?foreach ($arIB[COption::GetOptionString("dkst.ymreviews", "PTB_SEL_TIBLOCK")] as $iblock_id => $ib_name) {?>
						<option value="<?=$iblock_id?>"<?=(COption::GetOptionString("dkst.ymreviews", "PTB_SEL_IBLOCK")== $iblock_id ?' selected ':'')?>><?=$ib_name?></option>
					<?};?>					
					<?else:?>
					<option value="0"><?=GetMessage('PTB_NOT_SET')?></option>
					<?endif;?>
				</select>
			</td>
		</tr>
		<tr style="display:none;">
			<td><label for="ptb_gen_element_shag"><?=GetMessage("PTB_GEN_SHAG");?></label></td>
			<td><input type="text" name="ptb_gen_element_shag" id="ptb_gen_element_shag" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_SHAG", "1")?>"  /></td>
		</tr>
		<tr>
			<td><label for="ptb_gen_yid"><?=GetMessage("PTB_GEN_YID");?></label></td>
			<td><input type="checkbox" name="ptb_gen_yid" id="ptb_gen_yid" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_YID", "Y")?>" <?=(COption::GetOptionString("dkst.ymreviews", "PTB_GEN_YID", "Y")=="Y"?' checked ':'')?> onClick='this.value = this.checked ? "Y" : "N"' /></td>
		</tr>
		<tr>
			<td><label for="ptb_gen_yid_code"><?=GetMessage("PTB_GEN_YID_CODE");?></label></td>
			<td><input type="text" name="ptb_gen_yid_code" id="ptb_gen_yid_code" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_YID_CODE", "YANDEX_ID")?>"  /></td>
		</tr>
		<tr>
			<td><label for="ptb_gen_rating"><?=GetMessage("PTB_GEN_RATING");?></label></td>
			<td><input type="checkbox" name="ptb_gen_rating" id="ptb_gen_rating" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_RATING", "Y")?>" <?=(COption::GetOptionString("dkst.ymreviews", "PTB_GEN_RATING", "Y")=="Y"?' checked ':'')?> onClick='this.value = this.checked ? "Y" : "N"' /></td>
		</tr>
		<tr>
			<td><label for="ptb_gen_rating_code"><?=GetMessage("PTB_GEN_RATING_CODE");?></label></td>
			<td><input type="text" name="ptb_gen_rating_code" id="ptb_gen_rating_code" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_RATING_CODE", "YANDEX_RATING")?>"  /></td>
		</tr>
		<tr>
			<td><label for="ptb_gen_countrev"><?=GetMessage("PTB_GEN_COUNTREV");?></label></td>
			<td><input type="checkbox" name="ptb_gen_countrev" id="ptb_gen_countrev" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_COUNTREV", "Y")?>" <?=(COption::GetOptionString("dkst.ymreviews", "PTB_GEN_COUNTREV", "Y")=="Y"?' checked ':'')?> onClick='this.value = this.checked ? "Y" : "N"' /></td>
		</tr>
		<tr>
			<td><label for="ptb_gen_countrev_code"><?=GetMessage("PTB_GEN_COUNTREV_CODE");?></label></td>
			<td><input type="text" name="ptb_gen_countrev_code" id="ptb_gen_countrev_code" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_COUNTREV_CODE", "COUNT_REVIEWS")?>"  /></td>
		</tr>
		<tr>
			<td><label for="ptb_gen_reg"><?=GetMessage("PTB_GEN_REG");?></label></td>
			<td>
				<select id="ptb_gen_reg" name="ptb_gen_reg">
					<option value="NAME"<?=(COption::GetOptionString("dkst.ymreviews", "PTB_GEN_REG", "NAME")=="NAME"?' selected ':'')?>><?=GetMessage('PTB_GEN_REG1')?></option>
					<option value="PROP"<?=(COption::GetOptionString("dkst.ymreviews", "PTB_GEN_REG")=="PROP"?' selected ':'')?>><?=GetMessage('PTB_GEN_REG2')?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td><label for="ptb_gen_search_code"><?=GetMessage("PTB_GEN_SEARCH_CODE");?></label></td>
			<td><input type="text" name="ptb_gen_search_code" id="ptb_gen_search_code" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_SEARCH_CODE", "SEARCH_CODE")?>"  /></td>
		</tr>
		<tr>
			<td><label for="ptb_gen_isset"><?=GetMessage("PTB_GEN_ISSET");?></label></td>
			<td><input type="checkbox" name="ptb_gen_isset" id="ptb_gen_isset" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_ISSET", "Y")?>" <?=(COption::GetOptionString("dkst.ymreviews", "PTB_GEN_ISSET", "Y")=="Y"?' checked ':'')?> onClick='this.value = this.checked ? "Y" : "N"' /></td>
		</tr>
		<tr class="heading">
			<td colspan="2"><b><?=GetMessage("PTB_GEN_AUTHORIZE");?></b></td>
		</tr>
		<tr>
			<td class="adm-detail-valign-top" width="40%"><label for="ptb_gen_oauth_client"><b><?=GetMessage("PTB_GEN_IDENT");?></b></label></td>
			<td width="60%"><input type="text" size="36" name="ptb_gen_oauth_client" id="ptb_gen_oauth_client" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_OAUTH_CLIENT")?>"  />
			<br/>
			<small><?=GetMessage("PTB_GEN_OAUTH_CLIENT_ID_HELP");?></small>
			</td>
		</tr>
		<tr>
			<td class="adm-detail-valign-top" width="40%"><label for="ptb_gen_oauth_token"><b><?=GetMessage("PTB_GEN_TOKEN");?></b></label></td>
			<td width="60%"><input type="text" size="36" name="ptb_gen_oauth_token" id="ptb_gen_oauth_token" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_OAUTH_TOKEN")?>"  />
			<br/>
			<small><?=GetMessage("PTB_GEN_OAUTH_TOKEN_HELP");?></small>
			</td>
		</tr>		
		<tr>
			<td class="adm-detail-valign-top" width="40%"><label for="ptb_gen_oauth_login"><b><?=GetMessage("PTB_GEN_LOGIN_YANDEX");?></b></label></td>
			<td width="60%"><input type="text" size="36" name="ptb_gen_oauth_login" id="ptb_gen_oauth_login" value="<?=COption::GetOptionString("dkst.ymreviews", "PTB_GEN_OAUTH_LOGIN")?>"  />
			<br/>
			<small><?=GetMessage("PTB_GEN_OAUTH_LOGIN_HELP");?></small>
			</td>
		</tr>
<?
$tabControl->Buttons();
?>
<input type="button" id="start_generation" value="<?echo GetMessage("PTB_YMREVIEWS_START")?>" OnClick="GenerStart();" class="adm-btn-save" />
<input type="button" id="stop_generation" value="<?=GetMessage("PTB_YMREVIEWS_STOP")?>" OnClick="GenerStop();" disabled />
<input id="empty_code" value="<?=GetMessage("PTB_YMREVIEWS_SAVE")?>" type="submit" />
<?
$tabControl->End();
?>
</form>
<script>
var finish_get = 0;
var arIblocks = <?= CUtil::PhpToJsObject($arIB)?>;

var tiblockval = document.getElementById("ptb_sel_tiblock");


function changeIblockList(value) {
	var j, arControls = BX('ptb_sel_iblock');

	if (arControls)
		arControls.options.length = 0;

	if (value == 0)
	{
		arControls.options[0] = new Option('<?=GetMessage('PTB_NOT_SET')?>', '0');
	}

	for (j in arIblocks[value])
		arControls.options[arControls.options.length] = new Option(arIblocks[value][j], j);
}

function GenerStart() {
	finish_get = 0;
	var tiblock = document.getElementById("ptb_sel_tiblock");
	var iblock = document.getElementById("ptb_sel_iblock");
	var yid = document.getElementById('ptb_gen_yid').value;
	var yid_code = document.getElementById('ptb_gen_yid_code').value;
	var rating = document.getElementById('ptb_gen_rating').value;
	var countrev = document.getElementById('ptb_gen_countrev').value;
	
	if(tiblock.value == 0 || iblock.value == 0) {
		alert("<?=GetMessage("PTB_GEN_ERROR_IBLOCK_EMPTY");?>");
		return false
	};
	if(yid == "N" && rating == "N" && countrev == "N") {
		alert("<?=GetMessage("PTB_GEN_ERROR_NOTHING_SELECTED");?>");
		return false
	};
	if(yid_code == "") {
		alert("<?=GetMessage("PTB_GEN_ERROR_YID_CODE");?>");
		return false
	};

	ShowWaitWindow();
	document.getElementById('start_generation').disabled = "disabled";
	document.getElementById('empty_code').disabled = "disabled";
	document.getElementById('stop_generation').disabled = "";

	generation();
}
function GenerStop() {
	finish_get = 1;
	generation_finish();
	document.getElementById('ptb_window').innerHTML = '';
}
function generation(params) {
	var query = "ptb_gener_start=Y" + "&lang=<?echo LANG;?>";
	if(!params) {
		query += "&<?echo bitrix_sessid_get()?>";
		query += "&ptb_sel_tiblock=" + document.getElementById('ptb_sel_tiblock').value;
		query += "&ptb_sel_iblock=" + document.getElementById('ptb_sel_iblock').value;
		query += "&ptb_gen_element_shag=" + document.getElementById('ptb_gen_element_shag').value;
		query += "&ptb_gen_yid=" + document.getElementById('ptb_gen_yid').value;
		query += "&ptb_gen_yid_code=" + document.getElementById('ptb_gen_yid_code').value;
		query += "&ptb_gen_rating=" + document.getElementById('ptb_gen_rating').value;
		query += "&ptb_gen_rating_code=" + document.getElementById('ptb_gen_rating_code').value;
		query += "&ptb_gen_countrev=" + document.getElementById('ptb_gen_countrev').value;
		query += "&ptb_gen_countrev_code=" + document.getElementById('ptb_gen_countrev_code').value;
		query += "&ptb_gen_reg=" + document.getElementById('ptb_gen_reg').value;
		query += "&ptb_gen_search_code=" + document.getElementById('ptb_gen_search_code').value;
		query += "&ptb_gen_isset=" + document.getElementById('ptb_gen_isset').value;
		query += "&ptb_gen_oauth_login=" + document.getElementById('ptb_gen_oauth_login').value;
		query += "&ptb_gen_oauth_client=" + document.getElementById('ptb_gen_oauth_client').value;
		query += "&ptb_gen_oauth_token=" + document.getElementById('ptb_gen_oauth_token').value;
		query += "&ptb_lid=" + document.getElementById('ptb_lid').value;
	};

	if(finish_get != 1) {
		BX.ajax.post(
			'dkst_ymreviews.php?'+query,
			params,
			function(res){
				document.getElementById('ptb_window').innerHTML = res;	
			}
		);
	} else {
		document.getElementById('ptb_window').innerHTML = '';
	};
}

function generation_finish() {
	document.getElementById('start_generation').disabled = "";
	document.getElementById('empty_code').disabled = "";
	document.getElementById('stop_generation').disabled = "disabled";
	CloseWaitWindow();
}
</script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>