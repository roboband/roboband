<?
$MESS["PTB_YMREVIEWS_TITLE"] = "Отзывы о товаре с Яндекс.Маркет PRO";
$MESS["PTB_TAB"] = "Настройки модуля";
$MESS["PTB_YMREVIEWS_START"] = "Начать запись";
$MESS["PTB_YMREVIEWS_STOP"] = "Остановить";
$MESS["PTB_YMREVIEWS_SAVE"] = "Сохранить настройки";
$MESS["PTB_SEL_TIBLOCK"] = "Выберите тип инфоблока";
$MESS["PTB_SEL_IBLOCK"] = "Выберите инфоблок";
$MESS["PTB_NOT_SET"] = "Не выбрано";
$MESS["PTB_GEN_YID"] = "Записывать ID товара на Яндекс.Маркете";
$MESS["PTB_GEN_YID_CODE"] = "Код свойства с Яндекс ID";
$MESS["PTB_GEN_NEW_NAME"] = "ID товара на Яндекс.Маркет";
$MESS["PTB_GEN_NEW_RATING"] = "Рейтинг товара с Яндекс.Маркет";
$MESS["PTB_GEN_NEW_COUNT"] = "Количество отзывов с Яндекс.Маркет";
$MESS["PTB_GEN_RATING"] = "Записывать рейтинг товара";
$MESS["PTB_GEN_RATING_CODE"] = "Код свойства для рейтинга";
$MESS["PTB_GEN_COUNTREV"] = "Записывать количество отзывов";
$MESS["PTB_GEN_COUNTREV_CODE"] = "Код свойства для количества отзывов";
$MESS["PTB_GEN_SEARCH_CODE"] = "Код свойства с поисковым запросом";
$MESS["PTB_GEN_ISSET"] = "Пропускать товары с Яндекс ID";
$MESS["PTB_GEN_REG"] = "Искать товары";
$MESS["PTB_GEN_REG1"] = "по названию элемента";
$MESS["PTB_GEN_REG2"] = "по поисковому запросу из свойства";

$MESS["INFORM_BLOCK"] = '<b>Понравился модуль?</b> &mdash; <a target="_blank" href="http://marketplace.1c-bitrix.ru/solutions/dkst.ymreviews/" class="no_underline response"><u>напишите отзыв</u><i></i></a> <span>Есть вопросы? &mdash; посмотрите <a href="http://ym.dkwebstudio.ru/ym_instruction.pdf" target="_blank" class="no_underline instructions"><u>инструкцию</u></a> <span>Нашли ошибку? &mdash; у нас есть бесплатная <a href="mailto:support@dkwebstudio.ru">техподдержка</a></span></span>';
$MESS["PTB_GEN_REG2"] = "по поисковому запросу из свойства";
$MESS["PTB_GEN_REG2"] = "по поисковому запросу из свойства";

$MESS["PTB_GEN_DUBL3"] = "Пропустить";
$MESS["PTB_GEN_SHAG"] = "Количество обрабатываемых элементов за шаг";

$MESS["PTB_GEN_TOKEN"] = "Авторизационный токен (для запросов магазина к Яндексу)";
$MESS["PTB_GEN_IDENT"] = "Идентификатор приложения";
$MESS["PTB_GEN_LOGIN_YANDEX"] = "Логин пользователя";
$MESS["PTB_GEN_AUTHORIZE"] = "Настройки авторизации";

$MESS["PTB_GEN_OAUTH_TOKEN_HELP"] = "Алгоритм получения токена описан в <a href='http://api.yandex.ru/oauth/doc/dg/tasks/get-oauth-token.xml' target='_blank' >\"Руководстве разработчика\" -> \"Получение отладочного токена вручную\"</a>.";
$MESS["PTB_GEN_OAUTH_CLIENT_ID_HELP"] = "Алгоритм получения идентификатора приложения описан в <a href='http://api.yandex.ru/oauth/doc/dg/tasks/register-client.xml' target='_blank' >\"Руководстве разработчика\" -> \"Регистрация приложения\" </a> .";
$MESS["PTB_GEN_OAUTH_LOGIN_HELP"] = "Логин пользователя Яндекса, зарегистрировавшего магазин.";

$MESS["PTB_ERROR_SEARCH_CODE"] = "Свойство с поисковым запросом не существует.";
$MESS["PTB_ERROR_IBLOCK"] = "Не установлен модули \"Информационные блоки\". Работа не может быть продолжена.";
$MESS["PTB_NOT_ACCESS"] = "Ошибка доступа";
$MESS["PTB_GEN_ERROR_IBLOCK_EMPTY"] = "Выберите инфоблок";
$MESS["PTB_GEN_ERROR_NOTHING_SELECTED"] = "Не выбрано ни одно действие";
$MESS["PTB_GEN_ERROR_YID_CODE"] = "Введите код свойства с Яндекс ID";
$MESS["PTB_GEN_ERROR_DOCSEL"] = "Выберите для чего создать символьный код";

$MESS["PTB_PROPGRESS_SECTION"] = "Обработано разделов: ";
$MESS["PTB_PROPGRESS_ELEMENT"] = "Обработано элементов: ";
$MESS["PTB_PROPGRESS_ELEMENT_FAIL"] = "Пропущено элементов: ";
$MESS["PTB_PROGRESS_STATUS_ON"] = "Идет обработка данных...";
$MESS["PTB_PROGRESS_STATUS_OK"] = "Обработка данных закончена.";
$MESS["PTB_ERROR_LIST"] = "Раздел: #NAME#, Ошибка: #ERROR#";
$MESS["PTB_ERROR_LIST2"] = "Элемент: #NAME#, Ошибка: #ERROR#";
$MESS["PTB_ERROR_TEXT"] = "Ошибки:<br/>";
$MESS["PTB_PRIMTEXT"] = "<br/>** При очистке кода используются параметры формы:<br/>
&nbsp;&nbsp;- тип инфоблока и инфоблок<br/>
&nbsp;&nbsp;- количество обрабатываемых элементов за шаг<br/>
&nbsp;&nbsp;- генерировать символьный код разделов/элементов<br/><br/>
*** Если поле, символьный код, является <b>обязательным</b>, то очистка поля не будет произведена у раздела/элемента";
?>