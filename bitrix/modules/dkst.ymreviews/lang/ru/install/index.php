<?
$MESS["PTB_MODULE_NAME"] = "Отзывы о товаре с Яндекс.Маркет PRO";
$MESS["PTB_MODULE_DISCRIPTION"] = "Модуль для размещения отзывов о товарах с яндекс маркета";
$MESS["USERTYPE_DEL_ERROR"] = "Ошибка при удаление базы";
$MESS["MOD_INST_ERR"] = "МОДУЛЬ НЕ УСТАНОВЛЕН";
$MESS["MOD_INST_OK"] = "Модуль установлен";
$MESS["PTB_INSTALL_TITLE"] = "Установка модуля \"Отзывы о товаре с Яндекс.Маркет PRO\"";
$MESS["PTB_UNINSTALL_TITLE"] = "Удаление модуля \"Отзывы о товаре с Яндекс.Маркет PRO\"";
$MESS["PTB_PARTNER_NAME"] = "Студия DK";
$MESS["PTB_PARTNER_URI"] = "http://dkwebstudio.ru";
?>
