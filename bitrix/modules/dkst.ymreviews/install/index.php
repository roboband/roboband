<?
IncludeModuleLangFile(__FILE__);
if(class_exists("dkst_ymreviews")) return;

class dkst_ymreviews extends CModule {
	
	var $MODULE_ID = "dkst.ymreviews";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;

	function dkst_ymreviews()
	{
		$arModuleVersion = array();
		
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");
		
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		
		$this->MODULE_NAME = GetMessage("PTB_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("PTB_MODULE_DISCRIPTION");

		$this->PARTNER_NAME = GetMessage("PTB_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("PTB_PARTNER_URI");
	}

	function DoInstall()
	{
		global $APPLICATION;
		$this->InstallEvents();
		$this->InstallFiles();
		$APPLICATION->IncludeAdminFile(GetMessage("PTB_INSTALL_TITLE"),$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/dkst.ymreviews/install/step1.php");
	}

	function DoUninstall()
	{
		global $APPLICATION;
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_SHAG");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_YID");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_YID_CODE");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_RATING");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_RATING_CODE");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_COUNTREV");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_COUNTREV_CODE");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_REG");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_SEARCH_CODE");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_ISSET");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_OAUTH_LOGIN");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_OAUTH_CLIENT");
		COption::RemoveOption("dkst.ymreviews", "PTB_GEN_OAUTH_TOKEN");
		COption::RemoveOption("dkst.ymreviews", "PTB_SEL_TIBLOCK");
		COption::RemoveOption("dkst.ymreviews", "PTB_SEL_IBLOCK");
		$this->UninstallFiles();
		$APPLICATION->IncludeAdminFile(GetMessage("PTB_UNINSTALL_TITLE"),$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/dkst.ymreviews/install/unstep1.php");
	}

	function InstallFiles()
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/dkst.ymreviews/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin",true,true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/dkst.ymreviews/install/components",
                         $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true, false, ".svn" );
		RegisterModule($this->MODULE_ID);
	}

	function UninstallFiles()
	{
        DeleteDirFilesEx("/bitrix/components/dkst/yandex_reviews");
        DeleteDirFilesEx("/bitrix/components/dkst/yandex_reviews_ajax");
		DeleteDirFilesEx("/bitrix/admin/dkst_ymreviews.php");
		UnRegisterModule($this->MODULE_ID);
	}
}
?>
