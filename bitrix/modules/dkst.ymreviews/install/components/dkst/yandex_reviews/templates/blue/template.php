<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($this->__folder)
	$pathToTemplateFolder = $this->__folder ;
else
	$pathToTemplateFolder = str_replace(str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']), '', dirname(__FILE__));

?>

<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin(GetMessage('COMPOSITE_LOADING'));?>


<div class="yandex_market_reviews_store">
	<div class="reviews">
	</div>
	<a class="prev disabled" href="javascript:void(0)">< <?=GetMessage('PREV');?></a>
	<a class="page disabled">| <span class="load"><?=GetMessage('LOAD');?></span><span class="num"><?=GetMessage('PAGE');?> <span>1</span></span> |</a>
	<a class="next" href="javascript:void(0)"><?=GetMessage('NEXT');?> ></a>
	<a class="link" href="http://market.yandex.ru/product/<?=$arParams["YANDEX_ID"];?>/reviews"><?=GetMessage('LINK');?></a>
</div>                      

<script type="text/javascript">
function get_ym_reviews(page, flag, path, count)
{
	if(!flag) flag = false;
	
	var ym_reviews = $('.yandex_market_reviews_store');
	ym_reviews.find('.page .load').show()
	ym_reviews.find('.page .num').hide()
	ym_reviews.find('.prev').addClass("disabled");
	ym_reviews.find('.next').addClass("disabled");
	
	$.ajax({
		url: path+"/ajax.php",
		type: "POST",
		dataType: "html",
		data: "PAGE="+page,
		success: function(data){
			ym_reviews.find('.reviews').html(data)
			ym_reviews.find('.page .load').hide()
			ym_reviews.find('.page .num').show()
				
			if(ym_reviews.find('.reviews .total').length > 0)
			{
				var page = parseInt(ym_reviews.find('.reviews .page').text());
				var total = parseInt(ym_reviews.find('.reviews .total').text());
				var max = Math.floor(total/count);
				if(total % count != 0)
					max = max + 1;
					
				ym_reviews.find('.page .num span').html(page.toString() + " / " + max.toString());

				if(page > 1)
					ym_reviews.find(".prev.disabled").removeClass("disabled");
					
					
				if(page < max)
					ym_reviews.find(".next.disabled").removeClass("disabled");
					
				ym_reviews.find(".prev").unbind('click');
				ym_reviews.find(".prev").click(function(){
					if(page > 1){
						page = page - 1;
						get_ym_reviews(page, false, path, count);
					}
				});
				ym_reviews.find(".next").unbind('click');
				ym_reviews.find(".next").click(function(){
					if(page < max){
						page = page + 1;
						get_ym_reviews(page, false, path, count);
					}
				});
			}else{
				ym_reviews.find('.page .num span').html(" - ");
			}
			if(!flag)
			{
				$('html, body').animate({
					scrollTop: ym_reviews.offset().top
				}, 1000);
			}
		}
	});
}
	$(document).ready(function(){
		get_ym_reviews(1, true, '<?=$pathToTemplateFolder;?>','<?=$arParams["COUNT"];?>');
	})
</script>

<?if(method_exists($this, 'createFrame')) $frame->end();?>