<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($this->__folder)
	$pathToTemplateFolder = $this->__folder ;
else
	$pathToTemplateFolder = str_replace(str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']), '', dirname(__FILE__));

?>

<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin(GetMessage('COMPOSITE_LOADING'));?>


<div class="yandex_market_reviews_store">
	<div class="hiddenblock" id="newpage">1</div>
	<div class="reviews">
	</div>
	<div class="actions">
		<img class="load" src="<?=$pathToTemplateFolder;?>/images/ajax-loader.gif" alt="" />
		<a class="simple next"><?=GetMessage('SHOW_MORE');?></span></a>
	</div>
	<noindex>
	<a class="link" rel="nofollow" target="_blank" href="http://market.yandex.ru/product/<?=$arParams["YANDEX_ID"];?>/reviews"><?=GetMessage('LINK');?></a>
	</noindex>
</div>                      

<script type="text/javascript">
function get_ym_reviews(page, path, count)
{	
	var ym_reviews = $('.yandex_market_reviews_store');
	ym_reviews.find('.load').show();
	
	$.ajax({
		url: path+"/ajax.php",
		type: "POST",
		dataType: "html",
		data: "PAGE="+page,
		success: function(data){
			ym_reviews.find('.reviews').append(data);
			ym_reviews.find('.load').hide();
		}
	});
}
$(document).ready(function(){
	get_ym_reviews(1, '<?=$pathToTemplateFolder;?>','<?=$arParams["COUNT"];?>');
	
	$(".yandex_market_reviews_store .next").click(function(){
		var page = $("#newpage").text();
		page = parseInt(page);
		page = page + 1;
		$("#newpage").text(page);
		get_ym_reviews(page, '<?=$pathToTemplateFolder;?>', '<?=$arParams["COUNT"];?>');		
	});
})
</script>

<?if(method_exists($this, 'createFrame')) $frame->end();?>