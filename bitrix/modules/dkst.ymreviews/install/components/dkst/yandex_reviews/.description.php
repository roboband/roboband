<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("DKST_NAME"),
	"DESCRIPTION" => GetMessage("DKST_DESCRIPTION"),
	"ICON" => "/images/reviews.png",
	"CACHE_PATH" => "Y",
	"SORT" => 160,
	"PATH" => array(
		"ID" => "dkst_yandex_reviews",
		"NAME" => GetMessage("DKST_COMPONENTS"),
	),
);
?>