<?php

class CB1TeamOrderPost
{
    function OnOrderNewSendEmail($orderID, &$eventName, &$arFields)
    {
        //$orderID - номер заказа
        //$eventName = "SALE_NEW_ORDER"
        //$arFields - список полей почтового шаблона
        global $USER, $APPLICATION;
        $LOCAL_MODULE_NAME = "b1team.orderpost";
        IncludeModuleLangFile(__FILE__);

        // <editor-fold defaultstate="collapsed" desc="Загрузка данных">   
        
        $arOrder = CSaleOrder::GetList(
            array(),
            array("ID" => $orderID),
            false,
            false,
            array("USER_ID", "USER_DESCRIPTION", "ADDITIONAL_INFO", "COMMENTS", "DISCOUNT_VALUE", "CURRENCY", "PERSON_TYPE_ID","PAY_SYSTEM_ID", "DELIVERY_ID")
        )->GetNext();
        
        $rsOrderProp = CSaleOrderProps::GetList(
            array(), 
            array("PERSON_TYPE_ID" => $arOrder["PERSON_TYPE_ID"]),
            false,
            false,
            array()
        );
      
        $rsProp = CSaleOrderPropsValue::GetList(
            array(), 
            array("ORDER_ID" => $orderID),
            false,
            false,
            array()
        );
        
        $arPayment = CSalePaySystem::GetList(
            array(),
                array("ID" => $arOrder["PAY_SYSTEM_ID"], "PERSON_TYPE_ID" => $arOrder["PERSON_TYPE_ID"]),
                false,
                false,
                array("ID", "NAME", "DESCRIPTION")
        )->GetNext();
        
        
        if ($arOrder["DELIVERY_ID"] > 0)
        {
            $arDelivery = CSaleDelivery::GetList(
                    array(),
                    array("ID" => $arOrder["DELIVERY_ID"]),
                    false,
                    false,
                    array("ID", "NAME", "DESCRIPTION")
            )->GetNext();
        }
        else
        {
            list($DELIVERY_SERVICE, $DELIVERY_PROFILE) = explode(':', $arOrder["DELIVERY_ID"]);
            $arDelivery = CSaleDeliveryHandler::GetBySID($DELIVERY_SERVICE)->GetNext();
            //если есть указанный профиль доставки, до доставим его к имени
            if(isset($arDelivery["PROFILES"][$DELIVERY_PROFILE])) {
                $deliveryProfile = $arDelivery["PROFILES"][$DELIVERY_PROFILE];
                $arDelivery["NAME"] .= !empty($arDelivery["NAME"]) ? " (".$deliveryProfile["TITLE"].")" : $deliveryProfile["TITLE"];
            }
        }
        
        
        
        $idUser = $arOrder["USER_ID"];
        
        $arUser = CUser::GetList(
            $by = "ID", 
            $order = "ASC",
            array("ID" => $idUser),
            array("FIELDS" => array("ID", "LOGIN", "EMAIL", "NAME", "LAST_NAME", "SECOND_NAME", "ADMIN_NOTES"))
        )->Fetch();
        
        // </editor-fold> 
        
        //Выгрузка #КОДОВ# = значений
        
        // <editor-fold defaultstate="collapsed" desc="1. Заказ">        
 
        $arOrder["DELIVERY_PRICE"] = CurrencyFormat($arFields["DELIVERY_PRICE"], $arOrder["CURRENCY"]);
        $arOrder["DISCOUNT_VALUE"] = CurrencyFormat($arOrder["DISCOUNT_VALUE"], $arOrder["CURRENCY"]);
        unset($arOrder["CURRENCY"]);
        
        foreach ($arOrder as $code => $order)
            if ($code[0] != "~")
                $arFields["ORDER_".$code] = $order;
        
        // </editor-fold>  

        // <editor-fold defaultstate="collapsed" desc="2. Платежная система">
        
        if($arPayment) {
            foreach ($arPayment as $code => $payment) {
                if ($code[0] != "~") {
                    $arFields["PAYMENT_".$code] = $payment;
                }
            }
        } else {
            $arFields["PAYMENT_ID"] = "";
            $arFields["PAYMENT_NAME"] = "";
            $arFields["PAYMENT_DESCRIPTION"] = "";
        }
        
        // </editor-fold>        

        // <editor-fold defaultstate="collapsed" desc="3. Служба доставки">        
        
        if($arDelivery) {
            foreach ($arDelivery as $code => $delivery) {
                if ($code[0] != "~"){
                    $arFields["DELIVERY_".$code] = $delivery;
                }
            }
        } else {
            $arFields["DELIVERY_ID"] = "";
            $arFields["DELIVERY_NAME"] = "";
            $arFields["DELIVERY_DESCRIPTION"] = "";
        }
        
        // </editor-fold>        
 
        // <editor-fold defaultstate="collapsed" desc="4. Пользователь">
        
        foreach ($arUser as $code => $user)
            $arFields["USER_".$code] = $user;
        
        // </editor-fold>        

        // <editor-fold defaultstate="collapsed" desc="5. Профиль заказа">

        //Получаем коды свойств, которые указаны как местоположение
        while ($arProp = $rsOrderProp->GetNext()) {
            $arProps[$arProp["CODE"]] = $arProp;
            if ($arProp["IS_LOCATION"] == "Y") {
                $arPropLoc[] = $arProp["CODE"];
            }
        }
        
        while ($sale = $rsProp->GetNext()) {
            $arProp = $arProps[$sale["CODE"]];
            if (in_array($sale["CODE"], $arPropLoc)) {
                $loc = CSaleLocation::GetByID($sale["VALUE"], LANGUAGE_ID);
                //полуичм отдельно город, регион, страну
                $temp = array();
                if(!empty($loc["COUNTRY_NAME_LANG"]))
                    $temp[] = $loc["COUNTRY_NAME_LANG"];
                if(!empty($loc["REGION_NAME_LANG"]))
                    $temp[] = $loc["REGION_NAME_LANG"];
                if(!empty($loc["CITY_NAME_LANG"]))
                    $temp[] = $loc["CITY_NAME_LANG"];
                
                $sale["VALUE"] = implode(", ", $temp);
            }
            //для свойства типа "Список"
            if($arProp["TYPE"] == "SELECT" || $arProp["TYPE"] == "RADIO") {
                //если значение не пустое
                if(!empty($sale["VALUE"])) {
                    $arVal = CSaleOrderPropsVariant::GetByValue($arProp["ID"], $sale["VALUE"]);
                    $sale["VALUE"] = $arVal["NAME"];
                }
            }
            
            $arFields["PROFILE_".$sale["CODE"]] = $sale["VALUE"];
        }
         
        foreach ($arProps as $code => $arProp) {
            if (!isset($arFields["PROFILE_".$code])) {
                $arFields["PROFILE_".$code] = " ";
            }
        }
        
        // </editor-fold>        
        
        $next_str = "\n"; //Что подставлять для переноса на следующую строку. Для опций раздела "Добавлять автоматически"
        
        //Опции раздела "Список товаров"
        if (COption::GetOptionString($LOCAL_MODULE_NAME, "OrderListOverride") == "Y") {
            
            $next_str = "<br>";
            
            $ID = $orderID;
            
            $arOrder = CSaleOrder::GetByID($ID);
            
            // <editor-fold defaultstate="collapsed" desc="получим товары из корзины">
            
            $dbBasketItems = CSaleBasket::GetList(
                    array("NAME" => "ASC"),
                    array("ORDER_ID" => $ID),
                    false,
                    false,
                    array("ID", "PRODUCT_ID", "NAME", "QUANTITY", "PRICE", "CURRENCY")
            );
            $arBasketItems = array();
            $arProductIds = array(-1);
            while ($arBasketItem = $dbBasketItems->Fetch()) {
                
                //получим параметы к товару
                $dbProp = CSaleBasket::GetPropsList(
                        Array("SORT" => "ASC", "ID" => "ASC"), 
                        Array("BASKET_ID" => $arBasketItem["ID"], "!CODE" => array("CATALOG.XML_ID", "PRODUCT.XML_ID"))
                );
                while($arProp = $dbProp->GetNext())
                    $arBasketItem["PROPS"][] = $arProp;
                                    
                 $arBasketItems[$arBasketItem["ID"]] = $arBasketItem;
                 $arProductIds[] = $arBasketItem["PRODUCT_ID"];
            }
            
            // </editor-fold>  
 
            // <editor-fold defaultstate="collapsed" desc="получим товары с изображениями">
            
            $arProducts = array();
            
            //получим элементы ифноблока
            $dbProducts = CIBlockElement::GetList(
                array(),
                array("ID" => $arProductIds),
                false,
                false,
                array("ID", "DETAIL_PICTURE", "PREVIEW_PICTURE", "DETAIL_PAGE_URL")
            );
            while($obElement = $dbProducts->GetNextElement()) {
				$arElement = $obElement->GetFields();
				$arElement["PROPERTIES"] = $obElement->GetProperties();
                $file = 0;
                
                // если изображение есть
                if ($arElement["DETAIL_PICTURE"] > 0 || $arElement["PREVIEW_PICTURE"] > 0) {
                    if ($arElement["DETAIL_PICTURE"] > 0)
                        $file = $arElement["DETAIL_PICTURE"];
                    else
                        $file = $arElement["PREVIEW_PICTURE"];
                } else {
                    // если элемент является SKU
                    if ($arElement["PROPERTIES"]["CML2_LINK"]["VALUE"] > 0) {
                        //получимосновной товар
                        $resProduct = CIBlockElement::GetList(
                            array(), 
                            array("ID" => $arElement["PROPERTIES"]["CML2_LINK"]["VALUE"]), 
                            false, 
                            array("nTopCount" => 1), 
                            array("PREVIEW_PICTURE", "DETAIL_PICTURE")
                        );
                        if($arProduct = $resProduct->GetNext()) {
                            // если изображение есть
                            if ($arProduct["DETAIL_PICTURE"] > 0 || $arProduct["PREVIEW_PICTURE"] > 0) {
                                if ($arProduct["DETAIL_PICTURE"] > 0)
                                    $file = $arProduct["DETAIL_PICTURE"];
                                else
                                    $file = $arProduct["PREVIEW_PICTURE"];
                            }
                        }
                    }
                }

                //если определили изображение
                if($file > 0) {
                    $arElement["IMAGE"] = CFile::ResizeImageGet(
                        $file,
                        array("width" => 80, "height" => 300),
                        BX_RESIZE_IMAGE_PROPORTIONAL,
                        true
                    );
                }
                
                $arProducts[$arElement["ID"]] = $arElement;
            }
            
            // </editor-fold>  
            
            // <editor-fold defaultstate="collapsed" desc="Вывод товаров в таблицу">

            $borderColor = "#cccccc";
            
            $strOrderList = "<table width='100%' cellspacing='0' style='padding-top:10px;'>";
            $strOrderList .= "<thead>".
                                    "<tr>".
                                          "<th style='vertical-align:top; padding:5px 3px; border-top: 1px solid $borderColor; text-align:left;' colspan='2' >".GetMessage("B1T_CSHOP_EVENT_HELPER_SALE_NEW_ORDER_TH_NAME")."</th>".
                                          "<th style='vertical-align:top; padding:5px 3px; border-top: 1px solid $borderColor;'>".GetMessage("B1T_CSHOP_EVENT_HELPER_SALE_NEW_ORDER_TH_QUANTITY")."</th>".
                                          "<th style='vertical-align:top; padding:5px 3px; border-top: 1px solid $borderColor;' align='right'>".GetMessage("B1T_CSHOP_EVENT_HELPER_SALE_NEW_ORDER_TH_PRICE")."</th>".
                                          "<th style='vertical-align:top; padding:5px 3px; border-top: 1px solid $borderColor;' align='right'>".GetMessage("B1T_CSHOP_EVENT_HELPER_SALE_NEW_ORDER_TH_SUM")."</th>".
                                    "</tr>".
                            "</thead>";
            $protocol = $APPLICATION->IsHTTPS() ? "https":"http";
            foreach ($arBasketItems as $id => $arBasketItem) {
                $arProduct = $arProducts[$arBasketItem["PRODUCT_ID"]];
                
                $strOrderList.= "<tr>";
                
                $strOrderList.= "<td style='vertical-align:top; padding:5px 3px; border-top: 1px solid $borderColor;'>";

                if (COption::GetOptionString($LOCAL_MODULE_NAME, "OrderListShowPhoto") == "Y") {

                    if(is_array($arProduct["IMAGE"])) {
                        $img = $arProduct["IMAGE"];
						
						if (strpos($img["src"], "http") === 0)
							$strOrderList.="<img src='".$img["src"]."' width='".$img["width"]."' height='".$img["height"]."'/>";
						else
							$strOrderList.="<img src='$protocol://".$_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$img["src"]."' width='".$img["width"]."' height='".$img["height"]."'/>";
                    }

                }

                $strOrderList.= "&nbsp;</td>\n";
                      
                $href = "$protocol://".$_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$arProduct["DETAIL_PAGE_URL"];
                $href = str_replace("%2F","/",$href);
                $strOrderList.= "<td style='vertical-align:top; padding:5px 3px; border-top: 1px solid $borderColor;'>";
                if(COption::GetOptionString($LOCAL_MODULE_NAME, "OrderListShowLink") == "Y")
                        $strOrderList.= "<a href='$href'>".$arBasketItem["NAME"]."</a>";
                else
                        $strOrderList.= $arBasketItem["NAME"];

                if (COption::GetOptionString($LOCAL_MODULE_NAME, "OrderListShowParams") == "Y") {

                    if(!empty($arBasketItem["PROPS"])) {
                        $strOrderList.= "<br/>";
                        $strOrderList.= "<div style='padding-top: 5px;'>";
                        $strOrderList.= "<small>";
                        foreach($arBasketItem["PROPS"] as $val){
                            $strOrderList.= $val["NAME"].": ".$val["VALUE"]."<br />";
                        }
                        $strOrderList.= "</small>";
                        $strOrderList.= "</div>";
                    }

                }

                $strOrderList.= "</td>\n";

                $strOrderList.= "<td align='center' style='vertical-align:top; padding:5px 3px; border-top: 1px solid $borderColor;'>";
                $strOrderList.= intval($arBasketItem["QUANTITY"]);
                $strOrderList.= "</td>\n";


                $strOrderList.= "<td align='right' style='vertical-align:top; padding:5px 3px; border-top: 1px solid $borderColor;'>";
                $strOrderList.= SaleFormatCurrency($arBasketItem["PRICE"], $arBasketItem["CURRENCY"]);
                $strOrderList.= "</td>\n";

                $strOrderList.= "<td align='right' style='vertical-align:top; padding:5px 3px; border-top: 1px solid $borderColor;'>";
                $strOrderList.= SaleFormatCurrency($arBasketItem["PRICE"] * $arBasketItem["QUANTITY"], $arBasketItem["CURRENCY"]);
                $strOrderList.= "</td>\n";

                $strOrderList.= "</tr>";
            }

            $strOrderList .= "</table><br>";
            
            $arFields["ORDER_LIST"] = $strOrderList;
            // </editor-fold>  

        }
        
        //Опции раздела "Добавлять автоматически"
        if (COption::GetOptionString($LOCAL_MODULE_NAME, "AutoPayType") == "Y")
            $arFields["ORDER_LIST"] .= GetMessage("B1T_POSTEVENT_T_PAY_TYPE").$arPayment["NAME"].$next_str;
        if (COption::GetOptionString($LOCAL_MODULE_NAME, "AutoDelivType") == "Y")
            $arFields["ORDER_LIST"] .= GetMessage("B1T_POSTEVENT_T_DELIV_TYPE").$arDelivery["NAME"].$next_str;
        if (COption::GetOptionString($LOCAL_MODULE_NAME, "AutoComment") == "Y" && strlen($arOrder["USER_DESCRIPTION"]) > 0)
            $arFields["ORDER_LIST"] .= GetMessage("B1T_POSTEVENT_T_COMMENT").$arOrder["USER_DESCRIPTION"].$next_str;
        
    }
    
    /*
     * Обработка события отправки почтового уведомления
     */
    function OnBeforeEventSend($arFields, &$arTemplate) {
        
        //Если тип письма "Оформление нового заказа"
        if($arTemplate["EVENT_NAME"] == "SALE_NEW_ORDER") {
            //инициализируем все параметры как php-переменные
            foreach($arFields as $key => $value) {
                $$key = $value;
            }
            //заменим все вставки кода
            $matches = array();
            if(preg_match_all("/#%(.*?)%#/s",$arTemplate["MESSAGE"],$matches,PREG_SET_ORDER)) {
                foreach($matches as $match) {
                    $result = "";
                    $code = str_replace(array("#%=","%#"),array('$result=',";"),$match[0]);
                    eval($code);
                    $arTemplate["MESSAGE"] = str_replace($match[0], $result, $arTemplate["MESSAGE"]);
                    $arTemplate["MESSAGE_PHP"] = str_replace($match[0], $result, $arTemplate["MESSAGE_PHP"]);
                }
            }
        }
    }
}

?>
