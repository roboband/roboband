<?

$arModuleVersion = array();
$PathInstall = str_replace("\\", "/", __FILE__);
$PathInstall = substr($PathInstall, 0, strlen($PathInstall) - strlen("/index.php"));
IncludeModuleLangFile($PathInstall."/install.php");

Class b1team_orderpost extends CModule 
{
    var $MODULE_ID = "b1team.orderpost";
    var $MODULE_NAME;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function b1team_orderpost()	
    {
        $PathInstall = str_replace("\\", "/", __FILE__);
        $PathInstall = substr($PathInstall, 0, strlen($PathInstall) - strlen("/index.php"));
        include($PathInstall."/version.php");
        
        $this->MODULE_NAME = GetMessage("B1T_POSTEVENT_M_NAME");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->PARTNER_NAME = GetMessage("B1T_POSTEVENT_M_PARTNER");
        $this->MODULE_DESCRIPTION = GetMessage("B1T_POSTEVENT_M_DESCRIPTION");
        $this->PARTNER_URI = "http://b1team.ru/";
    }
    
    function InstallDB() 
    {
        RegisterModule("b1team.orderpost");
        return true;
    }
    
    function UnInstallDB()
    {
        UnRegisterModule("b1team.orderpost");
        return true;
    }

    function InstallEvents()
    {
        RegisterModuleDependences("sale", "OnOrderNewSendEmail", "b1team.orderpost", "CB1TeamOrderPost", "OnOrderNewSendEmail");
        RegisterModuleDependences("main", "OnBeforeEventSend", "b1team.orderpost", "CB1TeamOrderPost", "OnBeforeEventSend");
        
        return true;
    }

    function UnInstallEvents()
    {
        UnRegisterModuleDependences("sale", "OnOrderNewSendEmail", "b1team.orderpost", "CB1TeamOrderPost", "OnOrderNewSendEmail");
        UnRegisterModuleDependences("main", "OnBeforeEventSend", "b1team.orderpost", "CB1TeamOrderPost", "OnBeforeEventSend");
        return true;
    }

    function InstallFiles()
    {
        return true;
    }
    
    function UnInstallFiles()
    {
        return true;
    }
    
    function DoInstall() 
    {
        if (!IsModuleInstalled("b1team.orderpost")) 
        {
            $this->InstallFiles();
            $this->InstallEvents();
            $this->InstallDB();
        }
    }

    function DoUninstall()  
    {
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        $this->UnInstallDB();
    }
}

?>
