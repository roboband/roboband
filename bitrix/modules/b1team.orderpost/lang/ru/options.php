<?php

$MESS["B1T_POSTEVENT_T_SETTINGS"] = "Настройки";
$MESS["B1T_POSTEVENT_T_SETTINGS_MODULE"] = "Настройки модуля";
$MESS["B1T_POSTEVENT_T_SALE_LIST"] = "Список товаров";
$MESS["B1T_POSTEVENT_T_OVERRIDE_STANDART"] = "Заменить стандратной список";
$MESS["B1T_POSTEVENT_T_SHOW_PHOTO"] = "Отображать фото";
$MESS["B1T_POSTEVENT_T_SHOW_LINK"] = "Название как ссылка на детальный просмотр";
$MESS["B1T_POSTEVENT_T_SHOW_PARAMS"] = "Отображать параметры товара";
$MESS["B1T_POSTEVENT_T_AUTOADD"] = "Добавлять автоматически";
$MESS["B1T_POSTEVENT_T_PAYMENT_TYPE"] = "Способ оплаты";
$MESS["B1T_POSTEVENT_T_DELIVERY_TYPE"] = "Способ доставки";
$MESS["B1T_POSTEVENT_T_ORDER_COMMENT"] = "Комментарий к заказу";
$MESS["B1T_POSTEVENT_T_SAVE_BTN"] = "Сохранить";

?>
