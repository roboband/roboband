<?
// <editor-fold defaultstate="collapsed" desc="ЗАГОЛОВКИ">

$LOCAL_MODULE_NAME = "b1team.orderpost";

// подключим все необходимые файлы:
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
IncludeModuleLangFile(__FILE__); // подключим языковой файл
CModule::IncludeModule($LOCAL_MODULE_NAME);
        
$POST_RIGHT = $APPLICATION->GetGroupRight($LOCAL_MODULE_NAME); // получим права доступа текущего пользователя на модуль
if ($POST_RIGHT == "D") // если нет прав - отправим к форме авторизации с сообщением об ошибке
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Вспомогательные функции">
function SaveParam($param, $LOCAL_MODULE_NAME)
{
    if ($_POST[$param] == "Y")
        $val = "Y";
    else $val = "N";
    COption::SetOptionString($LOCAL_MODULE_NAME, $param, $val);
}

function LoadParam($param, $LOCAL_MODULE_NAME)
{
    if (COption::GetOptionString($LOCAL_MODULE_NAME, $param) == "Y")
        return "checked";
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Сохранение">

if ($REQUEST_METHOD == "POST"         // проверка метода вызова страницы
    && $POST_RIGHT == "W"             // проверка наличия прав на запись для модуля
    && check_bitrix_sessid()          // проверка идентификатора сессии
)
{
    if (strlen($RestoreDefaults) > 0)
    {
        COption::RemoveOption($LOCAL_MODULE_NAME);
    } else {
        SaveParam("OrderListOverride", $LOCAL_MODULE_NAME);
        SaveParam("OrderListShowPhoto", $LOCAL_MODULE_NAME);
        SaveParam("OrderListShowParams", $LOCAL_MODULE_NAME);
        SaveParam("AutoPayType", $LOCAL_MODULE_NAME);
        SaveParam("AutoDelivType", $LOCAL_MODULE_NAME);
        SaveParam("AutoComment", $LOCAL_MODULE_NAME);
    }
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Подготовка данных">

$aTabs[] = array(
    "DIV" =>  "tab1", 
    "TAB" =>  GetMessage("B1T_POSTEVENT_T_SETTINGS"), 
    "ICON"=>  "main_user_edit", 
    "TITLE"=> GetMessage("B1T_POSTEVENT_T_SETTINGS_MODULE")
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

$arAllOptions = array(
    GetMessage("B1T_POSTEVENT_T_SALE_LIST"),
    array("OrderListOverride", GetMessage("B1T_POSTEVENT_T_OVERRIDE_STANDART"),  "", array("checkbox", "Y")),
    array("OrderListShowPhoto", GetMessage("B1T_POSTEVENT_T_SHOW_PHOTO"),  "", array("checkbox", "Y")),
    array("OrderListShowLink", GetMessage("B1T_POSTEVENT_T_SHOW_LINK"),  "", array("checkbox", "Y")),
    array("OrderListShowParams", GetMessage("B1T_POSTEVENT_T_SHOW_PARAMS"),  "", array("checkbox", "Y")),
    GetMessage("B1T_POSTEVENT_T_AUTOADD"),
    array("AutoPayType", GetMessage("B1T_POSTEVENT_T_PAYMENT_TYPE"),  "", array("checkbox", "Y")),
    array("AutoDelivType", GetMessage("B1T_POSTEVENT_T_DELIVERY_TYPE"),  "", array("checkbox", "Y")),
    array("AutoComment", GetMessage("B1T_POSTEVENT_T_ORDER_COMMENT"),  "", array("checkbox", "Y"))
);

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Вывод формы">
?>

<form method="POST" Action="<?=$APPLICATION->GetCurPageParam()?>" ENCTYPE="multipart/form-data" name="post_form">
    <?=bitrix_sessid_post();?>
    <input type="hidden" name="lang" value="<?=LANG?>">

    <?
    $tabControl->Begin();

    $tabControl->BeginNextTab();

    __AdmSettingsDrawList($LOCAL_MODULE_NAME, $arAllOptions);

    $tabControl->Buttons();
    ?>

    <input type="submit" id="save_btn" class="adm-btn-save" value="<?=GetMessage("B1T_POSTEVENT_T_SAVE_BTN")?>">
    <input <?if ($POST_RIGHT < "W") echo "disabled" ?> type="submit" name="RestoreDefaults" title="<?echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>')" value="<?echo GetMessage("MAIN_RESTORE_DEFAULTS")?>">
    <?
    $tabControl->End();
    ?>
</form><?

// </editor-fold>

?>