<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h3>Яндекс</h3>
<p><a href='http://help.yandex.ru/mail/mail-clients.xml' target='_blank' rel='nofollow'>http://help.yandex.ru/mail/mail-clients.xml</a></p>
<ul>
 <li>адрес почтового сервера — <span class="tag codeph">smtp.yandex.ru</span>;</li>
 <li>защита соединения — <span class="tag">SSL</span>;</li>
 <li>порт — <span class="tag">465</span>;</li>
 <li>логин — <span class="tag">ваш почтовый адрес (yourname@yandex.ru)</span>.</li>
</ul>
<h3>Mail.ru</h3>
<p><a href='http://help.mail.ru/mail-help/mailer/mo' target='_blank' rel='nofollow'>http://help.mail.ru/mail-help/mailer/mo</a></p>
<ul>
 <li>адрес почтового сервера — <span class="tag codeph">smtp.mail.ru</span>;</li>
 <li>защита соединения — <span class="tag">SSL</span>;</li>
 <li>порт — <span class="tag">465</span>;</li>
 <li>логин — <span class="tag">ваш почтовый адрес (yourname@mail.ru)</span>.</li>
</ul>
<h3>Google Mail</h3>
<ul>
 <li>адрес почтового сервера — <span class="tag codeph">smtp.gmail.com</span>;</li>
 <li>использовать аутентификацию - <span class="tag">да</span>;</li> 
 <li>защита соединения — <span class="tag">SSL или TLS</span>;</li>
 <li>порт для TLS/STARTTLS — <span class="tag">587</span>;</li>
 <li>порт для SSL — <span class="tag">465</span>;</li>
 <li>логин — <span class="tag">ваш почтовый адрес (yourname@gmail.com)</span>.</li>
</ul>
<h4 style='color: red'>Важная информация для почты Google</h4>
<p>Из соображений безопасности ненадежные приложения блокируются, и при попытке войти в аккаунт появляется сообщение об ошибке "Неверный пароль". 
<a href='https://support.google.com/accounts/answer/6010255?hl=ru' target='_blank' rel='nofollow'>Подробнее здесь</a>
</p>