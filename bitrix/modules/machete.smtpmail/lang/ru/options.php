<?
/**
 * Языковые константы для файла options.php
 *
 */
 
$MESS['BTN_TEST_TITLE'] = "Проверить соединение";
$MESS['BTN_TEST_AJAX'] = "Проверка...";

 
$MESS['ERROR_TITLE'] = "Ошибка";
 
$MESS['FUNCTION_EXIST'] = 'Функция "custom_mail" существует. Возможно, она определена в файле /bitrix/php_interface/init.php, либо установлен другой модуль с аналогичными функциями. Модуль не может работать при таких условиях.';
$MESS['FUNCTION_EXIST_MIN'] = 'Функция "custom_mail" существует. Модуль не может работать при таких условиях.';
 
$MESS['TEST_MESSAGE_TITLE'] = 'Отладочная информация:';
 
$MESS['MAIN_TAB_SET'] = 'Параметры'; 
$MESS['MAIN_TAB_TITLE_SET'] = 'Отправка почты через протокол SMTP'; 

$MESS['TAB_SET2'] = 'Информация, примеры'; 
$MESS['TAB_SET2_TITLE'] = 'Настройки SMTP для популярных сервисов'; 

$MESS['TAB_SET3'] = 'О модуле'; 
$MESS['TAB_SET3_TITLE'] = 'Информация о модуле и разработчиках'; 

$MESS['testSuccesResults'] = "Отправка тестового письма прошла успешно.";
$MESS['testErrorResults'] = "Ошибки при отправке. Проверьте правильность настроек.";


$MESS['LABEL'] = 'Работать/не работать';

$MESS['BUTTON_APPLY']  = 'Применить';
$MESS['BUTTON_APPLY_TITLE']  = 'Сохранить изменения и остаться в форме';

$MESS['BUTTON_SAVE']  = 'Сохранить';
$MESS['BUTTON_SAVE_TITLE']  = 'Сохранить изменения и вернуться';

$MESS['BUTTON_RESET'] = 'Сбросить';
$MESS['BUTTON_RESET_TITLE']  = 'Сбросить изменения';

$MESS['GROUP_TITLE']  = 'Настройки соединения';
$MESS['USERLOGIN_GROUP_TITLE']  = 'Параметры пользователя';

$MESS['EMAIL_TITLE']  = 'Имя почтового ящика';

$MESS['PARAMS_EMAIL_NAME']  = 'Имя почтового ящика (например, ivan@mail.ru)';
$MESS['PARAMS_SERV_IMAP']  = 'Сервер входящей почты (IMAP-сервер)';
$MESS['PARAMS_PORT_IMAP']  = 'Порт для подключения по IMAP';

$MESS['PARAMS_SERV_POP']  = 'Сервер входящей почты (POP3-сервер)';
$MESS['PARAMS_PORT_POP']  = 'Порт для подключения по POP3';

$MESS['PARAMS_SERV_SMTP']  = 'Адрес почтового сервера (SMTP-сервер)';
$MESS['PARAMS_PORT_SMTP']  = 'Порт для подключения по SMTP (25, 465, ...)';
$MESS['PARAMS_TIMEOUT']  = 'Timeout соединения (сек)';

$MESS['PARAMS_SECURE_SMTP']  = 'Защита соединения (ssl, tls)';

$MESS['PARAMS_USERNAME'] = 'Имя пользователя (логин)';
$MESS['PARAMS_USERPASSWORD'] = 'Пароль';

$MESS['PARAMS_MODULE_ENABLED'] = 'Модуль включен';

// Тестовое письмо
$MESS['TEST_MESSAGE_SUBJECT'] = 'Тестовое письмо для проверки SMTP';
$MESS['TEST_MESSAGE'] = 'Это письмо отправлено для проверки подключения';


$MESS['TEST_INFO'] = '* Будет произведена попытка отправить письмо на собственный адрес.';
$MESS['SAVE_TEST_INFO'] = '* При сохранении настроек с параметром "Включен" будет произведена попытка отправить письмо на собственный адрес.';

$MESS['PARAMS_XMAILER'] = 'Программа-отправитель (заголовок X-Mailer)';
$MESS['PARAMS_PRIORITY'] = 'Приоритет (заголовок X-Priority)';
$MESS['PARAMS_REPLYTO'] = 'Обратный адрес (заголовок Reply-To)';

$MESS['ADVANCED_SETTINGS'] = "Дополнительные настройки";

$MESS['ADDITIONAL_HEADERS'] = "Дополнительные заголовки (разделить '\\n')";
$MESS['ADDITIONAL_HEADERS_INFO_TITLE'] = "Пример: ";
$MESS['ADDITIONAL_HEADERS_INFO'] = "Precedence: bulk\\nList-Unsubscribe:&lt;mymail@mydomain.ru&gt;";
?>


