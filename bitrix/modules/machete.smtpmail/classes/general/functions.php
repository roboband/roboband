<?
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// �������� ����� ����� SMTP

//�������� email �� ������
function machete_smtp_extract_emails($str){
    // ���������� ���������, ������� ��������� ��� email �� ������:
    $regexp = '/([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+/i';
    preg_match_all($regexp, $str, $m);

    return isset($m[0]) ? $m[0] : array();
}//machete_smtp_extract_emails

function machete_smtp_custom_mail_log($to, $subject, $message, $additional_headers, $additional_parameters) {
//�����������
$fp = fopen(dirname(__FILE__)."/mail.log", "a");
fwrite($fp, "\r\n==========================================\r\n");
fwrite($fp, $to." ".$subject." ".$message." ".$additional_headers." ".$additional_parameters);
fclose($fp);
 
return true;
}//custom_mail

//����� ������� �������� �����
function custom_mail($to, $subject, $message, $addh = "", $addp = "") {
// ���������� ���������� 
require_once (dirname(__FILE__).'/mail/PHPMailerAutoload.php');
try {
$mail = new PHPMailer();

 //��������� ��������� ������ (���� � ����������)
 $message_min = false;
 $charsetSearch = stripos($message, "charset=");
 if($charsetSearch>0) { 
  $charsetSearch = $charsetSearch+8; // charset=
  $message_min = substr($message, $charsetSearch);
  $charsetSearch2 = stripos($message_min, "\n");
  if($charsetSearch2>0) {
    $message_min = substr($message_min, 0, $charsetSearch2);
  }
 }
 
$mailCharset = SITE_CHARSET; //��������� ��-���������
if($message_min) {
 $mailCharset = trim($message_min);
}

//����� ��������
global $bDebug;
if(is_array($bDebug) && count($bDebug)>0) {
 $replyto = false;
 
 $from = $bDebug['from'];
 $from_full = "�������: �������� SMTP";
 $host = $bDebug['host'];
 $port = $bDebug['port'];
 $username = $bDebug['username'];
 $userpassword = $bDebug['userpassword'];
 $timeout = $bDebug['timeout'];
 $secure = $bDebug['secure']; 
 $mail->XMailer = $bDebug['xmailer'];
 $xpriority = $bDebug['priority']; 
 $replyto = $bDebug['replyto'];
 
 $smtp_additional_headers = $bDebug['add_headers']; 
 $mail->SMTPDebug = 2; 
 
} else {
 error_reporting(0); 
 // ��������� ��������� ���������� �� ��������
 $from = COption::GetOptionString('machete.smtpmail', 'params_email_name', false, SITE_ID);   
 $host = COption::GetOptionString('machete.smtpmail', 'params_serv_smtp', false, SITE_ID); //"smtp.mail.ru";
 $port = COption::GetOptionString('machete.smtpmail', 'params_port_smtp', false, SITE_ID); //"25"; 
 $username = COption::GetOptionString('machete.smtpmail', 'params_username', false, SITE_ID);  
 $userpassword = COption::GetOptionString('machete.smtpmail', 'params_userpassword', false, SITE_ID); 
 $timeout = COption::GetOptionString('machete.smtpmail', 'params_timeout', 3, SITE_ID);
 $secure = COption::GetOptionString('machete.smtpmail', 'params_secure', '', SITE_ID);
 $mail->XMailer = COption::GetOptionString('machete.smtpmail', 'params_x_mailer', '1c-Bitrix Mail Agent', SITE_ID);
 $xpriority = COption::GetOptionString('machete.smtpmail', 'params_priority', '3', SITE_ID); 
 $replyto = COption::GetOptionString('machete.smtpmail', 'params_replyto', '', SITE_ID); 
 $smtp_additional_headers = COption::GetOptionString('machete.smtpmail', 'params_add_headers', '', SITE_ID);
}

//�������� ���������
$mail->Host = $host;
$mail->SMTPSecure = strtolower($secure);
$mail->IsSMTP();
$mail->SMTPAuth = true;
$mail->Port = $port;
$mail->Username = $username;
$mail->Password = $userpassword;
$mail->Timeout = $timeout;
$mail->SMTPKeepAlive = true;

//��������� �����������
$arAddressesTo = explode(",", $to);
foreach($arAddressesTo as $addressTo) {
 $mail->AddAddress($addressTo); 
}

//��������� ����������
$replyto_flag = false; //��������� 

$arr = explode("\n", $addh);
if (is_array($arr)){
 foreach ($arr as $value) {
   if($value) {
    $arrr = explode(":", $value);
    switch($arrr[0]) {
      case "From": 
       $from_full = trim($arrr[1]);
       $pos = strpos($from_full, "<"); //�������� ��������� "�� ����"
       $from_full = substr($from_full, 0, $pos);
       unset($pos);
      break;      
      //��������� ����� ����� (CC � BCC)
      case "CC": 
       $arAddressesToCC = explode(",", $arrr[1]);
       foreach($arAddressesToCC as $addressToCC) { $mail->AddCC($addressToCC, '');}
       unset($arAddressesToCC);
      break;
      case "BCC": 
       $arAddressesToBCC = explode(",", $arrr[1]);
       foreach($arAddressesToBCC as $addressToBCC) { $mail->AddBCC($addressToBCC, '');}
       unset($arAddressesToBCC);
      break;
      case "Content-Type":
       $str = $arrr[1];
       //��������� ��������� ������
       $pos1 = stripos($str, "charset=");
       if($pos1>0) {
        //��������� ���� 
        $mailContentType = substr($str, 0, $pos1-2);
        $pos1 = $pos1+8; // charset=
        $str = substr($str, $pos1);
        $pos2 = stripos($str, ";");
        if($pos2>0) {
          $str = substr($str, 0, $pos2);
        }
        $mail->CharSet = trim($str); //������������� ��������� �� ��������� 
       }//����� ��������� ��������� ������    
       else { 
        $mailContentType = $str;
        $mail->CharSet = $mailCharset;
       } 
       //��� �����������
       $mail->ContentType = trim($mailContentType); 
       unset($str);
       unset($pos1);
       unset($pos2);
      break;
      case "X-Priority":
        if(!$xpriority && $arrr[1]) $xpriority = $arrr[1];
      break;
      case "Reply-To":
       $replyto_flag = true;
       $mail->AddCustomHeader($value);
      break;
      
      //��� ��������� ��������� ��������� �� �������
      default: $mail->AddCustomHeader($value);
    }//switch
   
   }//if value
 }
}

//���� ��� ��������� Reply-To � �������
if(!$replyto_flag && !empty($replyto)) {
 $arAddressesReplyTo = explode(",", $replyto);
 foreach($arAddressesReplyTo as $repl) { $mail->AddReplyTo($repl, '');}
 unset($arAddressesReplyTo);
 unset($repl); 
}

//�������������� ���������
if(!empty($smtp_additional_headers)) {
 $smtp_additional_headers = str_replace("\\n", "\n", $smtp_additional_headers);
 $new_addh = explode("\n", $smtp_additional_headers); 
 if(is_array($new_addh)){
  foreach ($new_addh as $value) {
   if($value) {
    $mail->AddCustomHeader($value); 
   }
  }
 }
 unset($new_addh);
 unset($smtp_additional_headers);
}

//���������
if($xpriority) {
 $mail->Priority = $xpriority;
} 

//�� ����
$from_full = iconv($mailCharset, $mail->CharSet."//TRANSLIT", $from_full);
$mail->From = $from;
$mail->FromName = $from_full;

//��������� ���� � ����
$mail->Body = $message;
$mail->Subject = $subject;

//�������� ������
if(is_array($bDebug) && count($bDebug)>0) {
 $status = $mail->send();
} else {
 $status = @$mail->send();
}

$mail->ClearAddresses();
$mail->ClearAttachments();
$mail->SmtpClose();

} catch (phpmailerException $e) {
 global $bDebug;//����� ��������
 if(is_array($bDebug) && count($bDebug)>0) { echo $e->errorMessage(); }
 $status = false;
} catch (Exception $e) { 
 global $bDebug;//����� ��������
 if(is_array($bDebug) && count($bDebug)>0) { echo $e->getMessage();}
 $status = false;
}

return $status;
} //end ����� custom_mail
?>