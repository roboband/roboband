<?
/**
 * ��������� ������ smtpmail
 *
 * @author  Machete
 * @since   01/04/2015
 *
 * @link    http://wachete.ru
 */
 
/**
 * ������������� ������
 */
$sModuleId  = 'machete.smtpmail';
$bDebug = false; //��� ������������
 
/**
 * ���������� ������ (��������� ��� � ����� include.php)
 */
CModule::IncludeModule($sModuleId);
 
/**
 * �������� ��������� (���� lang/ru/options.php)
 */
global $MESS;
IncludeModuleLangFile( __FILE__ );

// ������ ���� �����
$arAllOptions = Array(
	Array("params_email_name", GetMessage("PARAMS_EMAIL_NAME"), "", Array("text", 255)),	
	Array("params_serv_imap", GetMessage("PARAMS_SERV_IMAP"), "", Array("text", 255)),	
	Array("params_port_imap", GetMessage("PARAMS_PORT_IMAP"), "", Array("text", 255)),	
	Array("params_serv_pop", GetMessage("PARAMS_SERV_POP"), "", Array("text", 255)),
	Array("params_port_pop", GetMessage("PARAMS_PORT_POP"), "", Array("text", 255)),	
	Array("params_serv_smtp", GetMessage("PARAMS_SERV_SMTP"), "", Array("text", 255)),
	Array("params_port_smtp", GetMessage("PARAMS_PORT_SMTP"), "", Array("text", 255)),	
	Array("params_secure", GetMessage("PARAMS_SECURE_SMTP"), "", Array("text", 255)),		
	Array("params_username", GetMessage("PARAMS_USERNAME"), "", Array("text", 255)),
	Array("params_userpassword", GetMessage("PARAMS_USERPASSWORD"), "", Array("text", 255)),	
	Array("params_timeout", GetMessage("PARAMS_TIMEOUT"), "3", Array("text", 255)),
        Array("params_x_mailer", GetMessage("PARAMS_XMAILER"), "1c-Bitrix Mail Agent", Array("text", 255)),
        Array("params_priority", GetMessage("PARAMS_PRIORITY"), "3", Array("text", 255)),
        Array("params_replyto", GetMessage("PARAMS_REPLYTO"), "", Array("text", 255)),
        Array("params_add_headers", GetMessage("ADDITIONAL_HEADERS"), "", Array("text", 255))
);
$arAllOptions['ENABLED'] = Array("params_enabled", GetMessage("PARAMS_MODULE_ENABLED"), "N", Array("checkbox", "N"));

$bError = false;

if((COption::GetOptionString('machete.smtpmail', 'params_enabled', false, SITE_ID)!='Y') && function_exists("custom_mail")) {
$bError = true;
 ?>
<div class="adm-detail-toolbar" style='color: red; background: #fff; padding: 0 10px 15px 10px;'>
 <h1><?=GetMessage("ERROR_TITLE");?></h1>
 <?=GetMessage("FUNCTION_EXIST");?>
</div>  
<?}

///////////////////////////�������� ����������� ajax
if(isset($_POST['AJAX_TEST']) && $_POST['AJAX_TEST']=='Y' && check_bitrix_sessid()) {
 $APPLICATION->RestartBuffer();   
  
  global $bDebug;
  $bDebug = array();
  //����� ������ �� �������
  $bDebug['from'] = $_POST['params_email_name'];   
  $bDebug['host'] = $_POST['params_serv_smtp'];
  $bDebug['port'] = $_POST['params_port_smtp'];
  $bDebug['username'] = $_POST['params_username'];  
  $bDebug['userpassword'] = $_POST['params_userpassword']; 
  $bDebug['timeout'] = $_POST['params_timeout'];
  $bDebug['secure'] = $_POST['params_secure']; 
  $bDebug['xmailer'] = $_POST['params_x_mailer']; 
  $bDebug['priority'] = $_POST['params_priority'];  
  $bDebug['replyto'] = $_POST['params_replyto'];    
  $bDebug['add_headers'] = $_POST['params_add_headers'];
  
  $res = array();
  ob_start();
  
  //������� custom_mail ��� ����������
  if($bError) {
    $res['status'] = false;
    $res['html'] = GetMessage("FUNCTION_EXIST_MIN");
    echo json_encode($res);
    exit();
  }  
  include_once("classes/general/functions.php");
  $res['status'] = custom_mail($bDebug['from'], GetMessage("TEST_MESSAGE_SUBJECT"), GetMessage("TEST_MESSAGE"), "Content-Type: text/plain; charset=".SITE_CHARSET, "debug_smtp");
  $res['html'] = ob_get_contents();  
  unset($bDebug);     
  ob_end_clean();  
  
  //�������
  if($res['status'] && $res['status']>0) {
   echo 1;
  } else { //���������� ����������
   echo $res['html'];
  }   
  //echo json_encode($res); 
  
  unset($res);
 exit();
}
///////////////////////////����� �������� ����������� ajax

//������ ���������� ����������
if($REQUEST_METHOD == 'POST' && (!empty($_POST['Update']) || !empty($_POST['Apply'])) && check_bitrix_sessid()) {

    if($bError) {//������ - ������� ����������
      $_REQUEST['params_enabled'] = 'N';
     }
     
       foreach($arAllOptions as $arOption) {
			$name=$arOption[0];
			$val=$_REQUEST[$name];
			if($arOption[3][0]=="checkbox" && $val!="Y")
				$val="N";
			COption::SetOptionString($sModuleId, $name, $val, $arOption[1]);
	}

/////////////////////////// ������������ ����������� ��� ����������
if(isset($_REQUEST['params_enabled']) && $_REQUEST['params_enabled']=='Y' && !$bError) {
  //error_reporting(0);
  
  $saveTest = true;  
  $res = array();
  
  ob_start();
  // �������� ����������
  require_once("classes/general/functions.php");
  $from = COption::GetOptionString('machete.smtpmail', 'params_email_name', false, SITE_ID);    
  // ���������� �������� ������ �� ����������� �����  
  $res['status'] = custom_mail($from, GetMessage("TEST_MESSAGE_SUBJECT"), GetMessage("TEST_MESSAGE"), "Content-Type: text/plain; charset=".SITE_CHARSET, "debug_smtp");   

  // ���� ���� ������, ��������� ������
 if(!$res['status']) {  
  COption::SetOptionString($sModuleId, "params_enabled", "N",GetMessage("PARAMS_MODULE_ENABLED"));
  $res['html'] = ob_get_contents();
 } 
 unset($res);
 unset($from);
 ob_end_clean(); 
} // ����� ������������ ��� ����������

}//����� ���������� ����������

/**
 * ��������� ���� ���������������� ������ ��������
 */
$aTabs = array(
    array(
        'DIV'   => 'machete_smtpmail_edit1',
        'TAB'   => GetMessage('MAIN_TAB_SET'),
        'ICON'  => 'fileman_settings',
        'TITLE' => GetMessage('MAIN_TAB_TITLE_SET')
    ),
    array(
        'DIV'   => 'machete_smtpmail_info',
        'TAB'   => GetMessage('TAB_SET2'),
        'ICON'  => 'fileman_settings',
        'TITLE' => GetMessage('TAB_SET2_TITLE')
    ),
    array(
        'DIV'   => 'machete_smtpmail_about',
        'TAB'   => GetMessage('TAB_SET3'),
        'ICON'  => 'fileman_settings',
        'TITLE' => GetMessage('TAB_SET3_TITLE')
    ),      
);
 
/**
 * �������������� ����
 */
$oTabControl = new CAdmintabControl('tabControl', $aTabs);
$oTabControl->Begin();
 
/**
 * ���� ����� ����� �������� � ����������� ������
 */
?>
<?CUtil::InitJSCore(array('jquery'));?>


<?$oTabControl->BeginNextTab();?>

<form id='machete_settingsForm' method="POST" enctype="multipart/form-data" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars( $sModuleId )?>&lang=<?echo LANG?>">
    <?=bitrix_sessid_post()?>
	<tr>
        <td width="40%"><label for="option"><?echo GetMessage('PARAMS_MODULE_ENABLED');?>:</td>
        <td><input type="checkbox" name="params_enabled" <? if (COption::GetOptionString( $sModuleId, 'params_enabled', 'N' ) == 'Y'):?> checked="checked" <? endif; ?> value="Y" /></td>
    </tr>	

    <tr class="heading">
        <td colspan="2"><?=GetMessage('EMAIL_TITLE')?></td>
    </tr>	
	
    <tr>
        <td width="40%"><label for="option"><?echo GetMessage('PARAMS_EMAIL_NAME');?>:</td>
        <td><input size="50" type="text" name="params_email_name" value="<?=COption::GetOptionString( $sModuleId, 'params_email_name', '' );?>" /></td>
    </tr>
    <tr>	
	
    <tr class="heading">
        <td colspan="2"><?=GetMessage('GROUP_TITLE')?></td>
    </tr>

    <tr>
        <td width="40%"><label for="option"><?echo GetMessage('PARAMS_SERV_SMTP');?>:</td>
        <td><input size="50" type="text" name="params_serv_smtp" value="<?=COption::GetOptionString( $sModuleId, 'params_serv_smtp', '' );?>" /></td>
    </tr>
	<tr>
        <td width="40%"><label for="option"><?echo GetMessage('PARAMS_PORT_SMTP');?>:</td>
        <td><input size="50" type="text" name="params_port_smtp" value="<?=COption::GetOptionString( $sModuleId, 'params_port_smtp', '' );?>" /></td>
    </tr>
    
	<tr>
        <td width="40%"><label for="option"><?echo GetMessage('PARAMS_SECURE_SMTP');?>:</td>
        <td><input size="50" type="text" name="params_secure" value="<?=COption::GetOptionString( $sModuleId, 'params_secure', '' );?>" /></td>
    </tr>    
	
	<tr>
        <td width="40%"><label for="option"><?echo GetMessage('PARAMS_TIMEOUT');?>:</td>
        <td><input size="50" type="text" name="params_timeout" value="<?=COption::GetOptionString( $sModuleId, 'params_timeout', '4');?>" /></td>
    </tr>	
	
	
   <tr class="heading">
        <td colspan="2"><?=GetMessage('USERLOGIN_GROUP_TITLE')?></td>
    </tr>
	<tr>
        <td width="40%"><label for="option"><?echo GetMessage('PARAMS_USERNAME');?>:</td>
        <td><input size="50" type="text" name="params_username" value="<?=COption::GetOptionString( $sModuleId, 'params_username', '' );?>" /></td>
    </tr>
	<tr>
        <td width="40%"><label for="option"><?echo GetMessage('PARAMS_USERPASSWORD');?>:</td>
        <td><input size="50" type="password" name="params_userpassword" value="<?=COption::GetOptionString( $sModuleId, 'params_userpassword', '' );?>" /></td>
    </tr>
    
    
   <tr class="heading">
        <td colspan="2"><?=GetMessage('ADVANCED_SETTINGS')?></td>
    </tr>    
    <tr>
        <td width="40%"><label for="option"><?echo GetMessage('PARAMS_XMAILER');?>:</td>
        <td><input size="50" type="text" name="params_x_mailer" value="<?=COption::GetOptionString($sModuleId, 'params_x_mailer', '1c-Bitrix Mail Agent' );?>" /></td>
    </tr>    
    <tr>
        <td width="40%"><label for="option"><?echo GetMessage('PARAMS_PRIORITY');?>:</td>
        <td><input size="50" type="text" name="params_priority" value="<?=COption::GetOptionString($sModuleId, 'params_priority', '3' );?>" /></td>
    </tr>
    <tr>
        <td width="40%"><label for="option"><?echo GetMessage('PARAMS_REPLYTO');?>:</td>
        <td><input size="50" type="text" name="params_replyto" value="<?=COption::GetOptionString($sModuleId, 'params_replyto', '' );?>" /></td>
    </tr>    
    <tr>
        <td width="40%"><label for="option"><?echo GetMessage('ADDITIONAL_HEADERS');?>:</td>
        <td><input size="50" type="text" name="params_add_headers" value="<?=COption::GetOptionString($sModuleId, 'params_add_headers', '' );?>" placeholder="" /></td>
    </tr>   
    
    <tr> 
     <td width="40%"><label for="option"></td>
     <td><?echo GetMessage('ADDITIONAL_HEADERS_INFO_TITLE');?><span style='color:rgb(0, 93, 174)'><?echo GetMessage('ADDITIONAL_HEADERS_INFO');?></style></td>
    </tr>
    
    
    <tr>
      <td width="40%"></td>
      <td>
      <a href="javascript:void(0)" onclick="testSettings(this)" hidefocus="true" class="adm-btn"><?=GetMessage('BTN_TEST_TITLE')?></a><span id='testSettingsSpan' style='font-style: italic; padding: 0 10px; display: none;'><?=GetMessage('BTN_TEST_AJAX')?></span>
      <p style='margin: 10px 0 10px 0; color: rgb(100, 137, 0);'><?=GetMessage('TEST_INFO')?></p>
      </td>
    </tr>
    
    <tr id='testErrorResults' style='height: 30px; background: rgb(255, 195, 195); color: rgb(172, 50, 50); font-weight: bold; text-align: center; <?if(empty($res['html'])) {?>display: none;<?}?>'>
	 <td colspan="2"><?=GetMessage('testErrorResults')?></td>
    </tr>
    <tr id='testSuccesResults' style='height: 30px; background: rgb(192, 208, 139); color: rgb(66, 66, 66); font-weight: bold; text-align: center; display: none;'>
	 <td colspan="2"><?=GetMessage('testSuccesResults')?><?if($res['html']) echo $res['html'];?></td>
    </tr>
    
    <tr>
     <td colspan="2" id='testErrorResultsInfo' style='background: #fff; padding: 10px; display: none;'></td>
    </tr>
    
 
<?$oTabControl->BeginNextTab();?>
<?include_once('lang/ru/info.php');?>

<?$oTabControl->BeginNextTab();?>
<?include_once('lang/ru/about.php');?>


    <?$oTabControl->Buttons();?>
    <p style='margin: 5px 0 15px 0;'><?=GetMessage('SAVE_TEST_INFO')?></p>
    <input type="submit" name="Update" value="<?=GetMessage('BUTTON_SAVE')?>" title="<?=GetMessage('BUTTON_SAVE_TITLE')?>" class="adm-btn-save"/>
    <?/*<input type="submit" name="Apply" value="<?=GetMessage('BUTTON_APPLY')?>" title="<?=GetMessage('BUTTON_APPLY_TITLE')?>">    */?>
    <input type="reset" name="reset" value="<?= GetMessage('BUTTON_RESET')?>" title="<?=GetMessage('BUTTON_RESET_TITLE')?>" />
    <?/*<input type="hidden" name="Update" value="Y" />*/?>
	
</form>
<?$oTabControl->End();?>

<script>
function testSettings() { 
var request = $('#machete_settingsForm').serialize();
 $('#testSettingsSpan').show();
 $('#testErrorResults').hide();
 $('#testErrorResultsInfo').hide();
 $('#testSuccesResults').hide();

BX.ajax({
 url: '<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($sModuleId)?>&lang=<?echo LANG?>">',
 data: "AJAX_TEST=Y&"+request,
 method: 'POST',
 dataType: 'html', //json
 timeout: 10,
 async: true,
 processData: true,
 scriptsRunFirst: true,
 emulateOnload: true,
 start: true,
 cache: false,
 onsuccess: function(data){
   //console.log(data);
   
   $('#testSettingsSpan').hide();
   
   if(data == 1) {   
    $('#testErrorResults').hide();
    $('#testSuccesResults').show();
    $('#testErrorResultsInfo').hide();
   } else {
    $('#testSuccesResults').hide();
    $('#testErrorResults').show();
    $('#testErrorResultsInfo').html(data).show();
   }
   
 },
 onfailure: function(data){
  //console.log("ajax error: "+ data);
  $('#testSettingsSpan').hide();
 }
}); 

 return false;
}
</script>
