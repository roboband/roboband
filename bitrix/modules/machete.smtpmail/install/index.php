<?
/**
 * ����������� ������ smtpmail
 *
 * @author  Machete
 * @since   01/04/2015
 *
 * @link    http://wachete.ru
 */
 
/**
 * ���������� �������� ���������
 */
global $MESS;

$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-18);
@include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));
IncludeModuleLangFile($strPath2Lang."/install/index.php");
 
class machete_smtpmail extends CModule {
 
    const MODULE_ID = 'machete.smtpmail';
    var $MODULE_ID = 'machete.smtpmail';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
 
    /**
     * ������������� ������ ��� �������� "���������� ��������"
     */
   function __construct() {
	$arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");
        
        $this->PARTNER_NAME = GetMessage('PARTNER_NAME');
        $this->PARTNER_URI = GetMessage('PARTNER_URI');
        
        $this->MODULE_NAME = GetMessage('SMTP_MODULE_NAME'); 
        $this->MODULE_DESCRIPTION = GetMessage("SMTP_MODULE_DESC");
	
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
    }

    public function DoInstall() {
        if(!$this->InstallDB() || !$this->InstallEvents() || !$this->InstallFiles()) {
            return;
        } 
        RegisterModule($this->MODULE_ID);
    }
 
   public function DoUninstall() {
        if( !$this->UnInstallDB() || !$this->UnInstallEvents() || !$this->UnInstallFiles() ) {
            return;
        }
        UnRegisterModule($this->MODULE_ID);
    }
 
    public function InstallEvents() {
	
	    RegisterModuleDependences("main", "OnPageStart", $this->MODULE_ID, "CSMTPClass", "loading");
	
        return true;
    }
 
    public function UnInstallEvents() {
	    UnRegisterModuleDependences("main", "OnPageStart", $this->MODULE_ID, "CSMTPClass", "loading");
	
        return true;
    }
 
    public function InstallFiles() {
        return true;
    }
 
    public function UnInstallFiles() {
        return true;
    }
 
    public function InstallDB() {
        return true;
    }
 
    public function UnInstallDB() {
        return true;
    }
}

?>