<?
$MESS["PORTAL_WIZARD_NAME"] = "Аспро: Крутойшоп, интернет магазин";
$MESS["PORTAL_WIZARD_DESC"] = "Мастер создания интернет-магазина";

$MESS["KSHOP_MOD_INST_OK"] = "Поздравляем, модуль «Аспро: Крутойшоп, интернет магазин» успешно установлен!<br />
Для установки готового сайта, пожалуйста перейдите <a href='/bitrix/admin/wizard_list.php?lang=ru'>в список мастеров</a> <br />и выберите пункт «Установить» в меню мастера aspro:kshop";
$MESS["MOD_UNINST_OK"] = "Удаление модуля успешно завершено";
$MESS["SCOM_INSTALL_NAME_KSHOP"] = "Аспро: Крутойшоп, интернет магазин";
$MESS["SCOM_INSTALL_DESCRIPTION_KSHOP"] = "Мастер создания интернет-магазина «Аспро: Крутойшоп»";
$MESS["SCOM_INSTALL_TITLE"] = "Установка модуля";
$MESS["SCOM_UNINSTALL_TITLE"] = "Удаление модуля";
$MESS["SPER_PARTNER"] = "Аспро";
$MESS["PARTNER_URI"] = "http://www.aspro.ru";
$MESS["OPEN_WIZARDS_LIST"] = "Открыть список мастеров";
$MESS["INSTALL_KSHOP_SITE"] = "Установить готовый сайт";
?>