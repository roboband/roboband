<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="switcher_preload_images"><img src="<?=$this->GetFolder()?>/images/gear_small.png"><img src="<?=$this->GetFolder()?>/images/gear_big.png"></div>
<div class="style-switcher <?=$_COOKIE["styleSwitcher"] == 'open' ? 'active' : ''?>">
	<div class="header"><?=GetMessage("THEME_MODIFY")?><span class="switch"><div class="rotate_block"><i class="img_big"></i><i class="img_small"></i></div></span></div>
	<?foreach($arResult["ITEMS"] as $optionKey=>$option):?>
		<div class="block">
			<div class="block-title"><?=$option["NAME"];?></div>
			<div class="options" name="<?=$option["ID"]?>">
				<?foreach ($option["VALUES"] as $key => $value):?>
					<a href="<?=$APPLICATION->GetCurPageParam(strToLower($option["ID"]).'='.strToLower($value["VALUE"]), array(strToLower($option["ID"])))?>" name="<?=$value["VALUE"]?>" 
						<?if($value["VALUE"] == $option["CURRENT_VALUE"] || $option["TYPE"]=="COLOR"):?> class="<?endif;?><?if($value["VALUE"] == $option["CURRENT_VALUE"]):?>active<?endif;?><?if ($option["TYPE"]=="COLOR"):?> color<?endif;?><?if($value["VALUE"] == $option["CURRENT_VALUE"] || $option["TYPE"]=="COLOR"):?>"<?endif;?>>
						<? 
							switch ($option["TYPE"]) 
							{
								case "COLOR":?><div class="color_block" style="background: <?=$value["COMPONENT_VALUE"]?>" title="<?=$value["NAME"]?>"></div><?break; 
								case "TEXT":?><?=$value["NAME"]?><?break;
							}	
						?>
					</a>
				<?endforeach;?>
			</div>
		</div>
	<?endforeach;?>
	<div class="block">
		<div class="buttons">
			<a class="reset" href="<?=$APPLICATION->GetCurPageParam('theme=default', array('theme', 'color', 'width', 'menu', 'sidemenu'))?>"><?=GetMessage("THEME_RESET")?></a>
		</div>
	</div>
</div>