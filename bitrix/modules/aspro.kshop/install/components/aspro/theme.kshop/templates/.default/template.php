<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(false);?>
<div class="switcher_preload_images"><img src="<?=$this->GetFolder()?>/images/gear_small.png"><img src="<?=$this->GetFolder()?>/images/gear_big.png"></div>
<div class="style-switcher">
	<div class="header"><?=GetMessage("THEME_MODIFY")?><span class="switch"><div class="rotate_block"><i class="img_big"></i><i class="img_small"></i></div></span></div>
	<form method="POST" name="style-switcher">
		<?foreach($arResult["ITEMS"] as $optionKey=>$option):?>
			<div class="block">
				<div class="block-title"><?=$option["NAME"];?></div>
				<div class="options" name="<?=$option["ID"]?>">
					<input type="hidden" name="<?=strToLower($option["ID"])?>" value="<?=strToLower($option["CURRENT_VALUE"])?>" />
					<?foreach($option["VALUES"] as $key => $value):?>
						<a class="<?=($option["TYPE"] == "COLOR" ? 'color' : '')?> <?=($value["VALUE"] == $option["CURRENT_VALUE"] ? 'active' : '')?>" href="javascript:;" name="<?=$value["VALUE"]?>" data-option-id="<?=strToLower($option["ID"])?>" data-option-value="<?=strToLower($value["VALUE"])?>">
							<?if($option["TYPE"] == "COLOR"):?>
								<div class="color_block" style="background: <?=$value["COMPONENT_VALUE"]?>" title="<?=$value["NAME"]?>"></div>
							<?elseif($option["TYPE"] == "TEXT"):?>
								<?=$value["NAME"]?>
							<?endif;?>
						</a>
					<?endforeach;?>
				</div>
			</div>
		<?endforeach;?>
		<div class="block">
			<div class="buttons">
				<a class="reset" href="javascript:;"><?=GetMessage("THEME_RESET")?></a>
			</div>
		</div>
		<script type="text/javascript">
		$(document).ready(function(){
			if($.cookie('styleSwitcher') == 'open'){
				$('.style-switcher').addClass('active');
			}
			
			$('.style-switcher .switch').click(function(e){
				e.preventDefault();
				var styleswitcher = $(this).closest('.style-switcher');
				if(styleswitcher.hasClass('active')){
					styleswitcher.animate({left: '-' + styleswitcher.outerWidth() + 'px'}, 300).removeClass('active');
					$.removeCookie('styleSwitcher', {path: '/'});
				}
				else{
					styleswitcher.animate({left: '0'}, 300).addClass('active');
					$.cookie('styleSwitcher', 'open', {path: '/'});
				}
			});
			
			$('.style-switcher .reset').click(function(e){
				$('form[name=style-switcher]').append('<input type="hidden" name="theme" value="default" />');
				$('form[name=style-switcher]').submit();
			});
			
			var intervalIDsmall;
			var intervalIDbig;
			var angleSmall = 0;
			var angleBig = 0;
			var gearSmall = $(this).find('.style-switcher .rotate_block i.img_small');
			var gearBig = $(this).find('.style-switcher .rotate_block i.img_big');
			
			$('.style-switcher .options a').click(function(e){
				$(this).addClass('active').siblings().removeClass('active');
				$('form[name=style-switcher] input[name=' + $(this).data('option-id') + ']').val($(this).data('option-value'));
				$('form[name=style-switcher]').submit();
				intervalIDsmall = setInterval(function(){angleSmall-=3; $(gearSmall).rotate(angleSmall); },50);
				intervalIDbig = setInterval(function(){angleBig+=3; $(gearBig).rotate(angleBig); },50);
			});
			
			$('.style-switcher .rotate_block').hover(
				function(){  
					intervalIDsmall = setInterval(function(){angleSmall-=3; $(gearSmall).rotate(angleSmall); },50);
					intervalIDbig = setInterval(function(){angleBig+=3; $(gearBig).rotate(angleBig); },50);
				},
				function(){ 
					clearInterval(intervalIDsmall); 
					clearInterval(intervalIDbig); 
				}
			);
		});
		</script>
	</form>
</div>