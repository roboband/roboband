<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("iblock"))
	return;

// get DB charset
if($result = @mysql_query('SHOW VARIABLES LIKE "character_set_database";')){
	$arResult = mysql_fetch_row($result);
	$isUTF8 = $arResult[1] == 'utf8';
}	

// current IBLOCK_IDs
$arIblocks = $arSites = array();
$dbRes = CIBlock::GetList(array(), array("ACTIVE" => "Y"));
while($item = $dbRes->Fetch()){
	$arIblocks[$item["LID"]][$item["IBLOCK_TYPE_ID"]][$item["CODE"]][] = $item["ID"];
}

// current SITEs
$dbRes = CSite::GetList($by="sort", $order="desc", array("ACTIVE" => "Y"));
while($item = $dbRes->Fetch()){
	$arSites[$item["LID"]] = $item;
}
	
$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/catalog.xml"; 
$iblockXMLFilePrices = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/catalog_prices.xml"; 
$iblockCode = "aspro_kshop_catalog_".WIZARD_SITE_ID; 
$iblockCheckInstallCode = "aspro_kshop_catalog"; 
$iblockType = "aspro_kshop_catalog"; 

$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
$arIBlock = $rsIBlock->Fetch();
$iblockID = $arIBlock["ID"]; 

// check iblock user field UF_SECTION_TEMPLATE
$arUserFieldSectionTemplate = CUserTypeEntity::GetList(array(), array('ENTITY_ID' => 'IBLOCK_'.$iblockID.'_SECTION', 'FIELD_NAME' => 'UF_SECTION_TEMPLATE'))->Fetch();
$resUserFieldSectionTemplateEnum = CUserFieldEnum::GetList(array(), array('USER_FIELD_ID' => $arUserFieldSectionTemplate['ID']));
while($arUserFieldSectionTemplateEnum = $resUserFieldSectionTemplateEnum->GetNext()){
	$obEnum = new CUserFieldEnum;
	$obEnum->SetEnumValues($arUserFieldSectionTemplate['ID'], array($arUserFieldSectionTemplateEnum['ID'] => array('DEL' => 'Y')));
}
$obEnum = new CUserFieldEnum;
$obEnum->SetEnumValues($arUserFieldSectionTemplate['ID'], array(
	'n0' => array(
		'VALUE' => ($isUTF8 ? iconv('CP1251', 'UTF-8', '�������') : '�������'),
		'XML_ID' => 'block',
	),
	'n1' => array(
		'VALUE' => ($isUTF8 ? iconv('CP1251', 'UTF-8', '�������') : '�������'),
		'XML_ID' => 'list',
	),
	'n2' => array(
		'VALUE' => ($isUTF8 ? iconv('CP1251', 'UTF-8', '��������') : '��������'),
		'XML_ID' => 'table',
	),
));
$resUserFieldSectionTemplateEnum = CUserFieldEnum::GetList(array(), array('USER_FIELD_ID' => $arUserFieldSectionTemplate['ID']));
while($arUserFieldSectionTemplateEnum = $resUserFieldSectionTemplateEnum->GetNext()){
	$arUserFieldSectionTemplateEnums[$arUserFieldSectionTemplateEnum['XML_ID']] = $arUserFieldSectionTemplateEnum['ID'];
}
$bs = new CIBlockSection;
$resDB = CIBlockSection::GetList(array(), array('CODE' => 'sukhie_stroitelnye_smesi'), false, array('ID'));
while($arRes = $resDB->Fetch()){
	$res = $bs->Update($arRes["ID"], array("UF_SECTION_TEMPLATE" => $arUserFieldSectionTemplateEnums['list']));
}

// update iblock catalog propperties BRAND & SERVICES set LINK_IBLOCK_ID to actual
if(WIZARD_SITE_ID && $arIblocks){
	foreach($arSites as $siteID => $arSite){
		if($IBLOCK_ID = $arIblocks[WIZARD_SITE_ID]["aspro_kshop_catalog"]['aspro_kshop_catalog_'.WIZARD_SITE_ID][0]){
			if($IBLOCK_BRAND_ID = $arIblocks[WIZARD_SITE_ID]["aspro_kshop_content"]['aspro_kshop_brands_'.$siteID][0]){
				$dbRes = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $IBLOCK_ID, "CODE" => "BRAND"));
				while ($arProp = $dbRes->GetNext()){
					if($propID = $arProp['ID']){
						$arProp = array("NAME" => $arProp["NAME"], "ACTIVE" => $arProp["ACTIVE"], "SORT" => $arProp["SORT"], "CODE" => $arProp["CODE"], "XML_ID" => $arProp["XML_ID"], "SEARCHABLE" => $arProp["SEARCHABLE"], "HINT" => $arProp["HINT"], "WITH_DESCRIPTION" => $arProp['WITH_DESCRIPTION'], "FILTRABLE" => $arProp["FILTRABLE"], "MULTIPLE" => $arProp["MULTIPLE"], "DEFAULT_VALUE" => $arProp["DEFAULT_VALUE"], "IBLOCK_ID" => $arProp["IBLOCK_ID"], "SMART_FILTER" => "Y", "PROPERTY_TYPE" => $arProp["PROPERTY_TYPE"], "LINK_IBLOCK_ID" => $IBLOCK_BRAND_ID);
						$ibp = new CIBlockProperty;
						$ibp->Update($propID, $arProp);
					}
				}
				
				// update LINK_ID in goods
				$arOldBrandIDs = $arOldBrandNAMEs = $arNewBrandIDs = $arReplaceID = $arNeedToReplaceInGoodsIDs = array();
				$arRes = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $IBLOCK_ID, '!PROPERTY_BRAND_VALUE' => false), false, false, array('ID', 'PROPERTY_BRAND'));
				while($item = $arRes->Fetch()){
					$arOldBrandIDs[] = $arNeedToReplaceInGoods[$item['ID']] = $item['PROPERTY_BRAND_VALUE'];
				}
				$arOldBrandIDs = array_unique($arOldBrandIDs);
				
				if($arOldBrandIDs){
					$arRes = CIBlockElement::GetList(array(), array('ID' => $arOldBrandIDs), false, false, array('ID', 'IBLOCK_ID', 'NAME'));
					while($item = $arRes->Fetch()){
						if($item['IBLOCK_ID'] != $IBLOCK_BRAND_ID){
							$arOldBrandNAMEs[$item['ID']] = $item['NAME'];
						}
					}
				}
				
				if($arOldBrandNAMEs){
					$arRes = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $IBLOCK_BRAND_ID, 'NAME' => $arOldBrandNAMEs), false, false, array('ID', 'NAME'));
					while($item = $arRes->Fetch()){
						$arNewBrandIDs[$item['NAME']] = $item['ID'];
					}
				}
				
				foreach($arOldBrandNAMEs as $obID => $obN){
					if(isset($arNewBrandIDs[$obN])){
						$arReplaceID[$obID] = $arNewBrandIDs[$obN];
					}
				}
				
				if($arNeedToReplaceInGoods && $arReplaceID){
					foreach($arNeedToReplaceInGoods as $ID => $obID){
						if(isset($arReplaceID[$obID])){
							CIBlockElement::SetPropertyValuesEx($ID, $IBLOCK_ID, array('BRAND' => $arReplaceID[$obID]));
						}
					}
				}
			}
			
			if($IBLOCK_SERVICES_ID = $arIblocks[WIZARD_SITE_ID]["aspro_kshop_content"]['aspro_kshop_services_'.WIZARD_SITE_ID][0]){
				$dbRes = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $IBLOCK_ID, "CODE" => "SERVICES"));
				while ($arProp = $dbRes->GetNext()){
					if($propID = $arProp['ID']){
						$arProp = array("NAME" => $arProp["NAME"], "ACTIVE" => $arProp["ACTIVE"], "SORT" => $arProp["SORT"], "CODE" => $arProp["CODE"], "XML_ID" => $arProp["XML_ID"], "SEARCHABLE" => $arProp["SEARCHABLE"], "HINT" => $arProp["HINT"], "WITH_DESCRIPTION" => $arProp['WITH_DESCRIPTION'], "FILTRABLE" => $arProp["FILTRABLE"], "MULTIPLE" => $arProp["MULTIPLE"], "DEFAULT_VALUE" => $arProp["DEFAULT_VALUE"], "IBLOCK_ID" => $arProp["IBLOCK_ID"], "SMART_FILTER" => "N", "PROPERTY_TYPE" => $arProp["PROPERTY_TYPE"], "LINK_IBLOCK_ID" => $IBLOCK_SERVICES_ID);
						$ibp = new CIBlockProperty;
						$ibp->Update($propID, $arProp);
					}
				}
				
				// update LINK_ID in goods
				$arOldServiceIDs = $arOldServiceNAMEs = $arNewServiceIDs = $arReplaceID = $arNeedToReplaceInGoodsIDs = array();
				$arRes = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $IBLOCK_ID, '!PROPERTY_SERVICES_VALUE' => false), false, false, array('ID', 'PROPERTY_SERVICES'));
				while($item = $arRes->Fetch()){
					$arOldServiceIDs[] = $arNeedToReplaceInGoods[$item['ID']] =$item['PROPERTY_SERVICES_VALUE'];
				}
				$arOldServiceIDs = array_unique($arOldServiceIDs);
				
				if($arOldServiceIDs){
					$arRes = CIBlockElement::GetList(array(), array('ID' => $arOldServiceIDs), false, false, array('ID', 'IBLOCK_ID', 'NAME'));
					while($item = $arRes->Fetch()){
						if($item['IBLOCK_ID'] != $IBLOCK_SERVICES_ID){
							$arOldServiceNAMEs[$item['ID']] = $item['NAME'];
						}
					}
				}
				
				if($arOldServiceNAMEs){
					$arRes = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $IBLOCK_SERVICES_ID, 'NAME' => $arOldServiceNAMEs), false, false, array('ID', 'NAME'));
					while($item = $arRes->Fetch()){
						$arNewServiceIDs[$item['NAME']] = $item['ID'];
					}
				}
				
				foreach($arOldServiceNAMEs as $obID => $obN){
					if(isset($arNewServiceIDs[$obN])){
						$arReplaceID[$obID] = $arNewServiceIDs[$obN];
					}
				}
				
				if($arNeedToReplaceInGoods && $arReplaceID){
					foreach($arNeedToReplaceInGoods as $ID => $obID){
						if(isset($arReplaceID[$obID])){
							CIBlockElement::SetPropertyValuesEx($ID, $IBLOCK_ID, array('SERVICES' => $arReplaceID[$obID]));
						}
					}
				}
			}
		}
	}
}
?>