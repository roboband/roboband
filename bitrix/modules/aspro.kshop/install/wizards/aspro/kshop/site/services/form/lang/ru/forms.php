<?
	$MESS["FAQ_FORM_NAME"] = "Задать вопрос";
	$MESS["FAQ_BUTTON_NAME"] = "Отправить";
	$MESS["FAQ_FORM_QUESTION_1"] = "Ваше имя";
	$MESS["FAQ_FORM_QUESTION_2"] = "Контактный телефон";
	$MESS["FAQ_FORM_QUESTION_3"] = "E-mail";
	$MESS["FAQ_FORM_QUESTION_4"] = "Вопрос";
	
	$MESS["FEEDBACK_FORM_NAME"] = "Обратная связь";
	$MESS["FEEDBACK_BUTTON_NAME"] = "Отправить";
	$MESS["FEEDBACK_FORM_QUESTION_1"] = "Ваше имя";
	$MESS["FEEDBACK_FORM_QUESTION_2"] = "Контактный телефон";
	$MESS["FEEDBACK_FORM_QUESTION_3"] = "E-mail";
	$MESS["FEEDBACK_FORM_QUESTION_4"] = "Вопрос";
	$MESS["FEEDBACK_FORM_QUESTION_5"] = "Сообщение";
	
	$MESS["TOORDER_FORM_NAME"] = "Товар под заказ";
	$MESS["TOORDER_BUTTON_NAME"] = "Отправить";
	$MESS["TOORDER_FORM_QUESTION_1"] = "Ваше имя";
	$MESS["TOORDER_FORM_QUESTION_2"] = "Контактный телефон";
	$MESS["TOORDER_FORM_QUESTION_3"] = "E-mail";
	$MESS["TOORDER_FORM_QUESTION_4"] = "Название товара";
	$MESS["TOORDER_FORM_QUESTION_5"] = "ID товара";
	$MESS["TOORDER_FORM_QUESTION_6"] = "Сообщение";
	
	$MESS["RESUME_FORM_NAME"] = "Резюме";
	$MESS["RESUME_BUTTON_NAME"] = "Отправить";
	$MESS["RESUME_FORM_QUESTION_1"] = "Ваше имя";
	$MESS["RESUME_FORM_QUESTION_2"] = "Контактный телефон";
	$MESS["RESUME_FORM_QUESTION_3"] = "E-mail";
	$MESS["RESUME_FORM_QUESTION_4"] = "Желаемая должность";
	$MESS["RESUME_FORM_QUESTION_5"] = "Прикрепить файл";
	$MESS["RESUME_FORM_QUESTION_6"] = "Текст резюме";
	
	$MESS["FAQ_FORM_DESCRIPTION"] = "";
	
	$MESS["EVENT_NEW_ONE_CLICK_BUY_NAME"] = "Новый быстрый заказ";
	$MESS["EVENT_NEW_ONE_CLICK_BUY_DESCRIPTION"] = "#CLIENT_NAME# - Имя посетителя\n
#PHONE# - Телефон
#ORDER_ITEMS# - Состав заказа
#ORDER_PRICE# - Стоимость заказа
#COMMENT# - Комментарий
#RS_DATE_CREATE# - Дата и время создания заказа
#RS_ORDER_ID# - ID заказа";
	
	$MESS["EVENT_NEW_FAQ_NAME"] = "Новый вопрос с сайта";
	$MESS["EVENT_NEW_FAQ_DESCRIPTION"] = "#CLIENT_NAME# - Имя посетителя\n
#PHONE# - Телефон
#EMAIL# - Email
#QUESTION# - Вопрос";

	$MESS["EVENT_NEW_FEEDBACK_NAME"] = "Новый вопрос с сайта";
	$MESS["EVENT_NEW_FEEDBACK_DESCRIPTION"] = "#CLIENT_NAME# - Имя посетителя\n
#PHONE# - Телефон
#EMAIL# - Email
#QUESTION# - Вопрос";
	
	
	$MESS["NEW_FEEDBACK_EMAIL_SUBJECT"] = "Вопрос через обратную связь с сайта #SITE_NAME#";
	$MESS["NEW_FEEDBACK_EMAIL_TEXT"] = "Новый вопрос на сайте #SITE_NAME# (#RS_RESULT_ID#)<br />
Имя посетителя: #CLIENT_NAME#<br />
Телефон:#PHONE#<br />
Email:#EMAIL#<br />
Вопрос:#POST#<br /><br />
	
Вопрос отправлен: #RS_DATE_CREATE#
Просмотр результата на сайте: <a href='http://#SERVER_NAME#/bitrix/admin/form_result_edit.php?lang=ru&WEB_FORM_ID=#RS_FORM_ID#&RESULT_ID=#RS_RESULT_ID#&WEB_FORM_NAME=#RS_FORM_NAME#' target='_blank'>http://#SERVER_NAME#/bitrix/admin/form_result_edit.php?lang=ru&WEB_FORM_ID=#RS_FORM_ID#&RESULT_ID=#RS_RESULT_ID#&WEB_FORM_NAME=#RS_FORM_NAME#</a>";
	
	$MESS["EVENT_NEW_TOORDER_NAME"] = "Новый заказ товара под заказ с сайта";
	$MESS["EVENT_NEW_TOORDER_DESCRIPTION"] = "#CLIENT_NAME# - Имя посетителя\n
#PHONE# - Телефон
#EMAIL# - Email
#PRODUCT_NAME# - Название товара
#PRODUCT_ID# - ID товара
#MESSAGE# - Сообщение";
	
	
	$MESS["NEW_TOORDER_EMAIL_SUBJECT"] = "Заполнена форма \"Товар под заказ\" на сайте #SITE_NAME#";
	$MESS["NEW_TOORDER_EMAIL_TEXT"] = "Заполнена форма \"Товар под заказ\" на сайте #SITE_NAME# (#RS_RESULT_ID#)<br />
Имя посетителя: #CLIENT_NAME#<br />
Телефон: #PHONE#<br />
Email: #EMAIL#<br />
Название товара: #PRODUCT_NAME#<br />
ID товара: #PRODUCT_ID#<br />
Сообщение: #MESSAGE#<br /><br />
	
Запрос отправлен: #RS_DATE_CREATE#
Просмотр результата на сайте: <a href='http://#SERVER_NAME#/bitrix/admin/form_result_edit.php?lang=ru&WEB_FORM_ID=#RS_FORM_ID#&RESULT_ID=#RS_RESULT_ID#&WEB_FORM_NAME=#RS_FORM_NAME#' target='_blank'>http://#SERVER_NAME#/bitrix/admin/form_result_edit.php?lang=ru&WEB_FORM_ID=#RS_FORM_ID#&RESULT_ID=#RS_RESULT_ID#&WEB_FORM_NAME=#RS_FORM_NAME#</a>";
	
	
	$MESS["NEW_QUESTION_EMAIL_SUBJECT"] = "Вопрос с сайта #SITE_NAME#";
	$MESS["NEW_QUESTION_EMAIL_TEXT"] = "Новый вопрос на сайте #SITE_NAME# (#RS_RESULT_ID#)<br />
Имя посетителя: #CLIENT_NAME#<br />
Телефон:#PHONE#<br />
Email:#EMAIL#<br />
Вопрос:#QUESTION#<br /><br />
	
Вопрос отправлен: #RS_DATE_CREATE#
Просмотр результата на сайте: <a href='http://#SERVER_NAME#/bitrix/admin/form_result_edit.php?lang=ru&WEB_FORM_ID=#RS_FORM_ID#&RESULT_ID=#RS_RESULT_ID#&WEB_FORM_NAME=#RS_FORM_NAME#' target='_blank'>http://#SERVER_NAME#/bitrix/admin/form_result_edit.php?lang=ru&WEB_FORM_ID=#RS_FORM_ID#&RESULT_ID=#RS_RESULT_ID#&WEB_FORM_NAME=#RS_FORM_NAME#</a>";
	
	$MESS["NEW_RESUME_EMAIL_SUBJECT"] = "Новое резюме с сайта #SITE_NAME#";
	$MESS["NEW_RESUME_EMAIL_TEXT"] = "Новое резюме с сайта #SITE_NAME# (#RS_RESULT_ID#)<br />
Имя посетителя: #CLIENT_NAME#<br />
Телефон:#PHONE#<br />
Email:#EMAIL#<br />
Желаемая должность:#POST#<br />
Файл резюме:#FILE#<br />
Текст резюме:#RESUME_TEXT#<br /><br />
	
Резюме отправлено: #RS_DATE_CREATE#
Просмотр результата на сайте: <a href='http://#SERVER_NAME#/bitrix/admin/form_result_edit.php?lang=ru&WEB_FORM_ID=#RS_FORM_ID#&RESULT_ID=#RS_RESULT_ID#&WEB_FORM_NAME=#RS_FORM_NAME#' target='_blank'>http://#SERVER_NAME#/bitrix/admin/form_result_edit.php?lang=ru&WEB_FORM_ID=#RS_FORM_ID#&RESULT_ID=#RS_RESULT_ID#&WEB_FORM_NAME=#RS_FORM_NAME#</a>";
	
	
	$MESS["NEW_PRODUCT_REQUEST_EMAIL_SUBJECT"] = "Заявка на товар с сайта #SITE_NAME#";
	$MESS["NEW_PRODUCT_REQUEST_EMAIL_TEXT"] =  "Новая заявка на товар на сайте (#RS_RESULT_ID#)<br />
Имя посетителя: #CLIENT_NAME#<br />
Телефон:#PHONE#<br />
Товар:#PRODUCT#<br />
Комментарий:#COMMENT#<br /><br />
	
Заявка отправлена: #RS_DATE_CREATE#
Просмотр результата на сайте: <a href='http://#SERVER_NAME#/bitrix/admin/form_result_edit.php?lang=ru&WEB_FORM_ID=#RS_FORM_ID#&RESULT_ID=#RS_RESULT_ID#&WEB_FORM_NAME=#RS_FORM_NAME#' target='_blank'>http://#SERVER_NAME#/bitrix/admin/form_result_edit.php?lang=ru&WEB_FORM_ID=#RS_FORM_ID#&RESULT_ID=#RS_RESULT_ID#&WEB_FORM_NAME=#RS_FORM_NAME#</a>";
	
	
	$MESS["NEW_ONE_CLICK_BUY_EMAIL_SUBJECT"] = "Новый быстрый заказ с сайта #SITE_NAME#";
	$MESS["NEW_ONE_CLICK_BUY_EMAIL_TEXT"] =    "Новый быстрый заказ с сайта #SITE_NAME# (#RS_ORDER_ID#)<br />
Имя посетителя: #CLIENT_NAME#<br />
Телефон:#PHONE#<br />
Состав заказа:#ORDER_ITEMS#<br />
Стоимость заказа:#ORDER_PRICE#<br />
Комментарий:#COMMENT#<br /><br />
	
Заказ создан: #RS_DATE_CREATE#
Просмотр заказа на сайте: <a href='http://#SERVER_NAME#/bitrix/admin/sale_order_detail.php?ID=#RS_ORDER_ID#' target='_blank'>http://#SERVER_NAME#/bitrix/admin/sale_order_detail.php?ID=#RS_ORDER_ID#</a>";
?>