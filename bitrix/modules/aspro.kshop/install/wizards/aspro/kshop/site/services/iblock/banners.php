<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("iblock"))	return;	
	
function rewriteBannersType ($bannersIblockType, $bannersIblockID)
{
	$iblockCode = "aspro_kshop_banner_types_".WIZARD_SITE_ID; 
	$iblockType = "aspro_kshop_adv";
	$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType, "SITE_ID"=>WIZARD_SITE_ID));
	$iblockID = false;
	$iblockType = false;
	if ($arIBlock = $rsIBlock->Fetch())
	{
		$iblockID = $arIBlock["ID"];
		$iblockType = $arIBlock["IBLOCK_TYPE_ID"];
	}
	
	$rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),array("IBLOCK_TYPE" => $iblockType, "IBLOCK_ID" => $iblockID), false, false,array());
	$arTypes = array();
	while( $arElement = $rsElement->GetNext() ) { $arTypes[] = $arElement; }

	$arSelect = array("ID", "IBLOCK_TYPE", "SORT", "SITE_ID", "IBLOCK_ID", "CODE", "PROPERTY_URL_STRING", "PROPERTY_BUTTON1LINK", "PROPERTY_BUTTON2LINK");
	$rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),array("IBLOCK_TYPE" => $bannersIblockType, "IBLOCK_ID" => $bannersIblockID, "SITE_ID"=>WIZARD_SITE_ID), false, false, $arSelect);
	while( $arElement = $rsElement->GetNext() ) 
	{ 
		$arReplace = array();
		if (strpos($arElement["PROPERTY_URL_STRING_VALUE"], "#SITE_DIR#") !== false)
		{
			$arReplace["URL_STRING"] = str_replace( "#SITE_DIR#", WIZARD_SITE_DIR, $arElement["PROPERTY_URL_STRING_VALUE"] );
		}
		if (strpos($arElement["PROPERTY_BUTTON1LINK_VALUE"], "#SITE_DIR#") !== false)
		{
			$arReplace["BUTTON1LINK"] = str_replace( "#SITE_DIR#", WIZARD_SITE_DIR, $arElement["PROPERTY_BUTTON1LINK_VALUE"] );
		}
		if (strpos($arElement["PROPERTY_BUTTON2LINK_VALUE"], "#SITE_DIR#") !== false)
		{
			$arReplace["BUTTON2LINK"] = str_replace( "#SITE_DIR#", WIZARD_SITE_DIR, $arElement["PROPERTY_BUTTON2LINK_VALUE"] );
		}
		if (is_array($arTypes))
		{
			foreach($arTypes  as $key=> $arType) { if ($arType["CODE"]==$arElement["CODE"]) { $arReplace["TYPE_BANNERS"] = $arType["ID"]; } }	
		}	
		CIBlockElement::SetPropertyValuesEx( $arElement["ID"], $bannersIblockID, $arReplace); 
	}
}	
	
	

$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID;

if( COption::GetOptionString("aspro.kshop", "wizard_installed", "N", WIZARD_SITE_ID) == "Y" && !WIZARD_INSTALL_DEMO_DATA ){
	$iblockCode = "aspro_kshop_banners_".WIZARD_SITE_ID; 
	$iblockType = "aspro_kshop_adv";
	
	$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
	$iblockID = false; 
	if ($arIBlock = $rsIBlock->Fetch()){
		$iblockID = $arIBlock["ID"];
		WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_BANNERS_TYPE" => $iblockType));
		WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_BANNERS_ID" => $iblockID));
		CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/catalog/main/section.php", array("IBLOCK_BANNERS_TYPE" => $iblockType));
		CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/catalog/main/element.php", array("IBLOCK_BANNERS_TYPE" => $iblockType));
		CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/catalog/main/section.php", array("IBLOCK_BANNERS_ID" => $iblockID));
		CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/catalog/main/element.php", array("IBLOCK_BANNERS_ID" => $iblockID));
		
		
		rewriteBannersType($iblockType, $iblockID);
		
	}
	return;
}

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/banners.xml"; 
$iblockCode = "aspro_kshop_banners_".WIZARD_SITE_ID; 
$iblockCheckInstallCode = "aspro_kshop_banners"; 
$iblockType = "aspro_kshop_adv"; 

$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
$iblockID = false; 
if ($arIBlock = $rsIBlock->Fetch())
{
	$iblockID = $arIBlock["ID"]; 
	if (WIZARD_INSTALL_DEMO_DATA)
	{
		CIBlock::Delete($arIBlock["ID"]); 
		$iblockID = false; 
	}
}



if($iblockID == false)
{
	$permissions = Array(
			"1" => "X",
			"2" => "R"
		);
	$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "content_editor"));
	if($arGroup = $dbGroup -> Fetch())
	{
		$permissions[$arGroup["ID"]] = 'W';
	};
	$iblockID = WizardServices::ImportIBlockFromXML(
		$iblockXMLFile,
		$iblockCode,
		$iblockType,
		WIZARD_SITE_ID,
		$permissions
	);

	if ($iblockID < 1)
		return;
	
	//IBlock fields
	$iblock = new CIBlock;
	$arFields = array('ACTIVE' => 'Y', 
	'FIELDS' => array (
	  'IBLOCK_SECTION' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'ACTIVE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'Y',
	  ),
	  'ACTIVE_FROM' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'ACTIVE_TO' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SORT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '0',
	  ),
	  'PREVIEW_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'FROM_DETAIL' => 'N',
		  'SCALE' => 'Y',
		  'WIDTH' => 800,
		  'HEIGHT' => 800,
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'DELETE_WITH_DETAIL' => 'N',
		  'UPDATE_WITH_DETAIL' => 'N',
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'PREVIEW_TEXT_TYPE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'text',
	  ),
	  'PREVIEW_TEXT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'DETAIL_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'SCALE' => 'N',
		  'WIDTH' => '',
		  'HEIGHT' => '',
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'DETAIL_TEXT_TYPE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'text',
	  ),
	  'DETAIL_TEXT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'XML_ID' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'CODE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'UNIQUE' => 'N',
		  'TRANSLITERATION' => 'N',
		  'TRANS_LEN' => 100,
		  'TRANS_CASE' => 'L',
		  'TRANS_SPACE' => '-',
		  'TRANS_OTHER' => '-',
		  'TRANS_EAT' => 'Y',
		  'USE_GOOGLE' => 'N',
		),
	  ),
	  'TAGS' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_NAME' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'FROM_DETAIL' => 'N',
		  'SCALE' => 'N',
		  'WIDTH' => '',
		  'HEIGHT' => '',
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'DELETE_WITH_DETAIL' => 'N',
		  'UPDATE_WITH_DETAIL' => 'N',
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'SECTION_DESCRIPTION_TYPE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'text',
	  ),
	  'SECTION_DESCRIPTION' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_DETAIL_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'SCALE' => 'N',
		  'WIDTH' => '',
		  'HEIGHT' => '',
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'SECTION_XML_ID' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_CODE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'UNIQUE' => 'N',
		  'TRANSLITERATION' => 'N',
		  'TRANS_LEN' => 100,
		  'TRANS_CASE' => 'L',
		  'TRANS_SPACE' => '-',
		  'TRANS_OTHER' => '-',
		  'TRANS_EAT' => 'Y',
		  'USE_GOOGLE' => 'N',
		),
	  ),
	  'LOG_SECTION_ADD' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_SECTION_EDIT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_SECTION_DELETE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_ELEMENT_ADD' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_ELEMENT_EDIT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_ELEMENT_DELETE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	),
	'CODE' => $iblockCode, 
	'XML_ID' => $iblockCode);
	$iblock->Update($iblockID, $arFields);
}
else
{
	$arSites = array(); 
	$db_res = CIBlock::GetSite($iblockID);
	while ($res = $db_res->Fetch())
		$arSites[] = $res["LID"]; 
	if (!in_array(WIZARD_SITE_ID, $arSites))
	{
		$arSites[] = WIZARD_SITE_ID;
		$iblock = new CIBlock;
		$iblock->Update($iblockID, array("LID" => $arSites));
	}
}

//iblock user fields begin

	$dbSite = CSite::GetByID(WIZARD_SITE_ID);
	if($arSite = $dbSite -> Fetch()) $lang = $arSite["LANGUAGE_ID"];
	if(strlen($lang) <= 0) $lang = "ru";
	WizardServices::IncludeServiceLang("user_options.php", $lang);
	
	$db_prop = CIBlockProperty::GetList(array(), array("IBLOCK_ID"=>$iblockID));
	$arIblockProps = array();
	while ($prop = $db_prop->GetNext())
	{
		$arIblockProps[$prop["CODE"]] = $prop["ID"];
	}
	
	
	CUserOptions::SetOption( "form", "form_element_".$iblockID, array(
	"tabs" => "edit1--#--".GetMessage("FORM_FORM_ELEMENT_551_BANNER")."--,--ACTIVE--#--".GetMessage("FORM_FORM_ELEMENT_551_AKTIVNOST")."--,--ACTIVE_FROM--#--"
	.GetMessage("FORM_FORM_ELEMENT_551_NACHALO_AKTIVNOSTI")."--,--ACTIVE_TO--#--".GetMessage("FORM_FORM_ELEMENT_551_OKONCHANIE_AKTIVNOSTI")."--,--SORT--#--"
	.GetMessage("FORM_FORM_ELEMENT_551_SORTIROVKA")."--,--NAME--#--".GetMessage("FORM_FORM_ELEMENT_551_NAZVANIE")."--,--PROPERTY_".$arIblockProps["TYPE_BANNERS"]."--#--".GetMessage("FORM_FORM_ELEMENT_551_TIP_BANNERA")
	."--,--PREVIEW_PICTURE--#--".GetMessage("FORM_FORM_ELEMENT_551_AKTIVNAYA_KARTINKA")."--,--DETAIL_PICTURE--#--".GetMessage("FORM_FORM_ELEMENT_551_FON")."--,--PROPERTY_".$arIblockProps["TEXT_POSITION"]."--#--"
	.GetMessage("FORM_FORM_ELEMENT_551_RASPOLOZHENIE_TEKSTA")."--,--PROPERTY_".$arIblockProps["TARGETS"]."--#--".GetMessage("FORM_FORM_ELEMENT_551_TIP_PEREKHODA_TARGET")."--,--PROPERTY_"	.$arIblockProps["TEXTCOLOR"]."--#--"
	.GetMessage("FORM_FORM_ELEMENT_551_TSVET_TEKSTA")
	."--,--PROPERTY_".$arIblockProps["URL_STRING"]."--#--".GetMessage("FORM_FORM_ELEMENT_551_SSYLKA_S_BANNERA")."--,--PROPERTY_".$arIblockProps["BUTTON1TEXT"]."--#--".GetMessage("FORM_FORM_ELEMENT_551_TEKST_PERVOY_KNOPKI")
	."--,--PROPERTY_".$arIblockProps["BUTTON1LINK"]."--#--".GetMessage("FORM_FORM_ELEMENT_551_SSYLKA_S_PERVOY_KNOPKI")."--,--PROPERTY_".$arIblockProps["BUTTON2TEXT"]."--#--".GetMessage("FORM_FORM_ELEMENT_551_TEKST_VTOROY_KNOPKI")
	."--,--PROPERTY_".$arIblockProps["BUTTON2LINK"]."--#--".GetMessage("FORM_FORM_ELEMENT_551_SSYLKA_S_VTOROY_KNOPKI")."--,--PREVIEW_TEXT--#--".GetMessage("FORM_FORM_ELEMENT_551_TEKST_BANNERA")
	."--;--edit14--#--SEO--,--IPROPERTY_TEMPLATES_ELEMENT_META_TITLE--#--".GetMessage("FORM_FORM_ELEMENT_551_SHABLON_META_TITLE")
	."--,--IPROPERTY_TEMPLATES_ELEMENT_META_KEYWORDS--#--".GetMessage("FORM_FORM_ELEMENT_551_SHABLON_META_KEYWORDS")."--,--IPROPERTY_TEMPLATES_ELEMENT_META_DESCRIPTION--#--"
	.GetMessage("FORM_FORM_ELEMENT_551_SHABLON_META_DESCRIPTION")."--,--IPROPERTY_TEMPLATES_ELEMENT_PAGE_TITLE--#--".GetMessage("FORM_FORM_ELEMENT_551_ZAGOLOVOK_TOVARA")
	."--,--IPROPERTY_TEMPLATES_ELEMENTS_PREVIEW_PICTURE--#----".GetMessage("FORM_FORM_ELEMENT_551_NASTROYKI_DLYA_KARTINOK_ANONSA_ELEMENTOV")
	."--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_ALT--#--".GetMessage("FORM_FORM_ELEMENT_551_SHABLON_ALT")."--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_TITLE--#--"
	.GetMessage("FORM_FORM_ELEMENT_551_SHABLON_TITLE")."--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_NAME--#--".GetMessage("FORM_FORM_ELEMENT_551_SHABLON_IMENI_FAYLA")
	."--,--IPROPERTY_TEMPLATES_ELEMENTS_DETAIL_PICTURE--#----".GetMessage("FORM_FORM_ELEMENT_551_NASTROYKI_DLYA_DETALNYKH_KARTINOK_ELEMENTOV")."--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_ALT--#--"
	.GetMessage("FORM_FORM_ELEMENT_551_SHABLON_ALT")."--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_TITLE--#--".GetMessage("FORM_FORM_ELEMENT_551_SHABLON_TITLE")."--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_NAME--#--"
	.GetMessage("FORM_FORM_ELEMENT_551_SHABLON_IMENI_FAYLA")."--,--SEO_ADDITIONAL--#----".GetMessage("FORM_FORM_ELEMENT_551_DOPOLNITELNO")."--,--TAGS--#--".GetMessage("FORM_FORM_ELEMENT_551_TEGI")."--;--" ));					 

//iblock user fields end

WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_BANNERS_TYPE" => $iblockType));
WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_BANNERS_ID" => $iblockID));
CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/catalog/main/section.php", array("IBLOCK_BANNERS_TYPE" => $iblockType));
CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/catalog/main/element.php", array("IBLOCK_BANNERS_TYPE" => $iblockType));
CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/catalog/main/section.php", array("IBLOCK_BANNERS_ID" => $iblockID));
CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/catalog/main/element.php", array("IBLOCK_BANNERS_ID" => $iblockID));

rewriteBannersType ($iblockType, $iblockID);

?>