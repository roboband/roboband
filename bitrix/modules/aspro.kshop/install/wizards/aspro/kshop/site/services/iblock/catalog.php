<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("iblock"))
	return;

if(!CModule::IncludeModule("catalog"))
	return;

$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID;

if( COption::GetOptionString("aspro.kshop", "wizard_installed", "N", WIZARD_SITE_ID) == "Y" && !WIZARD_INSTALL_DEMO_DATA ){
	$iblockCode = "aspro_kshop_catalog_".WIZARD_SITE_ID; 
	$iblockType = "aspro_kshop_catalog";
	
	$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
	$iblockID = false; 
	if ($arIBlock = $rsIBlock->Fetch()){
		$iblockID = $arIBlock["ID"];
		WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_CATALOG_TYPE" => $iblockType));
		WizardServices::ReplaceMacrosRecursive($bitrixTemplateDir, array("IBLOCK_CATALOG_TYPE" => $iblockType));
		WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_CATALOG_ID" => $iblockID));
		WizardServices::ReplaceMacrosRecursive($bitrixTemplateDir, array("IBLOCK_CATALOG_ID" => $iblockID));
		CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/search.form/top/template.php", array("IBLOCK_CATALOG_TYPE" => $iblockType));
		CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/search.form/top/template.php", array("IBLOCK_CATALOG_ID" => $iblockID));
	}
	return;
}

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/catalog.xml"; 
$iblockXMLFilePrices = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/catalog_prices.xml"; 
$iblockCode = "aspro_kshop_catalog_".WIZARD_SITE_ID; 
$iblockCheckInstallCode = "aspro_kshop_catalog"; 
$iblockType = "aspro_kshop_catalog"; 

$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
$iblockID = false; 
if ($arIBlock = $rsIBlock->Fetch())
{
	$iblockID = $arIBlock["ID"]; 
	if (WIZARD_INSTALL_DEMO_DATA)
	{
		$arCatalog = CCatalog::GetByIDExt($arIBlock["ID"]); 
		if (is_array($arCatalog) && (in_array($arCatalog['CATALOG_TYPE'],array('P','X'))) == true) 
		{
			CCatalog::UnLinkSKUIBlock($arIBlock["ID"]);
			CIBlock::Delete($arCatalog['OFFERS_IBLOCK_ID']);
		}
		CIBlock::Delete($arIBlock["ID"]); 
		$iblockID = false; 
		COption::SetOptionString("ishop", "demo_deleted", "N", "", WIZARD_SITE_ID);
		
		$dbRes = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => 'IBLOCK_'.$arIBlock["ID"].'_SECTION'));
		while ($arRes = $dbRes->Fetch())
		{
			$userType = new CUserTypeEntity();
			$userType->Delete($arRes["ID"]);
		}
	}
}

$dbResultList = CCatalogGroup::GetList(Array(), Array("CODE" => "BASE"));
if(!($dbResultList->Fetch()))
{
	$arFields = Array();
	$rsLanguage = CLanguage::GetList($by, $order, array());
	while($arLanguage = $rsLanguage->Fetch())
	{
		WizardServices::IncludeServiceLang("catalog.php", $arLanguage["ID"]);
		$arFields["USER_LANG"][$arLanguage["ID"]] = GetMessage("WIZ_PRICE_NAME");
	}
	$arFields["BASE"] = "Y";
	$arFields["SORT"] = 100;
	$arFields["NAME"] = "BASE";
	$arFields["USER_GROUP"] = Array(1, 2);
	$arFields["USER_GROUP_BUY"] = Array(1, 2);
	CCatalogGroup::Add($arFields);
}

if($iblockID == false)
{
	$permissions = Array(
			"1" => "X",
			"2" => "R"
		);
	$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "sale_administrator"));
	if($arGroup = $dbGroup -> Fetch())
	{
		$permissions[$arGroup["ID"]] = 'W';
	}
	$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "content_editor"));
	if($arGroup = $dbGroup -> Fetch())
	{
		$permissions[$arGroup["ID"]] = 'W';
	}
	$iblockID = WizardServices::ImportIBlockFromXML(
		$iblockXMLFile,
		$iblockCode,
		$iblockType,
		WIZARD_SITE_ID,
		$permissions
	);
	$iblockID1 = WizardServices::ImportIBlockFromXML(
		$iblockXMLFilePrices,
		$iblockCode,
		$iblockType."_prices",
		WIZARD_SITE_ID,
		$permissions
	);
	
	if ($iblockID < 1)
		return;
	
	//IBlock fields
	$iblock = new CIBlock;
	$arFields = array('ACTIVE' => 'Y', 
	'FIELDS' => array (
	  'IBLOCK_SECTION' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'ACTIVE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'Y',
	  ),
	  'ACTIVE_FROM' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '=today',
	  ),
	  'ACTIVE_TO' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SORT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '0',
	  ),
	  'PREVIEW_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'FROM_DETAIL' => 'Y',
		  'SCALE' => 'Y',
		  'WIDTH' => 165,
		  'HEIGHT' => 165,
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'DELETE_WITH_DETAIL' => 'Y',
		  'UPDATE_WITH_DETAIL' => 'Y',
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'PREVIEW_TEXT_TYPE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'text',
	  ),
	  'PREVIEW_TEXT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'DETAIL_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'SCALE' => 'N',
		  'WIDTH' => '',
		  'HEIGHT' => '',
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'DETAIL_TEXT_TYPE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'text',
	  ),
	  'DETAIL_TEXT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'XML_ID' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'CODE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'UNIQUE' => 'Y',
		  'TRANSLITERATION' => 'Y',
		  'TRANS_LEN' => 100,
		  'TRANS_CASE' => 'L',
		  'TRANS_SPACE' => '_',
		  'TRANS_OTHER' => '_',
		  'TRANS_EAT' => 'Y',
		  'USE_GOOGLE' => 'N',
		),
	  ),
	  'TAGS' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_NAME' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'FROM_DETAIL' => 'Y',
		  'SCALE' => 'Y',
		  'WIDTH' => 120,
		  'HEIGHT' => 120,
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'DELETE_WITH_DETAIL' => 'Y',
		  'UPDATE_WITH_DETAIL' => 'Y',
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'SECTION_DESCRIPTION_TYPE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 'text',
	  ),
	  'SECTION_DESCRIPTION' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_DETAIL_PICTURE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => 
		array (
		  'SCALE' => 'N',
		  'WIDTH' => '',
		  'HEIGHT' => '',
		  'IGNORE_ERRORS' => 'N',
		  'METHOD' => 'resample',
		  'COMPRESSION' => 95,
		  'USE_WATERMARK_TEXT' => 'N',
		  'WATERMARK_TEXT' => '',
		  'WATERMARK_TEXT_FONT' => '',
		  'WATERMARK_TEXT_COLOR' => '',
		  'WATERMARK_TEXT_SIZE' => '',
		  'WATERMARK_TEXT_POSITION' => 'tl',
		  'USE_WATERMARK_FILE' => 'N',
		  'WATERMARK_FILE' => '',
		  'WATERMARK_FILE_ALPHA' => '',
		  'WATERMARK_FILE_POSITION' => 'tl',
		  'WATERMARK_FILE_ORDER' => NULL,
		),
	  ),
	  'SECTION_XML_ID' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => '',
	  ),
	  'SECTION_CODE' => 
	  array (
		'IS_REQUIRED' => 'Y',
		'DEFAULT_VALUE' => 
		array (
		  'UNIQUE' => 'Y',
		  'TRANSLITERATION' => 'Y',
		  'TRANS_LEN' => 100,
		  'TRANS_CASE' => 'L',
		  'TRANS_SPACE' => '_',
		  'TRANS_OTHER' => '_',
		  'TRANS_EAT' => 'Y',
		  'USE_GOOGLE' => 'N',
		),
	  ),
	  'LOG_SECTION_ADD' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_SECTION_EDIT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_SECTION_DELETE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_ELEMENT_ADD' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_ELEMENT_EDIT' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	  'LOG_ELEMENT_DELETE' => 
	  array (
		'IS_REQUIRED' => 'N',
		'DEFAULT_VALUE' => NULL,
	  ),
	),
	'CODE' => $iblockCode, 
	'XML_ID' => $iblockCode);
	$iblock->Update($iblockID, $arFields);
	
	//user fields for sections	
	$arLanguages = Array();
	$rsLanguage = CLanguage::GetList($by, $order, array());
	while($arLanguage = $rsLanguage->Fetch())
		$arLanguages[] = $arLanguage["LID"];
	
	$arUserFields = array("UF_SECTION_TEMPLATE", "UF_SECTION_DESCR");
	foreach ($arUserFields as $userField)
	{
		$arLabelNames = Array();
		foreach($arLanguages as $languageID)
		{
			WizardServices::IncludeServiceLang("property_names.php", $languageID);
			$arLabelNames[$languageID] = GetMessage($userField);
		}
	
		$arProperty["EDIT_FORM_LABEL"] = $arLabelNames;
		$arProperty["LIST_COLUMN_LABEL"] = $arLabelNames;
		$arProperty["LIST_FILTER_LABEL"] = $arLabelNames;
	
		$dbRes = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => 'IBLOCK_'.$iblockID.'_SECTION', "FIELD_NAME" => $userField));
		if ($arRes = $dbRes->Fetch())
		{
			$userType = new CUserTypeEntity();
			$userType->Update($arRes["ID"], $arProperty);
		}
		//if($ex = $APPLICATION->GetException())
			//$strError = $ex->GetString();
	}
}
else
{
	$arSites = array(); 
	$db_res = CIBlock::GetSite($iblockID);
	while ($res = $db_res->Fetch())
		$arSites[] = $res["LID"]; 
	if (!in_array(WIZARD_SITE_ID, $arSites))
	{
		$arSites[] = WIZARD_SITE_ID;
		$iblock = new CIBlock;
		$iblock->Update($iblockID, array("LID" => $arSites));
	}
}

//iblock user fields begin
	$dbSite = CSite::GetByID(WIZARD_SITE_ID);
	if($arSite = $dbSite -> Fetch()) $lang = $arSite["LANGUAGE_ID"];
	if(strlen($lang) <= 0) $lang = "ru";
	WizardServices::IncludeServiceLang("user_options.php", $lang);
	
	$db_prop = CIBlockProperty::GetList(array(), array("IBLOCK_ID"=>$iblockID));
	$arIblockProps = array();
	while ($prop = $db_prop->GetNext())
	{
		$arIblockProps[$prop["CODE"]] = $prop["ID"];
	}
	
	/*CUserOptions::SetOption( "form", "form_element_".$iblockID, array(
	"tabs" => "edit1--#--".GetMessage("FORM_FORM_ELEMENT_536_TOVAR")."--,--ACTIVE--#--".GetMessage("FORM_FORM_ELEMENT_536_AKTIVNOST")."--,--ACTIVE_FROM--#--".GetMessage("FORM_FORM_ELEMENT_536_NACHALO_AKTIVNOSTI")
	."--,--ACTIVE_TO--#--".GetMessage("FORM_FORM_ELEMENT_536_OKONCHANIE_AKTIVNOSTI")."--,--NAME--#--".GetMessage("FORM_FORM_ELEMENT_536_NAZVANIE")."--,--CODE--#--".GetMessage("FORM_FORM_ELEMENT_536_SIMVOLNYY_KOD")
	."--,--SORT--#--".GetMessage("FORM_FORM_ELEMENT_536_SORTIROVKA")."--,--PROPERTY_4773--#--".GetMessage("FORM_FORM_ELEMENT_536_KHIT_PRODAZH")."--,--PROPERTY_4774--#--".GetMessage("FORM_FORM_ELEMENT_536_SOVETUEM")
	."--,--PROPERTY_4775--#--".GetMessage("FORM_FORM_ELEMENT_536_NOVINKA")."--,--PROPERTY_4776--#--".GetMessage("FORM_FORM_ELEMENT_536_AKTSIYA")."--,--PROPERTY_4779--#--".GetMessage("FORM_FORM_ELEMENT_536_BREND")
	."--,--PROPERTY_4882--#--".GetMessage("FORM_FORM_ELEMENT_536_POKAZYVAT_NA_GLAVNOY")."--,--IBLOCK_ELEMENT_PROPERTY--#--".GetMessage("FORM_FORM_ELEMENT_536_ZNACHENIYA_SVOYSTV")."--,--IBLOCK_ELEMENT_PROP_VALUE--#----"
	.GetMessage("FORM_FORM_ELEMENT_536_ZNACHENIYA_SVOYSTV")."--,--PROPERTY_4780--#--".GetMessage("FORM_FORM_ELEMENT_536_TIP")."--,--PROPERTY_4781--#--".GetMessage("FORM_FORM_ELEMENT_536_TSVET")
	."--,--PROPERTY_4784--#--".GetMessage("FORM_FORM_ELEMENT_536_VES_KG")."--,--PROPERTY_4786--#--".GetMessage("FORM_FORM_ELEMENT_536_ARTIKUL")."--,--PROPERTY_4782--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_SHIRINA_VSPASHKI_SM")."--,--PROPERTY_4783--#--".GetMessage("FORM_FORM_ELEMENT_536_MOSHCHNOST_DVIGATELYA_L_S")."--,--PROPERTY_4838--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_TOLSHCHINA_SLOYA_DO_MM")."--,--PROPERTY_4892--#--".GetMessage("FORM_FORM_ELEMENT_536_KOLICHESTVO_KOMMENTARIEV_K_ELEMENTU")."--,--PROPERTY_4883--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_KOLICHESTVO_PROGOLOSOVAVSHIKH")."--,--PROPERTY_4797--#--".GetMessage("FORM_FORM_ELEMENT_536_NALICHIE_DISKA")."--,--PROPERTY_4792--#--".GetMessage("FORM_FORM_ELEMENT_536_POPULYARNOST")
	."--,--PROPERTY_4884--#--".GetMessage("FORM_FORM_ELEMENT_536_SUMMA_OTSENOK")."--,--PROPERTY_4891--#--".GetMessage("FORM_FORM_ELEMENT_536_TEMA_FORUMA_DLYA_KOMMENTARIEV")."--,--PROPERTY_4796--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_SHIRINA_SKASHIVANIYA_SM")."--,--PROPERTY_4798--#--".GetMessage("FORM_FORM_ELEMENT_536_YEMKOST_AKKUMULYATORA_A_CH")."--,--PROPERTY_4799--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_GLUBINA_PROPILA_MM")."--,--PROPERTY_4800--#--".GetMessage("FORM_FORM_ELEMENT_536_PLOSHCHAD_OBRABOTKI_M2")."--,--PROPERTY_4802--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_RAZMER_PATRONA_MM")."--,--PROPERTY_4803--#--".GetMessage("FORM_FORM_ELEMENT_536_MOSHCHNOST_VT")."--,--PROPERTY_4852--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_AERATOR")."--,--PROPERTY_4832--#--".GetMessage("FORM_FORM_ELEMENT_536_VOSPROIZVEDENIE_VIDEO")."--,--PROPERTY_4844--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_VREMYA_VYSYKHANIYA_CH")."--,--PROPERTY_4836--#--".GetMessage("FORM_FORM_ELEMENT_536_VREMYA_ZATVERDEVANIYA")."--,--PROPERTY_4807--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_VREMYA_RABOTY")."--,--PROPERTY_4850--#--".GetMessage("FORM_FORM_ELEMENT_536_VYPUSK")."--,--PROPERTY_4820--#--".GetMessage("FORM_FORM_ELEMENT_536_GRIL")
	."--,--PROPERTY_4833--#--".GetMessage("FORM_FORM_ELEMENT_536_DIAGONAL_DISPLEYA")."--,--PROPERTY_4831--#--".GetMessage("FORM_FORM_ELEMENT_536_DIAMETR_FILTRA")."--,--PROPERTY_4848--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_ZERKALO")."--,--PROPERTY_4851--#--".GetMessage("FORM_FORM_ELEMENT_536_MATERIAL")."--,--PROPERTY_4830--#--".GetMessage("FORM_FORM_ELEMENT_536_MODEL")
	."--,--PROPERTY_4821--#--".GetMessage("FORM_FORM_ELEMENT_536_MOSHCHNOST_GRILYA_VT")."--,--PROPERTY_4823--#--".GetMessage("FORM_FORM_ELEMENT_536_MOSHCHNOST_NAGREVA_VT")."--,--PROPERTY_4822--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_MOSHCHNOST_SVCH_VT")."--,--PROPERTY_4842--#--".GetMessage("FORM_FORM_ELEMENT_536_NAIMENOVANIE_DEKORA")."--,--PROPERTY_4815--#--".GetMessage("FORM_FORM_ELEMENT_536_NOSITELI")
	."--,--PROPERTY_4811--#--".GetMessage("FORM_FORM_ELEMENT_536_OBRABOTKA_SIGNALA")."--,--PROPERTY_4817--#--".GetMessage("FORM_FORM_ELEMENT_536_OBEM_MOROZILNOY_KAMERY")."--,--PROPERTY_4816--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_OBEM_KHOLODILNOY_KAMERY")."--,--PROPERTY_4819--#--".GetMessage("FORM_FORM_ELEMENT_536_OBEM_L")."--,--PROPERTY_4825--#--".GetMessage("FORM_FORM_ELEMENT_536_OPTICHESKOE_UVELICHENIE")
	."--,--PROPERTY_4849--#--".GetMessage("FORM_FORM_ELEMENT_536_PODVODKA")."--,--PROPERTY_4809--#--".GetMessage("FORM_FORM_ELEMENT_536_PODDERZHKA_HD")."--,--PROPERTY_4847--#--".GetMessage("FORM_FORM_ELEMENT_536_PODDON")
	."--,--PROPERTY_4814--#--".GetMessage("FORM_FORM_ELEMENT_536_POTREBLYAEMYY_TOK")."--,--PROPERTY_4843--#--".GetMessage("FORM_FORM_ELEMENT_536_RAZMER")."--,--PROPERTY_4805--#--".GetMessage("FORM_FORM_ELEMENT_536_RAZMER")
	." встроенной памяти--,--PROPERTY_4841--#--".GetMessage("FORM_FORM_ELEMENT_536_RAZMER")." доски--,--PROPERTY_4806--#--".GetMessage("FORM_FORM_ELEMENT_536_RAZMER")." оперативной памяти--,--PROPERTY_4834--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_RAZRESHENIE_DISPLEYA")."--,--PROPERTY_4824--#--".GetMessage("FORM_FORM_ELEMENT_536_RAZRESHENIE_MATRITSY_MPIKS")."--,--PROPERTY_4828--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_RAZRESHENIE_MATRITSY_MPIKS")."--,--PROPERTY_4837--#--".GetMessage("FORM_FORM_ELEMENT_536_RASKHOD")."--,--PROPERTY_4826--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_STABILIZATOR")."--,--PROPERTY_4846--#--".GetMessage("FORM_FORM_ELEMENT_536_STEKLO")."--,--PROPERTY_4813--#--".GetMessage("FORM_FORM_ELEMENT_536_TIP")
	." антирадара--,--PROPERTY_4812--#--".GetMessage("FORM_FORM_ELEMENT_536_TIP")." приемника--,--PROPERTY_4818--#--".GetMessage("FORM_FORM_ELEMENT_536_TIP")." управления--,--PROPERTY_4840--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_TOLSHCHINA_DOSKI_MM")."--,--PROPERTY_4810--#--".GetMessage("FORM_FORM_ELEMENT_536_UGOL_OBZORA")."--,--PROPERTY_4835--#--".GetMessage("FORM_FORM_ELEMENT_536_FASOVKA_KG")
	."--,--PROPERTY_4827--#--".GetMessage("FORM_FORM_ELEMENT_536_FOKUSNOE_RASSTOYANIE")."--,--PROPERTY_4808--#--".GetMessage("FORM_FORM_ELEMENT_536_FORMAT_ZAPISI")."--,--PROPERTY_4839--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_KHOZHDENIE_CHEREZ_CH")."--,--PROPERTY_4804--#--".GetMessage("FORM_FORM_ELEMENT_536_CHASTOTA_PROTSESSORA")."--,--LINKED_PROP--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_SVYAZANNYE_ELEMENTY")."--;--edit5--#--".GetMessage("FORM_FORM_ELEMENT_536_ANONS")."--,--PREVIEW_PICTURE--#--".GetMessage("FORM_FORM_ELEMENT_536_KARTINKA_DLYA_ANONSA")."--,--PREVIEW_TEXT--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_OPISANIE_DLYA_ANONSA")."--;--edit6--#--".GetMessage("FORM_FORM_ELEMENT_536_PODROBNO")."--,--DETAIL_PICTURE--#--".GetMessage("FORM_FORM_ELEMENT_536_DETALNAYA_KARTINKA")."--,--PROPERTY_4790--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_KARTINKI")."--,--PROPERTY_4791--#--".GetMessage("FORM_FORM_ELEMENT_536_FAYLY")."--,--DETAIL_TEXT--#--".GetMessage("FORM_FORM_ELEMENT_536_DETALNOE_OPISANIE")."--,--PROPERTY_4887--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_VIDEO_KOD_IZ_YOUTUBE")."--;--cedit1--#--".GetMessage("FORM_FORM_ELEMENT_536_SVYAZI")."--,--PROPERTY_4794--#--".GetMessage("FORM_FORM_ELEMENT_536_AKSESSUARY")."--,--PROPERTY_4795--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_POKHOZHIE_TOVARY")."--,--PROPERTY_4890--#--".GetMessage("FORM_FORM_ELEMENT_536_USLUGI")."--;--edit14--#--SEO--,--IPROPERTY_TEMPLATES_ELEMENT_META_TITLE--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_SHABLON_META_TITLE")."--,--IPROPERTY_TEMPLATES_ELEMENT_META_KEYWORDS--#--".GetMessage("FORM_FORM_ELEMENT_536_SHABLON_META_KEYWORDS")."--,--IPROPERTY_TEMPLATES_ELEMENT_META_DESCRIPTION--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_SHABLON_META_DESCRIPTION")."--,--IPROPERTY_TEMPLATES_ELEMENT_PAGE_TITLE--#--".GetMessage("FORM_FORM_ELEMENT_536_ZAGOLOVOK_TOVARA")."--,--IPROPERTY_TEMPLATES_ELEMENTS_PREVIEW_PICTURE--#----"
	.GetMessage("FORM_FORM_ELEMENT_536_NASTROYKI_DLYA_KARTINOK_ANONSA_ELEMENTOV")."--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_ALT--#--".GetMessage("FORM_FORM_ELEMENT_536_SHABLON_ALT")
	."--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_TITLE--#--".GetMessage("FORM_FORM_ELEMENT_536_SHABLON_TITLE")."--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_NAME--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_SHABLON_IMENI_FAYLA")."--,--IPROPERTY_TEMPLATES_ELEMENTS_DETAIL_PICTURE--#----".GetMessage("FORM_FORM_ELEMENT_536_NASTROYKI_DLYA_DETALNYKH_KARTINOK_ELEMENTOV")
	."--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_ALT--#--".GetMessage("FORM_FORM_ELEMENT_536_SHABLON_ALT")."--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_TITLE--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_SHABLON_TITLE")."--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_NAME--#--".GetMessage("FORM_FORM_ELEMENT_536_SHABLON_IMENI_FAYLA")
	."--,--SEO_ADDITIONAL--#----".GetMessage("FORM_FORM_ELEMENT_536_DOPOLNITELNO")."--,--TAGS--#--".GetMessage("FORM_FORM_ELEMENT_536_TEGI")."--;--edit2--#--".GetMessage("FORM_FORM_ELEMENT_536_RAZDELY")
	."--,--SECTIONS--#--".GetMessage("FORM_FORM_ELEMENT_536_RAZDELY")."--;--cedit2--#--".GetMessage("FORM_FORM_ELEMENT_536_DOPOLNITELNO")."--,--PROPERTY_4788--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_BAZOVAYA_EDINITSA")."--,--PROPERTY_4897--#--".GetMessage("FORM_FORM_ELEMENT_536_MAKSIMALNAYA_TSENA")."--,--PROPERTY_4896--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_MINIMALNAYA_TSENA")."--,--PROPERTY_4855--#--".GetMessage("FORM_FORM_ELEMENT_536_PROIZVODITEL")."--,--PROPERTY_4885--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_REYTING")."--,--PROPERTY_4787--#--".GetMessage("FORM_FORM_ELEMENT_536_REKVIZITY")."--,--PROPERTY_4789--#--".GetMessage("FORM_FORM_ELEMENT_536_STAVKI_NALOGOV")
	."--,--PROPERTY_4854--#--".GetMessage("FORM_FORM_ELEMENT_536_KHARAKTERISTIKI")."--,--PROPERTY_4785--#--".GetMessage("FORM_FORM_ELEMENT_536_SHTRIKHKOD")."--;--edit8--#--"
	.GetMessage("FORM_FORM_ELEMENT_536_TORGOVYE_PREDLOZHENIYA")."--,--OFFERS--#--".GetMessage("FORM_FORM_ELEMENT_536_TORGOVYE_PREDLOZHENIYA")."--;--" ));	*/		
	
//iblock user fields end

WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_CATALOG_TYPE" => $iblockType));
WizardServices::ReplaceMacrosRecursive($bitrixTemplateDir, array("IBLOCK_CATALOG_TYPE" => $iblockType));
WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, array("IBLOCK_CATALOG_ID" => $iblockID));
WizardServices::ReplaceMacrosRecursive($bitrixTemplateDir, array("IBLOCK_CATALOG_ID" => $iblockID));
CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/search.form/top/template.php", array("IBLOCK_CATALOG_TYPE" => $iblockType));
		CWizardUtil::ReplaceMacros($bitrixTemplateDir."/components/bitrix/search.form/top/template.php", array("IBLOCK_CATALOG_ID" => $iblockID));
?>