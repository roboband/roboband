<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/install/wizard_sol/wizard.php");?>
<script>
	<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/js/aspro.kshop/jquery-1.8.3.min.js");?>
	<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/js/aspro.kshop/jquery.keyboard.js");?>
	function setWizardBackgroundColor(theme)
	{
		switch (theme) 
		{
			case "aubergine":
				$(".instal-bg").css("backgroundColor", "rgb(137, 30, 103)");
				break
			case "azure_grey":
				$(".instal-bg").css("backgroundColor", "#117694");
				break
			case "blue":
				$(".instal-bg").css("backgroundColor", "#244D6D");
				break
			case "carrot":
				$(".instal-bg").css("backgroundColor", "rgb(140, 61, 43)");
				break	
			case "cerise":
				$(".instal-bg").css("backgroundColor", "rgb(108, 14, 46)");
				break
			case "green":
				$(".instal-bg").css("backgroundColor", "#094B11");
				break
			case "green_sea":
				$(".instal-bg").css("backgroundColor", "rgb(18, 110, 86)");
				break
			case "red":
				$(".instal-bg").css("backgroundColor", "rgb(125, 40, 50)");
				break
			case "turquoise":
				$(".instal-bg").css("backgroundColor", "rgb(11, 115, 111)");
				break
			case "violet":
				$(".instal-bg").css("backgroundColor", "rgb(59, 53, 118)");
				break
			case "yellow":
				$(".instal-bg").css("backgroundColor", "rgb(118, 94, 0)");
				break
			default:
				$(".instal-bg").css("backgroundColor", "#244D6D");
				break
		}
	}
	$(document).ready(function()
	{
		$("body").keyboard('ctrl+shift+f', { preventDefault : true }, function () { document.location.href = document.location.href+"&fast=y"; } )
	});
</script>
<?if (isset($_REQUEST["fast"]) && (strtolower($_REQUEST["fast"])=="y")):?>
	<script>
		$(document).ready(function()
		{
			if ($("input#installDemoData").length){$("input#installDemoData").attr("checked", "checked");}
			if ($(".wizard-next-button").length){if ($(".wizard-next-button").attr("value")!="������� �� ����"){$(".wizard-next-button").click();}}
		});
	</script>
<?endif;?>
<?

function setLastWritedIblockParams($id=false, $type=false, $code=false)
{
	$_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["ID"] = ($id && intVal($id)) ? intVal($id) : false;
	$_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["CODE"] = ($code && trim($code)) ? trim($code) : false;
	$_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["TYPE"] = ($type && trim($type)) ? trim($type) : false;
	
	if ( intVal($_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["ID"]) || 
		 trim($_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["TYPE"]) || 
		 trim($_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["CODE"]) ) 
	{return true;} else {return false;}
}

function getLastWritedIblockParams()
{
	$arResult = array(  "ID" 	=> ($_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["ID"] ? intVal($_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["ID"]) : false), 
						"TYPE"	=> ($_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["TYPE"] ? trim($_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["TYPE"]) : false),
						"CODE"	=> ($_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["CODE"] ? trim($_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]["CODE"]) : false));
	foreach ($arResult as $key=>$value){if (!$value){unset($arResult[$key]);}}
	return count($arResult) ? $arResult : false;
}

function clearLastWritedIblockParams()
{
	unset($_SESSION["WIZARD_LAST_WRITTED_IBLOCK"]);
	return true;
}

class SelectSiteStep extends CSelectSiteWizardStep
{
	function InitStep()
	{
		parent::InitStep();

		$wizard =& $this->GetWizard();
		$wizard->solutionName = "kshop";
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards/aspro/kshop/css/styles.css");
	}
}


class SelectTemplateStep extends CSelectTemplateWizardStep
{
	function InitStep()
	{
		$wizard =& $this->GetWizard();
		
		$this->SetStepID("select_template");
		$this->SetTitle(GetMessage("SELECT_TEMPLATE_TITLE"));
		$this->SetSubTitle(GetMessage("SELECT_TEMPLATE_SUBTITLE"));
		if (!defined("WIZARD_DEFAULT_SITE_ID"))
		{
			$this->SetPrevStep("select_site");
			$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));
		}
		else
		{
			$wizard =& $this->GetWizard(); 
			$wizard->SetVar("siteID", WIZARD_DEFAULT_SITE_ID); 
		}

		$this->SetNextStep("select_theme");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		
		$wizard->SetDefaultVars(Array("templateID" => "kshop"));
	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();
		
		$proactive = COption::GetOptionString("statistic", "DEFENCE_ON", "N");
		if ($proactive == "Y")
		{
			COption::SetOptionString("statistic", "DEFENCE_ON", "N");
			$wizard->SetVar("proactive", "Y");
		}
		else
		{
			$wizard->SetVar("proactive", "N");
		}

		if ($wizard->IsNextButtonClick())
		{
			$templatesPath = WizardServices::GetTemplatesPath($wizard->GetPath()."/site");
			$arTemplates = WizardServices::GetTemplates($templatesPath);		
			
			$templateID = $wizard->GetVar("templateID");

			if (!array_key_exists($templateID, $arTemplates))
				$this->SetError(GetMessage("wiz_template"));
				
		}
	}

	
	function ShowStep()
	{
	
		if (!CModule::IncludeModule("aspro.kshop")){
			$this->content .= "<p style='color:#b00'>".GetMessage("WIZ_NO_MODULE_KSHOP")."</p>";
			?>
			<script type="text/javascript">
			$(document).ready(function() {
				$('.wizard-next-button').remove();
			});
			</script>
			<?
		}
		else
		{
			$wizard =& $this->GetWizard();

			$templatesPath = WizardServices::GetTemplatesPath($wizard->GetPath()."/site");
			$arTemplates = WizardServices::GetTemplates($templatesPath);
			
			$templateID = $wizard->GetVar("templateID");
			$ThemeID = $wizard->GetVar($templateID."_themeID");
			$this->content .='<script type="text/javascript">$(document).ready(function(){setWizardBackgroundColor("'.$ThemeID.'");});</script>';

			if (empty($arTemplates))
				return;

			$templateID = $wizard->GetVar("templateID");
			if(isset($templateID) && array_key_exists($templateID, $arTemplates)){
			
				$defaultTemplateID = $templateID;
				$wizard->SetDefaultVar("templateID", $templateID);
				
			} else {
			
				$defaultTemplateID = COption::GetOptionString("main", "wizard_template_id", "", $wizard->GetVar("siteID")); 
				if (!(strlen($defaultTemplateID) > 0 && array_key_exists($defaultTemplateID, $arTemplates)))
				{
					if (strlen($defaultTemplateID) > 0 && array_key_exists($defaultTemplateID, $arTemplates))
						$wizard->SetDefaultVar("templateID", $defaultTemplateID);
					else
						$defaultTemplateID = "";
				}
			}

			global $SHOWIMAGEFIRST;
			$SHOWIMAGEFIRST = true;
			
			$this->content .= '<div id="solutions-container" class="inst-template-list-block">';
			foreach ($arTemplates as $templateID => $arTemplate)
			{
				if ($defaultTemplateID == "")
				{
					$defaultTemplateID = $templateID;
					$wizard->SetDefaultVar("templateID", $defaultTemplateID);
				}
			
				$this->content .= '<div class="inst-template-description">';
				$this->content .= $this->ShowRadioField("templateID", $templateID, Array("id" => $templateID, "class" => "inst-template-list-inp"));
				if ($arTemplate["SCREENSHOT"] && $arTemplate["PREVIEW"])
					$this->content .= CFile::Show2Images($arTemplate["PREVIEW"], $arTemplate["SCREENSHOT"], 150, 150, ' class="inst-template-list-img"');
				else
					$this->content .= CFile::ShowImage($arTemplate["SCREENSHOT"], 150, 150, ' class="inst-template-list-img"', "", true);

				$this->content .= '<label for="'.$templateID.'" class="inst-template-list-label">'.$arTemplate["NAME"].'<p>'.$arTemplate["DESCRIPTION"].'</p></label>';
				$this->content .= "</div>";

			}
			
			$this->content .= '</div>'; 
		}
	}
}

class SelectThemeStep extends CSelectThemeWizardStep
{
	function InitStep()
	{
		$this->SetStepID("select_theme");
		$this->SetTitle(GetMessage("SELECT_THEME_TITLE"));
		$this->SetSubTitle(GetMessage("SELECT_THEME_SUBTITLE"));
		$this->SetPrevStep("select_template");
		$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));
		$this->SetNextStep("site_settings");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();

		if ($wizard->IsNextButtonClick())
		{
			$templateID = $wizard->GetVar("templateID");
			$themeVarName = $templateID."_themeID";
			$themeID = $wizard->GetVar($themeVarName);

			$templatesPath = WizardServices::GetTemplatesPath($wizard->GetPath()."/site");
			$arThemes = WizardServices::GetThemes($templatesPath."/".$templateID."/themes");
		
			if (!array_key_exists($themeID, $arThemes))
				$this->SetError(GetMessage("wiz_template_color"));
		}
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();
		$templateID = $wizard->GetVar("templateID");

		$templatesPath = WizardServices::GetTemplatesPath($wizard->GetPath()."/site");
		$arThemes = WizardServices::GetThemes($templatesPath."/".$templateID."/themes");
		
		$templateID = $wizard->GetVar("templateID");
		$ThemeID = $wizard->GetVar($templateID."_themeID");
		$this->content .='<script type="text/javascript">$(document).ready(function(){setWizardBackgroundColor("'.$ThemeID.'");});</script>';

		if (empty($arThemes))
			return;

		$themeVarName = $templateID."_themeID";
		$ThemeID = $wizard->GetVar($templateID."_themeID");

		if(isset($ThemeID) && array_key_exists($ThemeID, $arThemes)){
			$defaultThemeID = $ThemeID;
			$wizard->SetDefaultVar($themeVarName, $ThemeID);
		} else {
			$defaultThemeID = COption::GetOptionString("main", "wizard_".$templateID."_theme_id", "", $wizard->GetVar("siteID")); 
	
			if (!(strlen($defaultThemeID) > 0 && array_key_exists($defaultThemeID, $arThemes)))
			{
				$defaultThemeID = COption::GetOptionString("main", "wizard_".$templateID."_theme_id", "");
				if (strlen($defaultThemeID) > 0 && array_key_exists($defaultThemeID, $arThemes))
					$wizard->SetDefaultVar($themeVarName, $defaultThemeID);
				else
					$defaultThemeID = "";
			}
		}

		$this->content = 
		'
		<script type="text/javascript">
		function SelectTheme(element, solutionId, imageUrl)
		{
			setWizardBackgroundColor(solutionId);
			
			var container = document.getElementById("solutions-container");
			var anchors = container.getElementsByTagName("SPAN");
			for (var i = 0; i < anchors.length; i++)
			{
				if (anchors[i].parentNode == container)
					anchors[i].className = "inst-template-color";
			}
			element.className = "inst-template-color inst-template-color-selected";
			var hidden = document.getElementById("selected-solution");
			if (!hidden) 
			{
				hidden = document.createElement("INPUT");
				hidden.type = "hidden"
				hidden.id = "selected-solution";
				hidden.name = "selected-solution";
				container.appendChild(hidden);
			}
			hidden.value = solutionId;

			var preview = document.getElementById("solution-preview");
			if (!imageUrl)
				preview.style.display = "none";
			else 
			{
				document.getElementById("solution-preview-image").src = imageUrl;
				preview.style.display = "";
			}
		}
		</script>'.
		'<div id="html_container">'.
		'<div class="inst-template-color-block" id="solutions-container">';
		$ii = 0;
		$arDefaultTheme = array(); 
		foreach ($arThemes as $themeID => $arTheme)
		{
			if ($defaultThemeID == "")
			{
				$defaultThemeID = $themeID;
				$wizard->SetDefaultVar($themeVarName, $defaultThemeID);
			}
			if ($defaultThemeID == $themeID)
				$arDefaultTheme = $arTheme;
			$ii++;

			$this->content .= '
					<span class="inst-template-color'.($defaultThemeID == $themeID ? " inst-template-color-selected" : "").'" ondblclick="SubmitForm(\'next\');"  onclick="SelectTheme(this, \''.$themeID.'\', \''.$arTheme["SCREENSHOT"].'\');">
						<span class="inst-templ-color-img">'.CFile::ShowImage($arTheme["PREVIEW"], 70, 70, ' border="0" class="solution-image"').'</span>
						<span class="inst-templ-color-name">'.$ii.'. '.$arTheme["NAME"].'</span>
					</span>';
		}
		$this->content .= '<script type="text/javascript">	$(document).ready(function(){setWizardBackgroundColor($(".inst-template-color-block .inst-template-color.inst-template-color-selected").attr("themeName"));});</script>';
		$this->content .= $this->ShowHiddenField($themeVarName, $defaultThemeID, array("id" => "selected-solution"));  
		$this->content .= 
			'</div>'.
			'<div id="solution-preview">'.
				'<b class="r3"></b><b class="r1"></b><b class="r1"></b>'.
					'<div class="solution-inner-item">'.
						CFile::ShowImage($arDefaultTheme["SCREENSHOT"], 450, 450, ' border="0" id="solution-preview-image"').
					'</div>'.
				'<b class="r1"></b><b class="r1"></b><b class="r3"></b>'.
			'</div>'.
		'</div>';
	}
	}

class SiteSettingsStep extends CSiteSettingsWizardStep
{
	function InitStep()
	{
		$wizard =& $this->GetWizard();
		$wizard->solutionName = "tires";
		parent::InitStep();

		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetTitle(GetMessage("WIZ_STEP_SITE_SET"));

		$siteID = $wizard->GetVar("siteID");
		//$themeID = $wizard->GetVar("themeID");
		
		
		$templateID = $wizard->GetVar("templateID");
		$themeVarName = $templateID."_themeID";
		$themeID = $wizard->GetVar($themeVarName);
		$this->content .='<script type="text/javascript">$(document).ready(function(){setWizardBackgroundColor("'.$themeID.'");});</script>';
		
		$siteLogo = $this->GetFileContentImgSrc(WIZARD_SITE_PATH."include/logo.php", "/bitrix/wizards/aspro/tires/site/templates/tires/themes/".$themeID."/images/logo.png");
       
	   // if (!file_exists("/bitrix/wizards/aspro/tires/site/templates/tires/themes/".$themeID."/lang/".LANGUAGE_ID."logo.png"))
	   if (!file_exists(WIZARD_SITE_PATH."include/logo.jpg"))
            $siteLogo = "/bitrix/wizards/aspro/tires/site/templates/tires/themes/".$themeID."/images/logo.png";
		
		if(COption::GetOptionString("tires", "wizard_installed", "N", $siteID) == "Y" && !WIZARD_INSTALL_DEMO_DATA)
			$this->SetNextStep("data_install");
		else
		{
			if(LANGUAGE_ID != "ru")
				$this->SetNextStep("pay_system");
			else
			$this->SetNextStep("shop_settings");
		}
		
		$templateID = $wizard->GetVar("templateID");
		
		$wizard->SetDefaultVars(Array("siteNameSet" => true));
		
		$wizard->SetDefaultVars(
			Array(
				"siteLogo" => $siteLogo,
				"siteLogoSet" => true,
				
				"siteName" => GetMessage("WIZ_COMPANY_NAME_DEF"),
				"siteTelephone" => GetMessage("WIZ_COMPANY_PHONE_DEF"),
				"siteWorkTime" => $this->GetFileContent(WIZARD_SITE_PATH."include/work_time.php", GetMessage("WIZ_COMPANY_WORK_TIME_DEF")),
				"siteCopy" => GetMessage("WIZ_COMPANY_COPY_DEF"),
				
				"shopVk" => GetMessage("WIZ_SHOP_VK_DEF"),
				"shopTwitter" => GetMessage("WIZ_SHOP_TWITTER_DEF"),
				"shopFacebook" => GetMessage("WIZ_SHOP_FACEBOOK_DEF"),
				
				"siteMetaDescription" => GetMessage("wiz_site_desc"),
				"siteMetaKeywords" => GetMessage("wiz_keywords"),
			)
		);
		
	}

	function ShowStep()
	{
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards/aspro/kshop/css/styles_rewrite.css");
		$wizard =& $this->GetWizard();
		
		$this->content .= '<div class="wizard-input-form">';
		if($wizard->GetVar('siteNameSet', true)){
			$this->content .= '
			<div class="wizard-input-form-block">
				<label for="siteName" class="wizard-input-title">'.GetMessage("WIZ_COMPANY_NAME").'</label><br />'
				.$this->ShowInputField('text', 'siteName', array("class"=>"wizard-field", "id" => "siteName")).'
			</div>';
		}
		
		if($wizard->GetVar('siteLogoSet', true)){
			$siteLogo = $wizard->GetVar("siteLogo", true);
	
	
			//$this->content .= var_dump($siteLogo);
	
			$this->content .= '
			<div class="wizard-input-form-block">
				<label for="siteLogo" class="wizard-input-title">'.GetMessage("WIZ_COMPANY_LOGO").'</label><br />'
				.CFile::ShowImage($siteLogo, 280, 40, "border=0 vspace=15") . '<br>' . 
				$this->ShowFileField("siteLogo", Array("show_file_info" => "N", "id" => "siteLogo")).'
			</div>';
		}
		
		$this->content .= '
		<div class="wizard-input-form-block">
			<label for="siteTelephone" class="wizard-input-title">'.GetMessage("WIZ_COMPANY_TELEPHONE").'</label><br />'
			.$this->ShowInputField('text', 'siteTelephone', array("class"=>"wizard-field", "id" => "siteTelephone")).'
		</div>';
			
		$this->content .= '
		<div class="wizard-input-form-block">
			<label for="siteWorkTime" class="wizard-input-title">'.GetMessage("WIZ_COMPANY_WORK_TIME").'</label><br />'
			.$this->ShowInputField('textarea', 'siteWorkTime', array("style"=>"font-size: 12px; font-weight: normal;","class"=>"wizard-field", "rows"=>"3", "id" => "siteWorkTime")).'
		</div>';

		$this->content .= '
		<div class="wizard-input-form-block">
			<label for="siteCopy" class="wizard-input-title">'.GetMessage("WIZ_COMPANY_COPY").'</label><br />'
			.$this->ShowInputField('textarea', 'siteCopy', array("class"=>"wizard-field", "rows"=>"3", "id" => "siteCopy")).'
		</div>';
		
		
//SocNets
		$this->content .= '
		<div class="wizard-input-form-block">
			<label for="shopFacebook" class="wizard-input-title">'.GetMessage("WIZ_SHOP_FACEBOOK").'</label><br />'
			.$this->ShowInputField('text', 'shopFacebook', array("class"=>"wizard-field", "id" => "shopFacebook")).'
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<label for="shopTwitter" class="wizard-input-title">'.GetMessage("WIZ_SHOP_TWITTER").'</label><br />'
			.$this->ShowInputField('text', 'shopTwitter', array("class"=>"wizard-field", "id" => "shopTwitter")).'
		</div>';
		if(LANGUAGE_ID == "ru"):
			$this->content .= '
			<div class="wizard-input-form-block">
				<label for="shopVk" class="wizard-input-title">'.GetMessage("WIZ_SHOP_VK").'</label><br />'
				.$this->ShowInputField('text', 'shopVk', array("class"=>"wizard-field", "id" => "shopVk")).'
			</div>';
		endif;		
/*---*/		

		$firstStep = COption::GetOptionString("main", "wizard_first" . substr($wizard->GetID(), 7)  . "_" . $wizard->GetVar("siteID"), false, $wizard->GetVar("siteID")); 
		$styleMeta = 'style="display:block"';
		if($firstStep == "Y") $styleMeta = 'style="display:none"';

		$this->content .= '
		<div  id="bx_metadata" '.$styleMeta.'>
			<div class="wizard-input-form-block">
				<div class="wizard-metadata-title">'.GetMessage("wiz_meta_data").'</div>
				<label for="siteMetaDescription" class="wizard-input-title">'.GetMessage("wiz_meta_description").'</label>
				'.$this->ShowInputField("textarea", "siteMetaDescription", array("class" => "wizard-field", "id" => "siteMetaDescription", "style" => "width:100%", "rows"=>"3")).'
			</div>';
		$this->content .= '
			<div class="wizard-input-form-block">
				<label for="siteMetaKeywords" class="wizard-input-title">'.GetMessage("wiz_meta_keywords").'</label><br>
				'.$this->ShowInputField('text', 'siteMetaKeywords', array("class" => "wizard-field", "id" => "siteMetaKeywords")).'
			</div>
		</div>';
		
//install Demo data		
		if($firstStep == "Y")
		{
			$this->content .= '
			<div class="wizard-input-form-block">'
				.$this->ShowCheckboxField(
					"installDemoData", 
					"Y", 
					(array("id" => "installDemoData", "onClick" => "if(this.checked == true){document.getElementById('bx_metadata').style.display='block';}else{document.getElementById('bx_metadata').style.display='none';}"))
				).'
				<label for="installDemoData" style="font-size: 14px;">'.GetMessage("wiz_structure_data").'</label>
			</div>';
			}
		else
		{
			$this->content .= $this->ShowHiddenField("installDemoData","Y");
		}
		
		if(LANGUAGE_ID != "ru")
		{
			CModule::IncludeModule("catalog");
			$db_res = CCatalogGroup::GetGroupsList(array("CATALOG_GROUP_ID"=>'1', "BUY"=>"Y", "GROUP_ID"=>2));
			if (!$db_res->Fetch())
			{
				$this->content .= '
				<div class="wizard-input-form-block">
					<label for="shopAdr">'.GetMessage("WIZ_SHOP_PRICE_BASE_TITLE").'</label>
					<div class="wizard-input-form-block-content">
						'. GetMessage("WIZ_SHOP_PRICE_BASE_TEXT1") .'<br><br>
						'. $this->ShowCheckboxField("installPriceBASE", "Y", 
						(array("id" => "install-demo-data")))
						. ' <label for="install-demo-data">'.GetMessage("WIZ_SHOP_PRICE_BASE_TEXT2").'</label><br />
						
					</div>
				</div>';	
			}
		}
		
		$this->content .= '</div>';
	}
	function OnPostForm()
	{
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards/aspro/kshop/css/styles_rewrite.css");
		$wizard =& $this->GetWizard();
		
		//set site name 
		COption::SetOptionString("main", "site_name", $wizard->GetVar("siteName"));	
		$arFields = Array("NAME" => $wizard->GetVar("siteName"));			
		$obSite = new CSite;
		$siteRes = $obSite->Update($wizard->GetVar("siteID"), $arFields);
		
		$res = $this->SaveFile("siteLogo", Array("extensions" => "gif,jpg,jpeg,png", "max_height" => 75, "max_width" => 226, "make_preview" => "Y"));
		if (file_exists(WIZARD_SITE_PATH."include/logo.jpg"))
		{
			$wizard->SetVar("siteLogoSet", true);
		}
	}
}

class ShopSettings extends CWizardStep
{
	function InitStep()
	{
		$this->SetStepID("shop_settings");
		$this->SetTitle(GetMessage("WIZ_STEP_SS"));
		$this->SetNextStep("person_type");
		$this->SetPrevStep("site_settings");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));

		$wizard =& $this->GetWizard();

		$siteStamp =$wizard->GetPath()."/site/templates/minimal/images/pechat.gif";
		$siteID = $wizard->GetVar("siteID");
		
		$wizard->SetDefaultVars(
			Array(
				"shopLocalization" => COption::GetOptionString("tires", "shopLocalization", "ru", $siteID),
				"shopEmail" => COption::GetOptionString("tires", "shopEmail", "sale@".$_SERVER["SERVER_NAME"], $siteID),
				"shopOfName" => COption::GetOptionString("tires", "shopOfName", GetMessage("WIZ_SHOP_OF_NAME_DEF"), $siteID),
				"shopLocation" => COption::GetOptionString("tires", "shopLocation", GetMessage("WIZ_SHOP_LOCATION_DEF"), $siteID),
				//"shopZip" => 101000,
				"shopAdr" => COption::GetOptionString("tires", "shopAdr", GetMessage("WIZ_SHOP_ADR_DEF"), $siteID),
				"shopINN" => COption::GetOptionString("tires", "shopINN", "1234567890", $siteID),
				"shopKPP" => COption::GetOptionString("tires", "shopKPP", "123456789", $siteID),
				"shopNS" => COption::GetOptionString("tires", "shopNS", "0000 0000 0000 0000 0000", $siteID),
				"shopBANK" => COption::GetOptionString("tires", "shopBANK", GetMessage("WIZ_SHOP_BANK_DEF"), $siteID),
				"shopBANKREKV" => COption::GetOptionString("tires", "shopBANKREKV", GetMessage("WIZ_SHOP_BANKREKV_DEF"), $siteID),
				"shopKS" => COption::GetOptionString("tires", "shopKS", "30101 810 4 0000 0000225", $siteID),
				"siteStamp" => COption::GetOptionString("tires", "siteStamp", $siteStamp, $siteID),

				"shopCompany_ua" => COption::GetOptionString("tires", "shopCompany_ua", "", $siteID),
				"shopEGRPU_ua" =>  COption::GetOptionString("tires", "shopCompany_ua", "", $siteID),
				"shopINN_ua" =>  COption::GetOptionString("tires", "shopINN_ua", "", $siteID),
				"shopNDS_ua" =>  COption::GetOptionString("tires", "shopNDS_ua", "", $siteID),
				"shopNS_ua" =>  COption::GetOptionString("tires", "shopNS_ua", "", $siteID),
				"shopBank_ua" =>  COption::GetOptionString("tires", "shopBank_ua", "", $siteID),
				"shopMFO_ua" =>  COption::GetOptionString("tires", "shopMFO_ua", "", $siteID),
				"shopPlace_ua" =>  COption::GetOptionString("tires", "shopPlace_ua", "", $siteID),
				"shopFIO_ua" =>  COption::GetOptionString("tires", "shopFIO_ua", "", $siteID),
				"shopTax_ua" =>  COption::GetOptionString("tires", "shopTax_ua", "", $siteID),

				"installPriceBASE" => COption::GetOptionString("tires", "installPriceBASE", "Y", $siteID),
			)
		);
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();
		$siteStamp = $wizard->GetVar("siteStamp", true);
		
		$templateID = $wizard->GetVar("templateID");
		$ThemeID = $wizard->GetVar($templateID."_themeID");
		$this->content .='<script type="text/javascript">$(document).ready(function(){setWizardBackgroundColor("'.$ThemeID.'");});</script>';
		
		if (!CModule::IncludeModule("catalog"))
		{
			$this->content .= "<p style='color:red'>".GetMessage("WIZ_NO_MODULE_CATALOG")."</p>";
			$this->SetNextStep("shop_settings");
		}
		else
		{
			/*$this->content .=
				$this->ShowSelectField("shopLocalization", array("ru" => GetMessage("WIZ_SHOP_LOCALIZATION_RUSSIA"), "ua" => GetMessage("WIZ_SHOP_LOCALIZATION_UKRAINE")), array("onchange" => "langReload()", "id" => "localization_select"))
				.' <label for="shopLocalization">'.GetMessage("WIZ_SHOP_LOCALIZATION").'</label><br />';*/

			$this->content .= '<div class="wizard-input-form">';

			$this->content .= '<div class="wizard-input-form-block">
				<label for="shopEmail" class="wizard-input-title">'.GetMessage("WIZ_SHOP_EMAIL").'</label><br />
				'.$this->ShowInputField('text', 'shopEmail', array("class" => "wizard-field", "id" => "shopEmail")).'
			</div>';	
			$this->content .= '<div class="wizard-input-form-block">
				<label for="shopOfName" class="wizard-input-title">'.GetMessage("WIZ_SHOP_OF_NAME").'</label><br />
				'.$this->ShowInputField('text', 'shopOfName', array("class" => "wizard-field", "id" => "shopOfName")).'
			</div>';			
	
			$this->content .= '<div class="wizard-input-form-block">
				<label for="shopLocation" class="wizard-input-title">'.GetMessage("WIZ_SHOP_LOCATION").'</label><br />';
				
			$this->content .= $this->ShowInputField('text', 'shopLocation', array("class" => "wizard-field", "id" => "shopLocation"));
			$this->content .= '</div>';			
	
			$this->content .= '
			<div class="wizard-input-form-block">
				<label for="shopAdr" class="wizard-input-title">'.GetMessage("WIZ_SHOP_ADR").'</label><br />
				'.$this->ShowInputField('textarea', 'shopAdr', array("class" => "wizard-field", "rows"=>"3", "id" => "shopAdr")).'
			</div>';


			$currentLocalization = $wizard->GetVar("shopLocalization");
			if (empty($currentLocalization))
				$currentLocalization = $wizard->GetDefaultVar("shopLocalization");
	 //ru
			$this->content .= '
			<div id="ru_bank_details" class="wizard-input-form-block" >
				<div class="wizard-catalog-title">'.GetMessage("WIZ_SHOP_BANK_TITLE").'</div>
				<table  class="wizard-input-table" >
					<tr>
						<td class="wizard-input-table-left">'.GetMessage("WIZ_SHOP_INN").':</td>
						<td class="wizard-input-table-right">'.$this->ShowInputField('text', 'shopINN', array('class' => 'wizard-field')).'</td>
					</tr>
					<tr>
						<td class="wizard-input-table-left">'.GetMessage("WIZ_SHOP_KPP").':</td>
						<td class="wizard-input-table-right">'.$this->ShowInputField('text', 'shopKPP', array('class' => 'wizard-field')).'</td>
					</tr>
					<tr>
						<td class="wizard-input-table-left">'.GetMessage("WIZ_SHOP_NS").':</td>
						<td class="wizard-input-table-right">'.$this->ShowInputField('text', 'shopNS', array('class' => 'wizard-field')).'</td>
					</tr>
					<tr>
						<td class="wizard-input-table-left">'.GetMessage("WIZ_SHOP_BANK").':</td>
						<td class="wizard-input-table-right">'.$this->ShowInputField('text', 'shopBANK', array('class' => 'wizard-field')).'</td>
					</tr>
					<tr>
						<td class="wizard-input-table-left">'.GetMessage("WIZ_SHOP_BANKREKV").':</td>
						<td class="wizard-input-table-right">'.$this->ShowInputField('text', 'shopBANKREKV', array('class' => 'wizard-field')).'</td>
					</tr>
					<tr>
						<td class="wizard-input-table-left">'.GetMessage("WIZ_SHOP_KS").':</td>
						<td class="wizard-input-table-right">'.$this->ShowInputField('text', 'shopKS', array('class' => 'wizard-field')).'</td>
					</tr>
					<tr>
						<td class="wizard-input-table-left">'.GetMessage("WIZ_SHOP_STAMP").':</td>
						<td class="wizard-input-table-right">'.$this->ShowFileField("siteStamp", Array("show_file_info"=> "N", "id" => "siteStamp")).'<br />'.CFile::ShowImage($siteStamp, 75, 75, "border=0 vspace=5", false, false).'</td>
						</tr>
				</table>
			</div>
			';

			if (CModule::IncludeModule("catalog"))
			{
				$db_res = CCatalogGroup::GetGroupsList(array("CATALOG_GROUP_ID"=>'1', "BUY"=>"Y", "GROUP_ID"=>2));
				if (!$db_res->Fetch())
				{
					$this->content .= '
					<div class="wizard-input-form-block">
						<label for="shopAdr">'.GetMessage("WIZ_SHOP_PRICE_BASE_TITLE").'</label>
						<div class="wizard-input-form-block-content">
							'. GetMessage("WIZ_SHOP_PRICE_BASE_TEXT1") .'<br><br>
							'. $this->ShowCheckboxField("installPriceBASE", "Y",
							(array("id" => "install-demo-data")))
							. ' <label for="install-demo-data">'.GetMessage("WIZ_SHOP_PRICE_BASE_TEXT2").'</label><br />

						</div>
					</div>';
				}
			}
			
			$this->content .= '</div>';
		}
	}
	
	function OnPostForm()
	{
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards/aspro/kshop/css/styles_rewrite.css");
		$wizard =& $this->GetWizard();
		$res = $this->SaveFile("siteStamp", Array("extensions" => "gif,jpg,jpeg,png", "max_height" => 70, "max_width" => 190, "make_preview" => "Y"));
	}

}

class PersonType extends CWizardStep
{
	function InitStep()
	{
		$this->SetStepID("person_type");
		$this->SetTitle(GetMessage("WIZ_STEP_PT"));
		$this->SetNextStep("pay_system");
		$this->SetPrevStep("shop_settings");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));

		$wizard =& $this->GetWizard();
		$shopLocalization = $wizard->GetVar("shopLocalization", true);

		if ($shopLocalization == "ua")
			$wizard->SetDefaultVars(
				Array(
					"personType" => Array(
						"fiz" => "Y",
						"fiz_ua" => "Y",
						"ur" => "Y",
					)
				)
			);
		else
			$wizard->SetDefaultVars(
				Array(
					"personType" => Array(
						"fiz" => "Y",
						"ur" => "Y",
					)
				)
			);
	}

	function ShowStep()
	{

		$wizard =& $this->GetWizard();
		$shopLocalization = $wizard->GetVar("shopLocalization", true);

		
		$templateID = $wizard->GetVar("templateID");
		$ThemeID = $wizard->GetVar($templateID."_themeID");
		$this->content .='<script type="text/javascript">$(document).ready(function(){setWizardBackgroundColor("'.$ThemeID.'");});</script>';
		
		$this->content .= '<div class="wizard-input-form">';
		$this->content .= '
		<div class="wizard-input-form-block">
			<div style="padding-top:15px">
				<div class="wizard-input-form-field wizard-input-form-field-checkbox">
					<div class="wizard-catalog-form-item">
						'.$this->ShowCheckboxField('personType[fiz]', 'Y', (array("id" => "personTypeF"))).' 
						<label for="personTypeF">'.GetMessage("WIZ_PERSON_TYPE_FIZ").'</label>
					</div>
					<div class="wizard-catalog-form-item">
						'.$this->ShowCheckboxField('personType[ur]', 'Y', (array("id" => "personTypeU"))).' 
						<label for="personTypeU">'.GetMessage("WIZ_PERSON_TYPE_UR").'</label>
					</div>';
					$this->content .= 
				'</div>
				<div class="wizard-catalog-form-item" style="font-size: 14px;">'.GetMessage("WIZ_PERSON_TYPE").'</div>
			</div>
		</div>';
		
		$this->content .= '</div>';
	}
	
	function OnPostForm()
	{
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards/aspro/kshop/css/styles_rewrite.css");
		$wizard = &$this->GetWizard();
		$personType = $wizard->GetVar("personType");

		if (empty($personType["fiz"]) && empty($personType["ur"]))
			$this->SetError(GetMessage('WIZ_NO_PT'));
	}

}

class PaySystem extends CWizardStep
{
	function InitStep()
	{
		$this->SetStepID("pay_system");
		$this->SetTitle(GetMessage("WIZ_STEP_PS"));
		$this->SetNextStep("data_install");
		if(LANGUAGE_ID != "ru")
			$this->SetPrevStep("site_settings");
		else
		$this->SetPrevStep("person_type");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));

		$wizard =& $this->GetWizard();

		if(LANGUAGE_ID == "ru")
		{
				$wizard->SetDefaultVars(
					Array(
						"paysystem" => Array(
							"cash" => "Y",
							"sber" => "Y",
							"bill" => "Y",
						),
						"delivery" => Array(
							"courier" => "Y",
							"self" => "Y",
							"russianpost" => "N",
						)
					)
				);
		}
		else
		{
			$wizard->SetDefaultVars(
				Array(
					"paysystem" => Array(
						"cash" => "Y",	
						"paypal" => "Y",
					),			
					"delivery" => Array(
						"courier" => "Y",	
						"self" => "Y",
						"dhl" => "Y",
						"ups" => "Y",
					)
				)
			);
		}
	}
	
	function OnPostForm()
	{
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards/aspro/kshop/css/styles_rewrite.css");
		$wizard = &$this->GetWizard();
		$paysystem = $wizard->GetVar("paysystem");

		if (empty($paysystem["cash"]) && empty($paysystem["sber"]) && empty($paysystem["bill"]) && empty($paysystem["paypal"]))
			$this->SetError(GetMessage('WIZ_NO_PS'));
	}

	function ShowStep()
	{

		$wizard =& $this->GetWizard();
		$shopLocalization = $wizard->GetVar("shopLocalization", true);
		$personType = $wizard->GetVar("personType");
		
		$templateID = $wizard->GetVar("templateID");
		$ThemeID = $wizard->GetVar($templateID."_themeID");
		$this->content .='<script type="text/javascript">$(document).ready(function(){setWizardBackgroundColor("'.$ThemeID.'");});</script>';
		
		$this->content .= '<div class="wizard-input-form">';
		$this->content .= '
		<div class="wizard-input-form-block">
			<div class="wizard-catalog-title">'.GetMessage("WIZ_PAY_SYSTEM_TITLE").'</div>
			<div class="wizard-input-form-field wizard-input-form-field-checkbox">
				<div class="wizard-catalog-form-item">
					'.$this->ShowCheckboxField('paysystem[cash]', 'Y', (array("id" => "paysystemC"))).' <label for="paysystemC">'.GetMessage("WIZ_PAY_SYSTEM_C").'</label></div>';
					if(LANGUAGE_ID == "ru")
					{
						/*	if($shopLocalization == "ua" && ($personType["fiz"] == "Y" || $personType["fiz_ua"] == "Y"))
							$this->content .= $this->ShowCheckboxField('paysystem[oshad]', 'Y', (array("id" => "paysystemO"))).' <label for="paysystemS">'.GetMessage("WIZ_PAY_SYSTEM_O").'</label><br />';
						else*/
						if ($personType["fiz"] == "Y")
							$this->content .= '<div class="wizard-catalog-form-item">'.$this->ShowCheckboxField('paysystem[sber]', 'Y', (array("id" => "paysystemS"))).' <label for="paysystemS">'.GetMessage("WIZ_PAY_SYSTEM_S").'</label></div>';
						if($personType["ur"] == "Y")
							$this->content .= '<div class="wizard-catalog-form-item">'.$this->ShowCheckboxField('paysystem[bill]', 'Y', (array("id" => "paysystemB"))).' <label for="paysystemB">'.GetMessage("WIZ_PAY_SYSTEM_B").'</label></div>';
					}
					else
					{
						$this->content .= '<div class="wizard-catalog-form-item">'.$this->ShowCheckboxField('paysystem[paypal]', 'Y', (array("id" => "paysystemP"))).' <label for="paysystemP">PayPal</label></div>';
					}
					$this->content .= '
					<div class="wizard-catalog-form-item" style="font-size: 14px;">'.GetMessage("WIZ_PAY_SYSTEM").'</div>
			</div>
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<div class="wizard-catalog-title">'.GetMessage("WIZ_DELIVERY_TITLE").'</div>
			<div class="wizard-input-form-field wizard-input-form-field-checkbox">
				<div class="wizard-catalog-form-item">'.$this->ShowCheckboxField('delivery[courier]', 'Y', (array("id" => "deliveryC"))).' <label for="deliveryC">'.GetMessage("WIZ_DELIVERY_C").'</label></div>
				<div class="wizard-catalog-form-item">'.$this->ShowCheckboxField('delivery[self]', 'Y', (array("id" => "deliveryS"))).' <label for="deliveryS">'.GetMessage("WIZ_DELIVERY_S").'</label></div>';
					if(LANGUAGE_ID == "ru")
					{
						//if ($shopLocalization != "ua")
							$this->content .= '<div class="wizard-catalog-form-item">'.$this->ShowCheckboxField('delivery[russianpost]', 'Y', (array("id" => "deliveryR"))).' <label for="deliveryR">'.GetMessage("WIZ_DELIVERY_R").'</label></div>';
					}
					else
					{
						$this->content .= '<div class="wizard-catalog-form-item">'.$this->ShowCheckboxField('delivery[dhl]', 'Y', (array("id" => "deliveryD"))).' <label for="deliveryD">DHL</label></div>';
						$this->content .= '<div class="wizard-catalog-form-item">'.$this->ShowCheckboxField('delivery[ups]', 'Y', (array("id" => "deliveryU"))).' <label for="deliveryU">UPS</label></div>';
					}
					$this->content .= '
				<div class="wizard-catalog-form-item" style="font-size: 14px;">'.GetMessage("WIZ_DELIVERY").'</div>	
			</div>
		</div>';

		$this->content .= '
		<div class="wizard-input-form-block">
			<div class="wizard-catalog-title">'.GetMessage("WIZ_LOCATION_TITLE").'</div>
			<div class="wizard-input-form-field wizard-input-form-field-checkbox">
				<div class="wizard-input-form-field wizard-input-form-field-checkbox">';
					if(LANGUAGE_ID == "ru")
					{
						$this->content .= '<div class="wizard-catalog-form-item">'.$this->ShowRadioField("locations_csv", "loc_ussr.csv", array("id" => "loc_ussr", "checked" => "checked"))
							." <label for=\"loc_ussr\">".GetMessage('WSL_STEP2_GFILE_USSR')."</label></div>";
					}
					$this->content .= '<div class="wizard-catalog-form-item">'.$this->ShowRadioField("locations_csv", "loc_usa.csv", array("id" => "loc_usa"))
						." <label for=\"loc_usa\">".GetMessage('WSL_STEP2_GFILE_USA')."</label></div>";
					$this->content .= '<div class="wizard-catalog-form-item">'.$this->ShowRadioField("locations_csv", "loc_cntr.csv", array("id" => "loc_cntr"))
						." <label for=\"loc_cntr\">".GetMessage('WSL_STEP2_GFILE_CNTR')."</label></div>";
					$this->content .= '<div class="wizard-catalog-form-item">'.$this->ShowRadioField("locations_csv", "", array("id" => "none"))
						." <label for=\"none\">".GetMessage('WSL_STEP2_GFILE_NONE')."</label></div>"; $this->content .= '
					<div class="wizard-catalog-form-item" style="font-size: 14px;">'.GetMessage("WIZ_DELIVERY_HINT").'</div>	
				</div>
			</div>
		</div>';
	}
}

class DataInstallStep extends CDataInstallWizardStep
{
	function InitStep()
	{	
		$wizard =& $this->GetWizard();
		$this->SetStepID("data_install");
		$this->SetTitle(GetMessage("wiz_install_data"));
		$this->SetSubTitle(GetMessage("wiz_install_data"));
		//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards/aspro/kshop/css/styles_rewrite.css");
		$templateID = $wizard->GetVar("templateID");
		$ThemeID = $wizard->GetVar($templateID."_themeID");
		$this->content .='<script type="text/javascript">$(document).ready(function(){setWizardBackgroundColor("'.$ThemeID.'");});</script>';
	}
	
	function CorrectServices(&$arServices)
	{	
		if($_SESSION["BX_kshop_LOCATION"] == "Y") $this->repeatCurrentService = true;
		else $this->repeatCurrentService = false;
		$wizard =& $this->GetWizard();
		
		$iblockParams = getLastWritedIblockParams();
		if ($iblockParams && intVal($iblockParams["ID"]) && trim($iblockParams["CODE"]))
		{
			switch ($iblockParams["CODE"]) //perform any manipulations with last installed infoblock
			{
				default: 
				break;
			}
		}
		clearLastWritedIblockParams(); //cuz correct need only once
		
		if($wizard->GetVar("installDemoData") != "Y")
		{
		}		
	}
}

class FinishStep extends CFinishWizardStep
{
	function InitStep()
	{
		$this->SetStepID("finish");
		$this->SetNextStep("finish");
		$this->SetTitle(GetMessage("FINISH_STEP_TITLE"));
		$this->SetNextCaption(GetMessage("wiz_go"));  
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards/aspro/kshop/css/styles_rewrite.css");
		
		$templateID = $wizard->GetVar("templateID");
		$ThemeID = $wizard->GetVar($templateID."_themeID");
		$this->content .='<script type="text/javascript">$(document).ready(function(){setWizardBackgroundColor("'.$ThemeID.'");});</script>';
		
		if($wizard->GetVar("installDemoData") == "Y")
		{
			if(!CModule::IncludeModule("iblock")) return;
		}
		
		if ($wizard->GetVar("proactive") == "Y")
			COption::SetOptionString("statistic", "DEFENCE_ON", "Y");
		
		$siteID = WizardServices::GetCurrentSiteID($wizard->GetVar("siteID"));
		$rsSites = CSite::GetByID($siteID);
		$siteDir = "/"; 
		if ($arSite = $rsSites->Fetch())
			$siteDir = $arSite["DIR"]; 

		$wizard->SetFormActionScript(str_replace("//", "/", $siteDir."/?finish"));

		$this->CreateNewIndex();
		
		COption::SetOptionString("main", "wizard_solution", $wizard->solutionName, false, $siteID); 
		
		$this->content .= GetMessage("FINISH_STEP_CONTENT");
		$this->content .= "";
		
		if ($wizard->GetVar("installDemoData") == "Y")
			$this->content .= GetMessage("FINISH_STEP_REINDEX");
			
		if(CModule::IncludeModule("aspro.kshop")) { CKShop::newAction("wizard_installed"); }
		COption::SetOptionString("aspro.kshop", "WIZARD_DEMO_INSTALLED", "Y");
		
	}

}
?>