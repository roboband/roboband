<?
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

class aspro_kshop extends CModule {
	var $MODULE_ID = "aspro.kshop";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

	function aspro_kshop(){
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("SCOM_INSTALL_NAME_KSHOP"); 
		$this->MODULE_DESCRIPTION = GetMessage("SCOM_INSTALL_DESCRIPTION_KSHOP");
		$this->PARTNER_NAME = GetMessage("SPER_PARTNER");
		$this->PARTNER_URI = GetMessage("PARTNER_URI");
	}
	
	function checkValid(){
		return true;
	}

	function InstallDB($install_wizard = true){
		global $DB, $DBType, $APPLICATION;

		if(preg_match ( '/.bitrixlabs.ru/' , $_SERVER["HTTP_HOST"])){
			RegisterModuleDependences("main", "OnBeforeProlog", "aspro.kshop", "CKShop", "correctInstall");
		}

		RegisterModule("aspro.kshop"); 
		RegisterModuleDependences("main", "OnBeforeProlog", "aspro.kshop", "CKShop", "ShowPanel");
		COption::SetOptionString("aspro.kshop", "COLOR_THEME", "YELLOW", "", SITE_ID);
		COption::SetOptionString("aspro.kshop", "BANNER_WIDTH", "NARROW", "", SITE_ID);
		COption::SetOptionString("aspro.kshop", "HEAD", "TYPE_1", "", SITE_ID);
		COption::SetOptionString("aspro.kshop", "BASKET", "NORMAL", "", SITE_ID);
		COption::SetOptionString("aspro.kshop", "SHOW_BREADCRUMBS_CATALOG_SUBSECTIONS", "Y", "", SITE_ID);
		COption::SetOptionString("aspro.kshop", "PHONE_MASK", "+9 (999) 999-99-99", "", SITE_ID);

		return true;
	}

	function UnInstallDB($arParams = array()){
		global $DB, $DBType, $APPLICATION;
		
		if(CModule::IncludeModule("aspro.kshop")){
			CKShop::newAction("delete");
		}
		UnRegisterModule("aspro.kshop");
		
		return true;
	}

	function InstallEvents(){
		RegisterModuleDependences("main", "OnBeforeUserRegister", "aspro.kshop", "CKShop", "OnBeforeUserUpdateHandler");
		RegisterModuleDependences("main", "OnBeforeUserUpdate", "aspro.kshop", "CKShop","OnBeforeUserUpdateHandler");
		RegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", "aspro.kshop", "CKShop", "DoIBlockAfterSave");
		RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "aspro.kshop", "CKShop", "DoIBlockAfterSave");
		RegisterModuleDependences("catalog", "OnPriceAdd", "aspro.kshop", "CKShop", "DoIBlockAfterSave");
		RegisterModuleDependences("catalog", "OnPriceUpdate", "aspro.kshop", "CKShop", "DoIBlockAfterSave");
		if(CModule::IncludeModule("aspro.kshop")){
			CKShop::newAction("install");
		}
		
		return true;
	}

	function UnInstallEvents(){
		UnRegisterModuleDependences("main", "OnBeforeUserRegister", "aspro.kshop", "CKShop", "OnBeforeUserUpdateHandler");
		UnRegisterModuleDependences("main", "OnBeforeUserUpdate", "aspro.kshop", "CKShop","OnBeforeUserUpdateHandler");
		UnRegisterModuleDependences("main", "OnBeforeProlog", "aspro.kshop", "CKShop", "ShowPanel");
		UnRegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", "aspro.kshop", "CKShop", "DoIBlockAfterSave");
		UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "aspro.kshop", "CKShop", "DoIBlockAfterSave");
		UnRegisterModuleDependences("catalog", "OnPriceAdd", "aspro.kshop", "CKShop", "DoIBlockAfterSave");
		UnRegisterModuleDependences("catalog", "OnPriceUpdate", "aspro.kshop", "CKShop", "DoIBlockAfterSave");
		return true;
	}
	
	function removeDirectory($dir){
		if($objs = glob($dir."/*")){
			foreach($objs as $obj){
				if(is_dir($obj)){
					CKShop::removeDirectory($obj);
				}
				else{
					if(!unlink($obj)){
						if(chmod($obj, 0777)){
							unlink($obj);
						}
					}
				}
			}
		}
		if(!rmdir($dir)){
			if(chmod($dir, 0777)){
				rmdir($dir);
			}
		}
	}

	function InstallFiles(){
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/aspro.kshop/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/aspro.kshop/install/js", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/aspro.kshop/install/wizards", $_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/aspro.kshop/install/images", $_SERVER["DOCUMENT_ROOT"]."/bitrix/images", true, true);

		if(preg_match('/.bitrixlabs.ru/', $_SERVER["HTTP_HOST"])){
			@set_time_limit(0);
			require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/fileman/include.php");
			CFileMan::DeleteEx(array("s1", "/bitrix/modules/aspro.kshop/install/wizards"));
		}

		return true;
	}

	function InstallPublic(){
	}

	function UnInstallFiles(){
		DeleteDirFilesEx("/bitrix/wizards/aspro/kshop/");

		return true;
	}

	function DoInstall(){
		global $APPLICATION, $step;

		$this->InstallFiles();
		$this->InstallDB(false);
		$this->InstallEvents();
		$this->InstallPublic();

		$APPLICATION->IncludeAdminFile(GetMessage("SCOM_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/aspro.kshop/install/step.php");
	}

	function DoUninstall(){
		global $APPLICATION, $step;

		$this->UnInstallDB();
		$this->UnInstallFiles();
		$this->UnInstallEvents();
		$APPLICATION->IncludeAdminFile(GetMessage("SCOM_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/aspro.kshop/install/unstep.php");
	}
}
?>