<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?><h3>Преимущества РобоБанды</h3>
 <br>
 <br>
<p>
	 В нашем ассортименте представлен только качественный товар от ведущих мировых производителей, с безупречным исполнением и функциональностью. Мы 100 раз подумаем прежде чем добавить в наш ассортимент очередную новику.<br>
</p>
<p>
	 Весь товар представленный в нашем ассортименте сертифицирован для продажи на территории Российской Федерации, официально ввезен на территорию страны и имеет гарантию производителя.
</p>
<p>
	 Каждый клиент для нас особенный, и что самое важное вы всегда помним об этом и стараемся вас не подвести.
</p>
<p>
	 Мы все знаем о нашем ассортименте и всегда посоветуем вам, как сделать правильный выбор, чтобы вы остались довольны покупкой и всегда помнили про нас :)
</p>
<p>
</p>
 <br>
<h3>Наша команда</h3>
 <br>
 <br>
<table border="1" cellpadding="1" cellspacing="1" class="">
<tbody>
<tr>
	<td>
 <img width="70" alt="RoboManager.png" src="/upload/medialibrary/0b0/0b0d5f5ecf3b978de4e339b7c1eb454d.png" height="70" title="RoboManager.png" style="font-weight: bold;">
	</td>
	<td>
		     
	</td>
	<td>
 <b>РобоКонсультант</b><br>
		 Гуру в мобильной технике и современных гаджетах, приветливый и веселый.<br>
		 Готов общаться с вами по телефону с понедельника по пятницу с 10 до 20 часов.
	</td>
</tr>
</tbody>
</table>
 <br>
<table border="1" cellpadding="1" cellspacing="1" class="">
<tbody>
<tr>
	<td>
 <img width="70" alt="RoboManager.png" src="/upload/medialibrary/988/98821ec6e9027cbbc57e8e1e47136873.png" height="70" title="RoboManager.png">
	</td>
	<td>
		     
	</td>
	<td>
 <b>РобоМенеджер</b><br>
		 Ветеран World of Warcraft, очень ответственный и тактичный трудится в пункте самовывоза.<br>
	</td>
</tr>
</tbody>
</table>
 <br>
<table border="1" cellpadding="1" cellspacing="1" class="">
<tbody>
<tr>
	<td>
 <img width="70" alt="RoboManager.png" src="/upload/medialibrary/89f/89fcb717e3593e0adcbe8ae6108ea49e.png" height="70" title="RoboWriter.png">
	</td>
	<td>
		     
	</td>
	<td>
 <b>РобоРайтер</b><br>
		 Заботится о порядке в каталоге товаров и наполнении сайта.
	</td>
</tr>
</tbody>
</table>
 <br>
<table border="1" cellpadding="1" cellspacing="1" class="">
<tbody>
<tr>
	<td>
 <img width="70" alt="RoboManager.png" src="/upload/medialibrary/187/187695d56ea674ea6d0bc1ddf69ccaca.png" height="70" title="RoboRunner.png" style="font-weight: bold;">
	</td>
	<td>
		      
	</td>
	<td>
 <b>РобоВоз Георгий</b><br>
		 Скромный, быстрый и очень вежливый передвигается на метро и везде успевает.  
	</td>
</tr>
</tbody>
</table>
 <br>
<table border="1" cellpadding="1" cellspacing="1" class="">
<tbody>
<tr>
	<td>
 <img width="70" alt="RoboManager.png" src="/upload/medialibrary/ed1/ed162f34cd467559898f11c0112f49ff.png" height="70" title="RoboRunner.png">
	</td>
	<td>
		      
	</td>
	<td>
 <b>РобоВоз Владимир</b><br>
		 Вежливый и пунктуальный, передвигается на машине, доставляет заказы в места куда на метро сложно добраться. <br>
	</td>
</tr>
</tbody>
</table>
 <br>
<table border="1" cellpadding="1" cellspacing="1" class="">
<tbody>
<tr>
	<td>
 <img width="70" alt="RoboManager.png" src="/upload/medialibrary/a5a/a5a3c7840a70d8320206f6f72cb6271e.png" height="70" title="RoboManager.png" style="font-weight: bold;">
	</td>
	<td>
		      
	</td>
	<td>
 <b>РобоАдмин</b><br>
		 Все настроил и запустил, благодаря ему все работает как надо, в офисе не появляется, сидит дома, почти не спит.
	</td>
</tr>
</tbody>
</table>
<blockquote>
	<table border="1" cellpadding="1" cellspacing="1" class="">
	<tbody>
	<tr>
		<td>
			 По вопросам закупок и партнерству, а так же по обслуживанию клиентов, вы можете связаться со мной.
		</td>
		<td>
			     
		</td>
		<td>
 <img width="70" alt="RoboBoss-V2.png" src="/upload/medialibrary/a26/a268cd829a1f28dcb5239e88d8361d10.png" height="70" title="RoboBoss-V2.png">
		</td>
		<td>
			     
		</td>
		<td>
 <b>РобоБосс</b><br>
 <a href="mailto:fedor@roboband.ru">fedor@roboband.ru</a>
		</td>
	</tr>
	</tbody>
	</table>
</blockquote>
<p>
	 ООО «Яблочные Решения»<br>
	 ОГРН: 1157746334360<br>
         105066, г. Москва, ул. Доброслободская, дом 6, строение 1<br>
</p>

<a href="http://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2508/*http://market.yandex.ru/shop/295804/reviews"><img src="http://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2505/*http://grade.market.yandex.ru/?id=295804&action=image&size=0" border="0" width="88" height="31" alt="Читайте отзывы покупателей и оценивайте качество магазина на Яндекс.Маркете" /></a><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>