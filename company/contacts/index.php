<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?><div class="contacts_description">
	 <?$APPLICATION->IncludeFile(SITE_DIR."include/contacts_text.php", Array(), Array("MODE" => "html", "NAME"  => GetMessage("CONTACTS_TEXT"),));?>
</div>
<div class="left_block clearfix">
	<div class="store_description">
		<div class="store_property">
			<div class="title">
				 Адрес:
			</div>
			<div class="value">
				 <?$APPLICATION->IncludeFile(SITE_DIR."include/address.php", Array(), Array( "MODE" => "text", "NAME" => "Адрес", ));?>
			</div>
		</div>
		<div class="store_property">
			<div class="title">
				 Телефон:
			</div>
			<div class="value">
				 <?$APPLICATION->IncludeFile(SITE_DIR."include/phone.php", Array(), Array( "MODE" => "text", "NAME" => "Телефон", ));?>
			</div>
		</div>
		<div class="store_property">
			<div class="title">
				 Email:
			</div>
			<div class="value">
				 <?$APPLICATION->IncludeFile(SITE_DIR."include/email.php", Array(), Array( "MODE" => "text", "NAME" => "Email", ));?>
			</div>
		</div>
		<hr>
 <span class="store_property">
		<div class="title">
			 Режим работы:
		</div>
		<div class="value">
			 <?$APPLICATION->IncludeFile(SITE_DIR."include/work_time.php", Array(), Array( "MODE" => "html", "NAME"  => "Время работы"));?>
		</div>
 </span>
	</div>
</div>
<div class="right_block">

<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=mT01z0_ATCcsqf5uFqNKd56pAFYVH1uE&width=542&height=450"></script>
	<h2 class="feedback_title">Обратная связь</h2>
	 <?Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("form-feedback-block");?> <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"inline",
	Array(
		"WEB_FORM_ID" => "3",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"USE_EXTENDED_ERRORS" => "Y",
		"SEF_MODE" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600000",
		"LIST_URL" => "",
		"EDIT_URL" => "",
		"SUCCESS_URL" => "?send=ok",
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"VARIABLE_ALIASES" => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID")
	)
);?> <?Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("form-feedback-block", "");?>
</div>
<div class="clearboth">
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>